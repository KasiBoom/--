//
//  UserManager.h
//  GameSM
//
//  Created by 王涛 on 15/8/3.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserManager : NSObject

- (void)saveUserInfo:(NSDictionary*)dic;

- (void)clearUserInfo;

@end
