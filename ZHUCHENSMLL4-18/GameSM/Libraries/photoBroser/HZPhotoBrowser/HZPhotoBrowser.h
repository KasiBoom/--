//
//  HZPhotoBrowser.h
//  photoBrowser
//
//  Created by huangzhenyu on 15/6/23.
//  Copyright (c) 2015年 eamon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HZPhotoBrowserView.h"
#import "UMSocial.h"

@class HZPhotoBrowser;

@protocol HZPhotoBrowserDelegate <NSObject>

- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index;
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index;

- (NSString*)photoDrowserWithIndex:(NSInteger)index;

- (void)singleClickView;

@end



typedef void (^ablock)(int i);

@interface HZPhotoBrowser : UIViewController<UMSocialUIDelegate>

@property(nonatomic,strong) ablock block;


@property (nonatomic, weak) UIView *sourceImagesContainerView;
@property (nonatomic, assign) int currentImageIndex;
@property (nonatomic, assign) NSInteger imageCount;//图片总数
@property (nonatomic, assign) int type;
@property (nonatomic, weak) id<HZPhotoBrowserDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *dataArr;

//@property(nonatomic,copy)void(^changePicArr)(NSMutableArray *arr);

- (void)show;
@end
