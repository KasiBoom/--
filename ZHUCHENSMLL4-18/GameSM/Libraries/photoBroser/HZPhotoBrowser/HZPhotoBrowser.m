//
//  HZPhotoBrowser.m
//  photoBrowser
//
//  Created by huangzhenyu on 15/6/23.
//  Copyright (c) 2015年 eamon. All rights reserved.
//

#import "HZPhotoBrowser.h"
#import "HZPhotoBrowserConfig.h"
#import "MobClick.h"
#import "UMSocial.h"
#import "Config.h"
#import "AppDelegate.h"
#import "MBProgressHUD+Add.h"
#import "Y_X_DataInterface.h"
#import "LoginAlertView.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"

#import "HomeViewController.h"

#define kAPPWidth1 self.view.bounds.size.width
#define kAppHeight1 self.view.bounds.size.height
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface HZPhotoBrowser() <UIScrollViewDelegate,CustomAlertViewDelegate>
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,assign) BOOL hasShowedPhotoBrowser;
@property (nonatomic,strong) UILabel *indexLabel;
@property (nonatomic,strong) UILabel *authorNameLabel;
@property (nonatomic,strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic,strong) UIButton *saveButton;
@property (nonatomic,strong) UIButton *shareButton;
@property (nonatomic,strong) UIButton *praiseButton;

@property (nonatomic,strong) UIView *saveView;
@property (nonatomic,strong) UIView *shareView;
@property (nonatomic,strong) UIView *praiseButtonView;

@end

@implementation HZPhotoBrowser

- (void)viewDidLoad
{
    [super viewDidLoad];
    _hasShowedPhotoBrowser = NO;
    self.view.backgroundColor = kPhotoBrowserBackgrounColor;
    [self addScrollView];
    [self addToolbars];
    [self setUpFrames];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    if (!_hasShowedPhotoBrowser) {
        [self showPhotoBrowser];
    }
}


- (void)viewDidDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
#pragma mark 重置各控件frame（处理屏幕旋转）
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self setUpFrames];
}

#pragma mark 设置各控件frame
- (void)setUpFrames
{
//    NSLog(@"-----%f,,,%f",self.view.bounds.size.width,self.view.bounds.size.height);
    CGRect rect = self.view.bounds;//
    rect.size.width += kPhotoBrowserImageViewMargin * 2;
    _scrollView.bounds = rect;
    _scrollView.center = CGPointMake(kAPPWidth1 *0.5, KScreenheight *0.5);
//    _scrollView.frame = CGRectMake(0, 0, KScreenWidth+10, KScreenheight-45);
    CGFloat y = 0;
    __block CGFloat w = kAPPWidth1;
    CGFloat h = kAppHeight1;
    
    //设置所有HZPhotoBrowserView的frame
    [_scrollView.subviews enumerateObjectsUsingBlock:^(HZPhotoBrowserView *obj, NSUInteger idx, BOOL *stop) {
        CGFloat x = kPhotoBrowserImageViewMargin + idx * (kPhotoBrowserImageViewMargin * 2 + w);
        obj.frame = CGRectMake(x, y, w, h);
    }];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.subviews.count * (kAPPWidth1 + 20), kAppHeight1-45);//_scrollView.frame.size.width
    _scrollView.contentOffset = CGPointMake(self.currentImageIndex * (kAPPWidth1+20), 0);
    
    
//    _indexLabel.bounds = CGRectMake(0, 0, 80, 30);
//    _indexLabel.center = CGPointMake(kAPPWidth1 * 0.5,  kAppHeight1 - 24);//
    _saveButton.frame = CGRectMake(ScreenSizeWidth/2-40, kAppHeight1 - 70, 80, 80);
//    _shareButton.frame = CGRectMake(kAPPWidth1 - 60,  -5, 80, 80);//kAppHeight1 - 48
//    _praiseButton.frame = CGRectMake(kAPPWidth1 -70, kAppHeight1 - 70, 80, 80);
    
//    _shareView.frame = CGRectMake(kAPPWidth1 - 60+20,  -5+20, 40, 40);
//    _praiseButtonView.frame = CGRectMake(kAPPWidth1 -70+20, kAppHeight1 - 70+20, 40, 40);
    _saveView.frame = CGRectMake(ScreenSizeWidth/2-40+20, kAppHeight1 - 70+20, 40, 40);
}

#pragma mark 显示图片浏览器
- (void)showPhotoBrowser
{
    CGRect rect = self.view.bounds;
    
    UIImageView *tempImageView = [[UIImageView alloc] init];
    tempImageView.frame = rect;
    tempImageView.image = [self placeholderImageForIndex:self.currentImageIndex];
    [self.view addSubview:tempImageView];
    tempImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    CGFloat placeImageSizeW = tempImageView.image.size.width == 0 ? self.view.bounds.size.width : tempImageView.image.size.width;
    CGFloat placeImageSizeH = tempImageView.image.size.height == 0 ? self.view.bounds.size.height : tempImageView.image.size.height;;
    
    CGRect targetTemp;
    
    if (!kIsFullWidthForLandScape) {
        if (kAPPWidth1 < kAppHeight1) {
            CGFloat placeHolderH = (placeImageSizeH * kAPPWidth1)/placeImageSizeW;
            if (placeHolderH <= kAppHeight1) {
                targetTemp = CGRectMake(0, (kAppHeight1 - placeHolderH) * 0.5 , kAPPWidth1, placeHolderH);
            } else {
                targetTemp = CGRectMake(0, 0, kAPPWidth1, placeHolderH);
            }
        } else {
            CGFloat placeHolderW = (placeImageSizeW * kAppHeight1)/placeImageSizeH;
            if (placeHolderW < kAPPWidth1) {
                targetTemp = CGRectMake((kAPPWidth1 - placeHolderW)*0.5, 0, placeHolderW, kAppHeight1);
            } else {
                targetTemp = CGRectMake(0, 0, placeHolderW, kAppHeight1);
            }
        }
        
    } else {
        CGFloat placeHolderH = (placeImageSizeH * kAPPWidth1)/placeImageSizeW;
        if (placeHolderH <= kAppHeight1) {
            targetTemp = CGRectMake(0, (kAppHeight1 - placeHolderH) * 0.5 , kAPPWidth1, placeHolderH);
        } else {
            targetTemp = CGRectMake(0, 0, kAPPWidth1, placeHolderH);
        }
    }
    
    _scrollView.hidden = YES;
    _indexLabel.hidden = YES;
    _saveButton.hidden = YES;
    _shareButton.hidden = YES;
    
    [UIView animateWithDuration:kPhotoBrowserShowDuration animations:^{
        tempImageView.frame = targetTemp;
    } completion:^(BOOL finished) {
        _hasShowedPhotoBrowser = YES;
        [tempImageView removeFromSuperview];
        _scrollView.hidden = NO;
        _indexLabel.hidden = NO;
        _saveButton.hidden = NO;
        if (_type !=2) {
            _shareButton.hidden = NO;
        }
    }];
}

#pragma mark 添加scrollview
- (void)addScrollView
{
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.frame = self.view.bounds;//CGRectMake(0, 0, KScreenWidth, KScreenheight-45)
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.hidden = YES;
    [self.view addSubview:_scrollView];
    
    for (int i = 0; i < self.imageCount; i++) {
        HZPhotoBrowserView *view = [[HZPhotoBrowserView alloc] init];
        view.imageview.tag = i;
        
        //处理单击
        __weak __typeof(self)weakSelf = self;
        view.singleTapBlock = ^(UITapGestureRecognizer *recognizer){
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf hidePhotoBrowser:recognizer];
        };
        
        [_scrollView addSubview:view];
    }
    
    [self setupImageOfImageViewForIndex:self.currentImageIndex];
}

#pragma mark 添加操作按钮
- (void)addToolbars
{
    //1.序标
//    UILabel *indexLabel = [[UILabel alloc] init];
//    indexLabel.textAlignment = NSTextAlignmentCenter;
//    indexLabel.textColor = [UIColor whiteColor];
//    indexLabel.font = [UIFont boldSystemFontOfSize:15];
//    indexLabel.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.3f];
//    indexLabel.bounds = CGRectMake(0, 0, 100, 40);
//    indexLabel.center = CGPointMake(kAPPWidth1 * 0.5, 30);
//    indexLabel.layer.cornerRadius = 15;
//    indexLabel.clipsToBounds = YES;
//    [self.view addSubview:indexLabel];
//    
//    
//    if (self.imageCount > 1) {
//        indexLabel.text = [NSString stringWithFormat:@"1/%ld", (long)self.imageCount];
//        _indexLabel = indexLabel;
//        //        [self.view addSubview:indexLabel];
//    }else{
//        indexLabel.hidden = YES;
//    }
    
    // 2.保存按钮
    UIButton *saveButton = [[UIButton alloc] init];
    [saveButton setImage:[UIImage imageNamed:@"xiazai_pic"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(savePic) forControlEvents:UIControlEventTouchUpInside];
    saveButton.bounds = CGRectMake(0, 0, 40, 40);
    saveButton.layer.cornerRadius = 20;
    
    self.saveView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    self.saveView.layer.masksToBounds = YES;
    self.saveView.layer.cornerRadius = 20;
    self.saveView.backgroundColor = RGBACOLOR(0.1, 0.1, 0.1, 0.3);
    [self.view addSubview:self.saveView];
    
    _saveButton = saveButton;
    [self.view addSubview:saveButton];
    
    //3.分享按钮
//    UIButton *shareBtn = [[UIButton alloc] init];
//    [shareBtn setImage:[UIImage imageNamed:@"fengxiang.png"] forState:UIControlStateNormal];
//    [shareBtn addTarget:self action:@selector(sharePic) forControlEvents:UIControlEventTouchUpInside];
//    shareBtn.bounds = CGRectMake(0, 0, 80, 80);
//    shareBtn.layer.cornerRadius = 20;
//
//    self.shareView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
//    self.shareView.layer.masksToBounds = YES;
//    self.shareView.layer.cornerRadius = 20;
//    self.shareView.backgroundColor = RGBACOLOR(0.1, 0.1, 0.1, 0.3);
//    [self.view addSubview:self.shareView];
////    [shareBtn addSubview:imageView];
////    [shareBtn sendSubviewToBack:imageView];
////    shareBtn.backgroundColor = RGBACOLOR(0.1, 0.1, 0.1, 0.3);
//    self.shareButton = shareBtn;
//    [self.view addSubview:shareBtn];
    
    //4.点赞按钮
//    UIButton *praiseButton = [[UIButton alloc] init];
//    NSString *isPraiseStr = [NSString stringWithFormat:@"%@",self.dataArr[self.currentImageIndex][@"isPraise"]];
//    [praiseButton setImage:[UIImage imageNamed:@"dianzan_2ppic"] forState:UIControlStateNormal];
//    if ([isPraiseStr isEqualToString:@"1"]) {
//        [praiseButton setImage:[UIImage imageNamed:@"dianzan_2pic"] forState:UIControlStateNormal];
//    }
//    
//    [praiseButton addTarget:self action:@selector(praisePic:) forControlEvents:UIControlEventTouchUpInside];
//    praiseButton.bounds = CGRectMake(0, 0, 40, 40);
//    praiseButton.layer.cornerRadius = 20;
//    
//    self.praiseButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
//    self.praiseButtonView.layer.masksToBounds = YES;
//    self.praiseButtonView.layer.cornerRadius = 20;
//    self.praiseButtonView.backgroundColor = RGBACOLOR(0.1, 0.1, 0.1, 0.3);
//    [self.view addSubview:self.praiseButtonView];
//    
//    self.praiseButton = praiseButton;
//    [self.view addSubview:praiseButton];
//    if (_type == 1 || _type == 2) {
//        self.praiseButton.hidden = YES;
//        self.shareButton.hidden = YES;
//    }
    //5.作者名字
//    NSString *str = [NSString stringWithFormat:@"UP主:%@",_dataArr[self.currentImageIndex][@"userIDInfo"][@"name"]];
//    CGSize s = [self sizeOfStr:str andFont:[UIFont systemFontOfSize:15] andMaxSize:CGSizeMake(MAXFLOAT, 30) andLineBreakMode:0];
//    UILabel *authorNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 20, s.width+20, 30)];
//    authorNameLabel.textAlignment = 1;
//    authorNameLabel.textColor = [UIColor whiteColor];
//    authorNameLabel.font = [UIFont systemFontOfSize:15];
//    authorNameLabel.backgroundColor = RGBACOLOR(0.1f, 0.1f, 0.1f, 0.3f);
//    authorNameLabel.layer.cornerRadius = 13;
//    authorNameLabel.clipsToBounds = YES;
//    self.authorNameLabel = authorNameLabel;
//    if (_dataArr[self.currentImageIndex][@"userIDInfo"]) {
//        self.authorNameLabel.hidden = NO;
//        self.authorNameLabel.text = [NSString stringWithFormat:@"UP主:%@",_dataArr[self.currentImageIndex][@"userIDInfo"][@"name"]];
//    }else{
//        self.authorNameLabel.hidden = YES;
//    }
//    
//    [self.view addSubview:authorNameLabel];
}
- (CGSize)sizeOfStr:(NSString *)str andFont:(UIFont *)font andMaxSize:(CGSize)size andLineBreakMode:(NSLineBreakMode)mode
{
    CGSize s;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>=7.0) {

        NSMutableDictionary  *mdic=[NSMutableDictionary dictionary];
        [mdic setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
        [mdic setObject:font forKey:NSFontAttributeName];
        s = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                           attributes:mdic context:nil].size;
    }
    else
    {
        s=[str sizeWithFont:font constrainedToSize:size lineBreakMode:mode];
    }
    return s;
}

#pragma mark - 点赞
//点赞图片
-(void)praisePic:(UIButton *)btn{
    if (!g_App.userID) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以点赞哦~"];
        [alertView show];
        return;
    }
    int index = _scrollView.contentOffset.x / _scrollView.bounds.size.width;
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_PRETTYIMAGESPRAISE,@"r",
                                    [NSString stringWithFormat:@"%@",_dataArr[index][@"id"]],@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(praisePicFinish:) method:POSTDATA];
}

#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
}
- (void)praisePicFinish:(NSDictionary*)dic {
    int index = _scrollView.contentOffset.x / _scrollView.bounds.size.width;
    NSMutableDictionary *changeDic = [[NSMutableDictionary alloc]initWithDictionary:self.dataArr[index]];
    int praiseCount = [[NSString stringWithFormat:@"%@",self.dataArr[index][@"praiseCount"]] intValue];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        if (![dic[@"results"] isEqualToString:@"取消赞"]) {
            praiseCount ++;
            [MBProgressHUD showSuccess:@"已点赞" toView:self.view];
            [self.praiseButton setImage:[UIImage imageNamed:@"dianzan_2pic"] forState:UIControlStateNormal];
            [changeDic setObject:@"1" forKey:@"isPraise"];
            [changeDic setObject:[NSString stringWithFormat:@"%i",praiseCount] forKey:@"praiseCount"];
        }else{
            praiseCount --;
            [MBProgressHUD showSuccess:@"取消赞" toView:self.view];
            [self.praiseButton setImage:[UIImage imageNamed:@"dianzan_2ppic"] forState:UIControlStateNormal];
            [changeDic setObject:@"0" forKey:@"isPraise"];
            [changeDic setObject:[NSString stringWithFormat:@"%i",praiseCount] forKey:@"praiseCount"];
        }
        
        [self.dataArr replaceObjectAtIndex:index withObject:changeDic];
        
    }
}

//分享图片
- (void)sharePic {
    
    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ps" to_page:@"" to_section:@"c" to_step:@"c" type:@"c" channel:@"" id:[NSString stringWithFormat:@"%d",self.currentImageIndex]];
    [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"is" to_section:@"i" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%d",self.currentImageIndex]];
    
    NSString *title = @"我在麦萌看到一张超赞的图片，安利给小伙伴";
    NSString *url = [[self highQualityImageURLForIndex:self.currentImageIndex] absoluteString];
    
    [UMSocialData defaultData].extConfig.qzoneData.title = title;;
    
    [UMSocialData defaultData].extConfig.qzoneData.url = url;
    
    [UMSocialData defaultData].extConfig.qqData.title = title;;
    [UMSocialData defaultData].extConfig.qqData.url = url;
    
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = url;
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = title;;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
    
    
    [UMSocialData defaultData].extConfig.sinaData.shareText = [NSString stringWithFormat:@"%@%@分享自@麦萌",title,url];
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:url];
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENGKEY
                                      shareText:url
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:@[UMShareToSina, UMShareToQQ, UMShareToQzone, UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:self];
    NSString *imageId = [self imageWithIndex:self.currentImageIndex];
    
    NSDictionary *dict = @{@"shareId" :imageId};
    [MobClick event:@"shareEvent" attributes:dict];
}

#pragma mark - UMSocialUIDelegate
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    [[LogHelper shared] writeToFilefrom_page:@"is" from_section:@"i" from_step:@"r" to_page:@"is" to_section:@"i" to_step:@"a" type:[[response.data allKeys] objectAtIndex:0] id:[NSString stringWithFormat:@"%d",self.currentImageIndex]];
    
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}

#pragma mark 保存图像

- (void)savePic {

    
    int index = _scrollView.contentOffset.x / _scrollView.bounds.size.width;
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"pd" to_page:@"" to_section:@"c" to_step:@"c" type:@"c" channel:@"" id:[NSString stringWithFormat:@"%d",index]];
    
    [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"idl" to_section:@"i" to_step:@"ac" type:@"" id:[NSString stringWithFormat:@"%d",index]];
    
    
    HZPhotoBrowserView *currentView = _scrollView.subviews[index];
    
    UIImageWriteToSavedPhotosAlbum(currentView.imageview.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error != NULL){
        //失败
        [MBProgressHUD showSuccess:@"保存失败" toView:self.view];
    }else{
        //成功
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
    }
}


- (void)saveImage
{
    int index = _scrollView.contentOffset.x / _scrollView.bounds.size.width;
    
    HZPhotoBrowserView *currentView = _scrollView.subviews[index];
    
    UIImageWriteToSavedPhotosAlbum(currentView.imageview.image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] init];
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    indicator.center = self.view.center;
    _indicatorView = indicator;
    [[UIApplication sharedApplication].keyWindow addSubview:indicator];
    [indicator startAnimating];
}

- (void)show
{
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:self animated:NO completion:nil];
}

#pragma mark 单击隐藏图片浏览器
- (void)hidePhotoBrowser:(UITapGestureRecognizer *)recognizer
{
   
    
    if (_delegate && [_delegate respondsToSelector:@selector(singleClickView)]) {
        [_delegate singleClickView];
    }
    [self dismissViewControllerAnimated:NO completion:^{
        if (_type != 1 && _type != 2){
            self.block(_currentImageIndex);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changePicArr" object:self.dataArr];
        }
    }];
    return;
    /*
     
     HZPhotoBrowserView *view = (HZPhotoBrowserView *)recognizer.view;
     UIImageView *currentImageView = view.imageview;
     
     UIView *sourceView = self.sourceImagesContainerView.subviews[self.currentImageIndex];
     UIView *parentView = [self getParsentView:sourceView];
     CGRect targetTemp = [sourceView.superview convertRect:sourceView.frame toView:parentView];
     
     // 减去偏移量
     if ([parentView isKindOfClass:[UITableView class]]) {
     UITableView *tableview = (UITableView *)parentView;
     targetTemp.origin.y =  targetTemp.origin.y - tableview.contentOffset.y;
     }
     
     CGFloat appWidth;
     CGFloat appHeight;
     if (kAPPWidth1 < kAppHeight1) {
     appWidth = kAPPWidth1;
     appHeight = kAppHeight1;
     } else {
     appWidth = kAppHeight1;
     appHeight = kAPPWidth1;
     }
     
     UIImageView *tempImageView = [[UIImageView alloc] init];
     tempImageView.image = currentImageView.image;
     if (tempImageView.image) {
     CGFloat tempImageSizeH = tempImageView.image.size.height;
     CGFloat tempImageSizeW = tempImageView.image.size.width;
     CGFloat tempImageViewH = (tempImageSizeH * appWidth)/tempImageSizeW;
     if (tempImageViewH < appHeight) {
     tempImageView.frame = CGRectMake(0, (appHeight - tempImageViewH)*0.5, appWidth, tempImageViewH);
     } else {
     tempImageView.frame = CGRectMake(0, 0, appWidth, tempImageViewH);
     }
     } else {
     tempImageView.backgroundColor = [UIColor whiteColor];
     tempImageView.frame = CGRectMake(0, (appHeight - appWidth)*0.5, appWidth, appWidth);
     }
     
     [self.view.window addSubview:tempImageView];
     
     [self dismissViewControllerAnimated:NO completion:nil];
     [UIView animateWithDuration:kPhotoBrowserHideDuration animations:^{
     tempImageView.frame = targetTemp;
     
     } completion:^(BOOL finished) {
     [tempImageView removeFromSuperview];
     }];
     
     */
}

#pragma mark 网络加载图片
- (void)setupImageOfImageViewForIndex:(NSInteger)index
{
    HZPhotoBrowserView *view = _scrollView.subviews[index];
    //    if (view.beginLoadingImage) return;
    [view setImageWithURL:[self highQualityImageURLForIndex:index] placeholderImage:[self placeholderImageForIndex:index]];
    
    //    if ([self highQualityImageURLForIndex:index]) {
    //        [view setImageWithURL:[self highQualityImageURLForIndex:index] placeholderImage:[self placeholderImageForIndex:index]];
    //    } else {
    //        view.imageview.image = [self placeholderImageForIndex:index];
    //    }
    //    view.beginLoadingImage = YES;
}

#pragma mark 获取控制器的view
- (UIView *)getParsentView:(UIView *)view{
    if ([[view nextResponder] isKindOfClass:[UIViewController class]] || view == nil) {
        return view;
    }
    return [self getParsentView:view.superview];
}

#pragma mark 获取低分辨率（占位）图片
- (UIImage *)placeholderImageForIndex:(NSInteger)index
{
    if ([self.delegate respondsToSelector:@selector(photoBrowser:placeholderImageForIndex:)]) {
        return [self.delegate photoBrowser:self placeholderImageForIndex:index];
    }
    return nil;
}

#pragma mark 获取高分辨率图片url
- (NSURL *)highQualityImageURLForIndex:(NSInteger)index
{
    if ([self.delegate respondsToSelector:@selector(photoBrowser:highQualityImageURLForIndex:)]) {
        return [self.delegate photoBrowser:self highQualityImageURLForIndex:index];
    }
    
    return nil;
}

- (NSString*)imageWithIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(photoDrowserWithIndex:)]) {
        return [self.delegate photoDrowserWithIndex:index];
    }
    return @"0";
}

#pragma mark - scrollview代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    int index = (scrollView.contentOffset.x + _scrollView.bounds.size.width * 0.5) / _scrollView.bounds.size.width;
    
    _indexLabel.text = [NSString stringWithFormat:@"%d/%ld", index + 1, (long)self.imageCount];
    long left = index - 2;
    long right = index + 2;
    left = left>0?left : 0;
    right = right>self.imageCount?self.imageCount:right;
    
    //预加载三张图片
    for (long i = left; i < right; i++) {
        [self setupImageOfImageViewForIndex:i];
    }
    
    
    //预加载三张图片
    //    for (long i = 0; i < self.imageCount; i++) {
    //        [self setupImageOfImageViewForIndex:i];
    //    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int autualIndex = scrollView.contentOffset.x  / _scrollView.bounds.size.width;
    //设置当前下标
    self.currentImageIndex = autualIndex;
    [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:[NSString stringWithFormat:@"%d",autualIndex]];

    NSString *str = [NSString stringWithFormat:@"UP主:%@",_dataArr[self.currentImageIndex][@"userIDInfo"][@"name"]];
    CGSize s = [self sizeOfStr:str andFont:[UIFont systemFontOfSize:15] andMaxSize:CGSizeMake(MAXFLOAT, 30) andLineBreakMode:0];
    self.authorNameLabel.frame = CGRectMake(8, 20, s.width+20, 30);
    if (_dataArr[self.currentImageIndex][@"userIDInfo"]) {
        self.authorNameLabel.hidden = NO;
        self.authorNameLabel.text = [NSString stringWithFormat:@"UP主:%@",_dataArr[self.currentImageIndex][@"userIDInfo"][@"name"]];
    }else{
        self.authorNameLabel.hidden = YES;
    }
    NSString *isPraiseStr = [NSString stringWithFormat:@"%@",self.dataArr[self.currentImageIndex][@"isPraise"]];
    if ([isPraiseStr isEqualToString:@"1"]) {
        [self.praiseButton setImage:[UIImage imageNamed:@"dianzan_2pic"] forState:UIControlStateNormal];
    }else{
        [self.praiseButton setImage:[UIImage imageNamed:@"dianzan_2ppic"] forState:UIControlStateNormal];
    }
    
    
    //将不是当前imageview的缩放全部还原 (这个方法有些冗余，后期可以改进)
    for (HZPhotoBrowserView *view in _scrollView.subviews) {
        if (view.imageview.tag != autualIndex) {
            view.scrollview.zoomScale = 1.0;
        }
        [view resetView];
    }
}

-(void)setDataArr:(NSArray *)dataArr{
    _dataArr = dataArr;
}
-(void)setType:(int)type{
    _type = type;
}
#pragma mark 横竖屏设置


- (BOOL)shouldAutorotate
{
    return shouldSupportLandscape;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if (shouldSupportLandscape) {
        return UIInterfaceOrientationMaskAll;
    } else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
