//
//  TaskWantData.h
//  bang
//
//  Created by lianghuigui on 15/6/29.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "yx_baseData.h"



@interface Want_item : yx_baseData

@property(nonatomic,readonly)NSString * jobStartDate;
@property(nonatomic,readonly)NSString * address;
@property(nonatomic,readonly)NSString * industryName;
@property(nonatomic,readonly)NSString * modelType;
//@property(nonatomic,readonly)NSString * 

@end



@interface TaskWantData : yx_baseData
@property(nonatomic,strong)NSMutableArray * results;
@end
