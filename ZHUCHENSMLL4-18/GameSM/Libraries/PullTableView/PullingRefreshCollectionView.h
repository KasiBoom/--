//
//  PullingRefreshCollectionView.h
//  GameSM
//
//  Created by 王涛 on 15/7/13.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kPRCStateNormal = 0,
    kPRCStatePulling = 1,
    kPRCStateLoading = 2,
    kPRCStateHitTheEnd = 3
} PRCState;

@interface CollectLoadingView : UIView
{
    UILabel *_stateLabel;
    UILabel *_dateLabel;
    UIImageView *_arrowView;
    UIActivityIndicatorView *_activityView;
    CALayer *_arrow;
    BOOL _loading;
}
@property (nonatomic,getter = isLoading) BOOL loading;
@property (nonatomic,getter = isAtTop) BOOL atTop;
@property (nonatomic) PRCState state;

- (id)initWithFrame:(CGRect)frame atTop:(BOOL)top;

- (void)updateRefreshDate:(NSDate *)date;

@end

@class PullingRefreshCollectionView;
@protocol PullingRefreshCollectionViewDelegate <NSObject>

@required
- (void)pullingCollectionViewDidStartRefreshing:(PullingRefreshCollectionView *)tableView;

@optional
//Implement this method if headerOnly is false
- (void)pullingCollectionViewDidStartLoading:(PullingRefreshCollectionView *)tableView;
//Implement the follows to set date you want,Or Ignore them to use current date
- (NSDate *)pullingCollectionViewRefreshingFinishedDate;
- (NSDate *)pullingCollectionViewLoadingFinishedDate;

@end

@interface PullingRefreshCollectionView : UICollectionView<UIScrollViewDelegate>
{
    CollectLoadingView *_headerView;
    CollectLoadingView *_footerView;
    UILabel *_msgLabel;
    BOOL _loading;
    BOOL _isFooterInAction;
    NSInteger _bottomRow;
}

@property (assign,nonatomic) id <PullingRefreshCollectionViewDelegate> pullingDelegate;
@property (nonatomic) BOOL autoScrollToNextPage;
@property (nonatomic) BOOL reachedTheEnd;
@property (nonatomic,getter = isHeaderOnly) BOOL headerOnly;

@property (nonatomic, assign) BOOL noPullRefresh;

- (id)initWithFrame:(CGRect)frame pullingDelegate:(id<PullingRefreshCollectionViewDelegate>)aPullingDelegate collectionViewLayout:(UICollectionViewFlowLayout*)layout;

- (void)collectionViewDidScroll:(UIScrollView *)scrollView;

- (void)collectionViewDidEndDragging:(UIScrollView *)scrollView;

- (void)collectionViewDidFinishedLoading;

- (void)collectionViewDidFinishedLoadingWithMessage:(NSString *)msg;

- (void)launchRefreshing;


@end


