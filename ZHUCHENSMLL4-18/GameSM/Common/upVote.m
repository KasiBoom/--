//
//  upVote.m
//  GameSM
//
//  Created by 顾鹏 on 16/1/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "upVote.h"
#import "goldView.h"
static upVote *_upVoteShare = nil;

@implementation upVote
+ (upVote *)shared;
{
    @synchronized(self)
    {
        if(!_upVoteShare){
            
            _upVoteShare = [[upVote alloc] init];
        }
        return _upVoteShare;
    }
}

- (void)startMiddleAnimitionTop:(NSString *)top bottom:(NSString *)bottom point:(CGPoint)point fatherView:(UIView *)fatherView{
    goldView *glodView = [[goldView alloc] initWithFrame:CGRectMake(0, 0, 100, 80) topText:top bottomText:bottom color:[UIColor colorWithRed:255/255.0 green:80/255.0 blue:0/255.0 alpha:1.0]];
    glodView.center = point;
    [fatherView addSubview:glodView];
    [UIView animateWithDuration:0.5 animations:^{
        glodView.transform =CGAffineTransformScale(glodView.transform, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            glodView.transform =CGAffineTransformScale(glodView.transform, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:2.0 animations:^{
                glodView.alpha = 0.2;
                glodView.center = CGPointMake(glodView.center.x, glodView.center.y - 30);
                
            } completion:^(BOOL finished) {
                [glodView removeFromSuperview];
            }];
        }];
    }];
    
    
}

- (void)startAnimitionTop:(NSString *)top bottom:(NSString *)bottom point:(CGPoint)point fatherView:(UIView *)fatherView color:(UIColor *)color{
    goldView *glodView = [[goldView alloc] initWithFrame:CGRectMake(0, 0, 100, 80) topText:top bottomText:bottom color:color];
    glodView.center = point;
    [fatherView addSubview:glodView];
    [UIView animateWithDuration:0.5 animations:^{
        glodView.center = CGPointMake(glodView.center.x, glodView.center.y - 30);
        glodView.transform =CGAffineTransformScale(glodView.transform, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            glodView.transform =CGAffineTransformScale(glodView.transform, 1.0/1.1, 1.0/1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:2.0 animations:^{
                glodView.alpha = 0.2;
                glodView.center = CGPointMake(glodView.center.x, glodView.center.y - 30);
                
            } completion:^(BOOL finished) {
                [glodView removeFromSuperview];
            }];
        }];
    }];
    
}

@end
