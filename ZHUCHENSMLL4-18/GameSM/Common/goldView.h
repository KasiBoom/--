//
//  goldView.h
//  goldText
//
//  Created by 顾鹏 on 16/1/15.
//  Copyright © 2016年 顾鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface goldView : UIView
- (instancetype)initWithFrame:(CGRect)frame topText:(NSString *)top bottomText:(NSString *)bottom color:(UIColor *)color;
@end
