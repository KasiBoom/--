//
//  UIViewController+Utils.h
//  GameSM
//
//  Created by 顾鹏 on 16/4/11.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)
+(UIViewController*) currentViewController;

@end
