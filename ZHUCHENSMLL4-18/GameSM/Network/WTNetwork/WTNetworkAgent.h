//
//  WTNetworkAgent.h
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef enum WTNetworkMethod {
    WTNetworkMethod_POST,
    WTNetworkMethod_GET
} WTNetworkMethod;

typedef void(^Complete)(id result);
typedef void(^uploadProgressBlock)(float progress);

@interface WTNetworkAgent : NSObject

+ (WTNetworkAgent*)sharedInstance;

-(void)sendData:(NSString*)method
            url:(NSString*)urlString
     parameters:(NSMutableDictionary*)requestParameters
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postDataWithFile:(NSString*)urlString
             parameters:(NSMutableDictionary*)requestParameters
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
         uploadProgress:(void (^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead))progressBlock
                success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)cancelRequestWithUrl:(NSString*)urlString;

-(void)cancelAllRequests;

- (void)cancelRequest;

@end
