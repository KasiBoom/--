//
//  WTInterface.m
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "WTInterface.h"

const NSString *API_KEY_CLIENTVERSION       =@"Clientversion";//客户端版本号
const NSString *API_KEY_DEVICETYPE          =@"Devicetype";//设备类型 1：浏览器设备 2：pc设备 3：Android设备 4：ios设备 5：windows phone设备
const NSString *API_KEY_REQUESTTIME         =@"Requesttime";//请求时的时间戳
const NSString *API_KEY_DEVICENTOKEN        =@"Devicetoken";
const NSString *API_KEY_LOGINTIME           =@"Logintime";//登录时间
const NSString *API_KEY_USERID              =@"Userid";//用户ID
const NSString *API_KEY_CHECKCODE           =@"Checkcode";//校验码


const NSString *API_VALUE_CLIENTVERSION     =@"1.0";
const NSString *API_VALUE_DEVICETYPE        =@"4";
const NSString *API_VALUE_USERID            =@"0";
const NSString *API_VALUE_CHECKCODE         =@"";
const NSString *API_VALUE_DEVICENTOKEN      =@"";
const NSString *API_VALUE_LOGINTIME         =@"";


const NSString *API_URL_USER_ADD            = @"user/add";
const NSString *API_URL_USER_LOGIN          = @"user/login";

static NSMutableDictionary* commonParams;

@implementation WTInterface

+ (void)initialize{
    commonParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                    API_VALUE_CLIENTVERSION,API_KEY_CLIENTVERSION,
                    API_VALUE_DEVICETYPE,API_KEY_DEVICETYPE,
                    API_VALUE_USERID,API_KEY_USERID,
                    API_VALUE_DEVICENTOKEN,API_KEY_DEVICENTOKEN,
                    API_VALUE_CHECKCODE,API_KEY_CHECKCODE,
                    API_VALUE_LOGINTIME,API_KEY_LOGINTIME,
                    nil];
}

// 获取数据字典
+ (NSMutableDictionary *)commonParams {
    return commonParams;
}
// 向公用字典中添加字段
+ (void)setCommonParam:(id)key value:(id)value{
    [commonParams setValue:value forKey:key];
}
// 查找key对应的一个对象
+ (id)commonParam:(id) key{
    return [commonParams objectForKey:key];
}
@end
