//
//  WTNetworkAgent.m
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "WTNetworkAgent.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Y_X_DataInterface.h"

@interface WTNetworkAgent () {
    AFHTTPRequestOperationManager *_manager;
}

@property (nonatomic,strong) AFHTTPRequestOperation *httpOperation;

@end

@implementation WTNetworkAgent

+ (WTNetworkAgent*)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        _manager = [[AFHTTPRequestOperationManager alloc] init];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        _manager.requestSerializer=[AFJSONRequestSerializer serializer];
    }
    return self;
}

- (void)request:(NSMutableDictionary *)parameter method:(NSString *)method block:(Complete)block {
    
}
-(void)sendData:(NSString*)method
            url:(NSString*)urlString
     parameters:(NSMutableDictionary*)requestParameters
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {

    NSMutableURLRequest *request = [_manager.requestSerializer requestWithMethod:method URLString:[NSString stringWithFormat:@"%@%@",INTERFACE_PREFIX,urlString] parameters:requestParameters error:nil];
    NSLog(@"url:%@",request.URL);
    
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success)
        {
            success(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure)
        {
            failure(operation, error);
        }
    }];
    
    [_manager.operationQueue addOperation:operation];
}

-(void)postDataWithFile:(NSString*)urlString
             parameters:(NSMutableDictionary*)requestParameters
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
         uploadProgress:(void (^)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead))progressBlock
                success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {

    NSMutableURLRequest *newRequest = [_manager.requestSerializer multipartFormRequestWithMethod:POSTDATA URLString:[NSString stringWithFormat:@"%@%@",INTERFACE_PREFIX,urlString]  parameters:requestParameters constructingBodyWithBlock:block error:nil];
    
    [newRequest setTimeoutInterval:12];
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:newRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success)
        {
            success(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure)
        {
            failure(operation, error);
        }
    }];
    
    if (progressBlock)
    {
        [operation setDownloadProgressBlock:progressBlock];
    }
    [_manager.operationQueue addOperation:operation];
}

-(void)cancelRequestWithUrl:(NSString*)urlString {
    for (NSOperation *operation in [_manager.operationQueue operations]) {
        if (![operation isKindOfClass:[AFHTTPRequestOperation class]]) {
            continue;
        }
        
        NSString *method = nil;
        NSString *path = urlString;
        if ((!method || [method isEqualToString:[[(AFHTTPRequestOperation *)operation request] HTTPMethod]]) && [path isEqualToString:[[[(AFHTTPRequestOperation *)operation request] URL] path]]) {
            [operation cancel];
        }
    }
}

-(void)cancelAllRequests {
    [_manager.operationQueue cancelAllOperations];
}

- (void)cancelRequest {
    [self.httpOperation cancel];
}

#pragma mark - 添加请求体信息
- (void)createHttpHeader {
    
}
@end
