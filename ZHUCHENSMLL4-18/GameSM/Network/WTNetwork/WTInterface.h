//
//  WTInterface.h
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

#define INTERFACE_PREFIX @"http://maimeng.api.appzd.net/index.php"//共同接口

#define GETDATA @"GET"
#define POSTDATA @"POST"

const NSString *API_KEY_CLIENTVERSION;
const NSString *API_KEY_DEVICETYPE;
const NSString *API_KEY_REQUESTTIME;
const NSString *API_KEY_DEVICENTOKEN;
const NSString *API_KEY_LOGINTIME;
const NSString *API_KEY_USERID;
const NSString *API_KEY_CHECKCODE;


const NSString *API_VALUE_CLIENTVERSION;
const NSString *API_VALUE_DEVICETYPE;
const NSString *API_VALUE_USERID;
const NSString *API_VALUE_CHECKCODE;
const NSString *API_VALUE_DEVICENTOKEN;
const NSString *API_VALUE_LOGINTIME;

extern const NSString *API_URL_USER_ADD;
extern const NSString *API_URL_USER_LOGIN;

@interface WTInterface : NSObject

+(void)initialize;
+(NSMutableDictionary *)commonParams ;
+(void) setCommonParam:(id)key value:(id)value;
+(id) commonParam:(id) key;

@end
