//
//  Y_X_DataInterface.h
//  YouYou_Work
//
//  Created by lianghuigui on 13-11-18.
//  Copyright (c) 2013年 lianghuigui. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "YK_API_request.h"

// api-360.appzd.net
//http://api-haoye.appzd.net/index.php?
//#define INTERFACE_PREFIXS @"http://maimeng.api.appzd.net/index.php?"//共同接口

//#ifdef DEBUG
//
//#define INTERFACE_PREFIX @"http://apitest.playsm.com/index.php?"//共同接口
//#define INTERFACE_PREFIXD @"http://apitest.playsm.com/index.php?"////线上测试环境
//#else

//#define INTERFACE_PREFIX @"http://api.playsm.com/index.php?"//共同接口
//#define INTERFACE_PREFIXD @"http://api.playsm.com/index.php?"////线上测试环境
//#endif

#define GETDATA @"GET"
#define POSTDATA @"POST"

//1.0  Http公共头名称
// 必须 --key
extern const NSString* YX_KEY_CLIENTVERSION;//客户端版本号
//----接口
extern const NSString* INTERFACE_PREFIX;
extern const NSString* INTERFACE_PREFIXD;

extern const NSString* YX_KEY_DEVICETYPE;
extern const NSString* YX_KEY_REQUESTTIME;
extern const NSString* YX_KEY_DEVICENTOKEN;
extern const NSString* YX_KEY_LOGINTIME;
extern const NSString* YX_KEY_USERID;
extern const NSString* YX_KEY_CHECKCODE;
extern const NSString* YX_KEY_DEVICEINFO;
extern const NSString *YX_KEY_CLENTID;

//----主题



//VALUE
extern const NSString* YX_VALUE_CLIENTVERSION;
extern const NSString* YX_VALUE_DEVICETYPE;
extern const NSString* YX_VALUE_USERID;
extern const NSString* YX_VALUE_CHECKCODE;
extern const NSString* YX_VALUE_DEVICENTOKEN;
extern const NSString *YX_VALUE_REQUESTTIME;
extern const NSString *YX_VALUE_DEVICEINFO;
extern const NSString *YX_VALUE_CLENTID;

extern const NSString *API_URL_USER_ADD;
extern const NSString *API_URL_USER_LOGIN;
extern const NSString *API_URL_USER_UNION_LOGIN;
extern const NSString *API_URL_USER_LOGOUT;

extern const NSString *API_URL_USER_UPDATE_PASSWORD;
extern const NSString *API_URL_USER_CHECK_VERIFY_CODE;
extern const NSString *API_URL_USER_DETAIL;
extern const NSString *API_URL_USER_UPDATE;
extern const NSString *API_URL_USER_CENTER;
extern const NSString *API_URL_USER_RESET_PASSWORD;
extern const NSString *API_URL_CONTENT_WITHUSERINFORM;

extern const NSString *API_URL_SYSTEM_SENDNOTE;
extern const NSString *API_URL_SYSTEM_GETUPTOKEN;

extern const NSString *API_URL_MESSAGE_LIST;
extern const NSString *API_URL_MESSAGE_DETAIL;
extern const NSString *API_URL_MESSAGE_PRAISE;

extern const NSString *API_URL_PRETTYIMAGESLIST;
extern const NSString *API_URL_PRETTYIMAGESDETAIL;
extern const NSString *API_URL_PRETTYIMAGESPRAISE;
extern const NSString *API_URL_UPLOADNICEPIC;

extern const NSString *API_URL_CONTENT_COMMENT_MESSAGE;
extern const NSString *API_URL_CONTENT_REPLY_MESSAGE;
extern const NSString *API_URL_CONTENT_WITH_USER_COMMENT;
extern const NSString *API_URL_ACTIONTYPE_WITH_USER_PRAISE;

extern const NSString *API_URL_GIFT_LIST;
extern const NSString *API_URL_GIFT_DETAIL;
extern const NSString *API_URL_GIFT_SWITCH;
extern const NSString *API_URL_GIFTCODE_GETCODE;
extern const NSString *API_URL_GIFTCODE_WITHUSER;

extern const NSString *API_URL_FEEDBACK_ADD;
extern const NSString *API_URL_FEEDBACK_ADD_CONSTANT;
extern const NSString *API_URL_ABOUT;

extern const NSString *API_URL_CONTENT_WITHUSERREPLY;
extern const NSString *API_URL_INFORMMESSAGE_WITHINFORM;

extern const NSString *API_URL_ADIMAGE_LIST;
extern const NSString *API_URL_ADIMAGE_LOG_ADD;

extern const NSString *API_URL_RECOMMENT_LIST;
extern const NSString *API_URL_RECOMMENTDOWNLOAD_LIST;
extern const NSString *API_URL_RECOMMENTCOLLECTION_LIST;
extern const NSString *API_URL_SEARCH_LIST;
extern const NSString *API_URL_TOP_LIST;
extern const NSString *API_URL_TOP_DETAIL;
extern const NSString *API_URL_CHAPTERALBUM_LIST;
extern const NSString *API_URL_CATEGORY_LIST;
extern const NSString *API_URL_CATEGORY_DETAL;
extern const NSString *API_URL_BOOKCOLLECTION_LIST;
extern const NSString *API_URL_SEARCHLIST_LIST;
extern const NSString *API_URL_CARTOONSET_LIST;
extern const NSString *API_URL_CARTOONCONTENT_LIST;
extern const NSString *API_URL_BOOKCOLLECTIONLIST_LIST;
extern const NSString *API_URL_CARTOONCHAPTER_DETAIL;
extern const NSString *API_URL_CARTOONUSERREADHISTORY_DETAIL;
extern const NSString *API_URL_CARTOONREADHISTORY_DETAIL;
extern const NSString *API_URL_CARTOONCHAPTERALBUM_DETAIL;
extern const NSString *API_URL_RECENTUPDATE_LIST;
extern const NSString *API_URL_RECOMMENTTOP_LIST;
extern const NSString *API_URL_CARTOONCOLLECTION_LIST;
extern const NSString *API_URL_RECOMMENTCOLLECTIN_LIST;
extern const NSString *API_URL_RECOMMENTLU_LIST;
extern const NSString *API_URL_EVERYBODYWATCHING_LIST;
extern const NSString *API_URL_CONTINUELIST;
extern const NSString *API_URL_ADIMAGE;
extern const NSString *API_URL_NEWRECOMMENTALL_LIST;
extern const NSString *API_URL_RECOMMENTALL_LIST;
extern const NSString *API_URL_CONTENT_DELETE;
extern const NSString *API_URL_CONTENT_WITHUSERCARTOONCOMMENT;
extern const NSString *API_URL_CARTOONSET_CONTENTLIST;
extern const NSString *API_URL_UPDATEINFO;
extern const NSString *API_URL_LOGO;
extern const NSString *API_URL_UPDATE;
extern const NSString *API_URL_UPCATEGROY;
extern const NSString *API_URL_SPECIAL_LIST;
extern const NSString *API_URL_SPECIAL_DETAIL;
extern const NSString *API_URL_PICTUREZAN;
extern const NSString *API_URL_PRAISEPIC;
extern const NSString *API_URL_USERPICCOMMMENT;
extern const NSString *API_URL_CARTOONCOMMENT;
extern const NSString *API_URL_SCAN;
@interface Y_X_DataInterface : NSObject

+(void)init;
+(NSMutableDictionary *)commonParams ;
+(void) setCommonParam:(id)key value:(id)value;
+(id) commonParam:(id) key;

@end
