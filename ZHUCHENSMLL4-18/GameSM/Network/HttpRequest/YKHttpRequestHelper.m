//
//  YKHttpRequestHelper.m
//  YK_B2C
//
//  Created by Guwei.Z on 11-4-28.
//  Modify by YuHui.Liu on 11-7-8
//  Copyright 2011 Yek. All rights reserved.
//

#import "YKHttpRequestHelper.h"
#import "ASIHTTPRequest.h"
#import "GDataXMLNode.h"

#pragma mark - YKHttpRequestHelper
@implementation YKHttpRequestHelper
@synthesize m_ResponseType;

-(id) initWithObject:(id)aobj action:(SEL)aaction{
	if(self=[super init]){
		//assert(aobj!=nil);
		object=[aobj retain];
		action=aaction;
        m_ResponseType = 0;
	}
	return self;
}
-(void) dealloc{
	[object release];
	[super dealloc];
}

-(void) onLoadFinished:(NSString *)responseString{
    
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err = nil;
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&err];
    if (jsonDic == NULL) {
        jsonDic = @{@"contentValue":responseString};
    }
    [jsonDic retain];
    // 判断是否成功获取数据
    if (jsonDic) {
        int errorCode=[[jsonDic objectForKey:@"code"] intValue];
        if (errorCode!=0 && errorCode != 51) {
                NSString *errorStr=[jsonDic objectForKey:@"error"];
                [self titleShowAlert:errorStr];
            }
    }
    
    [object performSelector:action withObject:[jsonDic autorelease]];
}

-(void)titleShowAlert:(NSString *)titleStr{
    NSLog(@"%@",titleStr);
//    CustomAlertView * alertView=[[CustomAlertView alloc]initWithTitle:@"脾气差的提示君" message:titleStr delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//    [alertView show];
}

-(void) onLoadFailed:(NSError *)error{
    // 如果返回的时XML
    if (m_ResponseType == 1) {
        GDataXMLDocument* xmlDoc=nil;
        [object performSelector:action withObject:xmlDoc];
        
        return ;
    }
    
    NSDictionary* jsonDic = nil;
	[object performSelector:action withObject:jsonDic];
}
- (void)sendASIHTTPRequestOBJ:(ASIHTTPRequest*)obj{
   
    
}

@end
