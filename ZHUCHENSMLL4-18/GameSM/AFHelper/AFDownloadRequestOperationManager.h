//
//  AFDownloadRequestOperationManager.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/12.
//  Copyright © 2015年 王涛. All rights reserved.
//


#import <Foundation/Foundation.h>
@class AFDownloadRequestOperation;
@class AFHTTPRequestOperation;
@interface AFDownloadRequestOperationManager : NSObject
@property(nonatomic,copy)NSArray *requestUrl;
@property(nonatomic,copy)NSString *filepath;
@property(nonatomic,copy)void(^completeRequestBlock)(AFHTTPRequestOperation *operation,id responseObject);
@property(nonatomic,copy)void(^failRequestBlock)(AFHTTPRequestOperation *operation,NSError *error);
@property(nonatomic,assign)BOOL isResum;
+ (id)sharedAFDownloadRequestOperationManager;
- (void)addDownRequest:(NSArray *)requestUrl targetPath:(NSString *)filepath
                                             shouldResum:(BOOL)isResum
                                         completeBlockRe:(void(^)(AFHTTPRequestOperation *operation,id responseObject))completeRequestBlock failBlockRe:(void(^)(AFHTTPRequestOperation *operation,NSError *error))failRequestBlock;

@end
