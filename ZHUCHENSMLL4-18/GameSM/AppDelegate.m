//
//  AppDelegate.m
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "AppDelegate.h"
#import "LYTabBarController.h"
#import "Y_X_DataInterface.h"
#import "XGPush.h"
#import "XGSetting.h"
#import "InfoDetailViewController.h"
#import "PicDetailViewController.h"
#import "GiftDetailController.h"
#import "HomeViewController.h"
#import "GiftViewController.h"
#import "WebViewController.h"
#import "UMSocial.h"
#import "GuideViewController.h"

#import "HomeViewController.h"
#import "MobClick.h"

#import "ReadController.h"
#import "TalkingData.h"

#import <AdSupport/AdSupport.h>
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaSSOHandler.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "YKHttpRequest.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "NSDate+OTS.h"
#import "DataBaseHelper.h"
#import "LogHelper.h"
#import "logModel.h"
#import "SaveWatchDownViewController.h"
#import "BookDetailsViewController.h"
@interface AppDelegate ()
{
    Reachability  *hostReach;
    UILocalNotification *_localNoti;
    UIButton *secondButton;
    NSTimer * timer;
    NSInteger    secondSum;

}
@property (nonatomic) BOOL isLaunchedByNotification;
@property (strong, nonatomic) UIImageView *adImageView;
@property (copy,   nonatomic)LYTabBarController *rootTab;
@end

@implementation AppDelegate

- (void)registerPushForIOS8{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //Types
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //Actions
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";//按钮的标示
    acceptAction.title = @"Accept";//按钮的标题
    
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.authenticationRequired = NO;//锁屏
    acceptAction.destructive = NO;
    
    
    //Categories
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    
    inviteCategory.identifier = @"INVITE_CATEGORY";
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    NSSet *categories = [NSSet setWithObjects:inviteCategory, nil];
    
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
}

- (void)registerPush{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _isLaunchedByNotification = NO;
    // Override point for customization after application launch.
    [Y_X_DataInterface init];
    [Y_X_DataInterface setCommonParam:YX_KEY_CLIENTVERSION value:Version];
    [UMSocialData setAppKey:UMENGKEY];
    [MobClick startWithAppkey:UMENGKEY reportPolicy:BATCH channelId:@"App store"];
    
    [UMSocialWechatHandler setWXAppId:WECHATAPPID appSecret:WECHATAPPSECRET url:@"http://www.umeng.com/social"];
    //设置手机QQ 的AppId，Appkey，和分享URL，需要#import "UMSocialQQHandler.h"
    [UMSocialQQHandler setQQWithAppId:QQAPPID appKey:QQAPPSECRET url:@"http://www.umeng.com/social"];
    //打开新浪微博的SSO开关，设置新浪微博回调地址，这里必须要和你在新浪微博后台设置的回调地址一致。若在新浪后台设置我们的回调地址，“http://sns.whalecloud.com/sina2/callback”，这里可以传nil ,需要 #import "UMSocialSinaHandler.h"
    [UMSocialSinaSSOHandler openNewSinaSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    
    [UMSocialConfig hiddenNotInstallPlatforms:@[UMShareToQQ,UMShareToQzone,UMShareToWechatSession,UMShareToWechatTimeline]];//隐藏指定没有安装客户端的平台。
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor blackColor];
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ISDEBUGEN"]  isEqualToString:@"1"]){
        INTERFACE_PREFIX = @"http://apitest.playsm.com/index.php?";
        INTERFACE_PREFIXD = @"http://apitest.playsm.com/index.php?";
    }else{
        INTERFACE_PREFIX = @"http://api.playsm.com/index.php?";
        INTERFACE_PREFIXD = @"http://api.playsm.com/index.php?";
    }
    
//    NSString *carrair = [self checkCarrier];
//    NSDictionary *advTist = @{@"adtype":@"3",@"width":@(ScreenSizeWidth),@"height":@(ScreenSizeHeight),@"pkgname":[[NSBundle mainBundle] bundleIdentifier],@"appname":[@"麦萌" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],@"ua":@"sssss",@"os":@(1),@"osv":[[UIDevice currentDevice] systemVersion],@"carrier":@(1),@"conn":@(3),@"ip":[self getIPAddress],@"density":@(2),@"brand":@"iphone",@"model":[self platformString],@"uuid":[self getIdfa],@"pw":@(ScreenSizeWidth),@"ph":@(ScreenSizeHeight),@"devicetype":@(1)};
    
//    [YK_API_request startLoad:@"http://m.adinall.com/api.m" extraParams:advTist object:self action:@selector(uploadInfo:) method:@"POST" andHeaders:dic];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager POST:@"http://m.adinall.com/api.m" parameters:advTist success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        NSLog(@"backData%@",backData);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
    [self.window makeKeyAndVisible];
    
    
    if ([Tool showGuideView]) {
    
        NSInteger todatDays = [[NSDate new] distanceNowDays];
        
        [[DataBaseHelper shared] addDateTime:todatDays];}

//        GuideViewController *guideVC = [[GuideViewController alloc] initWithNibName:@"GuideViewController" bundle:nil];
//        self.window.rootViewController = guideVC;
//    } else{
        
        _rootTab = [[LYTabBarController alloc] init];
        self.window.rootViewController = _rootTab;
//        self.pushDic = [[NSDictionary alloc] initWithDictionary:launchOptions];
        self.pushDic = [[NSDictionary alloc]initWithDictionary:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
        
        
//        [YK_API_request startGetLoad:API_URL_ADIMAGE extraParams:nil object:self action:@selector(advertiseMent:)];
//        [YK_API_request startGetLoad:@"http://api.playsm.com/index.php" extraParams:@{@"r":@"adImage/getBootAds"} object:nil action:@selector(advertiseMent:)];
        
//        [YK_API_request startLoad:@"http://api.playsm.com/index.php" extraParams:@{@"r":@"adImage/getBootAds"}  object:self action:@selector(advertiseMent:) method:@"GET" andHeaders:nil];
        
        secondSum = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DURATION"] intValue];
        
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVTISE"] length] > 0){
            if (![[NSUserDefaults standardUserDefaults] objectForKey:@"ADVISECOUNT"]) {
                [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"ADVISECOUNT"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else {
                NSInteger adviseCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVISECOUNT"] integerValue];
                adviseCount ++;
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:adviseCount] forKey:@"ADVISECOUNT"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            NSLog(@"adviseCount  %d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVISECOUNT"] integerValue]);
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVISECOUNT"] integerValue] > 3) {
                
            }else{
                
            NSLog(@"json%@",[[LogHelper shared] writeToApafrom_page:@"" from_section:@"a" from_step:@"r" to_page:@"ad" to_section:@"a" to_step:@"r" type:@"" id:@"0"]);
                
            self.adImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
            self.adImageView.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *tapAdImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpToDetails)];
            [self.adImageView addGestureRecognizer:tapAdImage];
            
            
            UIButton *skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
            skipButton.frame = CGRectMake(ScreenSizeWidth - 60, ScreenSizeHeight - 50, 50, 30);
            skipButton.backgroundColor = [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1.0];
            [skipButton setTitle:@"跳过" forState:UIControlStateNormal];
            [skipButton addTarget:self action:@selector(buttonSkip:) forControlEvents:UIControlEventTouchUpInside];
            
            
            secondButton = [UIButton buttonWithType:UIButtonTypeCustom];
            secondButton.frame = CGRectMake(ScreenSizeWidth - 110, ScreenSizeHeight - 50, 50, 30);
            [secondButton setTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"DURATION"] forState:UIControlStateNormal];
            [secondButton setTitleColor:[UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1.0] forState:UIControlStateNormal];
                [self.window addSubview:self.adImageView];
                
            
//            [self.adImageView setImageWithURL:[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVTISE"]];
//            [self.adImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVTISE"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                
//            }];
                [self.adImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVTISE"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    [self.adImageView addSubview:skipButton];
                                    [self.adImageView addSubview:secondButton];

                }];
                
//                [self.adImageView addSubview:skipButton];

//                [self.adImageView addSubview:secondButton];

            //    [self performSelector:@selector(removeAdImageView) withObject:nil afterDelay:3];
//            [self.window bringSubviewToFront:self.adImageView];
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(removeADView) userInfo:nil repeats:YES];
        
            }
        }
        
        
        if (self.pushDic) {
            _isLaunchedByNotification = YES;
            [self pushViewControllerNotifter];
        }
//    }
    
    [TalkingData setExceptionReportEnabled:YES];
    [TalkingData sessionStarted:@"D5BD7CE1EE13942BD33273A4832C3DF3" withChannelId:@"AppStore"];
    
    [XGPush startApp:2200132889 appKey:@"I35IPFT828GB"];
    NSString* tag= @"test-cao";
    
    
    [XGPush setTag: tag];
    [Y_X_DataInterface setCommonParam:YX_KEY_DEVICEINFO value:[UIDevice currentDevice].model];
    [Y_X_DataInterface setCommonParam:YX_KEY_CLENTID value:[self getIdfa]];
    [Y_X_DataInterface setCommonParam:YX_KEY_CLIENTVERSION value:Version];
    
    self.adId = [self getIdfa];
    
    
    //注销之后需要再次注册前的准备
    void (^successCallback)(void) = ^(void){
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
            
            float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];//获取系统版本号
            if(sysVer < 8){
                [self registerPush];
            }else{
                [self registerPushForIOS8];
            }
#else
            //iOS8之前注册push方法
            //注册Push服务，注册后才能收到推送
            [self registerPush];
#endif
        }
    };
    
    [XGPush initForReregister:successCallback];
    
//    [XGPush registerPush];  //注册Push服务，注册后才能收到推送
    
    
    //推送反馈(app不在前台运行时，点击推送激活时)
    //[XGPush handleLaunching:launchOptions];
    
    //推送反馈回调版本示例
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]handleLaunching's successBlock");
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]handleLaunching's errorBlock");
    };
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] length] == 0){
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"OPENTURN"];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DOWNLOADBUTTON"] length] == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"DOWNLOADBUTTON"];

    }
    
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //清除所有通知(包含本地通知)
    [XGPush handleLaunching:launchOptions successCallback:successBlock errorCallback:errorBlock];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
    hostReach = [Reachability reachabilityWithHostName:@"www.google.com"] ;//可以以多种形式初始化
    
    [hostReach startNotifier];  //开始监听,会启动一个run loop
    [self updateInterfaceWithReachability: hostReach];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"SYNTIME"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] forKey:@"SYNTIME"];
    }
    
    
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"APPEARED"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"APPEARED"];
    }
    return YES;
}


//- (void)requireLog:(NSDictionary *)dict{
//    if (![dict[@"status"] isEqualToString:@"0"]) {
//        [[LogHelper shared] writeToFilefrom_page:@"null" from_section:@"a" from_step:@"r" to_page:@"ad" to_section:@"a" to_step:@"r" type:@"" id:@"0"];
//        
//    }
//
//}

-(void)jumpToDetails{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"OUTVALUE"];
    
    if ([[ [NSUserDefaults standardUserDefaults] objectForKey:@"HOMEADCUSTOMTYPE" ] isEqualToString:@"1"]) {
        
        [[LogHelper shared] writeToFilefrom_page:@"ad" from_section:@"a" from_step:@"r" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:@"0"];
        if (_isLaunchedByNotification) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:str forKey:@"myValue"];
            [userDefaults synchronize];
            [UIView animateWithDuration:2.0 animations:^{
                self.adImageView.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
                self.adImageView.alpha = 0.f;
            } completion:^(BOOL finished) {
                [self.adImageView removeFromSuperview];
            }];
        }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"advSkip" object:nil];
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEADCUSTOMTYPE"] isEqualToString:@"5"]){
        [[LogHelper shared] writeToFilefrom_page:@"ad" from_section:@"a" from_step:@"r" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:@"0"];
        
        if (_isLaunchedByNotification) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:str forKey:@"testViewControllerValue"];
            [userDefaults synchronize];
            [UIView animateWithDuration:2.0 animations:^{
                self.adImageView.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
                self.adImageView.alpha = 0.f;
            } completion:^(BOOL finished) {
                [self.adImageView removeFromSuperview];
            }];
        }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"advSkip" object:nil];
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEADCUSTOMTYPE"] isEqualToString:@"21"]){
   
//        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"customValue"] forKey:@"OUTVALUE"];
        
        [[LogHelper shared] writeToFilefrom_page:@"ad" from_section:@"a" from_step:@"r" to_page:@"" to_section:@"" to_step:@"a" type:@"" id:@"0"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"OUTVALUE"]]];

    }
    
    
}

- (void)advertiseMent:(NSDictionary *)dict{
    
    
//    [self.adImageView setImage:[UIImage imageNamed:@"Default-568h"]];
//        [self.window addSubview:self.adImageView];
//        //    [self performSelector:@selector(removeAdImageView) withObject:nil afterDelay:3];
//        [self.window bringSubviewToFront:self.adImageView];
//            [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(removeADView) userInfo:nil repeats:NO];
    
    
}

- (void)buttonSkip:(UIButton *)buttonSkip{
//    [UIView animateWithDuration:1.0 animations:^{
    [timer invalidate];
        self.adImageView.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
        self.adImageView.alpha = 0.f;
//    } completion:^(BOOL finished) {
        [self.adImageView removeFromSuperview];
        self.window.rootViewController = _rootTab;
    
//    }];

}
- (void)advBack:(NSNotification *)noti{
    
//    NSLog(@"%@",noti);
}

//- (NSString*)getDeviceVersion
//{
//    size_t size;
//    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
//    char *machine = (char*)malloc(size);
//    sysctlbyname("hw.machine", machine, &size, NULL, 0);
//    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
//    free(machine);
//    return platform;to
//}
//
//- (NSString *) platformString{
//    NSString *platform = [self getDeviceVersion];
//    //iPhone
//    if ([platform isEqualToString:@"iPhone1,1"])   return@"iPhone 1G";
//    if ([platform isEqualToString:@"iPhone1,2"])   return@"iPhone 3G";
//    if ([platform isEqualToString:@"iPhone2,1"])   return@"iPhone 3GS";
//    if ([platform isEqualToString:@"iPhone3,1"])   return@"iPhone 4";
//    if ([platform isEqualToString:@"iPhone3,2"])   return@"Verizon iPhone 4";
//    if ([platform isEqualToString:@"iPhone3,3"])   return@"iPhone 4 (CDMA)";
//    if ([platform isEqualToString:@"iPhone4,1"])   return @"iPhone 4s";
//    if ([platform isEqualToString:@"iPhone5,1"])   return @"iPhone 5 (GSM/WCDMA)";
//    if ([platform isEqualToString:@"iPhone4,2"])   return @"iPhone 5 (CDMA)";
//    
//    //iPot Touch
//    if ([platform isEqualToString:@"iPod1,1"])     return@"iPod Touch 1G";
//    if ([platform isEqualToString:@"iPod2,1"])     return@"iPod Touch 2G";
//    if ([platform isEqualToString:@"iPod3,1"])     return@"iPod Touch 3G";
//    if ([platform isEqualToString:@"iPod4,1"])     return@"iPod Touch 4G";
//    if ([platform isEqualToString:@"iPod5,1"])     return@"iPod Touch 5G";
//    //iPad
//    if ([platform isEqualToString:@"iPad1,1"])     return@"iPad";
//    if ([platform isEqualToString:@"iPad2,1"])     return@"iPad 2 (WiFi)";
//    if ([platform isEqualToString:@"iPad2,2"])     return@"iPad 2 (GSM)";
//    if ([platform isEqualToString:@"iPad2,3"])     return@"iPad 2 (CDMA)";
//    if ([platform isEqualToString:@"iPad2,4"])     return@"iPad 2 New";
//    if ([platform isEqualToString:@"iPad2,5"])     return@"iPad Mini (WiFi)";
//    if ([platform isEqualToString:@"iPad3,1"])     return@"iPad 3 (WiFi)";
//    if ([platform isEqualToString:@"iPad3,2"])     return@"iPad 3 (CDMA)";
//    if ([platform isEqualToString:@"iPad3,3"])     return@"iPad 3 (GSM)";
//    if ([platform isEqualToString:@"iPad3,4"])     return@"iPad 4 (WiFi)";
//    if ([platform isEqualToString:@"i386"] || [platform isEqualToString:@"x86_64"])        return@"Simulator";
//    
//    return platform;
//}
//
//- (NSString *)getIPAddress {
//    NSString *address = @"error";
//    struct ifaddrs *interfaces = NULL;
//    struct ifaddrs *temp_addr = NULL;
//    int success = 0;
//    // retrieve the current interfaces - returns 0 on success
//    success = getifaddrs(&interfaces);
//    if (success == 0) {
//        // Loop through linked list of interfaces
//        temp_addr = interfaces;
//        while(temp_addr != NULL) {
//            if(temp_addr->ifa_addr->sa_family == AF_INET) {
//                // Check if interface is en0 which is the wifi connection on the iPhone
//                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
//                    // Get NSString from C String
//                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
//                }
//            }
//            temp_addr = temp_addr->ifa_next;
//        }
//    }
//    // Free memory
//    freeifaddrs(interfaces);
//    return address;
//
//}
//
//- (NSString*)checkCarrier
//{
//    
//    NSString *ret = [[NSString alloc]init];
//    
//    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
//    
//    CTCarrier *carrier = [info subscriberCellularProvider];
//    
//    if (carrier == nil) {
//        
//        return @"0";
//        
//    }
//    
//    NSString *code = [carrier mobileNetworkCode];
//    
//    if ([code isEqual: @""]) {
//        
//        return @"0";
//        
//    }
//    
//    if ([code isEqualToString:@"00"] || [code isEqualToString:@"02"] || [code isEqualToString:@"07"]) {
//        
//        ret = @"移动";
//        
//    }
//    
//    if ([code isEqualToString:@"01"]|| [code isEqualToString:@"06"] ) {
//        
//        ret = @"联通";
//        
//    }
//    
//    if ([code isEqualToString:@"03"]|| [code isEqualToString:@"05"] ) {
//        
//        ret = @"电信";;
//        
//    }
//    
//    NSLog(@"%@",ret);
//    
//    return ret;
//    
//}


//static int secondSum = 3;
- (void)removeADView
{
    NSLog(@"timer++++++");
    secondSum --;
    [secondButton setTitle:[NSString stringWithFormat:@"%d",secondSum] forState:UIControlStateNormal];
    if (secondSum == 0) {
        [UIView animateWithDuration:2.0 animations:^{
            self.adImageView.transform = CGAffineTransformMakeScale(1.5f, 1.5f);
            self.adImageView.alpha = 0.f;
        } completion:^(BOOL finished) {
            [self.adImageView removeFromSuperview];
            self.window.rootViewController = _rootTab;
            [timer invalidate];
        }];
        
    }
}


- (void)dealloc {
    [timer invalidate];
    timer = nil;
}



- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

//处理连接改变后的情况
- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    //对连接改变做出响应的处理动作。
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (status == ReachableViaWWAN) {  //没有连接到网络就弹出提实况
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"My App Name"
//                                                        message:@"NotReachable"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"YES" otherButtonTitles:nil];
//        [alert show];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CURRENTWLAN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else if(status == ReachableViaWiFi){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"CURRENTWLAN"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        NSDate *now = [NSDate date];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"updateCueTime"] length] == 0) {
            [self performSelector:@selector(isup) withObject:nil afterDelay:0.5];
            NSDate *updataTime = [now dateByAddingTimeInterval:7*24*60*60 - 8*60*60];
            NSString *date = [formatter stringFromDate:updataTime];
            [userDefaults setObject:date forKey:@"updateCueTime"];
            [userDefaults synchronize];
        }else{
            NSString *nextUpdateTime = [userDefaults stringForKey:@"updateCueTime"];
            NSDate *date1 = [formatter dateFromString:nextUpdateTime];
            NSTimeInterval time = [date1 timeIntervalSinceDate:now];
            if (time <= 0) {
                [userDefaults setObject:nil forKey:@"updateCueTime"];
                [self performSelector:@selector(isup) withObject:nil afterDelay:0.5];
                NSDate *updataTime = [now dateByAddingTimeInterval:7*24*60*60 - 8*60*60];
                NSString *date = [formatter stringFromDate:updataTime];
                [userDefaults setObject:date forKey:@"updateCueTime"];
                [userDefaults synchronize];
            }
        }
    }else if (status == NotReachable){
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"CURRENTWLAN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

-(void)isup{
    [MobClick checkUpdate:@"有新版本" cancelButtonTitle:@"奴家没网" otherButtonTitles:@"立即尝试"];
}

//挂起
- (void)applicationWillResignActive:(UIApplication *)application {
    isPush = NO;
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

//复原
- (void)applicationDidBecomeActive:(UIApplication *)application {
   
//计算时间提示book为空
    NSDate *now = [NSDate date];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"updateBookTipTime"] length] == 0) {
        NSDate *updataTime = [now dateByAddingTimeInterval:24*60*60];
        NSString *date = [formatter stringFromDate:updataTime];
        [userDefaults setObject:date forKey:@"updateBookTipTime"];
        [userDefaults synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ISSHOWTHEBOOKHELP"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        NSString *nextUpdateTime = [userDefaults stringForKey:@"updateBookTipTime"];
        NSDate *date1 = [formatter dateFromString:nextUpdateTime];
        NSTimeInterval time = [date1 timeIntervalSinceDate:now];
        if (time <= 0) {
            NSDate *updataTime = [date1 dateByAddingTimeInterval:24*60*60];
            NSString *date = [formatter stringFromDate:updataTime];
            [userDefaults setObject:date forKey:@"updateBookTipTime"];
            [userDefaults synchronize];
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ISSHOWTHEBOOKHELP"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }

    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    application.applicationIconBadgeNumber = 0;
    isPush = YES;
}

//终止
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //注册设备
//    NSLog(@"deviceToken%@",deviceToken);
    NSString *date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    date = [formatter stringFromDate:[NSDate date]];
    [Y_X_DataInterface setCommonParam:YX_VALUE_REQUESTTIME value:date];
    NSString * deviceTokenStr = [XGPush registerDevice: deviceToken];
    [Y_X_DataInterface setCommonParam:YX_KEY_DEVICENTOKEN value:deviceTokenStr];
    
    NSDictionary *dic = [Y_X_DataInterface commonParams];
    NSDictionary *dict;
    dict = @{@"r":@"device/synDeviceInfo"};
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:dict object:self action:@selector(uploadInfo:) method:@"POST" andHeaders:dic];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
}

-(void)uploadInfo:(NSDictionary *)upload{
//    NSLog(@"--%@",upload);
}

//接收到推送消息，处理推送消息 
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    NSLog(@"%@, %d", userInfo, isPush);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"haveNewNotice" object:userInfo];
    self.pushDic = [[NSDictionary alloc] initWithDictionary:userInfo];
    if (!isPush) {
        _isLaunchedByNotification = NO;http://zhidao.baidu.com/daily/view?id=7880
        [self pushViewControllerNotifter];
    } else {
    }
  
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    NSLog(@"内存警告");
}


//接受到本地消息
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{

    _localNoti.applicationIconBadgeNumber = 1;
    //notification是发送推送时传入的字典信息
    [XGPush localNotificationAtFrontEnd:notification userInfoKey:@"clockID" userInfoValue:@"myid"];
    
    //删除推送列表中的这一条
    [XGPush delLocalNotification:notification];
    //[XGPush delLocalNotification:@"clockID" userInfoValue:@"myid"];
    //清空推送列表
    //[XGPush clearLocalNotifications];
}

-(void)applicationDidFinishLaunching:(UIApplication *)application{
}

#pragma mark--CustomViewDelegate

- (void)customAlertViewBtnWasSeletecd:(CustomAlertView *)alertView withIndex:(NSInteger)index {
    
    if (index == 1) {//点击确定？
        [self pushViewControllerNotifter];
    }
}

-(void)pushViewControllerNotifter{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RefreshNotification object:nil];
    
    /**
     1资讯详情页
     2美图详情页
     3礼包详情页
     
     11资讯列表
     12美图列表
     13礼包列表
     
     20webview
     21外跳链接
     22刷新通知页
     **/
    
    LYTabBarController *mmdrawControl = (LYTabBarController *)[self.window rootViewController];
    UINavigationController *nav = [mmdrawControl.viewControllers objectAtIndex:mmdrawControl.selectedIndex];
    UIViewController *currentViewControl = nav.topViewController;
    
    NSInteger type = [[self.pushDic objectForKey:@"t"] integerValue];
    NSString *value = NSValueToString([self.pushDic objectForKey:@"v"]);
    
    switch (type) {
        case 1: {
            //            InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
            //            detailView.infoID = value;
            if (_isLaunchedByNotification) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:value forKey:@"myValue"];
                [userDefaults synchronize];
            }
            //            [currentViewControl.navigationController pushViewController:detailView animated:YES];
        }
            break;
        case 2: {
            PicDetailViewController *picDetailView = [[PicDetailViewController alloc] initWithNibName:@"PicDetailViewController" bundle:nil];
            picDetailView.imageID = value;
            [currentViewControl.navigationController pushViewController:picDetailView animated:YES];
        }
            break;
        case 3: {
            GiftDetailController *detailView = [[GiftDetailController alloc] initWithNibName:@"GiftDetailController" bundle:nil];
            detailView.giftID = value;
            [currentViewControl.navigationController pushViewController:detailView animated:YES];
        }
            break;
        case 5:{
            //            testViewController *detailView = [[testViewController alloc]init];
            //            detailView.valueId = value;
            
            if (_isLaunchedByNotification) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:value forKey:@"testViewControllerValue"];
                [userDefaults synchronize];
            }
            //            [mmdrawControl presentViewController:detailView animated:YES completion:nil];
        }
            break;
        case 10:{
            
            SaveWatchDownViewController *_SWDVC = [[SaveWatchDownViewController alloc] init];
            NSArray *numbers = [value componentsSeparatedByString:@","];
            
            NSMutableArray *bookInfoArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"isRead":@"1"} andChooseType:@"lastReadTime"];
            _SWDVC.bookDataArr = bookInfoArr;
            
            NSMutableArray *bookCollectionArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"collectionStatus":@"1"} andChooseType:@"collectTime"];
            _SWDVC.collectionArr = bookCollectionArr;
            _SWDVC.rootArr = (NSMutableArray *)numbers;
            [currentViewControl.navigationController presentViewController:_SWDVC animated:YES completion:nil];
            

        }
        case 11: case 12:{
            [mmdrawControl.tabBarView tabWasSelected:(UIButton*)[mmdrawControl.tabBarView viewWithTag:100]];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",type], @"type", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:HomeNotification object:dic];
        }
            break;
        case 13: {
            [mmdrawControl.tabBarView tabWasSelected:(UIButton*)[mmdrawControl.tabBarView viewWithTag:101]];
        }
            break;
        case 20: {
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.url = value;
            [currentViewControl.navigationController pushViewController:webView animated:YES];
        }
            break;
            
        case 21: {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:value]];
        }
            break;
        case 22: {
            [[NSNotificationCenter defaultCenter] postNotificationName:RefreshNotification object:nil];
        }
            break;
            
            
            
        default:
            break;
    }
    
}

- (NSString*)getIdfa {

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *adId = [userDefaults objectForKey:@"adId"];
    if (!adId) {
        adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        [userDefaults setObject:adId forKey:@"adId"];
    }
    return adId;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    
    return  [UMSocialSnsService handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return  [UMSocialSnsService handleOpenURL:url];
}

@end
