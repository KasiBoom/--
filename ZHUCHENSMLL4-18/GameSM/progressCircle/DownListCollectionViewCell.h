//
//  DownListCollectionViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/23.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UAProgressView;
@class cartoonIdModel;
@interface DownListCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageInfo;
@property (weak, nonatomic) IBOutlet UILabel *cartoonName;
@property (weak, nonatomic) IBOutlet UAProgressView *progressView;
@property (nonatomic, copy) cartoonIdModel *cartoonIdModel;
@property (nonatomic, assign) CGFloat progress;
@property (weak, nonatomic) IBOutlet UIView *progressBottomView;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (nonatomic, assign)BOOL isCommicHidden;
@property (weak, nonatomic) IBOutlet UIButton *chooseDelete;
@property(nonatomic,assign)BOOL isSelected;
@property (weak, nonatomic) IBOutlet UIButton *add;


//- (instancetype)initWithFrame:(CGRect)frame WithImageUrlName:(cartoonIdModel *)cartoonIdModel WithPotition:(CGFloat)progressPotition ;
@end
