//
//  LogHelper.m
//  GameSM
//
//  Created by 顾鹏 on 16/1/5.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "LogHelper.h"
#import "NSDate+OTS.h"
#import "AppDelegate.h"
#import "Y_X_DataInterface.h"
#import <objc/runtime.h>
#import "logModel.h"
#import "NSString+GPAditions.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <sys/sysctl.h>
#import "DataBaseHelper.h"
static LogHelper *_LogShare = nil;
@implementation LogHelper
{
    NSString *contentFile;
    logModel *logMode;
    NSString *model;
    NSString *ip;
    NSString *isnew;
    NSString *logStr;
}
+ (LogHelper *)shared
{
    @synchronized(self)
    {
        if(!_LogShare){
            
            _LogShare = [[LogHelper alloc] init];
        }
        return _LogShare;
    }
}

- (id)init
{
    
    if (self = [super init]) {
//        if (![[NSString stringWithFormat:@"%@",[[NSDate date] dayString]] isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:@"LOGTIME"]]) {
//            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[[NSDate date] dayString]] forKey:@"LOGTIME"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//
//        }
        model = [self getCurrentDeviceModel];
        ip = [self getIPAddress];
        
        logMode = [[logModel alloc] init];
        isnew = [self getIsNew];
        
        [self getFilePath];
        
    }
    return self;
}



- (NSString *)getCurrentDeviceModel{
    int mib[2];
    size_t len;
    char *machine;
    
    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);
    
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G (A1203)";
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G (A1241/A1324)";
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS (A1303/A1325)";
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4 (A1332)";
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4 (A1332)";
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4 (A1349)";
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S (A1387/A1431)";
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5 (A1428)";
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5 (A1429/A1442)";
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c (A1456/A1532)";
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c (A1507/A1516/A1526/A1529)";
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s (A1453/A1533)";
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s (A1457/A1518/A1528/A1530)";
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus (A1522/A1524)";
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6 (A1549/A1586)";
    
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G (A1213)";
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G (A1288)";
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G (A1318)";
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G (A1367)";
    if ([platform isEqualToString:@"iPod5,1"])   return @"iPod Touch 5G (A1421/A1509)";
    
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G (A1219/A1337)";
    
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2 (A1395)";
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2 (A1396)";
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2 (A1397)";
    if ([platform isEqualToString:@"iPad2,4"])   return @"iPad 2 (A1395+New Chip)";
    if ([platform isEqualToString:@"iPad2,5"])   return @"iPad Mini 1G (A1432)";
    if ([platform isEqualToString:@"iPad2,6"])   return @"iPad Mini 1G (A1454)";
    if ([platform isEqualToString:@"iPad2,7"])   return @"iPad Mini 1G (A1455)";
    
    if ([platform isEqualToString:@"iPad3,1"])   return @"iPad 3 (A1416)";
    if ([platform isEqualToString:@"iPad3,2"])   return @"iPad 3 (A1403)";
    if ([platform isEqualToString:@"iPad3,3"])   return @"iPad 3 (A1430)";
    if ([platform isEqualToString:@"iPad3,4"])   return @"iPad 4 (A1458)";
    if ([platform isEqualToString:@"iPad3,5"])   return @"iPad 4 (A1459)";
    if ([platform isEqualToString:@"iPad3,6"])   return @"iPad 4 (A1460)";
    
    if ([platform isEqualToString:@"iPad4,1"])   return @"iPad Air (A1474)";
    if ([platform isEqualToString:@"iPad4,2"])   return @"iPad Air (A1475)";
    if ([platform isEqualToString:@"iPad4,3"])   return @"iPad Air (A1476)";
    if ([platform isEqualToString:@"iPad4,4"])   return @"iPad Mini 2G (A1489)";
    if ([platform isEqualToString:@"iPad4,5"])   return @"iPad Mini 2G (A1490)";
    if ([platform isEqualToString:@"iPad4,6"])   return @"iPad Mini 2G (A1491)";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    return platform;
}

- (NSString *)getIsNew {
    NSInteger todatDays = [[NSDate new] distanceNowDays];
    NSLog(@"today%d",todatDays - [[[DataBaseHelper shared] fetchDateTime][0] integerValue]);
    if (todatDays - [[[DataBaseHelper shared] fetchDateTime][0] integerValue] > 1) {
        return @"0";
    }else{
        return @"1";
    }
    
}

- (NSString *)getPath{
    return contentFile;
}

- (NSString *)getFilePath
{
    
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) objectAtIndex:0] stringByAppendingPathComponent:@"log"];
    contentFile = path;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path] == NO)
        
    {
        
//        NSMutableDictionary *mainInfoDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"LOGTIME"]],@"dt",nil];
//        
//        [mainInfoDict addEntriesFromDictionary:[Y_X_DataInterface commonParams]];
//        
//        [[NSString jsonStringWithDictionary:mainInfoDict] writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSString *log = @" ";
        [log writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    return path;
    
}

- (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}




- (void)writeToFilefrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step
                     to_page:(NSString *)to_page
                  to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id{
    NSLog(@"contentFile%@",contentFile);
    logMode.dt = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
    logMode.from_page = from_page;
    logMode.to_page = to_page;
    logMode.from_section = from_section;
    logMode.to_section = to_section;
    logMode.from_step = from_step;
    logMode.to_step = to_step;
    logMode.type = type;
    logMode.id = id;
    logMode.ip = ip;
    logMode.model = model;
    logMode.isnew = isnew;
    logMode.qudao = @"appstore";
    
//    NSMutableString *fileContent = [NSMutableString string];
//    NSString *fileDict = [NSString stringWithContentsOfFile:contentFile encoding:NSUTF8StringEncoding error:nil];
//    
//    [fileContent appendString:fileDict];
//    [fileContent appendString:@"\n"];
    NSMutableDictionary *s = [self entityToDictionary:logMode];
    [s addEntriesFromDictionary:[Y_X_DataInterface commonParams]];
//    [fileContent appendString:[NSString jsonStringWithDictionary:s]];
    NSLog(@"postData%@",[NSString jsonStringWithDictionary:s]);
    
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_LOGO,@"r",
                                    [NSString jsonStringWithDictionary:s],@"postData",
                                    nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(requireLog:) method:POSTDATA];

    
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:@{@"r":API_URL_LOGO,@"postData":[NSString jsonStringWithDictionary:s]} object:self action:@selector(requireLog:) method:POSTDATA];
    
    logStr = [NSString jsonStringWithDictionary:s];
//    [fileContent writeToFile:contentFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (NSString *)writeToApafrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step to_page:(NSString *)to_page                   to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id{
    
    logMode.dt = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
    logMode.from_page = from_page;
    logMode.to_page = to_page;
    logMode.from_section = from_section;
    logMode.to_section = to_section;
    logMode.from_step = from_step;
    logMode.to_step = to_step;
    logMode.type = type;
    logMode.id = id;
    logMode.ip = ip;
    logMode.model = model;
    logMode.isnew = isnew;
    logMode.qudao = @"appstore";

    NSMutableDictionary *s = [self entityToDictionary:logMode];
    [s addEntriesFromDictionary:[Y_X_DataInterface commonParams]];
    NSString *toApaStr = [NSString jsonStringWithDictionary:s];
    return toApaStr;
}



- (void)writeToFilefrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step
                     to_page:(NSString *)to_page
                  to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id detail:(NSDictionary *)detail{
    NSLog(@"contentFile%@",detail);
    logMode.dt = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
    logMode.from_page = from_page;
    logMode.to_page = to_page;
    logMode.from_section = from_section;
    logMode.to_section = to_section;
    logMode.from_step = from_step;
    logMode.to_step = to_step;
    logMode.type = type;
    logMode.id = id;
    logMode.detail = detail;
    logMode.ip = ip;
    logMode.model = model;
    logMode.isnew = isnew;
    logMode.qudao = @"appstore";
//    logMode.detail = @{@"keyword":@"w"};
    //    NSMutableString *fileContent = [NSMutableString string];
//    NSString *fileDict = [NSString stringWithContentsOfFile:contentFile encoding:NSUTF8StringEncoding error:nil];
//    
//    [fileContent appendString:fileDict];
//    [fileContent appendString:@"\n"];
    NSMutableDictionary *s = [self entityToDictionary:logMode];
    [s addEntriesFromDictionary:[Y_X_DataInterface commonParams]];
    NSString *detailSS =[NSString jsonStringWithDictionary:s];
    
//    NSString *beifen = [detailSS mutableCopy];
//    NSInteger detailLength = detailSS.length;
//    NSRange range;
//    range = [detailSS rangeOfString:@"\"detail\":"];
//    range.length = range.location + range.length;
//    range.location = 0;
//    
//    NSMutableString *lastS = [NSMutableString string];
//    NSString *lastfile = [detailSS substringWithRange:range];
//    [lastS appendString:lastfile];
//    
//    [lastS appendString:[beifen substringFromIndex:(range.length + 1)]];
//    NSRange rang1 = [lastS rangeOfString:@"\"}"];
////    rang1.location = lastS.length - 4;
////    rang1.length = 4;
//   NSString *secondS = [lastS substringFromIndex:rang1.location+rang1.length+ 1];
//    NSMutableString *secondSS = [NSMutableString string];
//    NSString *secondFrontS = [lastS substringToIndex:rang1.location+rang1.length];
//    [secondSS appendString:secondFrontS];
//    [secondSS appendString:secondS];
//    
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:@{@"r":API_URL_LOGO,@"postData":detailSS} object:self action:@selector(requireLog:) method:POSTDATA];
    logStr = detailSS;
    
//    [fileContent appendString:secondSS];
    
//    [fileContent writeToFile:contentFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    
}


- (void)requireLog:(NSDictionary *)dict{
    NSLog(@"验证%@",dict);
    if (dict[@"status"] != 0) {
            NSMutableString *fileContent = [NSMutableString string];
            NSString *fileDict = [NSString stringWithContentsOfFile:contentFile encoding:NSUTF8StringEncoding error:nil];
        
            [fileContent appendString:fileDict];
            [fileContent appendString:@"\n"];
            [fileContent appendString:logStr];

            [fileContent writeToFile:contentFile atomically:YES encoding:NSUTF8StringEncoding error:nil];

    }
//    NSMutableString *fileContent = [NSMutableString string];
//    NSString *fileDict = [NSString stringWithContentsOfFile:contentFile encoding:NSUTF8StringEncoding error:nil];
//    
//    [fileContent appendString:fileDict];
//    [fileContent appendString:@"\n"];
//
//    [fileContent writeToFile:contentFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
//
//    if (![dict[@"status"] isEqualToString:@"0"]) {
//        [[LogHelper shared] writeToFilefrom_page:@"null" from_section:@"a" from_step:@"r" to_page:@"ad" to_section:@"a" to_step:@"r" type:@"" id:@"0"];
//    }

}


- (NSString *)writeToApafrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step to_page:(NSString *)to_page                   to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id detail:(NSString *)detail{
    logMode.dt = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
    logMode.from_page = from_page;
    logMode.to_page = to_page;
    logMode.from_section = from_section;
    logMode.to_section = to_section;
    logMode.from_step = from_step;
    logMode.to_step = to_step;
    logMode.type = type;
    logMode.id = id;
    logMode.detail = detail;
    logMode.ip = ip;
    logMode.model = model;
    logMode.isnew = isnew;
    logMode.qudao = @"appstore";
    
    NSMutableDictionary *s = [self entityToDictionary:logMode];
    [s addEntriesFromDictionary:[Y_X_DataInterface commonParams]];
    NSString *detailSS =[NSString jsonStringWithDictionary:s];
    
    NSString *beifen = [detailSS mutableCopy];
    NSRange range;
    range = [detailSS rangeOfString:@"\"detail\":"];
    range.length = range.location + range.length;
    range.location = 0;
    
    NSMutableString *lastS = [NSMutableString string];
    NSString *lastfile = [detailSS substringWithRange:range];
    [lastS appendString:lastfile];
    
    [lastS appendString:[beifen substringFromIndex:(range.length + 1)]];
    NSRange rang1 = [lastS rangeOfString:@"'}"];
    //    rang1.location = lastS.length - 2;
    //    rang1.length = 2;
    NSString *secondS = [lastS substringFromIndex:rang1.location+rang1.length + 1];
    NSMutableString *secondSS = [NSMutableString string];
    NSString *secondFrontS = [lastS substringToIndex:rang1.location+rang1.length];
    [secondSS appendString:secondFrontS];
    [secondSS appendString:secondS];
    
    return secondSS;
    
}


-(NSString*)DataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}



- (NSMutableDictionary *) entityToDictionary:(logModel *)entity
{
    
    Class clazz = [entity class];
    u_int count;
    
    objc_property_t* properties = class_copyPropertyList(clazz, &count);
    NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
    NSMutableArray* valueArray = [NSMutableArray arrayWithCapacity:count];
    
    for (int i = 0; i < count ; i++)
    {
        objc_property_t prop=properties[i];
        const char* propertyName = property_getName(prop);
        
        [propertyArray addObject:[NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
        
        //        const char* attributeName = property_getAttributes(prop);
        //        NSLog(@"%@",[NSString stringWithUTF8String:propertyName]);
        //        NSLog(@"%@",[NSString stringWithUTF8String:attributeName]);
        
        id value =  [entity performSelector:NSSelectorFromString([NSString stringWithUTF8String:propertyName])];
        if(value ==nil)
            [valueArray addObject:[NSNull null]];
        else {
            [valueArray addObject:value];
        }
        //        NSLog(@"%@",value);
    }
    
    free(properties);
    
    NSMutableDictionary* returnDic = [NSMutableDictionary dictionaryWithObjects:valueArray forKeys:propertyArray];
    NSLog(@"%@", returnDic);
    
    return returnDic;
}


- (NSString *)indicateAllLog {
    NSString *fileDict = [NSString stringWithContentsOfFile:contentFile encoding:NSUTF8StringEncoding error:nil];
    return fileDict;

}


@end
