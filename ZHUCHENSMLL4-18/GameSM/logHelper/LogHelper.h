//
//  LogHelper.h
//  GameSM
//
//  Created by 顾鹏 on 16/1/5.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
@class logModel;
@interface LogHelper : NSObject
+ (LogHelper *)shared;
- (NSString *)getPath;
- (void)writeToFilefrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step
                     to_page:(NSString *)to_page
                  to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id;
- (void)writeToFilefrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step
                     to_page:(NSString *)to_page
                  to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id detail:(NSDictionary *)detail;
- (NSString *)writeToApafrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step to_page:(NSString *)to_page                   to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id;
- (NSString *)writeToApafrom_page:(NSString *)from_page from_section:(NSString *)from_section from_step:(NSString *)from_step to_page:(NSString *)to_page                   to_section:(NSString *)to_section to_step:(NSString *)to_step type:(NSString *)type id:(NSString *)id detail:(NSDictionary *)detail;
- (NSString *)indicateAllLog ;
@end
