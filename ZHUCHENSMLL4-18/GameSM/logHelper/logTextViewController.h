//
//  logTextViewController.h
//  GameSM
//
//  Created by 顾鹏 on 16/4/20.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface logTextViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextView *logTextView;
- (IBAction)lastLog:(id)sender;

@end
