//
//  logModel.h
//  GameSM
//
//  Created by 顾鹏 on 16/1/5.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface logSmalllModel:NSObject
@property(nonatomic)NSString *keyword;
@end

@interface logModel : NSObject
@property (nonatomic, copy)NSString *dt;
@property (nonatomic, copy)NSString *from_page;
@property (nonatomic, copy)NSString *to_page;
@property (nonatomic, copy)NSString *from_section;
@property (nonatomic, copy)NSString *to_section;
@property (nonatomic, copy)NSString *from_step;
@property (nonatomic, copy)NSString *to_step;
@property (nonatomic, copy)NSString *type;
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSDictionary *detail;
@property (nonatomic, copy)NSString *ip;
@property (nonatomic, copy)NSString *model;
@property (nonatomic, copy)NSString *isnew;
@property (nonatomic, copy)NSString *qudao;
@property (nonatomic, copy)NSDictionary *detailw;
@end
