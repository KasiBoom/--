//
//  GSDownloadOperation.m
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import "GSDownloadOperation.h"
#import "AFHTTPRequestOperation+RequestParams.h"

@implementation GSDownloadOperation

- (instancetype)initWithURL:(NSString *)url targetPath:(NSString *)path
{
    self = [super init];
    if (self) {
        [self creatRequestWithURL:url targetPath:path];
    }
    return self;
}


- (void)creatRequestWithURL:(NSString *)url targetPath:(NSString *)path
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    self.downloadOperation = [[AFDownloadRequestOperation alloc] initWithRequest:request targetPath:path shouldResume:YES];
    __weak typeof(self) weakSelf = self;
    [self.downloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if ([weakSelf.delegate respondsToSelector:@selector(downloadOperationSuccess:chapterInfo:)]) {
        weakSelf.downOperationSuccess(weakSelf,@{});
//        [weakSelf.delegate downloadOperationSuccess:weakSelf chapterInfo:@{}];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"%@%@%@",operation.cartoonId,operation.chapterId,operation.url);
//        if ([weakSelf.delegate respondsToSelector:@selector(downloadOperationFail:chapterInfo:)]) {
//            [weakSelf.delegate downloadOperationFail:weakSelf chapterInfo:@{@"cartoonId":operation.cartoonId,
//                                                          @"chapterId":operation.chapterId,
//                                                          @"url":operation.url}];
//        }
        weakSelf.downOperationSuccess(weakSelf,@{});
    }];
}












@end
