//
//  GSDownloadManager.h
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString *const kGSDownloadManagerCartoonChange;


@interface GSDownloadManager : NSObject

+ (instancetype)manager;
/**
 *  开始全部队列
 *
 *  @param cartoonId 漫画id
 */
- (void)startDownloadWithCartoonId:(NSString *)cartoonId andDownLoad:(BOOL)isDownLoad;
/**
 *  开启剩余未下载漫画
 *
 *  @param cartoonId
 */
- (void)startLeftDownloadWithCartoonId:(NSString *)cartoonId;
/**
 *  恢复下载
 *
 *  @param cartoonId
 *  @param chapterId
 */
- (void)recoverDownloadWithCartoonId:(NSString *)cartoonId chapterId:(NSString *)chapterId;
/**
 *  暂停当前下载
 *
 *  @param cartoon
 *  @param chapterId 
 */
- (void)pauseDownloadWithCartoonId:(NSString *)cartoon chapterId:(NSString *)chapterId;
@end
