//
//  GSDownloadQueue.m
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import "GSDownloadQueue.h"
#import "NSString+MD5.h"
#import "AFHTTPRequestOperation+RequestParams.h"

@interface GSDownloadQueue ()<GSDownloadOperationDelegate>

@property (nonatomic, strong) NSNumber *recordedRequestId;

@property (nonatomic, strong) NSMutableDictionary *dispatchOperation;

@end

@implementation GSDownloadQueue


/*
    path 某一话的路径
 */
- (void)addOperationWithURLs:(NSArray *)urls target:(NSString *)path cartoonId:(NSString *)cartoonId chapterId:(NSString *)chapterId
{
    for (NSString *url in urls) {
        
        NSString *urlPath = [path stringByAppendingPathComponent:[url md5String]];
        GSDownloadOperation *downloadOperation = [[GSDownloadOperation alloc] initWithURL:url targetPath:urlPath];
        downloadOperation.downloadOperation.cartoonId = cartoonId;
        downloadOperation.downloadOperation.chapterId = chapterId;
        downloadOperation.downloadOperation.url = url;
        downloadOperation.delegate = self;
        [downloadOperation setDownOperationSuccess:^(GSDownloadOperation *downloadOperation, NSDictionary *info) {
            [self.delegate downloadQueueSuccess:self chapterInfo:info];
        }];
        NSNumber *requestId = [self generateRequestId];
        self.dispatchOperation[requestId] = downloadOperation;
        [self.operationQueue addOperation:downloadOperation.downloadOperation];
    }
}

#pragma mark - GSDownloadOperationDelegate
/*
    更新某一部 某一话 的状态
 
 */
- (void)downloadOperationSuccess:(GSDownloadOperation *)downloadOperation chapterInfo:(NSDictionary *)info
{
    NSLog(@"Success");
//    if ([self.delegate respondsToSelector:@selector(downloadQueueSuccess:chapterInfo:)]) {
        [self.delegate downloadQueueSuccess:self chapterInfo:info];
//    }
}

- (void)downloadOperationFail:(GSDownloadOperation *)downloadOperation chapterInfo:(NSDictionary *)info
{
    if ([self.delegate respondsToSelector:@selector(downloadQueueFail:chapterInfo:)]) {
        [self.delegate downloadQueueFail:self chapterInfo:info];
    }
}


#pragma mark - private methods

- (NSNumber *)generateRequestId
{
    if (_recordedRequestId == nil) {
        _recordedRequestId = @(1);
    } else {
        if ([_recordedRequestId integerValue] == NSIntegerMax) {
            _recordedRequestId = @(1);
        } else {
            _recordedRequestId = @([_recordedRequestId integerValue] + 1);
        }
    }
    return _recordedRequestId;
}

#pragma mark - getters

- (NSOperationQueue *)operationQueue
{
    if (_operationQueue == nil) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
    }
    return _operationQueue;
}

- (NSMutableDictionary *)dispatchOperation
{
    if (_dispatchOperation == nil) {
        _dispatchOperation = [[NSMutableDictionary alloc] init];
    }
    return _dispatchOperation;
}


@end
