//
//  GSDownloadQueue.h
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSDownloadOperation.h"

@class GSDownloadQueue;
@protocol GSDownloadQueueDelegate <NSObject>

- (void)downloadQueueSuccess:(GSDownloadQueue *) downloadQueue chapterInfo:(NSDictionary *)info;
- (void)downloadQueueFail:(GSDownloadQueue *) downloadQueue chapterInfo:(NSDictionary *)info;


@end


@interface GSDownloadQueue : NSObject

@property (nonatomic,copy)    NSString *chapterId;

@property (nonatomic, strong) NSOperationQueue *operationQueue;

@property (nonatomic, copy)NSString *cartoonId;

@property (nonatomic, weak) id<GSDownloadQueueDelegate> delegate;

- (void)addOperationWithURLs:(NSArray *)urls target:(NSString *)path cartoonId:(NSString *)cartoonId chapterId:(NSString *)chapterId;


@end
