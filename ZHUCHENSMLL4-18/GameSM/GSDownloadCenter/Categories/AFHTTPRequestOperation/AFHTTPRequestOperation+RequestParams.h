//
//  AFHTTPRequestOperation+RequestParams.h
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import "AFNetworking.h"

@interface AFHTTPRequestOperation (RequestParams)

@property (nonatomic, copy) NSString *cartoonId;

@property (nonatomic, copy) NSString *chapterId;

@property (nonatomic, copy) NSString *url;

@end
