//
//  GiftCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface GiftCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *giftTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *giftBtn;

+ (id)giftCellOwner:(id)owner;

@end
