//
//  CusProgressView.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/13.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CusProgressView.h"

@implementation CusProgressView
- (instancetype)initWithFrame:(CGRect)frame withName:(NSString *)commicName withPercent:(float)commicPercent{
    if (self = [super initWithFrame:frame]) {
        UILabel *commicLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        commicLabel.text = commicName;
        commicLabel.textColor = [UIColor blackColor];
        [self addSubview:commicLabel];
        
        _progressView = [[UIProgressView alloc] init];
        _progressView.frame = CGRectMake(60, 20, 100, 30);
        [self addSubview:_progressView];
        
        _commicPercent = [[UILabel alloc] initWithFrame:CGRectMake(300, 10, 50, 20)];
        _commicPercent.textColor = [UIColor blackColor];
        _commicPercent.font = [UIFont systemFontOfSize:10.0];
        [self addSubview:_commicPercent];
        
    }
    return self;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
