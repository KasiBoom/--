//
//  DownLoadListViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "DownLoadListViewController.h"
#import "Config.h"
#import "cartoonDetailModel.h"
#import "CusProgressView.h"
#import "DataBaseHelper.h"
#import "DownLoadChapteViewController.h"
#import "chapterIdModel.h"
#import "DownListCollectionViewCell.h"
#import "UAProgressView.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface DownLoadListViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
//    UITableView *_tableViewDown;
    NSTimer *time;
    NSArray *_downArr;
    UIProgressView *progressview;
    NSInteger current;
    NSMutableArray *saveChapters;
    NSMutableArray *saveCartoonIdArr;
    NSMutableDictionary *_cellDict;
    BOOL isDeleteCommic;
    NSMutableArray *_deleteArr;
    UIButton            *onOff;
    UIButton            *searchContents;
    NSMutableDictionary *_chooseButtonDict;
    UIView              *bottomView;
    NSMutableArray *_dataSourceArr;
    UIImageView         *_downloadTipImageView;

}
@property (nonatomic,copy)UICollectionView *downCollectionView;
@end

static DownLoadListViewController *DownLoadListVC ;

@implementation DownLoadListViewController

//- (void)viewWillAppear:(BOOL)animated{
//    saveCartoonIdArr = [[DataBaseHelper shared] fetchSaveCartoonId];
//    [_downCollectionView reloadData];
//}
-(void)creatTipImageView{
    _downloadTipImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    _downloadTipImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_downloadTipImageView];
    _downloadTipImageView.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    saveChapters = [[DataBaseHelper shared] fetchpart:@"19" chapterId:@""];
    _cellDict = [NSMutableDictionary dictionary];
    _deleteArr = [NSMutableArray array];
    _chooseButtonDict = [NSMutableDictionary dictionary];
    saveCartoonIdArr = [[DataBaseHelper shared] fetchSaveCartoonId];
    
    _dataSourceArr = [NSMutableArray array];
    for (int i = 0; i < saveCartoonIdArr.count; i ++) {
        [_dataSourceArr addObject:@(0)];
    }

    [self.view addSubview:self.downCollectionView];
    [self addNoti];
//    [self removeNoti];
    [self creatTipImageView];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenSizeHeight - 110, ScreenSizeWidth, 50)];
    bottomView.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    bottomView.backgroundColor = [UIColor clearColor];
    bottomView.hidden = YES;
    [self.view addSubview:bottomView];
    
    onOff = [UIButton buttonWithType:UIButtonTypeCustom];
    onOff.frame = CGRectMake(5, 0, (ScreenSizeWidth - 15)/2, 40);
    onOff.titleLabel.font = [UIFont systemFontOfSize:13.0];
    onOff.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    onOff.layer.cornerRadius = 4.0;
    [onOff setTitle:@"全选" forState:UIControlStateNormal];
    [onOff setTitle:@"取消全选" forState:UIControlStateSelected];
    [onOff setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [onOff addTarget:self action:@selector(wholeOnOff:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:onOff];
    
    
    searchContents = [UIButton buttonWithType:UIButtonTypeCustom];
    searchContents.frame = CGRectMake((ScreenSizeWidth - 15)/2 + 10, 0,(ScreenSizeWidth - 15)/2, 40);
    searchContents.titleLabel.font = [UIFont systemFontOfSize:13.0];
    searchContents.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    searchContents.layer.cornerRadius = 4.0;
    [searchContents setTitle:@"删除" forState:UIControlStateNormal];
    [searchContents setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchContents addTarget:self action:@selector(searchContentBtn:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:searchContents];
    
    if (saveCartoonIdArr.count == 0) {
        _downloadTipImageView.image = [UIImage imageNamed:@"xiazaiTip.png"];
        _downloadTipImageView.hidden = NO;
    }else{
        _downloadTipImageView.hidden = YES;
    }
    
    
}


- (void)wholeOnOff:(UIButton *)onOffBtn {
    onOffBtn.selected = !onOffBtn.selected;
    if (onOffBtn.selected) {
        //        for (UIButton *chooseButton in _chooseButtonDict.allValues) {
        //            chooseButton.selected = YES;
        //            [_deleteArr addObject:@(chooseButton.tag)];
        //        }
        
        [_dataSourceArr removeAllObjects];
        for (int i = 0; i < saveCartoonIdArr.count; i ++) {
            [_dataSourceArr addObject:@(1)];
            [_deleteArr addObject:@(i)];
        }
        [self.downCollectionView reloadData];
        
    }else{
        //        for (UIButton *chooseButton in _chooseButtonDict.allValues) {
        //            chooseButton.selected = NO;
        //            [_deleteArr removeObject:@(chooseButton.tag)];
        //        }
        [_dataSourceArr removeAllObjects];
        for (int i = 0; i < saveCartoonIdArr.count; i ++) {
            [_dataSourceArr addObject:@(0)];
            
        }
        [_deleteArr removeAllObjects];
        [self.downCollectionView reloadData];
        
    }
}

- (void)searchContentBtn:(UIButton *)searchContentBtn{
    searchContentBtn.selected = !searchContentBtn.selected;
    if (searchContentBtn.selected) {
        
        NSMutableArray *indexPaths = [NSMutableArray array];
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
        NSMutableArray *cartoonIdArr = [NSMutableArray array];
        for(int i = 0;i < _deleteArr.count;i ++){
            NSInteger chooseChapInt = [_deleteArr[i] integerValue];
            [indexSet addIndex:[_deleteArr[i] integerValue]];
            [indexPaths addObject:[NSIndexPath indexPathForRow:chooseChapInt inSection:0]];
            
            [cartoonIdArr addObject:[saveCartoonIdArr[chooseChapInt] cartoonId]];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            NSString *MapLayerDataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[saveCartoonIdArr[chooseChapInt] cartoonId]]];
            BOOL bRet = [fileMgr fileExistsAtPath:MapLayerDataPath];
            if (bRet) {
                NSError *err;
                [fileMgr removeItemAtPath:MapLayerDataPath error:&err];
            }
        }
        
        for (int j = 0;j < cartoonIdArr.count ; j ++) {
            [[DataBaseHelper shared] deleteCartoonId:cartoonIdArr[j]];
            [[DataBaseHelper shared] deleteChapterId:cartoonIdArr[j]];
        }
        [saveCartoonIdArr removeObjectsAtIndexes:indexSet];
        [_dataSourceArr removeObjectsAtIndexes:indexSet];

//        [self.downCollectionView reloadData];
        [self.downCollectionView deleteItemsAtIndexPaths:indexPaths];
        [_deleteArr removeAllObjects];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATEDOWNLOAD" object:nil];
    }
}

#pragma mark 初始化
- (UICollectionView *)downCollectionView{
    if (_downCollectionView == nil) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumInteritemSpacing = 5;
        flowLayout.minimumLineSpacing = 5;
        
        _downCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight-64) collectionViewLayout:flowLayout];
        _downCollectionView.delegate = self;
        _downCollectionView.dataSource = self;
        _downCollectionView.backgroundColor = [UIColor whiteColor];
        
        UINib *cellNib = [UINib nibWithNibName:@"DownListCollectionViewCell" bundle:nil];
        [_downCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"simpleCell"];
        
    }
    return _downCollectionView;
}


- (void)removeNoti{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeWhole" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DELETECOMMIC" object:nil];
}

- (void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeWholeProgress:) name:@"changeWhole" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteCommic:) name:@"DELETECOMMIC" object:nil];
}

- (void)deleteCommic:(NSNotification *)noti{
    isDeleteCommic = !isDeleteCommic;
    if(isDeleteCommic){
//        [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:5.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//            bottomView.frame = CGRectMake(0, ScreenSizeHeight - 100, ScreenSizeWidth, 40);
//        } completion:^(BOOL finished) {
//            
//        }];

        bottomView.hidden = NO;
    }else{
//        [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:5.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//            bottomView.frame = CGRectMake(0, ScreenSizeHeight - 60, ScreenSizeWidth, 40);
//        } completion:^(BOOL finished) {
//            
//        }];

        bottomView.hidden = YES;
        
//        for (UIButton *button in _chooseButtonDict.allValues) {
//            [button removeFromSuperview];
//        }
//        
//        [_chooseButtonDict removeAllObjects];

    }
    [self.downCollectionView reloadData];

}


- (void)changeWholeProgress:(NSNotification *)noti{
    NSString *cartoonId = noti.object;
    DownListCollectionViewCell *downCell = _cellDict[cartoonId];
    
    NSArray *cartoonArr = [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:cartoonId];
    NSMutableArray *carStatusArr = [NSMutableArray array];
    for (chapterIdModel *chapterIdModel in cartoonArr) {
        if ([chapterIdModel.status isEqualToString:@"1"]) {
            [carStatusArr addObject:chapterIdModel];
        }
    }
    
    [downCell.progressView setProgress:((float)carStatusArr.count/cartoonArr.count)];
        downCell.progressLabel.text = [NSString stringWithFormat:@"%.f%%",((float)carStatusArr.count/cartoonArr.count) * 100];
    
}


#pragma mark --collectionDelegate,collectionDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return saveCartoonIdArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *identify = @"detailscell";
//    CategoryDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
//    if (!cell) {
//    }
//    cell.bookModel = _bookArr[indexPath.row];
//    NSMutableArray *data = [self. objectAtIndex:indexPath.section];
//    NSString *cellData = [data objectAtIndex:indexPath.row];
    static NSString *cellIdentifier = @"simpleCell";
    DownListCollectionViewCell *downListCollectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];    
    
    downListCollectionCell.cartoonIdModel = saveCartoonIdArr[indexPath.row];
    
    if (isDeleteCommic) {
        //        UIButton *add = [UIButton buttonWithType:UIButtonTypeCustom];
        //        add.frame = CGRectMake(cell.width - 40, 0, 30, 30);
        //        [add setImage:[UIImage imageNamed:@"recipe_normal"] forState:UIControlStateNormal];
        //        [add setImage:[UIImage imageNamed:@"recipe_add"] forState:UIControlStateSelected];
        //        [add addTarget:self action:@selector(addDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        //        add.tag = indexPath.row;
        //        [cell addSubview:add];
        
        //        [_chooseButtonDict setObject:add forKey:@(indexPath.row)];
        
        downListCollectionCell.add.hidden = NO;
        downListCollectionCell.add.selected =  [_dataSourceArr[indexPath.row] boolValue];
    }else{
        downListCollectionCell.add.hidden = YES;
        
    }
    
    return downListCollectionCell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KScreenWidth-24)/3.0, (KScreenWidth-24)/3.0*___Scale+20);//self.view.bounds.size.width
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(4, 4, 4, 4);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (isDeleteCommic) {
        DownListCollectionViewCell *cateCell =  (DownListCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cateCell.isSelected = !cateCell.isSelected;
        
        [_dataSourceArr replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:cateCell.isSelected]];
        
        NSLog(@"cell iSselect%d",cateCell.isSelected);
        if (cateCell.selected) {
            [_deleteArr addObject:@(indexPath.row)];
        }else {
            [_deleteArr removeObject:@(indexPath.row)];
        }
    }else{
        [[LogHelper shared] writeToFilefrom_page:@"cdl" from_section:@"c" from_step:@"l" to_page:@"cdd" to_section:@"c" to_step:@"d" type:@"" id:[saveCartoonIdArr[indexPath.row] cartoonId]];
        
        DownLoadChapteViewController *downChapterVC = [[DownLoadChapteViewController alloc] init];
        downChapterVC.cartoonId = [saveCartoonIdArr[indexPath.row] cartoonId];
        downChapterVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:downChapterVC];
        [self presentViewController:downChapterVC animated:YES completion:nil];
    }
}


//- (void)addDeleteBtn:(UIButton *)addDeleteButton{
//    addDeleteButton.selected = !addDeleteButton.selected;
//    DownListCollectionViewCell *cell = (DownListCollectionViewCell *)addDeleteButton.superview;
//  NSInteger row = [[_downCollectionView indexPathForCell:cell] row];
//    if (addDeleteButton.selected) {
//        [_deleteArr addObject:@(row)];
//    }else {
//        [_deleteArr removeObject:@(row)];
//    }
//}

//- (void)tapCommic:(UITapGestureRecognizer *)tap{
//    
//}
//
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    NSLog(@"======================>?");
//    return saveCartoonIdArr.count;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 100;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
////    cell = [[CusDownTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
////    cell.documentUrl = @"1";
////    cell.progressView =
//    
//    CusDownTableViewCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"CusDownTableViewCell" owner:nil options:nil] lastObject];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    cell.cartoonModel = saveCartoonIdArr[indexPath.row];
//    [_cellDict setObject:cell forKey:[NSString stringWithFormat:@"%@",[saveCartoonIdArr[indexPath.row] cartoonId]]];
//    return cell;
//    
//    
//}
//
//- (void)customeDidFinish{
//    
//}
//
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//        return UITableViewCellEditingStyleDelete;
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return @"删除";
//}
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return YES;
//}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSUInteger row = [indexPath row]; //获取当前行
////    [self.noteListremoveObjectAtIndex:row]; //在数据中删除当前对象
//    [[DataBaseHelper shared] deleteCartoonId:[saveCartoonIdArr[indexPath.row] cartoonId]];
//    [[DataBaseHelper shared] deleteChapterId:[saveCartoonIdArr[indexPath.row] cartoonId]];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//
//    NSFileManager *fileMgr = [NSFileManager defaultManager];
//    NSString *MapLayerDataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[saveCartoonIdArr[indexPath.row] cartoonId]]];
//    BOOL bRet = [fileMgr fileExistsAtPath:MapLayerDataPath];
//    if (bRet) {
//        NSError *err;
//        [fileMgr removeItemAtPath:MapLayerDataPath error:&err];
//    }
//
//    [saveCartoonIdArr removeObjectAtIndex:indexPath.row];
//    
//    
//    
//    [_tableViewDown deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATEDOWNLOAD" object:nil];
//}
//
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    DownLoadChapteViewController *downChapterVC = [[DownLoadChapteViewController alloc] init];
//    downChapterVC.cartoonId = [saveCartoonIdArr[indexPath.row] cartoonId];
//    downChapterVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:downChapterVC];
//    [self presentViewController:nav animated:YES completion:nil];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
