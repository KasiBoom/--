//
//  FaceBagCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "FaceBagCell.h"
#import "FaceBagModel.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface FaceBagCell ()
{
    UIImageView *_imageView;
}
@end

@implementation FaceBagCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
       
    }
    return self;
}
-(void)createView{
    [_imageView removeFromSuperview];
    _imageView = [[UIImageView alloc]init];
    _imageView.frame = CGRectMake(5, 5, 35, 35);
    _imageView.image = [UIImage imageNamed:_model.imageName];
    [self.contentView addSubview:_imageView];
}

-(void)setModel:(FaceBagModel *)model{
    _model = model;
    [self createView];
}

@end
