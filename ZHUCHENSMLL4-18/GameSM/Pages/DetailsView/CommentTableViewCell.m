//
//  CommentTableViewCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5
#define LFCellID @"commentTVC"

#import "CommentTableViewCell.h"
#import "CommentModel.h"
#import "Config.h"

@interface  CommentTableViewCell ()<UITextViewDelegate>
{
    UIImageView     *_iv;
    UILabel         *_nameLabel;
    UILabel         *_timeStatus;
    UILabel         *_contentLabel;
    UIButton        *_btn;
    NSMutableArray  *_pictiueArr;
    NSMutableArray  *_faceNameArr;
    UIView          *_testIv;
    float           _sum;
    UIScrollView    *_contentScrollView;
    BOOL            _OUT;
    UITextView      *_labelFu;
}
@end

@implementation CommentTableViewCell
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


-(instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //        [self createView];
    }
    return self;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    UITextView *tv = object;
    CGFloat bottomCorrent = ([tv bounds].size.height - [tv contentSize].height*[tv zoomScale])/2.0;
    bottomCorrent = (bottomCorrent <0.0 ? 0.0 : bottomCorrent);
    tv.contentOffset = (CGPoint){.x=0,.y=-bottomCorrent};
}

-(void)createView{
    _sum = 0;
    _iv = [[UIImageView alloc]initWithFrame:CGRectMake(TMARGIN, 2*TMARGIN, 36, 36)];
    _iv.layer.masksToBounds = YES;
    _iv.layer.cornerRadius = 18;
    [self.contentView addSubview:_iv];
    
    self.floorLabel = [[UILabel alloc]initWithFrame:CGRectMake(TMARGIN, 50, 36, 20)];
    self.floorLabel.textColor = RGBACOLOR( 154, 205, 208, 1.0);
    self.floorLabel.font = [UIFont systemFontOfSize:12.0];
    self.floorLabel.textAlignment = 1;
    [self.contentView addSubview:self.floorLabel];
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(2*TMARGIN + 40, 2*TMARGIN+2, 200, 14)];
    _nameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];
    _nameLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nameLabel];
    
    _timeStatus = [[UILabel alloc]initWithFrame:CGRectMake(2*TMARGIN + 40, 2*TMARGIN+18, ScreenSizeWidth - 2*TMARGIN + 40, 10)];
    _timeStatus.textColor = [UIColor grayColor];
    _timeStatus.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_timeStatus];
    
    

// UITextView-富文本
    NSString *cententString = _model.content;
    
//    cententString = [cententString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
//    cententString = [cententString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    _labelFu = [[UITextView alloc]initWithFrame:CGRectMake(2*TMARGIN + 40, TMARGIN + 34, KScreenWidth - 2*TMARGIN-50, 20)];
    _labelFu.font = [UIFont boldSystemFontOfSize:16];
    _labelFu.userInteractionEnabled = NO;
    _labelFu.bounces = NO;
    _labelFu.editable = NO;
    [self.contentView addSubview:_labelFu];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 8;// 字体的行间距
//    paragraphStyle.minimumLineHeight = 5;
//    paragraphStyle.alignment = 3;
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
//表情
    _faceNameArr = [NSMutableArray array];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
    }
#warning 正则表达式 "\\[[^\\]]+\\]"--[]------##--"\\#[^\\#]+\\#"
    NSString *regexString = @"\\[[^\\]]+\\]";
    NSMutableAttributedString *attri = [_labelFu.attributedText mutableCopy];//_model.content
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
    NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
    if (arrr.count>0) {
        for (int i =0 ; i < arrr.count; i ++) {
            NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
            NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
            NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
            [attri appendAttributedString:attrStr1];
            NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
            for (int j = 0; j < _faceNameArr.count; j ++) {
                if ([faceStr isEqualToString:_faceNameArr[j]]) {
                    NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                    attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                    attch.bounds = CGRectMake(0, -2, 30, 30);
                    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                    [attri appendAttributedString:string1];
                    break;
                }
            }
            cententString = [cententString substringFromIndex:result.range.location+result.range.length];
        }
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
        [attri appendAttributedString:attrStr2];
        [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attri boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50, MAXFLOAT) options:options context:nil];

        _labelFu.attributedText = [[NSAttributedString alloc] initWithAttributedString:attri];
//        CGSize s = [self sizeOfStr:_labelFu.text andFont:[UIFont systemFontOfSize:16] andMaxSize:CGSizeMake(MAXFLOAT, MAXFLOAT) andLineBreakMode:0];
        _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 34, KScreenWidth - 2*TMARGIN-50, rect.size.height+12);
    }else{
        NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
        NSRange allRange = [cententString rangeOfString:cententString];
        [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
        [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50, MAXFLOAT) options:options context:nil];

        _labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
        int height = (int)rect.size.height/19;
        _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 34, KScreenWidth - 2*TMARGIN-50, height*27+5);
    }
    
    //----赋值
    if (_model.replyUserIDInfo) {
        _nameLabel.text = _model.replyUserIDInfo[@"name"];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"回复:%@  %@", _model.userIDInfo[@"name"],_model.createTimeValue]];
        NSString *nameString = [NSString stringWithFormat:@"%@",_model.userIDInfo[@"name"]];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0] range:NSMakeRange(3,nameString.length+3-1)];
        _timeStatus.attributedText = str;
//        _timeStatus.text = [NSString stringWithFormat:@"回复:%@  %@", _model.userIDInfo[@"name"],_model.createTimeValue];
    }else{
        _nameLabel.text = _model.userIDInfo[@"name"];
        _timeStatus.text = _model.createTimeValue;
    }
    
    
    
    
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(globalQueue, ^{
        __block UIImage *image = nil;
        dispatch_sync(globalQueue, ^{
            if (_model.replyUserIDInfo) {
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model.replyUserIDInfo[@"images"]]]]];
            }else{
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model.userIDInfo[@"images"]]]]];
            }
            
        });
        dispatch_sync(dispatch_get_main_queue(), ^{
            _iv.image = image;
        });
    });
}

- (CGSize)sizeOfStr:(NSString *)str andFont:(UIFont *)font andMaxSize:(CGSize)size andLineBreakMode:(NSLineBreakMode)mode
{
    NSLog(@"版本号:%f",[[[UIDevice currentDevice]systemVersion]doubleValue]);
    CGSize s;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>=7.0) {
        // NSDictionary *dic=@{NSFontAttributeName:font};
        NSMutableDictionary  *mdic=[NSMutableDictionary dictionary];
        [mdic setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
        [mdic setObject:font forKey:NSFontAttributeName];
        s = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine
                           attributes:mdic context:nil].size;//| NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
    }
    else
    {
        s=[str sizeWithFont:font constrainedToSize:size lineBreakMode:mode];
    }
    return s;
}
-(void)setModel:(CommentModel *)model{
    _model = model;
    
    [self createView];
}
-(CGFloat)cellHeight{
    return TMARGIN + 32+_labelFu.bounds.size.height+6;
}
//自定义分割线
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect); //上分割线，
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(20, rect.size.height-0.5, rect.size.width - 40, 0.5));    //    CGContextSetStrokeColorWithColor(context, [UIColor orangeColor].CGColor);
    //    CGContextStrokeRect(context, CGRectMake(5, rect.size.height, rect.size.width - 10, 1));
}

@end
