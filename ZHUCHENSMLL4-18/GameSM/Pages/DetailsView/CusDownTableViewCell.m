//
//  CusDownTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CusDownTableViewCell.h"
#import "cartoonIdModel.h"
#import "UIImageView+AFNetworking.h"
#import "DataBaseHelper.h"
#import "chapterIdModel.h"
#import "GSDownloadManager.h"
@implementation CusDownTableViewCell
{
    NSInteger current;

}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        
    }
    return self;

}

- (void)changeProgress:(NSNotification *)noti{
    [_progressView setProgress:[noti.object floatValue]];
}
//    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
//        NSString *sandboxPath = NSHomeDirectory();
//        NSString *documentPath = [sandboxPath
//                                  stringByAppendingPathComponent:@"Documents"];
//
//       NSString * FileName=[documentPath stringByAppendingPathComponent:@"HELLO"];//fileName就是保存文件的文件名
//
//
//        for (int i = 0; i < 20; i ++) {
//        
//            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.playsm.com/cartoon/9efdf2d448e57d635e54313ec37e5fec_001_00%d.jpg",i]] options:1 progress:^(NSUInteger receivedSize, long long expectedSize) {
//                
//            } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
//                NSLog(@"error%@",error);
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    [data writeToFile:[NSString stringWithFormat:@"%@00%d",FileName,i] atomically:YES];
//                });
//                
//            }];
//
//        }
//        
//    }
//    return self;
//}
- (void)setDocumentUrl:(NSString *)documentUrl{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeProgress:) name:@"CHANGEPROGRESS" object:nil];

}

- (void)setCartoonModel:(cartoonIdModel *)cartoonModel{
    _cartoonModel = cartoonModel;
    [_cartoonImage setImageWithURL:[NSURL URLWithString:cartoonModel.image]];
    _cartoonName.text = cartoonModel.name;
    NSArray *cartoonArr = [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:cartoonModel.cartoonId];
    NSMutableArray *carStatusArr = [NSMutableArray array];
    for (chapterIdModel *chapterIdModel in cartoonArr) {
        if ([chapterIdModel.status isEqualToString:@"1"]) {
            [carStatusArr addObject:chapterIdModel];
            
        }
    }
    if (carStatusArr.count/cartoonArr.count == 1) {
        [_ONOFFButton  setImage:[UIImage imageNamed:@"gouxuan"] forState:UIControlStateNormal];

    }
    [_progressView setProgress:((float)carStatusArr.count/cartoonArr.count)];
    _progressLabel.text = [NSString stringWithFormat:@"%.f%%",((float)carStatusArr.count/cartoonArr.count) * 100];
    
}

- (void)customeDidFail{
    NSLog(@"失败");
}
- (void)customeDidFinish{
    NSLog(@"成功");
}

- (void)awakeFromNib {
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)ONOFFTouch:(id)sender {
//    [[GSDownloadManager manager] startDownloadWithCartoonId:<#(NSString *)#>];
    UIButton *downLoadBtn = (UIButton *)sender;
    downLoadBtn.selected = !downLoadBtn.selected;
    if (downLoadBtn.selected) {
        [[GSDownloadManager manager] startLeftDownloadWithCartoonId:_cartoonModel.cartoonId];
    }else{
        [[GSDownloadManager manager] pauseDownloadWithCartoonId:_cartoonModel.cartoonId chapterId:nil];
    }
    
}
@end
