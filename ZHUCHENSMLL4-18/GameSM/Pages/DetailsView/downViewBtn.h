//
//  downViewBtn.h
//  GameSM
//
//  Created by 顾鹏 on 15/10/22.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface downViewBtn : UIView
@property(nonatomic,copy)NSString *desTitle;
- (void)setDesTitle:(NSString *)desTitle andTarget:(id)target andAction:(SEL)action;
@end
