//
//  DownLoadViewController.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownLoadViewController : UIViewController
@property(nonatomic,copy)NSArray *charpList;
@property(nonatomic,copy)NSArray *allInfo;
@property (weak, nonatomic) IBOutlet UIScrollView *fatherScrollView;
- (IBAction)zhenFanButton:(id)sender;


@end
