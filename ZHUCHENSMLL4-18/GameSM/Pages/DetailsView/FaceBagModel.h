//
//  FaceBagModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FaceBagModel : NSObject

@property (nonatomic, strong) NSString *imageName;

@end
