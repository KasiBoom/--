//
//  testViewController.h
//  GameSM
//
//  Created by 顾鹏 on 15/10/19.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentTableViewController.h"
#import "CommentTableViewCell.h"
#import "BaseViewController.h"

@class DetailsModel;
@class downViewBtn;
@interface testViewController :BaseViewController

@property (nonatomic, strong)NSString *type;
@property (nonatomic, strong)NSString *contentID;

@property (nonatomic,copy)DetailsModel *detailModel;
@property (nonatomic,copy)NSString *detailStr;
@property (nonatomic,copy)NSString *valueId;
@property (nonatomic,copy)NSString *customType;
@property (strong,nonatomic)CommentTableViewController *commentTC;

@property (strong, nonatomic)  UITableView *tableViewUI;
@property (strong, nonatomic)  UIView *topView;
@property (strong, nonatomic)  UIView *bottomView;
@property (nonatomic, strong)   UIControl *touchBgView;
@property (strong, nonatomic)  UIScrollView *SuperScrollView;
@property (strong, nonatomic)  UIView *bgBigView;
@property (strong, nonatomic)  UIView *bgBigView1;
@property (strong, nonatomic)  UIImageView *bgCartoonImageView;
@property (strong, nonatomic)  UIButton *updownButton;
@property (strong, nonatomic)  UIImageView *commicImage;
@property (strong, nonatomic)  UILabel *commicName;
@property (strong, nonatomic)  UILabel *commicAuthor;
@property (strong, nonatomic)  UILabel *updateTime;
@property (strong, nonatomic)  UILabel *updateValueLabel;
@property (strong, nonatomic)  UILabel *contenttLabel;
@property (strong, nonatomic)  UITextView *commicDes;
@property (strong, nonatomic)  UIView *fatherView;
@property (strong, nonatomic)  UIButton *addSav;
@property (strong, nonatomic)  UIView *SuperView;
@property (strong, nonatomic)  UIButton *countinueWatch;
@property (strong, nonatomic)  UIScrollView *commicCounts;
@property (nonatomic,copy)void(^changeCollection)(void);

@property (nonatomic, assign) int chooseIndex;


@end
