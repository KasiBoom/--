//
//  DownLoadChapteViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "DownLoadChapteViewController.h"
#import "Config.h"
#import "DataBaseHelper.h"
#import "CusDownChapterTableViewCell.h"
#import "chapterIdModel.h"
#import "SecondReadController.h"
#import "Y_X_DataInterface.h"
#import "testViewController.h"
#import "GSDownloadManager.h"
#import "cartoonIdModel.h"
#import "CustomTool.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface DownLoadChapteViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView         *_tableViewDown;
    NSMutableArray      *_chapterArr;
    NSMutableDictionary *_CellDict;
    NSMutableArray      *_chooseDeleteArr;
    UIButton            *downLoadBtn;
    NSMutableArray      *_indexChooseArr;
    UILabel             *_titleName;
    UIButton            *onOff;
    BOOL                _isStart;
    UIButton            *searchContents;
    UIButton            *chooseAll;
    NSMutableArray      *_selectedButtonArr;
    NSArray             *_chapterIdList;
    UIView *view;
}

@end

@implementation DownLoadChapteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    view = [CustomTool createNavView];
    [self.view addSubview:view];
    _titleName = [[UILabel alloc]init];
    //    _titleName.center = CGPointMake(ScreenSizeWidth/2, 10);
    //    _titleName.size = CGSizeMake(200, 50);
    _titleName.frame = CGRectMake(60, 30, KScreenWidth-120, 30);
    _titleName.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    _titleName.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    _titleName.textAlignment = NSTextAlignmentCenter;
    _titleName.text = [[[DataBaseHelper shared] fetchSingleCartoonId:_cartoonId] name];
    [view addSubview:_titleName];

    
    [self Nav];
    [self addNoti];
    // Do any additional setup after loading the view from its nib.
    _CellDict = [NSMutableDictionary dictionary];
    _chooseDeleteArr = [NSMutableArray array];
    _indexChooseArr = [[NSMutableArray alloc] init];
    _tableViewDown = [[UITableView alloc] initWithFrame:CGRectMake(0, 64,ScreenSizeWidth , ScreenSizeHeight - 40 - 64) style:UITableViewStylePlain];
    _tableViewDown.delegate = self;
    _tableViewDown.dataSource = self;
    [self.view addSubview:_tableViewDown];
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenSizeHeight - 40, ScreenSizeWidth, 40)];
    bottomView.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    [self.view addSubview:bottomView];
    
    onOff.frame = CGRectMake(0, 0, ScreenSizeWidth/3, 40);
    onOff.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [onOff setTitle:@"全部开始" forState:UIControlStateNormal];
    [onOff setTitle:@"全部停止" forState:UIControlStateSelected];
    [onOff setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [onOff addTarget:self action:@selector(wholeOnOff:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:onOff];
    
    
    searchContents = [UIButton buttonWithType:UIButtonTypeCustom];
    searchContents.frame = CGRectMake(2 * ScreenSizeWidth/3, 0, ScreenSizeWidth/3, 40);
    searchContents.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [searchContents setTitle:@"查看目录" forState:UIControlStateNormal];
    [searchContents setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchContents addTarget:self action:@selector(searchContentBtn:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:searchContents];
    
    chooseAll = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseAll.frame = CGRectMake(2 * ScreenSizeWidth/3, 0, ScreenSizeWidth/3, 40);
    chooseAll.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [chooseAll setTitle:@"全部选择" forState:UIControlStateNormal];
    [chooseAll setTitle:@"取消选择" forState:UIControlStateSelected];
    [chooseAll addTarget:self action:@selector(chooseAll:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:chooseAll];
    chooseAll.hidden = YES;
    
    
    
//---分割字符串未漫画所有id
    cartoonIdModel *idModel = [[cartoonIdModel alloc]init];
    idModel = [[DataBaseHelper shared] fetchSaveCartoonId][0];
    NSString *idallstr = idModel.idAllStr;
    _chapterIdList = [idallstr componentsSeparatedByString:@","];
}

- (void)chooseAll:(UIButton *)chooseAllBtn{
    chooseAllBtn.selected = !chooseAllBtn.selected;
//    downLoadBtn.selected = !downLoadBtn.selected;
    if(chooseAllBtn.selected){
        [downLoadBtn setImage:[UIImage imageNamed:@"shanchu"] forState:UIControlStateSelected];}else{
            [downLoadBtn setImage:[UIImage imageNamed:@"quxiao"] forState:UIControlStateSelected];
        }
    NSArray *visiableCells = [_tableViewDown visibleCells];
    for (int j = 0; j < visiableCells.count; j ++) {
        CusDownChapterTableViewCell *cell = visiableCells[j];
        cell.chooseDelete.selected = chooseAllBtn.selected;
    }
    for (int i = 0; i < _chapterArr.count; i ++) {
        [_selectedButtonArr replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:chooseAllBtn.selected]];
    }
    
    
//    for (CusDownChapterTableViewCell * cell in _CellDict.allValues) {
//        cell.chooseDelete.selected = !cell.chooseDelete.selected;
//        cell.isCellSelected = YES;
//        [_chooseDeleteArr addObject:cell];
//        [_selectedButtonArr replaceObjectAtIndex:<#(NSUInteger)#> withObject:<#(nonnull id)#>]
//    }
}

- (void)wholeOnOff:(UIButton *)wholeButton{
    wholeButton.selected = !wholeButton.selected;
    if (wholeButton.selected) {
        [[GSDownloadManager manager] startLeftDownloadWithCartoonId:_cartoonId];
        downLoadBtn.hidden = YES;
    }else{
        [[GSDownloadManager manager] pauseDownloadWithCartoonId:_cartoonId chapterId:nil];
        downLoadBtn.hidden = NO;
    }
}



- (void)searchContentBtn:(UIButton *)searchContentBtn{
    testViewController *vc = [[testViewController alloc]init];
    vc.detailStr = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_cartoonId],
    [vc setChangeCollection:^{
        
    }];
    
    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(void)misBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCellProgress:) name:@"changeCellProgress" object:nil];
}

- (void)changeCellProgress:(NSNotification *)noti{
    
//    static dispatch_once_t onceToken;
//
//    dispatch_once(&onceToken, ^{
        onOff.selected = YES;
//    });
    
    downLoadBtn.hidden = YES;
    if(_CellDict.count > 0){
        
        CusDownChapterTableViewCell *cell = _CellDict[noti.object[@"tag"]];
        
        if(_chapterArr.count == [_tableViewDown indexPathForCell:cell].row + 1){
            onOff.hidden = YES;
            downLoadBtn.hidden = NO;
        }
            
        [cell.chapterProgress setProgress:[noti.object[@"proprtion"] floatValue]];
        cell.chapterProtion.text = [NSString stringWithFormat:@"%.f%%",[noti.object[@"proprtion"] floatValue]*100];
        cell.ONOFFButton.selected = YES;
        if([noti.object[@"proprtion"] floatValue] == 1.0){
            [cell.ONOFFButton setImage:[UIImage imageNamed:@"gouxuan"] forState:UIControlStateSelected];
            cell.ONOFFButton.userInteractionEnabled = NO;
            _chapterArr = [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:_cartoonId];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeWhole" object:_cartoonId];
        }
    }
}

- (void)Nav{
//    UINavigationBar *bar = self.navigationController.navigationBar;
//    bar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
//    
//    
//    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    backButton.frame = CGRectMake(0, 0, 33, 33);
//    [backButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backBtn;
//    
//    
//    downLoadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    downLoadBtn.frame = CGRectMake(0, 0, 66, 26);
//    [downLoadBtn setImage:[UIImage imageNamed:@"bianji"] forState:UIControlStateNormal];
//    [downLoadBtn setImage:[UIImage imageNamed:@"quxiaobianji"] forState:UIControlStateSelected];
//    [downLoadBtn addTarget:self action:@selector(bianjiViewControl:) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *downLoadBtnItem = [[UIBarButtonItem alloc]initWithCustomView:downLoadBtn];
//    self.navigationItem.rightBarButtonItem = downLoadBtnItem;
//
//    UINavigationItem *item = self.navigationItem;
//    item.titleView = _titleName;
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 25, 33, 33);
    [backButton setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    
    _titleName = [[UILabel alloc]initWithFrame:CGRectMake(60, 30, KScreenWidth-120, 30)];
    _titleName.center = CGPointMake(ScreenSizeWidth/2, 45);
    _titleName.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    _titleName.textColor = [UIColor whiteColor];
    _titleName.textAlignment = NSTextAlignmentCenter;
    [view addSubview:_titleName];
    
    downLoadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    downLoadBtn.frame = CGRectMake(KScreenWidth - 80, 30, 66, 26);
    [downLoadBtn setImage:[UIImage imageNamed:@"bianji"] forState:UIControlStateNormal];
    [downLoadBtn setImage:[UIImage imageNamed:@"quxiao"] forState:UIControlStateSelected];
    [downLoadBtn addTarget:self action:@selector(bianjiViewControl:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:downLoadBtn];
    
}

- (void)bianjiViewControl:(UIButton *)bianjiVC{
    bianjiVC.selected = !bianjiVC.selected;
    
//    onOff.hidden = !onOff.hidden;
    searchContents.hidden = !searchContents.hidden;
    chooseAll.hidden = !chooseAll.hidden;
    
    NSMutableArray *chooseChap = [NSMutableArray array];
    for(int i = 0;i < _selectedButtonArr.count;i ++){
        if ([_selectedButtonArr[i] integerValue] == 1) {
            [chooseChap addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    if (bianjiVC.selected) {
        
        for (CusDownChapterTableViewCell * cell in _CellDict.allValues) {
        
//        [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            cell.chooseDelete.hidden = !bianjiVC.selected;
//            cell.contentView.frame = CGRectMake(30, 0, cell.frame.size.width, cell.frame.size.height);
            
//            } completion:nil];
        }
    }else if(chooseChap.count > 0){
//            NSInteger chapterCount = chooseChap.count;
            NSMutableArray *indexPaths = [NSMutableArray array];
            NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
            for(int i = 0;i < chooseChap.count;i ++){
                NSInteger chooseChapInt = [chooseChap[i] integerValue];
                [indexSet addIndex:[chooseChap[i] integerValue]];
                [indexPaths addObject:[NSIndexPath indexPathForRow:chooseChapInt inSection:0]];
                
                [[DataBaseHelper shared] deleteSingleChapterId:[_chapterArr[chooseChapInt] chapterId]];
                
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            NSString *MapLayerDataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",_cartoonId]];
            NSString *lastPath = [MapLayerDataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[_chapterArr[chooseChapInt] chapterId]]];
            BOOL bRet = [fileMgr fileExistsAtPath:lastPath];
            if (bRet) {
                NSError *err;
                [fileMgr removeItemAtPath:lastPath error:&err];
              }
            }
            [_chapterArr removeObjectsAtIndexes:indexSet];
            
            [_tableViewDown deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//            [_indexChooseArr removeAllObjects];
//            [_chooseDeleteArr removeAllObjects];
        
            [_selectedButtonArr removeObjectsAtIndexes:indexSet];
        
            if(_chapterArr.count == 0){
                [[DataBaseHelper shared] deleteCartoonId:_cartoonId];
            }
            
//            for (CusDownChapterTableViewCell * cell in _CellDict.allValues) {
//                
////                [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//                    cell.chooseDelete.hidden = !bianjiVC.selected;
////                    cell.contentView.frame = CGRectMake(30, 0, cell.frame.size.width, cell.frame.size.height);
//                
////                } completion:nil];
//                
//            }
        [_tableViewDown reloadData];
        
        [downLoadBtn setImage:[UIImage imageNamed:@"quxiao"] forState:UIControlStateSelected];
        
        
        }else if (_chooseDeleteArr.count == 0){
            for (CusDownChapterTableViewCell * cell in _CellDict.allValues) {
                
//                [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    cell.chooseDelete.hidden = !bianjiVC.selected;
//                    cell.contentView.frame = CGRectMake(35, 0, cell.frame.size.width, cell.frame.size.height);
                
//                } completion:nil];
                
            }
        }
    
}

- (void)setCartoonId:(NSString *)cartoonId{
    _cartoonId = cartoonId;
    _selectedButtonArr = [NSMutableArray array];
    _chapterArr = [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:cartoonId];
    for (int i = 0; i < _chapterArr.count; i ++) {
        [_selectedButtonArr addObject:[NSNumber numberWithBool:NO]];
    }
    
    
    
    onOff = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[DataBaseHelper shared] fetchSingleIsSaveExist:_cartoonId]) {
        onOff.hidden = YES;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"======================>?");
    return _chapterArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CusDownChapterTableViewCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"CusDownChapterTableViewCell" owner:nil options:nil] lastObject];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.chapIdModel = _chapterArr[indexPath.row];
    [cell.chooseDelete addTarget:self action:@selector(chooseDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"selected%d",cell.isSelected);
    cell.chooseDelete.selected = [_selectedButtonArr[indexPath.row] integerValue];
    if (downLoadBtn.selected) {
        cell.chooseDelete.hidden = NO;
    }else {
        cell.chooseDelete.hidden = YES;
    }
    
    [_CellDict setObject:cell forKey:[NSString stringWithFormat:@"%@",[_chapterArr[indexPath.row] chapterId]]];
    
    return cell;
    
    
}

- (void)chooseDeleteBtn:(UIButton *)chooseDeleteBtn{
    
    CusDownChapterTableViewCell *cell = (CusDownChapterTableViewCell *)[[chooseDeleteBtn superview] superview] ;
    NSNumber * addIndex =   @([[_tableViewDown indexPathForCell:cell] row]);
    chooseDeleteBtn.selected = !chooseDeleteBtn.selected;
//    cell.isCellSelected = chooseDeleteBtn.selected;
    [_selectedButtonArr replaceObjectAtIndex:[addIndex integerValue] withObject:[NSNumber numberWithBool:chooseDeleteBtn.selected]];
    
    if ([_selectedButtonArr containsObject:[NSNumber numberWithBool:YES]] ) {
        [downLoadBtn setImage:[UIImage imageNamed:@"shanchu"] forState:UIControlStateSelected];
    }else{
        [downLoadBtn setImage:[UIImage imageNamed:@"quxiao"] forState:UIControlStateSelected];
        
    }

    NSLog(@"chooseDelete.s%d",chooseDeleteBtn.selected);
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CONTINUEREADINRECOMMENT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if(!downLoadBtn.selected){
    CusDownChapterTableViewCell *cusDownChapterTable = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cusDownChapterTable.chapterProgress.progress == 1.0) {
        [[LogHelper shared] writeToFilefrom_page:@"cdd" from_section:@"c" from_step:@"d" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"" id:[_chapterArr[indexPath.row] chapterId] ];
        
        
        NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,[_chapterArr[indexPath.row] chapterId]];
        SecondReadController *readC = [[SecondReadController alloc]init];
        
        NSArray *urlArr = [str componentsSeparatedByString:@"&"];
        NSString *chapterStr = [urlArr[1] substringFromIndex:10];
        NSArray *dataArr = [[DataBaseHelper shared] fetchAlbumsWithChapterId:chapterStr];
        [readC setChangeMark:^(NSInteger tagMark) {
            
        }];
        
        readC.charpList1 = _chapterIdList;
        readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:readC animated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:str];
        }];
    }}
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row]; //获取当前行
    //    [self.noteListremoveObjectAtIndex:row]; //在数据中删除当前对象

    [[DataBaseHelper shared] deleteSingleChapterId:[_chapterArr[indexPath.row] chapterId]];
    [[LogHelper shared] writeToFilefrom_page:@"cdd" from_section:@"c" from_step:@"d" to_page:@"cdde" to_section:@"c" to_step:@"a" type:@"'" id:[_chapterArr[indexPath.row] chapterId]];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *MapLayerDataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[_chapterArr[indexPath.row] cartoonId]]];
    NSString *lastPath = [MapLayerDataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[_chapterArr[indexPath.row] chapterId]]];
    BOOL bRet = [fileMgr fileExistsAtPath:lastPath];
    if (bRet) {
        NSError *err;
        [fileMgr removeItemAtPath:lastPath error:&err];
    }
    
    [_chapterArr removeObjectAtIndex:indexPath.row];
    
    [_tableViewDown deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
