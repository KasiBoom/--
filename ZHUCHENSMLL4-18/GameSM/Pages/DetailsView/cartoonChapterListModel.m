//
//  cartoonChapterListModel.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/2.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "cartoonChapterListModel.h"
#import "DataBaseHelper.h"
@implementation cartoonChapterListModel
+ (NSMutableArray *)parasArr:(NSArray *)CartoonArr {
    NSMutableArray *carMusArr = [NSMutableArray array];
    for (NSDictionary *chaplistDict in CartoonArr) {
        cartoonChapterListModel *charModel = [[cartoonChapterListModel alloc] init];
        charModel.id = chaplistDict[@"id"];
        charModel.name = chaplistDict[@"name"];
        charModel.cartoonName = chaplistDict[@"cartoonName"];
        charModel.cartoonId = chaplistDict[@"cartoonId"];
        charModel.priority = chaplistDict[@"priority"];
        charModel.status = chaplistDict[@"status"];
        charModel.downloadStatus = chaplistDict[@"downloadStatus"];
        charModel.readStatus = chaplistDict[@"readStatus"];
        charModel.totalChapterSize = chaplistDict[@"totalChapterSize"];
        [carMusArr addObject:charModel];
        [[DataBaseHelper shared] insertChapList:charModel];
    }
    return carMusArr;
    
}
@end
