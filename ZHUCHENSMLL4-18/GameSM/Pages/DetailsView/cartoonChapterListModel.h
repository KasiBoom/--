//
//  cartoonChapterListModel.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/2.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cartoonChapterListModel : NSObject
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *cartoonId;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *cartoonName;
@property(nonatomic,copy)NSString *priority;
@property(nonatomic,copy)NSString *status;
@property(nonatomic,copy)NSString *downloadStatus;
@property(nonatomic,copy)NSString *readStatus;
@property(nonatomic,copy)NSString *totalChapterSize;
+ (NSMutableArray *)parasArr:(NSArray *)CartoonArr;

@end
