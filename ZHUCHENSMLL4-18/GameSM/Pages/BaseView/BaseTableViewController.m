//
//  BaseTableViewController.m
//  bang
//
//  Created by 王涛 on 15/6/5.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "BaseTableViewController.h"
#import "Config.h"
#import "ConstantViewController.h"
@interface BaseTableViewController ()<UIActionSheetDelegate>
{
    UITextView *_hiddenText;
}
@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    page = 1;
    size = 10;
    
    
    
//    [UIApplication sharedApplication].applicationSupportsShakeToEdit = YES;
////     并让自己成为第一响应者
//    [self becomeFirstResponder];
    
//    _hiddenText = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
//    [self.view addSubview:_hiddenText];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - PullingRefreshTableViewDelegate

-(void)refreshData
{
    [self loadData:NO];
}

-(void)loadMoreData
{
    [self loadData:YES];
}

-(void)loadData:(BOOL)isOrmore{
    
}

- (void)pullingTableViewDidStartRefreshing:(PullingRefreshTableView *)tableView
{
    [self performSelector:@selector(refreshData) withObject:nil afterDelay:0.2f];
    
}

- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView
{
    [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.2f];
}

- (NSDate *)pullingTableViewRefreshingFinishedDate
{
    return [NSDate date];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
        [_tableView tableViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
        [_tableView tableViewDidEndDragging:scrollView];
    }
}

#pragma mark - getter & setter
- (PullingRefreshTableView*)tableView {
    if (!_tableView) {
        _tableView = [[PullingRefreshTableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64) pullingDelegate:self];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = [UIColor colorWithRed:207/255.0 green:207/255.0 blue:207/255.0 alpha:1];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.backgroundView = nil;
        _tableView.scrollsToTop = YES;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSMutableArray*)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

#pragma mark - 摇一摇相关方法
// 摇一摇开始摇动
//- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
//    NSLog(@"开始摇动");
//    return;
//}
//
//// 摇一摇取消摇动
//- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event {
//    NSLog(@"取消摇动");
//    return;
//}
//
//// 摇一摇摇动结束
//- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
//    if (event.subtype == UIEventSubtypeMotionShake) { // 判断是否是摇动结束
//        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"意见反馈", nil];
//        [actionSheet showInView:self.view];
//    }
//    return;
//}
//

//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(buttonIndex == 0){
//    ConstantViewController *constantVC = [[ConstantViewController alloc] init];
//        [self presentViewController:constantVC animated:YES completion:nil];
//    }
//}
@end
