//
//  BaseTableViewCell.m
//  bang
//
//  Created by 王涛 on 15/5/25.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "Config.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.size.width = ScreenSizeWidth;
    [super setFrame:frame];
}

@end
