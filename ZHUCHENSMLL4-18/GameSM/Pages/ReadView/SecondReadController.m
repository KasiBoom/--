//
//  SecondReadController.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/28.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "SecondReadController.h"
#import "MBProgressHUD.h"
//#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "AFNetworking.h"
#import "PullingRefreshTableView.h"
#import "Y_X_DataInterface.h"
#import "cartoonChapterListModel.h"
#import "FeedbackViewController.h"
#import "MBProgressHUD.h"
#import "Config.h"
#import "cartoonDetailModel.h"
#import "DataBaseHelper.h"
#import "NSString+MD5.h"
#import "CustomTool.h"
#import "SSTabBarItem.h"
#import "testViewController.h"
#import "POPSpringAnimation.h"
#import "CartoonInfoAll.h"
#import "PullPsCollectionView.h"
#import "PullingRefreshTableView.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
#import "GridLayout.h"
#import "GPCollectionCell.h"
#import "secondReadView.h"
#import <CoreLocation/CoreLocation.h>
#import "UMSocial.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface SecondReadController ()<UIScrollViewDelegate,
                                  UITableViewDataSource,
                                    UITableViewDelegate,
                                    UIAlertViewDelegate,
                            UIGestureRecognizerDelegate,
                     UICollectionViewDelegateFlowLayout,
                        PullingRefreshTableViewDelegate,
                               UICollectionViewDelegate,
                             UICollectionViewDataSource,
                                 LoginAlertViewDelegate
                           >
{
    CartoonInfoAll  *_cartoonAllInfo;
    UIImageView     *_netImageView;
    UIButton        *_refreshBtn0;
    UIButton        *_refreshBtn1;
    UIButton        *_refreshBtn2;
    NSString *_urlStr;
    NSMutableArray *_dataArr;
    NSMutableArray *_dataArrModel;
    NSMutableArray *_currentArray;
//    UICollectionView *_scrollView;
    UIButton *_setBtn;
    NSInteger markCount;
    NSInteger sliderValue;
    NSString        *_nextChapterId;
    NSString        *_lastChapterId;
    NSString        *_nowCartoonId;
    NSString        *_loadMoreUrl;
//    NSString        *_downloadId;
    
    UISlider        *_pageNumControl;
    UISlider        *_lightSlider;
    UILabel         *_pageNumLabel;
    UILabel         *_helpLabel;
    UILabel         *_nameLabel;
    UILabel         *_waitLabel;
    UILabel         *_statusLabel;
    
    UIView          *_tagView;
    UIView          *_OverView;
    UIView          *_topView;
    UIView          *_bottonView;
    UIView          *_rightView;
    UIView          *_leftView;
    UIView          *_selectView;
    UIView          *_helpView;
    UIView          *_helpTransverseView;
    UIImageView     *_imageView;
    UIView          *_bagV;
    UIButton        *_rightViewBtn;
    UIScrollView    *_selectScrollView;
    UILabel         *_timeLabel;
    UIScrollView    *fatherScr;
    UILabel         *_pagesMark;

    BOOL             _isAppear;
    BOOL             _isChoose;
    BOOL             _isSun;
    BOOL             _isDownLoad;
    BOOL             _SUN;
    BOOL             _isTouch;
    BOOL             _killScr;
    BOOL             _zoomInOrOut;
    
    
    float _lightNum;
    float _ii;
    
    NSMutableDictionary    *_extraInfoDict;
    NSMutableDictionary    *_downInfoDict;
    NSMutableDictionary    *_upInfoDict;
    NSTimer                *_timer;
    NSInteger              _markCountPage;
    NSArray        *_imageBtnArr;
    NSArray        *_nameBtnArr;
    
    NSArray        *_imageRightBtn;
    NSArray        *_nameRightBtn;
    
    
    cartoonDetailModel *_cartoonChapModel;
    
    
    CGFloat lastScale;
    CGFloat _newY;
    CGFloat _oldY;
    
    CGPoint _beginPoint;
    
    CGFloat mixScale;
    CGFloat maxScale;
    GridLayout *Layout;
    BOOL             _isTransverse;
    BOOL             _isBigStatus;
    NSIndexPath      *_jumpIndex;
    BOOL             _isLandscape;
    CGFloat         _jump;
    UILabel         *_leftLabel;
    UIControl       *_rightControl;
    UIControl       *_leftControl;
    BOOL            _isLeftRight;
    UIPinchGestureRecognizer *pinGes;
    
    NSInteger _pageMemoryNum;
    
    UIImageView *_selectBgImageView;
    BOOL            _showNoMore;
    
    BOOL        _isAssignment;
    BOOL        _isZoomIn;
    BOOL        _isZoomOut;
    
    CGFloat     _isAssignmentNum;
    CGFloat     _currentScale;
    CGFloat     _lastScale;
    CGFloat     _scale;
    CGFloat     _initScale;
    CGFloat     _preOneScale;
    CGFloat     _recordScale;
    BOOL        _isZero;
    BOOL        _intNum;
    CGPoint     _doublePoint;
    //2.7
    NSString    *_shareUrl;
    NSString    *_titleStr;
    NSString    *_shareImage;
}
@end

@implementation SecondReadController

- (instancetype)init{
    if(self = [super init]){
        _charpList = [[NSArray alloc] init];
    }
    return self;
}
//摇一摇
//-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    _isTouch = YES;
    NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
    UITouch *touch = [allTouches anyObject];   //视图中的所有对象
    _doublePoint = [touch locationInView:self.view.window]; //返回触摸点在视图中的当前坐标
    NSLog(@"----%f----%f",_doublePoint.x,_doublePoint.y);
    
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    _isTouch = NO;
    NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
    UITouch *touch = [allTouches anyObject];   //视图中的所有对象
    CGPoint touchPoint = [touch locationInView:self.view.window]; //返回触摸点在视图中的当前坐标
    if (touchPoint.y>64) {
        if (_isAppear) {
            [self misSet];
        }
    }
}
-(void)singleTap:(UIEvent *)event{
    if (_isTouch) {
        NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
        UITouch *touch = [allTouches anyObject];   //视图中的所有对象
        CGPoint touchPoint = [touch locationInView:self.view];
        if (_isAppear) {
            if (touchPoint.x < 0.1 && touchPoint.y < 0.1) {
                [self misSet];
            }
        }
        
        if (!_isLandscape) {
            if (_isTransverse) {
                if (touchPoint.y>64) {
                    if (touchPoint.x<100 && touchPoint.x>1) {
                        [self leftControlClick];
                    }else if (touchPoint.x>ScreenSizeWidth-100){
                        
                        [self rightControlClick];
                    }else{
                        [self memoryData];
                        [self misSet];
                    }
                }
                
            }else{
                if (touchPoint.y>64) {
                    if (touchPoint.x<100) {
                        if (_isAppear) {
                            if (touchPoint.y<120) {
                                [self misRead];
                                return;
                            }else{
                                [self misSet];
                            }
                        }
                        CGFloat y = _scrollView.contentOffset.y;
                        y -=KScreenheight;
                        [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y) animated:YES];
                        if (y < 0){
                            [self loadMoreData];
                        }
                    }else if (touchPoint.x>ScreenSizeWidth-100){
                        CGFloat y = _scrollView.contentOffset.y;
                        y +=KScreenheight;
                        [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y) animated:YES];
                        
                        CGSize size1 = _scrollView.contentSize;
                        if (y>=size1.height - KScreenWidth) {
                            [self loadMoreNextData];
                        }
                        if (_isAppear) {
                            [self misSet];
                        }
                    }else{
                        [self memoryData];
                        [self misSet];
                    }
                }
            }
        }else{
            if ((touchPoint.x > KScreenWidth - 150 && touchPoint.y < 150)|| (touchPoint.x > KScreenWidth - 150 && touchPoint.y > KScreenheight - 150)) {
                CGFloat y = _scrollView.contentOffset.y;
                y -=KScreenWidth;
                [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y) animated:YES];
                if (y < 0){
                    [self loadMoreData];
                }
                if (_isAppear) {
                    [self misSet];
                }
                
            }else if ((touchPoint.x < 150 && touchPoint.y < 150)|| (touchPoint.x < 150 && touchPoint.y > KScreenheight - 150)) {
                CGFloat y = _scrollView.contentOffset.y;
                y +=KScreenWidth;
                [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y) animated:YES];
                
                CGSize size1 = _scrollView.contentSize;
                if (y>=size1.height - KScreenWidth) {
                    [self loadMoreNextData];
                }
                if (_isAppear) {
                    [self misSet];
                }
            }else{
                [self memoryData];
                [self misSet];
            }
        }
    }

}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    _isZero = NO;
    if (_isTouch) {
//        if (_isTransverse) {
//            
////            [NSObject cancelPreviousPerformRequestsWithTarget:self];
//            NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
//            UITouch *touch = [allTouches anyObject];   //视图中的所有对象
//            CGPoint touchPoint;
//            touchPoint = [touch locationInView:self.view.window];
//            
//            //返回触摸点在视图中的当前坐标
//            //        NSLog(@"===%f--%f",touchPoint.x,touchPoint.y);
//            
//            if (!_isLandscape) {
//                if (_isTransverse) {
//                    if (touchPoint.y>64) {
//                        if (touchPoint.x<100 ) {
//                            [self leftControlClick];
//                        }else if (touchPoint.x>ScreenSizeWidth-100){
////                            CGFloat x = _scrollView.contentOffset.x;
////                            x +=KScreenWidth;
////                            [_scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
////                            
////                            CGSize size1 = _scrollView.contentSize;
////                            if (x>=size1.width - KScreenWidth) {
////                                [self loadMoreNextData];
////                            }
////                            if (_isAppear) {
////                                [self misSet];
////                            }
//                            [self rightControlClick];
//                        }else{
//                            [self memoryData];
//                            [self misSet];
//                        }
//                    }
//                    
//                }else{
//                    if (touchPoint.y>64) {
//                        if (touchPoint.x<100) {
//                            if (_isAppear) {
//                                if (touchPoint.y<120) {
//                                    [self misRead];
//                                    return;
//                                }else{
//                                    [self misSet];
//                                }
//                            }
//                            CGFloat y = _scrollView.contentOffset.y;
//                            y -=KScreenheight*2/3;
//                            [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y) animated:YES];
//                            if (y < 0){
//                                [self loadMoreData];
//                            }
//                        }else if (touchPoint.x>ScreenSizeWidth-100){
//                            CGFloat y = _scrollView.contentOffset.y;
//                            y +=KScreenheight*2/3;
//                            [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, y) animated:YES];
//                            
//                            CGSize size1 = _scrollView.contentSize;
//                            if (y>=size1.height - KScreenWidth) {
//                                [self loadMoreNextData];
//                            }
//                            if (_isAppear) {
//                                [self misSet];
//                            }
//                        }else{
//                            [self memoryData];
//                            [self misSet];
//                        }
//                    }
//                }
//            }
//        }else{
            [self performSelector:@selector(singleTap:) withObject:event afterDelay:0.2];
//        }
    }

//    UITouch *touch = [touches anyObject];
//    NSTimeInterval delaytime = 1.0;
//    if (touch.tapCount == 1) {
//        [self performSelector:@selector(singleTap:) withObject:event afterDelay:delaytime];
//    }else if (touch.tapCount == 2){
//        [NSObject cancelPreviousPerformRequestsWithTarget:self];
////        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTap:) object:event];
//        [self performSelector:@selector(doubleTap) withObject:nil afterDelay:0];
//    }
}

-(void)memoryData{
    int i = (int)_pageMemoryNum;
    cartoonDetailModel * cartoonModel = _dataArr[i];
    markCount = i;
    _cartoonChapModel = _dataArr[i];
    _pageNumControl.minimumValue = 1;
    _pageNumControl.maximumValue = [cartoonModel.countTotal integerValue];
    [_pageNumControl setValue:[cartoonModel.priority integerValue]];
    sliderValue = _pageNumControl.value;
    _pageNumLabel.text = [NSString stringWithFormat:@"%@/%@",cartoonModel.priority,cartoonModel.countTotal];
    _nameLabel.text = cartoonModel.chapterName;
    _titleStr = cartoonModel.chapterName;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    _dataArr = [NSMutableArray array];
    _extraInfoDict = [[NSMutableDictionary alloc] init];
    _currentArray = [[NSMutableArray alloc] init];
    _downInfoDict = [[NSMutableDictionary alloc] init];
    _upInfoDict = [[NSMutableDictionary alloc] init];
    
    lastScale = 0.0f;
    mixScale = 1.0f;
    maxScale = 2.0f;
    _newY = 0;
    _currentScale = 1.0;
    _lastScale = 1.0;
    _preOneScale = 1.0;
    _isZero = NO;
    _SUN = YES;
    _killScr = NO;
    _isTransverse=NO;
    _showNoMore = YES;
    _isZoomIn = YES;
    _isZoomOut = NO;
    _isAssignment = YES;
    _zoomInOrOut = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self addNoti];
    [self createScrollView];
    [self createSetUI];
    [self createTAGView];
    [self createHelpView];//帮助栏
    [self createHelpTransverseView];
//    NSTimer *time = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(saveCurrentPage) userInfo:nil repeats:YES];
    _netImageView = [CustomTool createImageView];
    [self.view addSubview:_netImageView];
//    UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(misReadNoMessage)];
//    [_netImageView addGestureRecognizer:tapImage];
    
//    [self spring];
    _rightControl = [[UIControl alloc] initWithFrame:CGRectMake(KScreenWidth - 80, 80, 80, KScreenheight )];
    [_rightControl addTarget:self action:@selector(rightControlClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_rightControl];
    
    _leftControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 80, 80, KScreenheight)];
    [_leftControl addTarget:self action:@selector(leftControlClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_leftControl];
    
    for (UICollectionView *view in self.view.subviews) {
        view.multipleTouchEnabled = YES;
    }
    
//    self.view.multipleTouchEnabled = YES;
//    _scrollView.multipleTouchEnabled = YES;

}

- (void)rightControlClick{
    if (_isLeftRight) {
        NSIndexPath *path = [NSIndexPath indexPathForItem:_jumpIndex.item-1 inSection:0];
        if (_jumpIndex.item <= 0) {
            [self loadMoreData];
        }else{
            [_scrollView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }else{
        NSLog(@"----%@---%lu----%@",_jumpIndex,_dataArr.count,_dataArr);
        NSIndexPath *path = [NSIndexPath indexPathForItem:_jumpIndex.item+1 inSection:0];
        if (_jumpIndex.item >= _dataArr.count - 1) {
            [self loadMoreNextData];
//            [_scrollView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            
        }else{
            [_scrollView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    if (_isAppear) {
        [self misSet];
    }
}

- (void)leftControlClick{
    if(_isLeftRight){
        NSIndexPath *path = [NSIndexPath indexPathForItem:_jumpIndex.item+1 inSection:0];
        if (_jumpIndex.item >= _dataArr.count - 1) {
            [self loadMoreNextData];
        }else{
            [_scrollView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
        
    }else{
        
        
        NSIndexPath *path = [NSIndexPath indexPathForItem:_jumpIndex.item-1 inSection:0];
        if (_jumpIndex.item <= 0) {
            [self loadMoreData];
        }else{
            [_scrollView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    if (_isAppear) {
        [self misSet];
    }

}

//-(void)viewDidAppear:(BOOL)animated{
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CONTINUEREADINRECOMMENT"] isEqualToString:@"2"]){
//        [self dismissViewControllerAnimated:NO completion:nil];
//    }
//}
-(void)viewWillAppear:(BOOL)animated{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CONTINUEREADINRECOMMENT"] isEqualToString:@"2"]){
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
- (void)viewDidDisappear:(BOOL)animated{
    [_timer invalidate];
    if (_killScr) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    
//    [self.view removeFromSuperview];
}

-(void)misSelf{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)misReadNoMessage{
    _scrollView = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)saveCurrentPage {
//    NSArray *currentCell = []
//}



- (void)incrementCounter{
    NSDate * senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString * locationString=[dateformatter stringFromDate:senddate];
    _timeLabel.text = locationString;
    
    NSArray *currentCellArr = [_scrollView visibleCells];
    UITableViewCell *currentCell = currentCellArr.firstObject;
    NSIndexPath *indexPath = [_scrollView indexPathForCell:currentCell];

    [[NSUserDefaults standardUserDefaults] setObject:[_dataArr[indexPath.row] id] forKey:@"currentAlbumId"];
    [[NSUserDefaults standardUserDefaults] setObject:[_dataArr[indexPath.row] chapterId]  forKey:@"currentCharpteId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)getChoose{
    if (_charpList.count == 0) {
        NSString * selectUrl = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,[_dataArr[0] cartoonId]];
        //        NSLog(@"%@",_selectUrl);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:selectUrl parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 id backData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:NSJSONReadingMutableContainers
                                                                 error:nil];
                 NSDictionary *dic = backData[@"results"];
                 NSArray *cartoonChapListArr = dic[@"cartoonChapterList"];//.count
                 _charpList = [cartoonChapterListModel parasArr:cartoonChapListArr];
                 
                 _selectScrollView = [[UIScrollView alloc]init];
                 _selectScrollView.backgroundColor = [UIColor clearColor];//[UIColor colorWithPatternImage:[UIImage imageNamed:@"di"]];
                 _selectScrollView.frame = CGRectMake(KScreenWidth*1.0/5, KScreenheight*1.0/2-35*4, KScreenWidth*3.0/5,35*8);
                 _selectScrollView.hidden = YES;
                 _selectBgImageView.hidden = YES;
                 [self.view addSubview:_selectScrollView];
                 
                 CGFloat width = _selectScrollView.frame.size.width/3;
                 CGFloat height = 30.0f;
                 _selectScrollView.contentSize = CGSizeMake(5,(_charpList.count/3+1) * 35+5);
                 
                 
                 for (int i = 0 ; i < _charpList.count ; i ++) {
                     cartoonChapterListModel *carModel = _charpList[i];
                     UIButton *btn = [[UIButton alloc]init];
                     btn.frame = CGRectMake(2 + width * (i%3),5 + (height+5) * (i/3),width - 5,height);
                     [btn setBackgroundImage:[UIImage imageNamed:@"xuanjikuang"] forState:UIControlStateNormal];
                     [btn setTitle:[NSString stringWithFormat:@"%@话",carModel.priority] forState:UIControlStateNormal];
                     btn.titleLabel.textColor = [UIColor whiteColor];
                     btn.tag = [carModel.id integerValue];
                     btn.titleLabel.font = [UIFont systemFontOfSize:10];
                     [btn.layer setCornerRadius:2.0];
                     [btn.layer setBorderWidth:2.0];
                     btn.backgroundColor = [UIColor blackColor];
                     btn.layer.borderColor = (__bridge CGColorRef)([UIColor redColor]);
                     [btn.layer setMasksToBounds:YES];
                     [btn addTarget:self action:@selector(SelectWorkBtnClike:) forControlEvents:UIControlEventTouchUpInside];
                     [_selectScrollView addSubview:btn];
                 }
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             }];
    }
}

- (void)setCharpList1:(NSArray *)charpList1{
    if (charpList1.count == 0) {
    }else{
        _charpList = charpList1;
    }
    _selectBgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth*1.0/5-5, (KScreenheight*1.0/2-35*4)-10, KScreenWidth*3.0/5+10,35*8+20)];
    _selectBgImageView.backgroundColor = [UIColor blackColor];
    _selectBgImageView.alpha = 0.5;
    _selectBgImageView.hidden = YES;
    _selectBgImageView.layer.cornerRadius = 5;
    _selectBgImageView.layer.borderWidth = 1;
    _selectBgImageView.layer.borderColor = [UIColor redColor].CGColor;
    [self.view addSubview:_selectBgImageView];
    
    _selectScrollView = [[UIScrollView alloc]init];
    _selectScrollView.backgroundColor = [UIColor clearColor];//[UIColor colorWithPatternImage:[UIImage imageNamed:@"di"]];
    _selectScrollView.frame = CGRectMake(KScreenWidth*1.0/5, KScreenheight*1.0/2-35*4, KScreenWidth*3.0/5,35*8);
    _selectScrollView.hidden = YES;
    
    [self.view addSubview:_selectScrollView];
    CGFloat width = _selectScrollView.frame.size.width/3;
    CGFloat height = 30.0f;
    _selectScrollView.contentSize = CGSizeMake(5,(_charpList.count/3+1) * 35+5);
    
    for (int i = 0 ; i < _charpList.count ; i ++) {
        cartoonChapterListModel *carModel = _charpList[i];
        UIButton *btn = [[UIButton alloc]init];
        btn.frame = CGRectMake(2 + width * (i%3),5 + (height+5) * (i/3),width - 5,height);
        [btn setBackgroundImage:[UIImage imageNamed:@"xuanjikuang"] forState:UIControlStateNormal];
        [btn setTitle:[NSString stringWithFormat:@"%d话",i+1] forState:UIControlStateNormal];
        btn.titleLabel.textColor = [UIColor whiteColor];
        btn.tag = [_charpList[i] intValue];
        btn.titleLabel.font = [UIFont systemFontOfSize:10];
        [btn.layer setCornerRadius:2.0];
        [btn.layer setBorderWidth:2.0];
        btn.backgroundColor = [UIColor blackColor];
        btn.layer.borderColor = (__bridge CGColorRef)([UIColor redColor]);
        [btn.layer setMasksToBounds:YES];
        [btn addTarget:self action:@selector(SelectWorkBtnClike:) forControlEvents:UIControlEventTouchUpInside];
        [_selectScrollView addSubview:btn];
    }
}

- (void)setCharpList:(NSArray *)charpList{
    if (charpList.count == 0) {
    }else{
        _charpList = charpList;
    }
    _selectBgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth*1.0/5-5, (KScreenheight*1.0/2-35*4)-10, KScreenWidth*3.0/5+10,35*8+20)];
    _selectBgImageView.backgroundColor = [UIColor blackColor];
    _selectBgImageView.alpha = 0.5;
    _selectBgImageView.hidden = YES;
    _selectBgImageView.layer.cornerRadius = 5;
    _selectBgImageView.layer.borderWidth = 1;
    _selectBgImageView.layer.borderColor = [UIColor redColor].CGColor;
    [self.view addSubview:_selectBgImageView];
    
    _selectScrollView = [[UIScrollView alloc]init];
    _selectScrollView.backgroundColor = [UIColor clearColor];//[UIColor colorWithPatternImage:[UIImage imageNamed:@"di"]];
    _selectScrollView.frame = CGRectMake(KScreenWidth*1.0/5, KScreenheight*1.0/2-35*4, KScreenWidth*3.0/5,35*8);
    _selectScrollView.hidden = YES;
    [self.view addSubview:_selectScrollView];
    
    CGFloat width = _selectScrollView.frame.size.width/3;
    CGFloat height = 30.0f;
    _selectScrollView.contentSize = CGSizeMake(5,(_charpList.count/3+1) * 35+5);
    
    for (int i = 0 ; i < _charpList.count ; i ++) {
        cartoonChapterListModel *carModel = _charpList[i];
        UIButton *btn = [[UIButton alloc]init];
        btn.frame = CGRectMake(2 + width * (i%3),5 + (height+5) * (i/3),width - 5,height);
        [btn setBackgroundImage:[UIImage imageNamed:@"xuanjikuang"] forState:UIControlStateNormal];
        [btn setTitle:[NSString stringWithFormat:@"%@话",carModel.priority] forState:UIControlStateNormal];
        btn.titleLabel.textColor = [UIColor whiteColor];
        btn.tag = [carModel.id integerValue];
        btn.titleLabel.font = [UIFont systemFontOfSize:10];
        [btn.layer setCornerRadius:2.0];
        [btn.layer setBorderWidth:2.0];
        btn.backgroundColor = [UIColor blackColor];
        btn.layer.borderColor = (__bridge CGColorRef)([UIColor redColor]);
        [btn.layer setMasksToBounds:YES];
        [btn addTarget:self action:@selector(SelectWorkBtnClike:) forControlEvents:UIControlEventTouchUpInside];
        [_selectScrollView addSubview:btn];
    }
}

- (void)SelectWorkBtnClike:(UIButton *)selectButton {
    [self misSet];
    _selectScrollView.hidden = YES;
    _selectBgImageView.hidden = YES;
    NSString *loadMoreUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,(long)selectButton.tag];
    _loadMoreUrl = loadMoreUrl;
    if ([[DataBaseHelper shared] isExistsCartoonId:[NSString stringWithFormat:@"%d",selectButton.tag]]) {
        [_dataArr removeAllObjects];
        NSArray *cartoonModelArray = [[DataBaseHelper shared] fetchAlbumsWithChapterId:[NSString stringWithFormat:@"%d",selectButton.tag]];
        cartoonDetailModel *cartoonDetail = cartoonModelArray[0];
        _lastChapterId = cartoonDetail.lastChapterId;
        _nextChapterId = cartoonDetail.nextChapterId;
        
        NSRange range = NSMakeRange(0, [cartoonModelArray count]);
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
        [ _dataArr insertObjects:cartoonModelArray atIndexes:indexSet];
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        [_scrollView reloadData];
        _isDownLoad = YES;
        return;
    }else{
        
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"]&&_markCountPage == 1){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这集漫画未下载且您在非WIFI环境,您确定继续观看吗" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"接着看", nil];
            alert.tag = 100;
            [alert show];
            return;
        }else{
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            [manager GET:_loadMoreUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                _extraInfoDict = [backData[@"extraInfo"] mutableCopy];
                [_downInfoDict setDictionary: [backData[@"extraInfo"] mutableCopy]];
                [_upInfoDict setDictionary: [backData[@"extraInfo"] mutableCopy]];
                _lastChapterId = _upInfoDict[@"lastChapterId"];
                _nextChapterId = _downInfoDict[@"nextChapterId"];
                _dataArr = [cartoonDetailModel parasArr:backData];
                Layout.isHeng = _isTransverse;
                Layout.cellHeightArr = _dataArr;
                
                _isDownLoad = NO;
                [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
                [_scrollView reloadData];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您的网络不给力啊" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }];
        }
        
    }
    
    
    
    
}

#pragma mark --CollectionView

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GPCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellid" forIndexPath:indexPath];
    
//    if (_isTransverse) {
            while([cell.contentView.subviews lastObject]!=nil){
                [[cell.contentView.subviews lastObject] removeFromSuperview];
            }
//          
//        UIScrollView *view = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
//        view.delegate = self;
//        view.maximumZoomScale = 2.0;
//        view.minimumZoomScale = 1.0;
//        view.tag = indexPath.row;
////                view.zooming = YES;
//        
//        view.contentSize = CGSizeMake(0,[_dataArr[indexPath.row] propotion] * KScreenWidth);
////        if ([_dataArr[indexPath.row] propotion] * KScreenWidth > KScreenheight) {
////            cartoonImage.frame = CGRectMake(0, 0, KScreenWidth, [_dataArr[indexPath.row] propotion] * KScreenWidth);
////        }
//        view.backgroundColor = [UIColor redColor];
//        
//        //        view.contentOffset = CGPointMake(0, 20);
//        view.userInteractionEnabled = YES;
//        [cell.contentView addSubview:view];
//        
////        UIPinchGestureRecognizer *pinchGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(singlePinGes:)];
////        [cell.contentLabel addGestureRecognizer:pinchGes];
//        
//        [view addSubview:cell.contentLabel];
//    }
    
    
    
//    [cell layoutIfNeeded];
    cartoonDetailModel *cartoonDetail = _dataArr[indexPath.row];
    
    [cell.contentScrollView setZoomScale:1.0];
        if (_isTransverse) {
            cell.contentScrollView.scrollEnabled = YES;
            cell.contentScrollView.bounces = YES;
            cell.contentScrollView.userInteractionEnabled = YES;
            cell.contentScrollView.delegate = self;
            cell.contentScrollView.maximumZoomScale = 1.5;
            cell.contentScrollView.minimumZoomScale = 1.0;
            cell.contentLabel.frame = CGRectMake(0, 0, cell.frame.size.width, KScreenWidth * cartoonDetail.propotion);
            if (KScreenWidth * cartoonDetail.propotion < KScreenheight) {
                cell.contentLabel.center = CGPointMake(KScreenWidth/2, KScreenheight/2);
            }

            cell.contentScrollView.contentOffset = CGPointMake(0, 0);
            cell.contentScrollView.contentSize = CGSizeMake(0, KScreenWidth * cartoonDetail.propotion);
            
        }else {
            cell.contentScrollView.scrollEnabled = NO;
            
            cell.contentScrollView.bounces = NO;
            
//            cell.contentScrollView.delegate = nil;
            cell.contentScrollView.maximumZoomScale = 1.0;
            cell.contentScrollView.minimumZoomScale = 1.0;
            
        }
    cell.tag = indexPath.item;
    
    
    if (_isDownLoad) {
        NSString *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@/%@/%@",cartoonDetail.cartoonId,cartoonDetail.chapterId,[cartoonDetail.images md5String]]];
//        cartoonImage.image = [UIImage imageWithContentsOfFile:imagePath];//从文件中提取
        
        [cell setImageUrlll:nil andImage:[UIImage imageWithContentsOfFile:imagePath] andHeight:KScreenWidth * cartoonDetail.propotion andIsTran:_isTransverse];
        _pageMemoryNum = indexPath.row;
        
        _pagesMark.text = [NSString stringWithFormat:@"%@话 %@/%@",cartoonDetail.chapterIndex,cartoonDetail.priority,cartoonDetail.countTotal];
        
    }else {
        NSString *imagesS = [_dataArr[indexPath.row] images];
        [cell setImageUrlll:imagesS andImage:nil andHeight:KScreenWidth * cartoonDetail.propotion andIsTran:_isTransverse];
        
//        [cartoonImage sd_setImageWithURL:[NSURL URLWithString:imagesS]];
        //        [cartoonImage setImageWithURL:[NSURL URLWithString:[_dataArr[indexPath.row] images]]];
        _pagesMark.text = [NSString stringWithFormat:@"%@话 %@/%@",cartoonDetail.chapterIndex,cartoonDetail.priority,cartoonDetail.countTotal];
        _pageMemoryNum = indexPath.row;
        
    }
    _shareUrl = cartoonDetail.shareUrl;
    _shareImage = cartoonDetail.images;
    
//        NSString *imagesS = [_dataArr[indexPath.row] images];
//        NSLog(@"imagess%@",imagesS);
    
    
//        [cartoonImage setImageWithURL:[NSURL URLWithString:[_dataArr[indexPath.row] images]]];
//        _pagesMark.text = [NSString stringWithFormat:@"%@话 %@/%@",cartoonDetail.chapterIndex,cartoonDetail.priority,cartoonDetail.countTotal];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(misTapSet:)];
//    [cell addGestureRecognizer:tap];
    
//    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomLarge:)];
//    
//    doubleTap.numberOfTapsRequired = 2;
////    [tap requireGestureRecognizerToFail:doubleTap];
//    [cell addGestureRecognizer:doubleTap];
    
    _jumpIndex = indexPath;
    return cell;
}

- (void)zoomLarge:(UITapGestureRecognizer *)tapGes{
    if (_isTransverse) {
        NSLog(@"className%@",[NSString stringWithUTF8String:object_getClassName(tapGes.view.subviews[0].subviews[0])]);
        GPCollectionCell *im = (GPCollectionCell *)tapGes.view;
        cartoonDetailModel *cartoonDetail = _dataArr[im.tag];
        
        [im.contentScrollView setZoomScale:1.0 animated:YES];
        im.contentLabel.frame = CGRectMake(0, 0, im.frame.size.width, KScreenWidth * cartoonDetail.propotion);
        
//        [im zoomToRect:CGRectMake(0, 0, im.size.width, im.size.height) animated:YES];
//        im.transform = CGAffineTransformIdentity;
//        [im setZoomScale:1.0];
        
    }else{
        
        if (_isBigStatus) {
            
            Layout.scale = 1.0;
            [Layout invalidateLayout];
            _isBigStatus = NO;
            
        }else {
            Layout.pinchPoint = [tapGes locationInView:self.view];
            
            Layout.scale = 1.5;
            [Layout invalidateLayout];
            _isBigStatus = YES;
            
        }
    }
}

- (void)singlePinGes:(UIPinchGestureRecognizer *)pinchGes {
    pinchGes.view.transform = CGAffineTransformScale(pinchGes.view.transform, pinchGes.scale, pinchGes.scale);
    //每次缩放手势处理完成后，都必须将当前缩放比例设为1
    
    UIScrollView *v =    (UIScrollView *)pinchGes.view.superview.superview.superview;
    v.contentSize = CGSizeMake(ScreenSizeWidth * pinchGes.scale, pinchGes.view.height * pinchGes.scale);
    pinchGes.scale = 1;
    
}

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    NSLog(@"arr%lu", (unsigned long)scrollView.subviews.count);
//
//    NSLog(@"class%@",[NSString stringWithUTF8String:object_getClassName(scrollView.subviews[0])]);
//    NSLog(@"class1%@",[NSString stringWithUTF8String:object_getClassName(scrollView.subviews[1])]);
//    NSLog(@"class2%@",[NSString stringWithUTF8String:object_getClassName(scrollView.subviews[2])]);
    
    if (scrollView.subviews.count <= 1) {
        return scrollView.subviews[0];
    }else{
            NSLog(@"class2%@",[NSString stringWithUTF8String:object_getClassName(scrollView.subviews[2])]);
        
        return  scrollView.subviews[2];
    }
}

//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale {
//    NSLog(@"scale%f",scale);x
//}
//
//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale {
//    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width * scale, scrollView.contentSize.height * scale);
//}
//

- (void)misTapSet:(UITapGestureRecognizer *)tapGes {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tapGes.view.tag inSection:0];
    cartoonDetailModel * cartoonModel =  _dataArr[indexPath.row];
    markCount = indexPath.row;
    _cartoonChapModel = _dataArr[indexPath.row];
    _pageNumControl.minimumValue = 1;
    _pageNumControl.maximumValue = [cartoonModel.countTotal integerValue];
    [_pageNumControl setValue:[cartoonModel.priority integerValue]];
    sliderValue = _pageNumControl.value;
    _pageNumLabel.text = [NSString stringWithFormat:@"%@/%@",cartoonModel.priority,cartoonModel.countTotal];
    _nameLabel.text = cartoonModel.chapterName;
    _titleStr = cartoonModel.chapterName;
    [self misSet];

}


//- (void)swipGes:(UISwipeGestureRecognizer *)ges{
//    if (ges.direction == UISwipeGestureRecognizerDirectionUp) {
//        [UIView animateWithDuration:0.5 animations:^{
//
//            ges.view.center = CGPointMake(ges.view.center.x, ges.view.center.y - 200);
//        }];
//    }else{
//        [UIView animateWithDuration:0.5 animations:^{
//
//            ges.view.center = CGPointMake(ges.view.center.x, ges.view.center.y + 200);
//        }];
//        
//    }
//}


//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (!_isLandscape) {
//        if (_isTransverse) {
//            return CGSizeMake(KScreenWidth, KScreenheight);
//            
//
//        }else{
//        return CGSizeMake(KScreenWidth, [_dataArr[indexPath.row] propotion] * KScreenWidth);
//        
//    }
//    }else{
//        return CGSizeMake(KScreenheight, [_dataArr[indexPath.row] propotion] * KScreenheight);
//    }
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    cartoonDetailModel * cartoonModel =  _dataArr[indexPath.row];
    markCount = indexPath.row;
    _cartoonChapModel = _dataArr[indexPath.row];
    _pageNumControl.minimumValue = 1;
    _pageNumControl.maximumValue = [cartoonModel.countTotal integerValue];
    [_pageNumControl setValue:[cartoonModel.priority integerValue]];
    sliderValue = _pageNumControl.value;
    _pageNumLabel.text = [NSString stringWithFormat:@"%@/%@",cartoonModel.priority,cartoonModel.countTotal];
    _nameLabel.text = cartoonModel.chapterName;
    _titleStr = cartoonModel.chapterName;
    [self misSet];
}




//_topView.frame = CGRectMake(0, -64, HWIDTH, 64);
//_bottonView.frame = CGRectMake(0, HHEIGHT + 20, HWIDTH, 64);
//_rightView.frame = CGRectMake(HWIDTH, HHEIGHT/2-35, 64, 70);
//_leftView.frame = CGRectMake(-64, 160, 64, HHEIGHT - 320);


-(void)misSet{
    
    if (!_isLandscape) {
        if (_isAppear) {//消失
//            if (_isTransverse) {
//                [self.view addSubview:_leftControl];
//                [self.view addSubview:_rightControl];
//            }
            _selectScrollView.hidden = YES;
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.0f];
            [UIView setAnimationDuration:0.2f];
            [UIView setAnimationCurve:UIViewAnimationCurveLinear];
            [UIView setAnimationRepeatCount:1];
            [UIView setAnimationDelegate:self];
            
            _isChoose = !_isChoose;
            
            _topView.frame = CGRectMake(0, -64, KScreenWidth, 64);
            _bottonView.frame = CGRectMake(0, KScreenheight + 20, KScreenWidth, 85);
            _rightView.frame = CGRectMake(KScreenWidth, KScreenheight/2-90, 60, 120);
            _leftView.frame = CGRectMake(-64, 90 * __Scale, 50, 300);
            
            //    _selectScrollView.alpha = 0.0;
            [UIView commitAnimations];
            _scrollView.scrollEnabled = YES;
            _selectBgImageView.hidden = YES;
            _tagView.hidden = NO;
            
        }else {//出现
//            [_leftControl removeFromSuperview];
//            [_rightControl removeFromSuperview];
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.0f];
            [UIView setAnimationDuration:0.2f];
            [UIView setAnimationCurve:UIViewAnimationCurveLinear];
            [UIView setAnimationRepeatCount:1];
            [UIView setAnimationDelegate:self];
            
            _topView.frame = CGRectMake(0, 0, KScreenWidth, 64);
            _bottonView.frame = CGRectMake(0, KScreenheight-85, KScreenWidth, 85);//HHEIGHT/6
            _rightView.frame = CGRectMake(KScreenWidth - 60, KScreenheight/2-90,60 , 120);//
            _leftView.frame = CGRectMake(0, 90 * __Scale, 50, 300);
            
            [UIView commitAnimations];
            _scrollView.scrollEnabled = NO;
            _tagView.hidden = YES;
            
        }
        _isAppear = !_isAppear;
    }else{
        if (_isAppear) {//消失
//            [self.view addSubview:_leftControl];
//            [self.view addSubview:_rightControl];
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            _isChoose = !_isChoose;
            
            [UIView animateWithDuration:0.2 animations:^{
                _topView.center = CGPointMake(KScreenWidth + 32, KScreenheight/2);
                _leftView.center = CGPointMake(KScreenWidth/2 - 22, -25);
                _bottonView.center = CGPointMake(-42.5, KScreenheight/2);
                _rightView.center = CGPointMake(KScreenWidth/2, KScreenheight + 30);
            }];
            
            _isChoose = NO;
            _selectScrollView.hidden = YES;
            _scrollView.scrollEnabled = YES;
             _selectBgImageView.hidden = YES;
            _tagView.hidden = NO;
            
            
        }else {//出现
            
//            [_leftControl removeFromSuperview];
//            [_rightControl removeFromSuperview];
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            
            [UIView animateWithDuration:0.2 animations:^{
                _topView.center = CGPointMake(KScreenWidth - 16, KScreenheight/2);
                _leftView.center = CGPointMake(KScreenWidth/2-22, 25);
                _bottonView.center = CGPointMake(42.5, KScreenheight/2);
                _rightView.center = CGPointMake(KScreenWidth/2, KScreenheight - 30);
            }];
            
            _scrollView.scrollEnabled = NO;
            _tagView.hidden = YES;
            
        }
        _isAppear = !_isAppear;
    }
}

-(void)misRead{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HAVELOGINED"] isEqualToString:@"1"]&&g_App.userInfo.userID == nil) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以保存阅读记录哦~"];
        [alertView show];
        return;
        
    }
    
    _killScr = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",_lightSlider.value] forKey:@"LIGHTSLIDERVALUE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"%f",_lightSlider.value);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//    NSDictionary * readDict = @{@"chapterId":_cartoonChapModel.chapterId,@"r":@"cartoonReadHistory/add",@"albumId":_cartoonChapModel.id};
    
//    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userID":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }else {
//        headers = @{@"userID":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }
//    [YK_API_request startLoad:@"http://api.playsm.com/index.php" extraParams:readDict object:self action:@selector(saveReadData:) method:@"POST" andHeaders:headers];
    
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(saveReadData:) method:POSTDATA];
    
    
    [self saveReadData:nil];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CONTINUEREADINRECOMMENT"] isEqualToString:@"1"]) {
        testViewController *testVC = [[testViewController alloc]init];
        testVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_nowCartoonId];
        testVC.detailStr =str;
        [testVC setChangeCollection:^{
            
        }];
        [self presentViewController:testVC animated:YES completion:^{
            
            _changeMark([_cartoonChapModel.chapterId integerValue]);
            
        }];
        
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"CONTINUEREADINRECOMMENT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self dismissViewControllerAnimated:YES completion:^{
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTag" object:nil];
            _changeMark([_cartoonChapModel.chapterId integerValue]);
            
        }];
    }
    
    
}

- (void)saveReadData:(id)sender{
    _cartoonAllInfo = [[CartoonInfoAll alloc] init];
//    _cartoonAllInfo.
    
//    NSDictionary * readDict = @{@"chapterId":_cartoonChapModel.cartoonId,@"r":@"cartoonReadHistory/add",@"albumId":_cartoonChapModel.id};
    
//    _allInfo = @[dict[@"params"][@"id"],dic[@"name"],dic[@"images"]];
    
    _cartoonAllInfo.id = _cartoonChapModel.cartoonId;
    _cartoonAllInfo.currentReadChapterId = _cartoonChapModel.chapterId;
    _cartoonAllInfo.currentReadAlbumId = _cartoonChapModel.id;
    _cartoonAllInfo.images = _allInfo[2];
    _cartoonAllInfo.name = _allInfo[1];
    _cartoonAllInfo.isRead = @"1";
    _cartoonAllInfo.lastReadTime = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
    [[DataBaseHelper shared] insertExistCartoonInfo:_cartoonAllInfo andValue:@{@"isRead":@"1",@"lastReadTime": [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]],@"currentReadAlbumId":_cartoonChapModel.id,@"currentReadChapterId":_cartoonChapModel.chapterId,@"chapterIndex":_cartoonChapModel.chapterIndex}];
    NSLog(@"chapterId%ld",(long)[_cartoonChapModel.chapterId integerValue]);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
    
}

-(void)createSelectWorkView{
}
-(void)createHelpTransverseView{
    _helpTransverseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenheight, KScreenWidth)];
    _helpTransverseView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
    _helpTransverseView.center = CGPointMake(KScreenWidth/2, KScreenheight/2);
    _helpTransverseView.alpha = 0;
    _helpTransverseView.userInteractionEnabled = YES;
    [self.view addSubview:_helpTransverseView];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenheight, KScreenWidth)];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.6;
    [_helpTransverseView addSubview:bgView];
    
    UIView *lineView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 100, 112, 12)];
    lineView1.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView1];
    
    UIView *lineView2 = [[UIView alloc]initWithFrame:CGRectMake(100, 0, 12, 112)];
    lineView2.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView2];
    
    UIView *lineView3 = [[UIView alloc]initWithFrame:CGRectMake(KScreenheight-112, 100, 112, 12)];
    lineView3.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView3];
    
    UILabel *labelUp1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 35, 100, 30)];
    labelUp1.text = @"上一页";
    labelUp1.textAlignment = 1;
    labelUp1.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    labelUp1.textColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:labelUp1];
    UILabel *labelUp2 = [[UILabel alloc]initWithFrame:CGRectMake(KScreenheight - 100, 35, 100, 30)];
    labelUp2.text = @"上一页";
    labelUp2.textAlignment = 1;
    labelUp2.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    labelUp2.textColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:labelUp2];
    
    UILabel *labelList = [[UILabel alloc]initWithFrame:CGRectMake(KScreenheight/2 - 50, KScreenWidth/2 - 15, 100, 30)];
    labelList.text = @"菜单";
    labelList.textAlignment = 1;
    labelList.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    labelList.textColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:labelList];
    
    UILabel *labelDown1 = [[UILabel alloc]initWithFrame:CGRectMake(0, KScreenWidth-65, 100, 30)];
    labelDown1.text = @"下一页";
    labelDown1.textAlignment = 1;
    labelDown1.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    labelDown1.textColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:labelDown1];
    UILabel *labelDown2 = [[UILabel alloc]initWithFrame:CGRectMake(KScreenheight - 100, KScreenWidth-65, 100, 30)];
    labelDown2.text = @"下一页";
    labelDown2.textAlignment = 1;
    labelDown2.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    labelDown2.textColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:labelDown2];
    
    
    
    UIView *lineView4 = [[UIView alloc]initWithFrame:CGRectMake(KScreenheight-112, 0, 12, 112)];
    lineView4.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView4];
    
    UIView *lineView5 = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenWidth-112, 112, 12)];
    lineView5.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView5];
    
    UIView *lineView6 = [[UIView alloc]initWithFrame:CGRectMake(100, KScreenWidth-112, 12, 112)];
    lineView6.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView6];
    
    UIView *lineView7 = [[UIView alloc]initWithFrame:CGRectMake(KScreenheight-112, KScreenWidth-112, 112, 12)];
    lineView7.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView7];
    
    UIView *lineView8 = [[UIView alloc]initWithFrame:CGRectMake(KScreenheight-112, KScreenWidth-112, 12, 112)];
    lineView8.backgroundColor = [UIColor whiteColor];
    [_helpTransverseView addSubview:lineView8];
    
    UITapGestureRecognizer *tapMisSet = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(misAll)];
    [_helpTransverseView addGestureRecognizer:tapMisSet];
    
}

-(void)createHelpView{
    _helpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    _helpView.userInteractionEnabled = YES;
    [self.view addSubview:_helpView];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.6;
    [_helpView addSubview:bgView];
    
    UIView *lineView1 = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth/3-6, 0, 12, KScreenheight)];
    lineView1.backgroundColor = [UIColor whiteColor];
    lineView1.tag = 1001;
    [_helpView addSubview:lineView1];
    UIView *lineView2 = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth/3*2-6, 0, 12, KScreenheight)];
    lineView2.tag = 1002;
    lineView2.backgroundColor = [UIColor whiteColor];
    [_helpView addSubview:lineView2];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, KScreenheight/2 - 10, 100, 20)];
    label1.text = @"上一页";
    label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label1.textColor = [UIColor whiteColor];
    label1.textAlignment = 1;
    label1.tag = 2001;
    [_helpView addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth-100, KScreenheight/2 - 10, 100, 20)];
    label2.text = @"下一页";
    label2.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label2.textColor =[UIColor whiteColor];
    label2.textAlignment = 1;
    label2.tag = 2002;
    [_helpView addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2.0 - 50 , KScreenheight/2.0 - 75, 100, 150)];
    label3.text = @"菜单";
    label3.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label3.textColor = [UIColor whiteColor];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.alpha = 1;
    [_helpView addSubview:label3];
    
    //    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2.0 - 75 ,  0, 150, 150)];
    //    label4.text = @"水平观看模式";
    //    label4.font = [UIFont fontWithName:@"Helvetica" size:20];
    //    label4.textColor = [UIColor greenColor];
    //    label4.textAlignment = 1;
    //    label4.alpha = 1;
    //    [_helpView addSubview:label4];
    
    UITapGestureRecognizer *tapMisSet = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(misAll)];
    [_helpView addGestureRecognizer:tapMisSet];
    _helpView.alpha = 0;
}

-(void)misAll{
    _helpView.alpha = 0;
    _helpTransverseView.alpha = 0;
    [self misSet];
}
-(void)helpUI:(UIButton *)btn{
    if (_isLandscape) {
        _helpTransverseView.alpha = 1;
    }else{
        _helpView.alpha = 1;
    }
    [self misSet];
    [btn addTarget:self action:@selector(helpUI:) forControlEvents:UIControlEventTouchUpInside];
}



-(void)createSetUI{
    _OverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    _OverView.backgroundColor = [UIColor blackColor];
    _OverView.alpha = 0.0;
    _OverView.userInteractionEnabled = NO;
    [self.view addSubview:_OverView];
    
    //--------上视图
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0, -64, KScreenWidth, 64)];
    _topView.backgroundColor = [UIColor blackColor];
    _topView.userInteractionEnabled = YES;
    [self.view addSubview:_topView];
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 20, 100, 40)];
//    [backBtn setBackgroundImage:[UIImage imageNamed:@"fanhui@2x"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(misRead) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:backBtn];
    UIImageView *backImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    backImage.image = [UIImage imageNamed:@"fanhui@2x"];
    [backBtn addSubview:backImage];
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 30, KScreenWidth - 100, 30)];
    _nameLabel.textAlignment = 1;//--
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    [_topView addSubview:_nameLabel];
    
    UIButton *helpBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth - 40, 30, 27, 30)];
    [helpBtn setBackgroundImage:[UIImage imageNamed:@"fenxiang_2"] forState:UIControlStateNormal];
    helpBtn.tag = 200;
    [helpBtn addTarget:self action:@selector(shareBtnClike:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:helpBtn];
    //--------左视图
    _leftView = [[UIView alloc]initWithFrame:CGRectMake(-64, KScreenWidth/2/2-64, 50, 300)];//HHEIGHT-320
    _leftView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_leftView];
    
    _lightSlider= [[UISlider alloc]init];
    _lightSlider.frame = CGRectMake(-97, 120.5, 245, 50);
    if (self.view.frame.size.height<=569 && self.view.frame.size.height>=500) {
        _lightSlider.frame = CGRectMake(-77, 100, 200, 50);
    }else if (self.view.frame.size.height <= 481){
        _lightSlider.frame = CGRectMake(-77,66, 200, 50);
    }
    
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"]){
        _lightSlider.value = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"] floatValue];
        if (_lightSlider.value <0.6) {
            _lightSlider.minimumValue = 0.2;
            _lightSlider.maximumValue = 0.6;
            _SUN = NO;
        }else{
            _lightSlider.minimumValue = 0.6;
            _lightSlider.maximumValue = 1.0;
            _SUN = YES;
        }
        _OverView.alpha = 1.0 - _lightSlider.value;
    }else{
        _lightSlider.minimumValue = 0.6;
        _lightSlider.maximumValue = 1.0;
        _lightSlider.value = 1.0;
    }
    
    _lightNum = _lightSlider.value;
    _lightSlider.tintColor = [UIColor redColor];
    _lightSlider.userInteractionEnabled = YES;
    [_lightSlider setThumbImage:[UIImage imageNamed:@"circlePoint"] forState:UIControlStateNormal];
    [_lightSlider addTarget:self action:@selector(lightChange:) forControlEvents:UIControlEventValueChanged];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
    _lightSlider.transform = trans;
    [_leftView addSubview:_lightSlider];
    
    _leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _leftView.bounds.size.height -40, _leftView.bounds.size.width, 40)];
    _leftLabel.text = @"亮度";
    _leftLabel.textAlignment = NSTextAlignmentCenter;
    _leftLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    _leftLabel.textColor = [UIColor whiteColor];
    _leftLabel.numberOfLines = 0;
    [_leftView addSubview:_leftLabel];
    
    //--------右视图
    _rightView = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth, KScreenheight/2-90, 60, 120)];//HHEIGHT/2
    _rightView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_rightView];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"]){
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"]);
        _lightSlider.value = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"] doubleValue];
        if (_lightSlider.value <0.6) {
            _imageRightBtn = @[@"xuanji",@"baitian"];//@"fankui_read"
            _nameRightBtn = @[@"选集",@"正常"];//@"反馈"
            _SUN = NO;
        }else{
            _imageRightBtn = @[@"xuanji",@"yejian"];
            _nameRightBtn = @[@"选集",@"夜间"];
            _SUN = YES;
        }
        
    }else{
        _imageRightBtn = @[@"xuanji",@"yejian",];
        _nameRightBtn = @[@"选集",@"夜间"];
    }
    
    for (int i = 0 ; i < 2; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 60*i, 60, 40);//HHEIGHT/6 - 30 - 25
        [btn setImage:[UIImage imageNamed:_imageRightBtn[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(rightBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 1000 + i;
        [_rightView addSubview:btn];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 60*i+40, 60, 20)];
        label.text = _nameRightBtn[i];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Helvetica" size:14];
        label.tag = 1001+i;
        [_rightView addSubview:label];
        
    }
    //--------下视图
    _bottonView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenheight, KScreenWidth, 85)];//HHEIGHT/6
    _bottonView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_bottonView];
    
    _pageNumControl= [[UISlider alloc]initWithFrame:CGRectMake(KScreenWidth/4 - 5, 10, KScreenWidth - 120, 10)];
    _pageNumControl.center = CGPointMake(KScreenWidth/2, 15);
    _pageNumControl.backgroundColor = [UIColor blackColor];
    _pageNumControl.minimumValue = 1;
    _pageNumControl.maximumValue = 20;
    _pageNumControl.tintColor = [UIColor redColor];
    _pageNumControl.userInteractionEnabled = YES;
    [_pageNumControl setThumbImage:[UIImage imageNamed:@"circlePoint"] forState:UIControlStateNormal];
    [_pageNumControl addTarget:self action:@selector(changePage) forControlEvents:UIControlEventValueChanged];
    [_bottonView addSubview:_pageNumControl];
    
    
    _pageNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/8 - 25 * _Scale, 10, KScreenWidth/8, 10)];
    _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value+1,(unsigned long)_dataArr.count];
    _pageNumLabel.textColor = [UIColor whiteColor];
    _pageNumLabel.font = [UIFont systemFontOfSize:12.0];
    [_bottonView addSubview:_pageNumLabel];
    
//    if([[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"]){
//        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"]);
//        _lightSlider.value = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LIGHTSLIDERVALUE"] doubleValue];
//        if (_lightSlider.value <0.6) {
//            _imageBtnArr = @[@"baitian",@"xuanji",@"fankui_read",@"hegnxiangyidong",@"hengxiang"];//shangyihua@3x   henping@3x,@"baocun_1@3x"
//            _nameBtnArr = @[@"正常",@"选集",@"反馈",@"垂直移动",@"竖屏"];//横屏,@"保存"
////            _SUN = NO;
//        }else{
//            _imageBtnArr = @[@"yejian",@"xuanji",@"fankui_read",@"hegnxiangyidong",@"hengxiang"];//shangyihua@3x   henping@3x,@"baocun_1@3x"
//            _nameBtnArr = @[@"夜间",@"选集",@"反馈",@"垂直移动",@"竖屏"];//横屏,@"保存"
////            _SUN = YES;
//        }
//        
//    }else{
//        _imageBtnArr = @[@"yejian",@"xuanji",@"fankui_read",@"hegnxiangyidong",@"hengxiang"];//shangyihua@3x   henping@3x,@"baocun_1@3x"
//        _nameBtnArr = @[@"夜间",@"选集",@"反馈",@"垂直移动",@"竖屏"];//横屏,@"保存"
//    }
    _imageBtnArr = @[@"hengxiang",@"hegnxiangyidong",@"fanye_3",@"fankui_read",@"bangzhu"];//shangyihua@3x   henping@3x,@"baocun_1@3x"
    _nameBtnArr = @[@"横屏",@"横向移动",@"右手翻页",@"反馈",@"帮助"];
    for (int i = 0 ; i < _nameBtnArr.count; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(KScreenWidth*1.0/_nameBtnArr.count * i + 45, 30, 25, 25);//HHEIGHT/6 - 30 - 25
        btn.center = CGPointMake(KScreenWidth*1.0/_nameBtnArr.count*i+KScreenWidth*1.0/_nameBtnArr.count/2, 41);
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth*1.0/_nameBtnArr.count*i, 63, KScreenWidth*1.0/_nameBtnArr.count, 20)];
        label.text = _nameBtnArr[i];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Helvetica" size:14];
        label.tag = 1001+i;
        [_bottonView addSubview:label];
        [btn setImage:[UIImage imageNamed:_imageBtnArr[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 10000 + i;
        [_bottonView addSubview:btn];
        if (i == 1 ) {
            [btn setImage:[UIImage imageNamed:@"shuxiang"] forState:UIControlStateSelected];
        }
        if ( i == 0) {
            [btn setImage:[UIImage imageNamed:@"hengxiang"] forState:UIControlStateSelected];
        }
    }
    
    
    
    
//    _rightViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _rightViewBtn.frame = CGRectMake(0 , _rightView.bounds.size.height/2-35, KScreenWidth/6, 40);
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, _rightView.bounds.size.height/2+5, KScreenWidth/6, 30)];
//    label.text = nameRightBtn[0];
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont fontWithName:@"Helvetica" size:14];
//    label.textAlignment = NSTextAlignmentCenter;
//    [_rightView addSubview:label];
//    [_rightViewBtn setImage:[UIImage imageNamed:imageRightBtn[0]] forState:UIControlStateNormal];
//    _rightViewBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 8, 5,8);
//    _rightViewBtn.tag = 100 ;
//    [_rightViewBtn addTarget:self action:@selector(rightBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
    
    
   
}
-(void)shareBtnClike:(UIButton *)btn{
    
    if (_dataArr.count == 0) {
        return;
    }
    [UMSocialData defaultData].extConfig.qzoneData.title = _titleStr;
    [UMSocialData defaultData].extConfig.qzoneData.url = _shareUrl;
    
    [UMSocialData defaultData].extConfig.qqData.title = _titleStr;
    [UMSocialData defaultData].extConfig.qqData.url = _shareUrl;
    
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = _titleStr;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = _shareUrl;
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = _titleStr;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = _shareUrl;
    
    
    [UMSocialData defaultData].extConfig.sinaData.shareText = [NSString stringWithFormat:@"%@%@分享自@麦萌",_titleStr,_shareUrl];
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:_shareImage];
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENGKEY
                                      shareText:nil
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:@[UMShareToSina, UMShareToQQ, UMShareToQzone, UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:self];
    
    
    
}

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    
    [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"d" to_page:@"crs" to_section:@"c" to_step:@"r" type:[[response.data allKeys] objectAtIndex:0] id:_cartoonId];

//    [[LogHelper shared] writeToFilefrom_page:@"ms" from_section:@"m" from_step:@"r" to_page:@"ms" to_section:@"m" to_step:@"a" type:[[response.data allKeys] objectAtIndex:0] id:@"0"];
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        
        //        [[upVote shared] startAnimitionTop:@"分享成功" bottom:@"+9999" point:CGPointMake(KScreenWidth/2, KScreenheight/2) fatherView:self.view];
        
        
        
//        [self addIntergration:@"4"];
    }
}


-(void)createTAGView{
    
    
    
    
    _tagView = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth - 145, 3.5, 142.5, 13.5)];
    _tagView.backgroundColor = [UIColor blackColor];
    _tagView.alpha = 0.5;
    [self.view addSubview:_tagView];
    
    
    
    NSDate * senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString * locationString=[dateformatter stringFromDate:senddate];
    
    _pagesMark = [[UILabel alloc] initWithFrame:CGRectMake(2,2,66,10)];//CGRectGetMaxX(_timeLabel.frame), 2, 30, 10
    _pagesMark.backgroundColor = [UIColor clearColor];
    _pagesMark.textColor = [UIColor whiteColor];
    _pagesMark.textAlignment = 1;
    _pagesMark.font = [UIFont systemFontOfSize:10.0];
    [_tagView addSubview:_pagesMark];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_pagesMark.frame)+1, 2, 30, 10)];
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.textAlignment = 0;
    _timeLabel.font = [UIFont systemFontOfSize:10];
    _timeLabel.text = locationString;
    [_tagView addSubview:_timeLabel];
    
    _statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_timeLabel.frame), 2, 23, 10)];
    _statusLabel.textColor = [UIColor whiteColor];
    _statusLabel.textAlignment = 0;
    _statusLabel.font = [UIFont systemFontOfSize:10];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"]) {
        _statusLabel.text = @"mob";
    }else{
        _statusLabel.text = @"WIFI";
    }
    
    [_tagView addSubview:_statusLabel];
    
    
    double barray =[self batteryMoniter];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(CGRectGetMaxX(_statusLabel.frame), 3, 19, 8);
    [button setBackgroundImage:[UIImage imageNamed:@"dianchi"] forState:UIControlStateNormal];
    [_tagView addSubview:button];
    [self drawRect:barray andButton:button];
    
}


- (void)drawRect:(double)barray andButton:(UIButton *)btn
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor*aColor = [UIColor colorWithRed:1 green:0.0 blue:0 alpha:1];
    
    CGContextStrokeRect(context,CGRectMake(0, 0, 19, 8));
    CGContextFillRect(context,CGRectMake(0, 0, 19, 8));
    CGContextSetLineWidth(context, 2.0);
    aColor = [UIColor whiteColor];
    CGContextSetFillColorWithColor(context, aColor.CGColor);
    aColor = [UIColor whiteColor];
    CGContextSetStrokeColorWithColor(context, aColor.CGColor);
    CGContextAddRect(context,CGRectMake(0, 0, 19, 8));
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    gradient1.frame = CGRectMake(0, 0, 19 * barray , 8);
    gradient1.colors = [NSArray arrayWithObjects:(id)[UIColor whiteColor].CGColor,
                        (id)[UIColor whiteColor].CGColor,nil];
    [btn.layer insertSublayer:gradient1 atIndex:0];
}


- (double)batteryMoniter {
    UIDevice *device = [UIDevice currentDevice];
    device.batteryMonitoringEnabled = YES;
    if (device.batteryState == UIDeviceBatteryStateUnknown) {
        NSLog(@"UnKnow");
    }else if (device.batteryState == UIDeviceBatteryStateUnplugged){
        NSLog(@"Unplugged");
    }else if (device.batteryState == UIDeviceBatteryStateCharging){
        NSLog(@"Charging");
    }else if (device.batteryState == UIDeviceBatteryStateFull){
        NSLog(@"Full");
    }
    NSLog(@"%f",device.batteryLevel);
    return device.batteryLevel;
}



- (void)lightChange:(UISlider *)lightSlide{
    _OverView.alpha = 1.0 - _lightSlider.value;
    _lightNum = _lightSlider.value;
}

- (void)changePage{
    NSLog(@"markCount%ld", markCount + ((NSInteger)_pageNumControl.value - sliderValue));
    NSUInteger currentIndexs[2] = {0, markCount + ((NSInteger)_pageNumControl.value - sliderValue) };
    NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:currentIndexs length:2];
    
    
    if (_isTransverse) {
        [_scrollView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];

    }else{
    [_scrollView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    
//    [_scrollView scrollToRowAtIndexPath:indexPath
//                       atScrollPosition:UITableViewScrollPositionTop animated:NO];
    _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%0.0f",_pageNumControl.value,_pageNumControl.maximumValue ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createScrollView{
    
//    fatherScr = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
////    fatherScr.contentSize = CGSizeMake(KScreenWidth + 100, KScreenheight);
//    fatherScr.showsHorizontalScrollIndicator = NO;
//    fatherScr.directionalLockEnabled = YES;
//    [self.view addSubview:fatherScr];
    
    
    
    NSMutableArray *a = [NSMutableArray array];
    Layout = [[GridLayout alloc]init];
    Layout.cellHeightArr = a;
//    Layout.minimumInteritemSpacing = 10;
//    Layout.minimumLineSpacing = 10;
//    Layout.sectionInset = UIEdgeInsetsMake(10, kCategoryInteritemSpacing, 0, 10);
//    Layout.itemSize = CGSizeMake(kCategoryCellWidth, kCategoryCellWidth+30);
    
    
    _scrollView = [[secondReadView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth,ScreenSizeHeight) collectionViewLayout:Layout];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.delegate = self;
    _scrollView.dataSource = self;
    _scrollView.backgroundColor = [UIColor blackColor];
    [_scrollView registerClass:[GPCollectionCell class] forCellWithReuseIdentifier:@"cellid"];
    
    
//    _scrollView = [[PullPsCollectionView alloc]initWithFrame:CGRectMake(0, -44, KScreenWidth, KScreenheight+108) pullingDelegate:self];
//    _scrollView.bounces = YES;
//    _scrollView.bouncesZoom = YES;
//    _scrollView.dataSource = self;
//    _scrollView.delegate = self;
//    _scrollView.showsVerticalScrollIndicator = NO;
//    _scrollView.directionalLockEnabled = YES;
////    _scrollView.minimumZoomScale=1.0f;
////    
////    _scrollView.maximumZoomScale=1.3f;
//    
//    
//    _scrollView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _scrollView.backgroundView = nil;
    
    [self.view addSubview:_scrollView];
    
    
//    UISwipeGestureRecognizer *swipeGesLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesLeft:)];
//    swipeGesLeft.direction = UISwipeGestureRecognizerDirectionLeft;
//    [_scrollView addGestureRecognizer:swipeGesLeft];
//    
//    UISwipeGestureRecognizer *swipeGesRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesLeft:)];
//    swipeGesRight.direction = UISwipeGestureRecognizerDirectionRight;
//    [_scrollView addGestureRecognizer:swipeGesRight];
//    
//    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGes:)];
//    panGes.delegate = self;
//    [_scrollView addGestureRecognizer:panGes];
    
    pinGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureRecognizer:)];
    [_scrollView addGestureRecognizer:pinGes];
//
//    
    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTap)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [_scrollView addGestureRecognizer:doubleTapGestureRecognizer];
    
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    
//    UIPanGestureRecognizer *panGes = (UIPanGestureRecognizer *)gestureRecognizer;
//    CGPoint velocity = [panGes translationInView:self.view];
//    NSLog(@"velocity%@",NSStringFromCGPoint(velocity));
//    if (velocity.y > 0) {
//        return NO;
//    }else{
    
        return YES;
    

}



//- (void)swipeGesLeft:(UISwipeGestureRecognizer*)swipeGes
//{
//    if (swipeGes.direction == UISwipeGestureRecognizerDirectionLeft) {
//        swipeGes.view.frame = CGRectMake(ScreenSizeWidth - swipeGes.view.size.width , swipeGes.view.frame.origin.y,swipeGes.view.size.width, swipeGes.view.size.height);
//    }else{
//        swipeGes.view.frame = CGRectMake(0, swipeGes.view.frame.origin.y,swipeGes.view.size.width, swipeGes.view.size.height);
//
//    }
//}

//- (void)panGes:(UIPanGestureRecognizer *)panGes
//{
//    NSLog(@"失败是事hihihihi");
    //得到当前手势在视图中得偏移量
//    CGPoint point = [panGes translationInView:self.view];
//    //得到一个新的矩阵，通过当前视图的原来的矩阵在x,y方向移动偏移量
//    
//    CGPoint velocity = [panGes velocityInView:_scrollView];
//    if(velocity.x > 0)//right
//    {
//        //my action here
//        if ( [panGes state] == UIGestureRecognizerStateEnded )
//            panGes.view.transform = CGAffineTransformTranslate(panGes.view.transform, point.x, point.y);
//        }
//        else if(velocity.x < 0)//left
//        {
//            //my action here
//            if ( [panGes state] == UIGestureRecognizerStateEnded )
//            { panGes.view.transform = CGAffineTransformTranslate(panGes.view.transform, point.x, point.y);
//            }
//            else if(velocity.y > 0)//down
//            {
//                //my action here
//                if ( [panGes state] == UIGestureRecognizerStateEnded )
//                { //my action when swipe finished here }
//                    
//                }
//                else if(velocity.y < 0)//up
//                {
//                    //my action here
//                    if ( [panGes state] == UIGestureRecognizerStateEnded )
//                    { //my action when swipe finished here }
//                        
//                    }
//
//                }}}
//    //手势处理完成后，必须对当前手势在视图中得偏移量清零
//    [panGes setTranslation:CGPointZero inView:self.view];
//}
//
- (void)panGes:(UIPanGestureRecognizer *)panGestureReconginzer
{
//    //得到当前手势在视图中得偏移量
//    CGPoint point = [panGes translationInView:self.view];
//    //得到一个新的矩阵，通过当前视图的原来的矩阵在x,y方向移动偏移量
//    panGes.view.transform = CGAffineTransformTranslate(panGes.view.transform, point.x, point.y);
//    
//    //手势处理完成后，必须对当前手势在视图中得偏移量清零
//    [panGes setTranslation:CGPointZero inView:self.view];
    
    
    if (panGestureReconginzer.state == UIGestureRecognizerStateChanged)
    {
        CGFloat translation = [panGestureReconginzer translationInView:self.view].y;
        
        if (translation<0)
        {
            
//            当translation >0就表示想右滑动，那这里如果关闭手势啊 ？
            //panGestureReconginzer.cancelsTouchesInView =YES;
            // [self.contentView removeGestureRecognizer:panGestureReconginzer];
            return;
        }
        else{
            
                CGPoint point = [panGestureReconginzer translationInView:self.view];
                //得到一个新的矩阵，通过当前视图的原来的矩阵在x,y方向移动偏移量
                panGestureReconginzer.view.transform = CGAffineTransformTranslate(panGestureReconginzer.view.transform, point.x, point.y);
            
                //手势处理完成后，必须对当前手势在视图中得偏移量清零
                [panGestureReconginzer setTranslation:CGPointZero inView:self.view];
                
            
        }}
    
}
//双击还原
- (void)doubleTap{
    if (_isAppear) {
        [self misSet];
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (!_isTransverse) {
        _intNum = YES;
        Layout.pointY = _scrollView.contentOffset.y;
        Layout.pointX = _scrollView.contentOffset.x;
        
        if (!_isLandscape) {
            if (!_isTransverse) {
                Layout.pointChangeY = _doublePoint.y;
                Layout.pointChangeX = _doublePoint.x;
            }else{
                Layout.pointChangeY = ScreenSizeHeight/2.0;
                Layout.pointChangeX = ScreenSizeWidth/2.0;
            }
        }else{
            Layout.pointChangeY = ScreenSizeWidth/2.0;
            Layout.pointChangeX = ScreenSizeHeight/2.0;
        }
        
        if (_zoomInOrOut) {
            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.0002 target:self selector:@selector(zoomOutView:) userInfo:nil repeats:YES];
            [timer setFireDate:[NSDate distantPast]];
        }else{
            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.0005 target:self selector:@selector(reductionView:) userInfo:nil repeats:YES];
            [timer setFireDate:[NSDate distantPast]];
        }
    }else{
        GPCollectionCell *cell = _scrollView.visibleCells[0];
        if (_zoomInOrOut){
            [cell.contentScrollView setZoomScale:20 animated:YES];
            _zoomInOrOut = NO;
        }else{
            [cell.contentScrollView setZoomScale:1.0 animated:YES];
            _zoomInOrOut = YES;
        }
    }
}
//双击缩小
-(void)reductionView:(NSTimer *)timer{
    _zoomInOrOut = YES;
    if (_ii==0) {
        _ii = 0.04;
    }
    if (_intNum) {
        _ii = (_currentScale - 1.0)/100.0;
        _intNum = NO;
    }
    if (_currentScale>1) {
        Layout.scale = _currentScale-_ii;
        _currentScale = _currentScale-_ii;
        _lastScale = _currentScale-_ii;
    }else if (_currentScale<=1){
        Layout.scale = 1;
        [Layout invalidateLayout];
        _currentScale = 1.0;
        Layout.pointScale = 1.0;
        _lastScale = 1.0;
        [timer invalidate];
    }
}
//双击放大
-(void)zoomOutView:(NSTimer *)timer{
    _zoomInOrOut = NO;
    if (_ii==0) {
        _ii = 0.03;
    }
    if (_intNum) {
        _ii = 1.0/100.0;
        _intNum = NO;
    }
    if (_currentScale<2) {
        Layout.scale = _currentScale+_ii;
        _currentScale = _currentScale+_ii;
        _lastScale = _currentScale+_ii;
        
    }else if (_currentScale>=2){
        Layout.scale = 2;
        [Layout invalidateLayout];
        _currentScale = 2.0;
        Layout.pointScale = 2.0;
        _lastScale = 2.0;
        [timer invalidate];
    }
}
//捏合缩放
- (void)pinchGestureRecognizer:(UIPinchGestureRecognizer *)pinchGes{
    _isZero = NO;
    if (pinchGes.state == UIGestureRecognizerStateBegan) {
        Layout.pointChangeY = ([pinchGes locationOfTouch:0 inView:pinchGes.view].y+[pinchGes locationOfTouch:1 inView:pinchGes.view].y)/2.0-_scrollView.contentOffset.y;
        Layout.pointChangeX = (([pinchGes locationOfTouch:0 inView:pinchGes.view].x+[pinchGes locationOfTouch:1 inView:pinchGes.view].x)/2.0-_scrollView.contentOffset.x);
        Layout.pointY = _scrollView.contentOffset.y;
        Layout.pointX = _scrollView.contentOffset.x;
        NSLog(@"=====%f,%f,%f,%f", Layout.pointChangeY, Layout.pointChangeX,Layout.pointY,Layout.pointX);
    }else if (pinchGes.state == UIGestureRecognizerStateChanged) {
        _scale = [pinchGes scale]+_currentScale-1;
        if (_scale < _currentScale) {
            if (_scale>=1) {
                _scale = [pinchGes scale] - (_currentScale - _scale)*1.5+_currentScale-1;
            }else{
                _scale = [pinchGes scale] + (_currentScale - _scale)/2.5+_currentScale-1;
            }
        }
        if (_scale>=0.6) {
            Layout.scale = _scale;
                _lastScale = _scale;
        }
    }else if(pinchGes.state == UIGestureRecognizerStateEnded){
        _currentScale = _lastScale;
        Layout.pointScale = _currentScale;
        _isZero = YES;
        _zoomInOrOut = NO;
        if (_lastScale>2) {
            Layout.pointY = _scrollView.contentOffset.y;
            Layout.pointX = _scrollView.contentOffset.x;
            _intNum = YES;
            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.0002 target:self selector:@selector(zoomOutToTwo:) userInfo:nil repeats:YES];
            [timer setFireDate:[NSDate distantPast]];
        }else if (_lastScale<1){
            _zoomInOrOut = YES;
            Layout.pointY = _scrollView.contentOffset.y;
            Layout.pointX = _scrollView.contentOffset.x;
            _intNum = YES;
            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.0002 target:self selector:@selector(zoomOutToOne1:) userInfo:nil repeats:YES];
            [timer setFireDate:[NSDate distantPast]];
        }
    }
}
-(void)zoomOutToOne1:(NSTimer *)timer{
    if (_ii==0) {
        _ii = 0.04;
    }
    if (_intNum) {
        _ii = (_currentScale - 1.0)/100.0;
        _intNum = NO;
    }
    
    if (_currentScale<1) {
        Layout.scale = _currentScale-_ii;
        _currentScale = _currentScale-_ii;
        _lastScale = _currentScale-_ii;
        
    }else if (_currentScale>=1){
        Layout.scale = 1;
        [Layout invalidateLayout];
        _currentScale = 1.0;
        Layout.pointScale = 1.0;
        _lastScale = 1.0;
        [timer invalidate];
    }
}

-(void)zoomOutToTwo:(NSTimer *)timer{
    if (_ii==0) {
        _ii = 0.04;
    }
    if (_intNum) {
        _ii = (_currentScale - 2.0)/100.0;
        _intNum = NO;
    }
    
    if (_currentScale>2) {
        Layout.scale = _currentScale-_ii;
        _currentScale = _currentScale-_ii;
        _lastScale = _currentScale-_ii;
        
    }else if (_currentScale<=2){
        Layout.scale = 2;
        [Layout invalidateLayout];
        _currentScale = 2.0;
        Layout.pointScale = 2.0;
        _lastScale = 2.0;
        [timer invalidate];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _jump = _scrollView.contentOffset.y;
   
//    NSLog(@"===%f====%f",_scrollView.contentOffset.x,_scrollView.contentOffset.y);
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
//        [_scrollView tableViewDidScroll:scrollView];
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
        
        if (_isZero) {
            if (scrollView.contentOffset.x <= 0){
                CGPoint offset = scrollView.contentOffset;
                offset.x = 0;
                scrollView.contentOffset = offset;
            }else if (scrollView.contentOffset.x + ScreenSizeWidth>= ScreenSizeWidth*_currentScale){
                CGPoint offset = scrollView.contentOffset;
                offset.x = ScreenSizeWidth*_currentScale - ScreenSizeWidth;
                scrollView.contentOffset = offset;
            }
        }
        
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        
    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGSize size1 = _scrollView.contentSize;
    CGFloat maximumOffset = size1.height;

    if ([scrollView isKindOfClass:[UITableView class]]) {
//        [_scrollView tableViewDidEndDragging:scrollView];
       
        
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
        
        if (_isTransverse) {
            if (_scrollView.contentOffset.x>=size1.width - KScreenWidth ) {
                [self loadMoreNextData];
            }else if (_scrollView.contentOffset.x < 0){
                [self loadMoreData];
            }
            
        }else{
            if (_scrollView.contentOffset.y>=maximumOffset - KScreenheight ) {
                [self loadMoreNextData];
            }else if (_scrollView.contentOffset.y < 0){
                [self loadMoreData];
            }
        }
        if (_isAppear) {
            [self misSet];
        }
        
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        if (_isAppear) {
            [self misSet];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
        
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        
    }
}


-(void)initImageData:(NSNotification *)noti{
    _urlStr = noti.object;
    NSArray *urlArr = [_urlStr componentsSeparatedByString:@"&"];
    NSString *chapterStr = [urlArr[1] substringFromIndex:10];
    _isDownLoad = [[DataBaseHelper shared] isExistsCartoonId:chapterStr];
    if (_isDownLoad) {
      _dataArr = [[DataBaseHelper shared] fetchAlbumsWithChapterId:chapterStr];
        _nextChapterId = [_dataArr[0] nextChapterId];
        _lastChapterId = [_dataArr[0] lastChapterId];
        _nowCartoonId = [_dataArr[0] cartoonId];
        Layout.isHeng = _isTransverse;
        Layout.cellHeightArr = _dataArr;
        [_scrollView reloadData];
    }else{
        [self analysisData];
    }
}

-(void)analysisData{
//    [YK_API_request startLoad:_urlStr extraParams:@{} object:self action:@selector(asyRequire:) method:GETDATA];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:_urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        _commentListArr = backData[@"results"];
//        
//        for (NSDictionary *dict in _commentListArr) {
//            CommentModel *model = [[CommentModel alloc]init];
//            [model setValuesForKeysWithDictionary:dict];
//            [_dataArr addObject:model];
//        }
//        [_tableView reloadData];
//        _isLoadComment = NO;
        [self asyRequire:backData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

- (void)asyRequire:(NSDictionary *)backData{
    
    NSMutableArray *cartoonInfoArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"id":_allInfo[0]} andChooseType:@"lastReadTime"];
    CartoonInfoAll *cartoonAll;
    if (cartoonInfoArr.count > 0) {
        cartoonAll = cartoonInfoArr[0];
    }
    _netImageView.alpha = 0;
    if (backData) {
        _timer = [NSTimer scheduledTimerWithTimeInterval: 1
                                                  target: self
                                                selector: @selector(incrementCounter)
                                                userInfo: nil
                                                 repeats: YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        
        _extraInfoDict = [backData[@"extraInfo"] mutableCopy];
        _cartoonAllInfo = [[CartoonInfoAll alloc] init];
        
        [_downInfoDict addEntriesFromDictionary: [backData[@"extraInfo"] mutableCopy]];
        [_upInfoDict setDictionary: [backData[@"extraInfo"] mutableCopy]];
        _lastChapterId = _upInfoDict[@"lastChapterId"];
        _nextChapterId = _downInfoDict[@"nextChapterId"];
        _nowCartoonId = backData[@"results"][0][@"cartoonId"];
        //    NSArray *arr = backData[@"results"];
        _dataArr = [cartoonDetailModel parasArr:backData];
        Layout.cellHeightArr = _dataArr;
        
        _pageNumControl.minimumValue = 1;
        _pageNumControl.maximumValue = [_extraInfoDict[@"countTotal"] integerValue];
        _pageNumLabel.text = [NSString stringWithFormat:@"%@/%@",@"1",_extraInfoDict[@"countTotal"]];
        [_scrollView reloadData];
        
        
        if (cartoonAll.currentReadAlbumId) {
            
            if ([cartoonAll.currentReadAlbumId integerValue] >= [[_dataArr[0] id] integerValue] && [cartoonAll.currentReadAlbumId integerValue] <= [[_dataArr.lastObject id] integerValue]) {
                NSUInteger ii[2] = {0,[cartoonAll.currentReadAlbumId integerValue] - [[_dataArr[0] id] integerValue]};
                NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:ii length:2];
//                [_scrollView scrollToRowAtIndexPath:indexPath
//                                   atScrollPosition:UITableViewScrollPositionTop animated:NO];
                [_scrollView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
                _pageNumControl.value = [cartoonAll.currentReadAlbumId integerValue] - [[_dataArr[0] id] integerValue];
                
            }
        }
        
        [self getChoose];
    }else{
        [_refreshBtn2 removeFromSuperview];
        [_refreshBtn1 removeFromSuperview];
        
        _netImageView.alpha = 1;
        _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight);
        _refreshBtn0 = [CustomTool createBtn];
        _refreshBtn0.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 80, 100, 30);
        [_netImageView addSubview:_refreshBtn0];
        [_refreshBtn0 addTarget:self action:@selector(analysisData) forControlEvents:UIControlEventTouchUpInside];
    }
}
                       
-(void)addNoti{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"continueLu1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"content" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"readDetails" object:nil];
    
}

-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"misAll" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"giveLoad" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"content" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"idNum" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cartoonNum" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"continueLu1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"readDetails" object:nil];
}

- (void)pullingTableViewDidStartRefreshing:(PullingRefreshTableView *)tableView {
    [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.2f];
}

- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView {
    [self performSelector:@selector(loadMoreNextData) withObject:nil afterDelay:0.2f];
}



#pragma mark loadMoreData
- (void)loadMoreNextData{
    [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"d" to_page:@"cr" to_section:@"c" to_step:@"d" type:@"move" id:_nextChapterId];

    
    if ([[DataBaseHelper shared] isExistsCartoonId:_nextChapterId]) {//下一话
        [_dataArr addObjectsFromArray:[[DataBaseHelper shared] fetchAlbumsWithChapterId:_nextChapterId]];
        
        _nextChapterId  = [_dataArr.lastObject nextChapterId];
        [_scrollView reloadData];
//        [_scrollView tableViewDidFinishedLoading];
        
        return;
    }else{
        _markCountPage ++;
        _isDownLoad = NO;
        _upInfoDict[@"nextChapterId"] = _nextChapterId;
        
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"]&&_markCountPage == 1){
        UIAlertView *nullWatch = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"当前为非WIFI网络,继续阅读将消耗流量。是否继续阅读" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续阅读", nil];
        nullWatch.tag = 1;
        [nullWatch show];
        return;
    }
    
    if (_upInfoDict[@"nextChapterId"] != [NSNull null] && [_upInfoDict[@"nextChapterId"] length] != 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSString *loadMoreUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_upInfoDict[@"nextChapterId"]?_upInfoDict[@"nextChapterId"]: _extraInfoDict[@"nextChapterId"]];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:loadMoreUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            _markCountPage = 0;
            id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            _upInfoDict = backData[@"extraInfo"];
            _nextChapterId = _upInfoDict[@"nextChapterId"];
            NSArray *arr = backData[@"results"];
            NSMutableArray *cartoonModelArray = [cartoonDetailModel parasArr:backData];
            
            NSInteger countData = _dataArr.count;
            for(int i = 0;i < cartoonModelArray.count;i ++){
                [_dataArr addObject:cartoonModelArray[i]];
            }
            Layout.isHeng = _isTransverse;
            Layout.cellHeightArr = _dataArr;
            
            NSMutableArray *indexpaths = [[NSMutableArray alloc] init];
            [_scrollView reloadData];
            
            NSIndexPath *dexp = [NSIndexPath indexPathForItem:countData inSection:0];
            if(_isTransverse){
                [_scrollView scrollToItemAtIndexPath:dexp atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
                
            }else{
                [_scrollView scrollToItemAtIndexPath:dexp atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            }
            
//            [_scrollView tableViewDidFinishedLoading];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

            [_refreshBtn0 removeFromSuperview];
            [_refreshBtn2 removeFromSuperview];
            
            _netImageView.alpha = 1;
            _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight);
            _refreshBtn1 = [CustomTool createBtn];
            _refreshBtn1.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 80, 100, 30);
            [_netImageView addSubview:_refreshBtn1];
            [_refreshBtn1 addTarget:self action:@selector(loadMoreNextData) forControlEvents:UIControlEventTouchUpInside];
        }];
    }else{
        if (_showNoMore) {
            _showNoMore = NO;
            UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-100, ScreenSizeHeight - 150, 200, 45)];
            tipView.text = @"没有更多章节了哦!";
            tipView.layer.masksToBounds = YES;
            tipView.textAlignment = 1;
            tipView.textColor = [UIColor whiteColor];
            tipView.layer.cornerRadius = 22.5;
            tipView.backgroundColor = [UIColor blackColor];
            tipView.alpha = 0.4;
            [self.view addSubview:tipView];
            [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:1.0];
        }
        
        CGFloat offset = _scrollView.contentSize.height - _scrollView.bounds.size.height;
        if (offset > 0)
        {
            [_scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
        }
    }
}

-(void)timerFireMethod:(NSTimer *)theTimer{
    UIAlertView *promptAlert = (UIAlertView *)[theTimer userInfo];
    [promptAlert dismissWithClickedButtonIndex:0 animated:NO];
    promptAlert = NULL;
}

- (void)loadMoreData{
    [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"d" to_page:@"cr" to_section:@"c" to_step:@"d" type:@"move" id:_lastChapterId];
    
    _netImageView.alpha = 0;
    if ([[DataBaseHelper shared] isExistsCartoonId:_lastChapterId]) {//上一话
        NSArray *cartoonModelArray = [[DataBaseHelper shared] fetchAlbumsWithChapterId:_lastChapterId];
        NSRange range = NSMakeRange(0, [cartoonModelArray count]);
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
        
        cartoonDetailModel *cartoonDetail = cartoonModelArray[0];
        _lastChapterId = cartoonDetail.lastChapterId;
        
        [ _dataArr insertObjects:cartoonModelArray atIndexes:indexSet];
        [_scrollView reloadData];
        
        return;
    }else{
        _markCountPage ++;
        _isDownLoad = NO;
        _downInfoDict[@"lastChapterId"] = _lastChapterId;
        
    }
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"]&&_markCountPage == 1){
        UIAlertView *nullWatch = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"当前为非WIFI网络,继续阅读将消耗流量。是否继续阅读" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续阅读", nil];
        nullWatch.tag = 0;
        [nullWatch show];
        return;
        
    }
    
    
//    if (_lastChapterId) {
//       NSArray *cartoonModelArray = [[DataBaseHelper shared] fetchAlbumsWithChapterId:_lastChapterId];
//
//        NSRange range = NSMakeRange(0, [cartoonModelArray count]);
//        
//        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
//        [ _dataArr insertObjects:cartoonModelArray atIndexes:indexSet];
//        [_scrollView reloadData];
//        _lastChapterId = [_dataArr[0] lastChapterId];
//        [_scrollView tableViewDidFinishedLoading];
//        return;
//    }
    
    if (_downInfoDict[@"lastChapterId"] != [NSNull null] && [_downInfoDict[@"lastChapterId"] length] != 0) {
        NSString *  loadMoreUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_downInfoDict[@"lastChapterId"]];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:loadMoreUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            _markCountPage = 0;
            id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            _downInfoDict = backData[@"extraInfo"];
            _lastChapterId = _downInfoDict[@"lastChapterId"];
            
            NSArray *arr = backData[@"results"];
            NSMutableArray *cartoonModelArray = [cartoonDetailModel parasArr:backData];
            //        _pageNumControl.minimumValue = 1;
            //        _pageNumControl.maximumValue = [_downInfoDict[@"countTotal"] integerValue];
            //        [_pageNumControl setValue:cartoonModelArray.count animated:NO];
            //        _pageNumLabel.text = [NSString stringWithFormat:@"%@/%@",_downInfoDict[@"countTotal"],_downInfoDict[@"countTotal"]];
            
            
            NSRange range = NSMakeRange(0, [cartoonModelArray count]);
            
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
            [ _dataArr insertObjects:[cartoonDetailModel parasArr:backData] atIndexes:indexSet];
            NSMutableArray *indexpaths = [[NSMutableArray alloc] init];
            for (int i = 0; i < cartoonModelArray.count; i ++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [indexpaths addObject:indexPath];
            }
            Layout.cellHeightArr = _dataArr;
            
            
            [_scrollView insertItemsAtIndexPaths:indexpaths];
//            [_scrollView insertRowsAtIndexPaths:indexpaths withRowAnimation:UITableViewRowAnimationFade];
            
            NSUInteger ii[2] = {0, cartoonModelArray.count - 1};
            NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:ii length:2];
//            [_scrollView scrollToRowAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            if(_isTransverse){
                [_scrollView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
                
            }else{
                [_scrollView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            [_scrollView tableViewDidFinishedLoading];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

            [_refreshBtn0 removeFromSuperview];
            [_refreshBtn1 removeFromSuperview];
            
            _netImageView.alpha = 1;
            _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight);
            _refreshBtn2 = [CustomTool createBtn];
            _refreshBtn2.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 80, 100, 30);
            [_netImageView addSubview:_refreshBtn2];
            [_refreshBtn2 addTarget:self action:@selector(loadMoreData) forControlEvents:UIControlEventTouchUpInside];
        }];
    }else{
        if (_showNoMore) {
            _showNoMore = NO;
            UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-100, ScreenSizeHeight - 150, 200, 45)];
            tipView.text = @"没有更多章节了哦!";
            tipView.layer.masksToBounds = YES;
            tipView.textAlignment = 1;
            tipView.textColor = [UIColor whiteColor];
            tipView.layer.cornerRadius = 22.5;
            tipView.backgroundColor = [UIColor blackColor];
            tipView.alpha = 0.4;
            [self.view addSubview:tipView];
            [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:1.0];
        }
        
        [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, 0) animated:YES];
    }
}

-(void)rightBtnCliked:(UIButton *)btn{
    if(btn.tag == 1001){
        [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"a" to_page:@"cn" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
        UILabel *label = (UILabel *)[_rightView viewWithTag:1002];
        
        if (_SUN) {
            [btn setImage:[UIImage imageNamed:@"baitian"] forState:UIControlStateNormal];
            label .text = @"正常";
            _lightSlider.maximumValue = 0.6;
            _lightSlider.minimumValue = 0.2;
            _lightSlider.value = _lightNum-0.4;
            _lightNum = _lightSlider.value;
            _OverView.alpha =1-_lightSlider.value ;
            _SUN = NO;
            
        }else{
            
            [btn setImage:[UIImage imageNamed:@"yejian"] forState:UIControlStateNormal];
            label .text = @"夜间";
            _lightSlider.maximumValue = 1.0;
            _lightSlider.minimumValue = 0.6;
            _lightSlider.value = _lightNum + 0.4;
            _lightNum = _lightSlider.value;
            _OverView.alpha = 1 - _lightSlider.value ;
            _SUN = YES;
        }
        
        
    }else if(btn.tag == 1000){
        if(_isChoose){
            _selectScrollView.hidden = YES;
             _selectBgImageView.hidden = YES;
            _isChoose = !_isChoose;
        }else{
            
            _selectScrollView.hidden = NO;
             _selectBgImageView.hidden = NO;
            _isChoose = !_isChoose;
        }
        
    }
}

-(void)bottonBtnClicked:(UIButton *)btn{
    if (btn.tag == 10003){
        [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"a" to_page:@"sf" to_section:@"c" to_step:@"a" type:@"'" id:@"0"];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        FeedbackViewController *feedbackView = [[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
        UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
        feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:nac animated:YES completion:^{
        }];
    }else if (btn.tag == 10001){
        UILabel *label = (UILabel *)[_bottonView viewWithTag:1002];
        [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"a" to_page:@"cm" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
        
        if (!_isLandscape) {
            
            NSIndexPath *visCell = [_scrollView indexPathsForVisibleItems][0];
            NSIndexPath *lastCell;
            if (visCell.item - 1 >= 0) {
                lastCell = [NSIndexPath indexPathForItem:visCell.item - 1 inSection:0];
            }else{
                lastCell = [NSIndexPath indexPathForItem:visCell.item  inSection:0];
            }
            
            if (_isTransverse) {
                _isTransverse = NO;
                [MBProgressHUD showSuccess:@"垂直模式" toView:self.view];
                [_scrollView addGestureRecognizer:pinGes];

                Layout.isHeng = _isTransverse;
                
//                Layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//                Layout.minimumLineSpacing = 0;
//                Layout.minimumInteritemSpacing = 0;
                _scrollView.collectionViewLayout = Layout;
                _scrollView.pagingEnabled = NO;
//                [_scrollView setNeedsLayout];
//                NSIndexPath *inde = [NSIndexPath indexPathForItem:_jumpIndex.item-2 inSection:0];
                [_scrollView reloadData];
                [_scrollView scrollToItemAtIndexPath:lastCell atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
//
                label.text = @"横向移动";
//                [_leftControl removeFromSuperview];
//                [_rightControl removeFromSuperview];                
            }else {
                _isTransverse = YES;
                [_scrollView removeGestureRecognizer:pinGes];
                [MBProgressHUD showSuccess:@"横向模式" toView:self.view];
//                Layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//                Layout.minimumLineSpacing = 0;
//                Layout.minimumInteritemSpacing = 0;
                Layout.isHeng = _isTransverse;
                _scrollView.collectionViewLayout = Layout;
                _scrollView.pagingEnabled = YES;
//                [_scrollView setNeedsDisplay];
//                NSIndexPath *inde = [NSIndexPath indexPathForItem:_jumpIndex.item-2 inSection:0];
//                _scrollView.visibleCells[0]
                [_scrollView reloadData];
                [_scrollView scrollToItemAtIndexPath:lastCell atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
//                for (UIView *view in _scrollView.visibleCells[0].subviews) {
//                    NSLog(@"view%@",view);
//                }
 //                [_scrollView scrollToItemAtIndexPath:inde atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
                
                
                label.text = @"垂直移动";
//                [self.view addSubview:_leftControl];
//                [self.view addSubview:_rightControl];
            }
            btn.selected = !btn.selected;
        }else{
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
            [self.view addSubview:view];
            [MBProgressHUD showError:@"需竖屏" toView:view];
            view.tag = 10010;
            view.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
            [self performSelector:@selector(killView) withObject:nil afterDelay:1];
        }//        [_scrollView setNeedsLayout];
    }else if (btn.tag == 10000){
        [[LogHelper shared] writeToFilefrom_page:@"cr" from_section:@"c" from_step:@"a" to_page:@"cs" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
        _isLandscape = !_isLandscape;
        UILabel *label = (UILabel *)[_bottonView viewWithTag:1001];
        UILabel *label1 = (UILabel *)[_bottonView viewWithTag:1002];
        UIButton *button1 = (UIButton *)[_bottonView viewWithTag:10001];
        UIButton *button = (UIButton *)[_topView viewWithTag:200];
        if (_isLandscape) {
            //横屏
//            Layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//            Layout.minimumLineSpacing = 0;
//            Layout.minimumInteritemSpacing = 0;
            _scrollView.collectionViewLayout = Layout;
            _scrollView.pagingEnabled = NO;
            //            [_scrollView scrollToItemAtIndexPath:_jumpIndex atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            label1.text = @"横向移动";
            
//            [_leftControl removeFromSuperview];
//            [_rightControl removeFromSuperview];
            button1.selected = NO;
            _isTransverse = NO;
//            button.hidden = YES;
            label.text = @"竖屏";
            fatherScr.contentSize = CGSizeMake(KScreenheight,0);
            _scrollView.frame = CGRectMake(0, 0, KScreenheight, KScreenWidth);
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
            [UIView animateWithDuration:0.2 animations:^{
                _tagView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
                _tagView.center = CGPointMake(KScreenWidth-15, KScreenheight-80);
                _nameLabel.center = CGPointMake(_topView.size.width/2 + KScreenWidth/2 - 50, _topView.size.height - 15);
                
//                _selectScrollView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
//                _selectScrollView.center = CGPointMake(KScreenWidth/2+20, KScreenheight/2);
                
                _scrollView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
                _scrollView.center = CGPointMake(KScreenWidth/2, KScreenheight/2);
                [_scrollView setContentOffset:CGPointMake(0, _jump*KScreenheight/KScreenWidth) animated:YES];
                
                _topView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
                _topView.center = CGPointMake(KScreenWidth - 16, KScreenheight/2-146);//667
                
                button.center = CGPointMake(KScreenheight-25, 45);
                
                _leftView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
                _leftView.center = CGPointMake(KScreenWidth/2-22, 25);
                
                _rightView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
                _rightView.center = CGPointMake(KScreenWidth/2, KScreenheight - 30);
                
                if (self.view.frame.size.height<=569 && self.view.frame.size.height>=500) {
                    _topView.center = CGPointMake(KScreenWidth - 16, KScreenheight/2-124);
                    _leftView.center = CGPointMake(KScreenWidth/2+20, 25);
                    _leftLabel.center = CGPointMake(25, _lightSlider.size.height-40);
                }else if (self.view.frame.size.height <= 481){
                    _topView.center = CGPointMake(KScreenWidth - 16, KScreenheight/2-80);
                    _leftView.center = CGPointMake(KScreenWidth/2+20, 25);
                    _leftLabel.center = CGPointMake(25, _lightSlider.size.height-40);
                }else if (self.view.frame.size.height >= 736){
                    _topView.center = CGPointMake(KScreenWidth - 16, KScreenheight/2-161.3);
                }
                
                _bottonView.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
                _bottonView.center = CGPointMake(42.5, KScreenheight/2);
                
                
                Layout.isHengPin = _isLandscape;
                Layout.isHeng = NO;
                _scrollView.collectionViewLayout = Layout;

                [_scrollView reloadData];
            }];
            CGRect frameTop = _topView.frame;
            frameTop.size.height = KScreenheight;
            _topView.frame = frameTop;
            if (self.view.frame.size.height<=569 ) {
                CGRect frameLeft = _leftView.frame;
                frameLeft.size.width -= 80;
                _leftView.frame = frameLeft;
                
                CGRect frameSlider = _lightSlider.frame;
                frameSlider.size.height -= 80;
                _lightSlider.frame = frameSlider;
            }
            
        }else{
            label.text = @"横屏";
            fatherScr.contentSize = CGSizeMake( KScreenWidth,0);
            _scrollView.frame = CGRectMake(0, 0, KScreenheight, KScreenWidth);
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            
            [UIView animateWithDuration:0.2 animations:^{
                
                _topView.transform = CGAffineTransformMakeRotation(M_PI*0);
                _leftView.transform = CGAffineTransformMakeRotation(M_PI*0);
                _bottonView.transform = CGAffineTransformMakeRotation(M_PI*0);
                _rightView.transform = CGAffineTransformMakeRotation(M_PI*0);
                _tagView.transform = CGAffineTransformMakeRotation(M_PI*0);
                button.transform = CGAffineTransformMakeRotation(M_PI*0);
                button.frame = CGRectMake(KScreenWidth - 40, 30, 30, 30);
//                _selectScrollView.transform = CGAffineTransformMakeRotation(M_PI*0);
                [_scrollView setContentOffset:CGPointMake(0, _jump*KScreenWidth/KScreenheight) animated:YES];
                _topView.frame = CGRectMake(0, 0, KScreenWidth, 64);
                _bottonView.frame = CGRectMake(0, KScreenheight-85, KScreenWidth, 85);//HHEIGHT/6
                _leftView.frame = CGRectMake(0, 90 * __Scale, 50, 300);
                _rightView.frame = CGRectMake(KScreenWidth - 60, KScreenheight/2-90, 60, 180);
                _tagView.center = CGPointMake(KScreenWidth-74, 10);
                _nameLabel.center = CGPointMake(_topView.size.width/2, _topView.size.height - 15);
                
                _scrollView.transform = CGAffineTransformMakeRotation(M_PI*0);
                _scrollView.center = CGPointMake(KScreenWidth/2, KScreenheight/2);
                
                if (self.view.frame.size.height<=569 && self.view.frame.size.height>=500) {
                    _leftLabel.center = CGPointMake(25, _leftView.size.height - 20);
                }else if (self.view.frame.size.height <= 481){
                    _leftLabel.center = CGPointMake(25, _leftView.size.height - 20);
                }
                Layout.isHengPin = _isLandscape;
                Layout.isHeng = NO;
                _scrollView.collectionViewLayout = Layout;

                [_scrollView reloadData];
            }];
            
            if (self.view.frame.size.height<=569 ) {
                CGRect frameSlider = _lightSlider.frame;
                frameSlider.size.height += 80;
                _lightSlider.frame = frameSlider;
            }
        }
    }else if (btn.tag == 10002){
        UILabel *label = (UILabel *)[_bottonView viewWithTag:1003];
        if (!_isLandscape) {
            if (_isTransverse) {
                if (_isLeftRight) {
                    label.text = @"右手翻页";
                    [MBProgressHUD showSuccess:@"左手翻页模式" toView:self.view];
                }else{
                    [MBProgressHUD showSuccess:@"右手翻页模式" toView:self.view];
                    label.text = @"左手翻页";
                }
                _isLeftRight = !_isLeftRight;
                btn.selected = !btn.selected;
            }else{
                [MBProgressHUD showError:@"需横向移动" toView:self.view];
            }
        }else{
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
            [self.view addSubview:view];
            [MBProgressHUD showError:@"需竖屏" toView:view];
            view.tag = 10010;
            view.transform = CGAffineTransformMakeRotation(M_PI*-1.5);
            [self performSelector:@selector(killView) withObject:nil afterDelay:1];        }
    }else if (btn.tag == 10004){
        if (_isLandscape) {
            _helpTransverseView.alpha = 1;
        }else{
            _helpView.alpha = 1;
        }
        [self misSet];
        [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }

}


-(void)killView{
    UIView *view = (UIView *)[self.view viewWithTag:10010];
    [view removeFromSuperview];
}

- (void)setCartoonId:(NSString *)cartoonId WithChapterId:(NSString *)chapterId{
//    NSMutableArray *albumArr = [[DataBaseHelper shared] fetchAlbumsWithCartoonId:cartoonId andChapterId:chapterId];
    
}

#pragma mark alertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 0) {
        if (buttonIndex == 1) {
            if (_downInfoDict[@"lastChapterId"] != [NSNull null]) {
                NSString *  loadMoreUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_downInfoDict[@"lastChapterId"]];
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                [manager GET:loadMoreUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    _markCountPage = 0;
                    id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                    //        NSLog(@"=======%@",backData);
                    
                    _downInfoDict = backData[@"extraInfo"];
                    _lastChapterId = _downInfoDict[@"lastChapterId"];
                    
                    NSArray *arr = backData[@"results"];
                    NSMutableArray *cartoonModelArray = [cartoonDetailModel parasArr:backData];
                    
                    //        _pageNumControl.minimumValue = 1;
                    //        _pageNumControl.maximumValue = [_downInfoDict[@"countTotal"] integerValue];
                    //        [_pageNumControl setValue:cartoonModelArray.count animated:NO];
                    //        _pageNumLabel.text = [NSString stringWithFormat:@"%@/%@",_downInfoDict[@"countTotal"],_downInfoDict[@"countTotal"]];
                    
                    
                    NSRange range = NSMakeRange(0, [cartoonModelArray count]);
                    
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
                    [ _dataArr insertObjects:[cartoonDetailModel parasArr:backData] atIndexes:indexSet];
                    NSMutableArray *indexpaths = [[NSMutableArray alloc] init];
                    for (int i = 0; i < cartoonModelArray.count; i ++) {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                        [indexpaths addObject:indexPath];
                    }
//                    [_scrollView insertRowsAtIndexPaths:indexpaths withRowAnimation:UITableViewRowAnimationFade];
                    
                    NSUInteger ii[2] = {0, cartoonModelArray.count - 1};
                    NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:ii length:2];
//                    [_scrollView scrollToRowAtIndexPath:indexPath
//                                       atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                    [_scrollView tableViewDidFinishedLoading];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                }];
            }else{
//                [_scrollView tableViewDidFinishedLoading];
                if (_showNoMore) {
                    _showNoMore = NO;
                    UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-100, ScreenSizeHeight - 150, 200, 45)];
                    tipView.text = @"没有更多章节了哦!";
                    tipView.layer.masksToBounds = YES;
                    tipView.textAlignment = 1;
                    tipView.textColor = [UIColor whiteColor];
                    tipView.layer.cornerRadius = 22.5;
                    tipView.backgroundColor = [UIColor blackColor];
                    tipView.alpha = 0.4;
                    [self.view addSubview:tipView];
                    [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:1.0];
                }
                
                
            }
        }
    }else if (alertView.tag == 1){
        if (buttonIndex == 1) {
            if (_upInfoDict[@"nextChapterId"] != [NSNull null]) {
                NSString *loadMoreUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_upInfoDict[@"nextChapterId"]?_upInfoDict[@"nextChapterId"]: _extraInfoDict[@"nextChapterId"]];
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                [manager GET:loadMoreUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    _markCountPage = 0;
                    id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                    _upInfoDict = backData[@"extraInfo"];
                    _nextChapterId = _upInfoDict[@"nextChapterId"];
//                    NSArray *arr = backData[@"results"];
                    NSMutableArray *cartoonModelArray = [cartoonDetailModel parasArr:backData];
                    
                    
                    for(int i = 0;i < cartoonModelArray.count;i ++){
                        [_dataArr addObject:cartoonModelArray[i]];
                    }
//                    NSMutableArray *indexpaths = [[NSMutableArray alloc] init];
                    [_scrollView reloadData];
//                    [_scrollView tableViewDidFinishedLoading];
                    
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                }];
            }else{
//                [_scrollView tableViewDidFinishedLoading];
                if (_showNoMore) {
                    _showNoMore = NO;
                    UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-100, ScreenSizeHeight - 150, 200, 45)];
                    tipView.text = @"没有更多章节了哦!";
                    tipView.layer.masksToBounds = YES;
                    tipView.textAlignment = 1;
                    tipView.textColor = [UIColor whiteColor];
                    tipView.layer.cornerRadius = 22.5;
                    tipView.backgroundColor = [UIColor blackColor];
                    tipView.alpha = 0.4;
                    [self.view addSubview:tipView];
                    [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:1.0];
                }
                
            }
        }
    }else if (alertView.tag == 100){
        if (buttonIndex == 1) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            [manager GET:_loadMoreUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [self misSet];
                _selectScrollView.hidden = YES;
                 _selectBgImageView.hidden = YES;
                id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                _extraInfoDict = [backData[@"extraInfo"] mutableCopy];
                [_downInfoDict setDictionary: [backData[@"extraInfo"] mutableCopy]];
                [_upInfoDict setDictionary: [backData[@"extraInfo"] mutableCopy]];
                _lastChapterId = _upInfoDict[@"lastChapterId"];
                _nextChapterId = _downInfoDict[@"nextChapterId"];
                _dataArr = [cartoonDetailModel parasArr:backData];
                
                _isDownLoad = NO;
                [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
                [_scrollView reloadData];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您的网络不给力啊" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }];
        }
    }

}
-(void)missTipView:(UILabel *)label{
    _showNoMore = YES;
    [label removeFromSuperview];
}
- (void)spring{
    
    POPSpringAnimation* framePOP = [POPSpringAnimation animationWithPropertyNamed:kPOPViewBackgroundColor];
    
    framePOP.springSpeed = 10.f;
    
    framePOP.springBounciness = 4.f;
    
    framePOP.toValue = [UIColor greenColor];
    
    [framePOP setCompletionBlock:^(POPAnimation * anim , BOOL finsih) {
        
        if (finsih) {
            
            
        }
        
    }];
    
    [_scrollView pop_addAnimation:framePOP forKey:@"go"];
    
}

#pragma mark alertDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark 已下载漫画话
-(void)setDownLoadCharpList:(NSArray *)downLoadCharpList{
    _downLoadCharpList = downLoadCharpList;
}




- (BOOL)shouldAutorotate {
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(BOOL)canBecomeFirstResponder{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


