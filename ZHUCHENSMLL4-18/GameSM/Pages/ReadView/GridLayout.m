//
//  GridLayout.m
//  GridViewDemo
//
//

#import "GridLayout.h"
//#import "GridViewCell.h"
#import "GridViewLayoutAttribute.h"
#import "cartoonDetailModel.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface GridLayout()

@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, strong) NSMutableArray *itemFrames;


@end

@implementation GridLayout


- (id)init
{
    self = [super init];
    if (self)
    {
        // set default value to properties.
        //    self.itemSize = (CGSize){200, 100};
        //    self.itemSpacing = 0;
        //    self.lineSpacing = 0;
        //    self.numberOfColumns = 10;
        self.contentSize = CGSizeZero;
        self.maxScale = 2.0f;
        self.minScale = 1.0f;
        self.scale = 1.0f;
        self.pointScale = 1.0;
        self.scalePoint = YES;
        self.initScale1 = 0;
    }
    return self;
}

#pragma mark - UICollectionViewLayout

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)oldBounds
{
    return NO;
}

- (CGSize)collectionViewContentSize {
    
    return self.contentSize;
}

- (void)prepareLayout {
    [super prepareLayout];
    [self prepareItemsLayout];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    // NSLog(@"layoutAttributesForElementsInRect x / y - %f / %f", rect.origin.x, rect.origin.y);
    NSMutableArray *attributesArray = [NSMutableArray array];
    
    for (NSInteger item = 0; item < _cellHeightArr.count; item++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        UICollectionViewLayoutAttributes *cellAttributes
        = [self layoutAttributesForItemAtIndexPath:indexPath];
        
        [attributesArray addObject:cellAttributes];
        //      NSLog(@"OK..");
        
    }
    return attributesArray;
    
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)path {
    //  NSInteger index = [path indexAtPosition:1];
    //    NSLog(@"self.itemframe%dpath.item%d",self.itemFrames.count,path.item);
    CGRect itemFrame ;
    if (self.itemFrames.count - 1>=path.item) {
        itemFrame  = [[self.itemFrames objectAtIndex:path.item] CGRectValue];
        
    }
    //  GridViewLayoutAttribute *attributes
    //  = [GridViewLayoutAttribute layoutAttributesForCellWithIndexPath:path];
    //  int column = index % self.numberOfColumns;
    //  attributes.size = [self scaledItemSize:column];
    //  attributes.center = CGPointMake(CGRectGetMidX(itemFrame), CGRectGetMidY(itemFrame));
    
    //    UICollectionViewLayoutAttributes *attr = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:path];
    
    GridViewLayoutAttribute *attributes
    = [GridViewLayoutAttribute layoutAttributesForCellWithIndexPath:path];
    //      int column = path.item % self.cellHeightArr.count;
    
    
    if (_isHeng) {
        attributes.size = CGSizeMake(itemFrame.size.width, itemFrame.size.height);
        attributes.center = CGPointMake(itemFrame.origin.x + KScreenWidth/2, KScreenheight/2);
        //        attributes.frame = itemFrame;
    }else{
        attributes.size = [self scaledItemSize:0 andCartoonDetail:_cellHeightArr[path.item]];
        attributes.center = CGPointMake(CGRectGetMidX(itemFrame), CGRectGetMidY(itemFrame));
//        attributes.center = CGPointMake(itemFrame.origin.x + itemFrame.size.width/2 , CGRectGetMidY(itemFrame));
    }
    
   
    
    if(_isHengPin){
        attributes.size = [self scaledItemSize:0 andCartoonDetail:_cellHeightArr[path.item]];
        attributes.center = CGPointMake(CGRectGetMidX(itemFrame), CGRectGetMidY(itemFrame));
        
    }
    //    attr.size = CGSizeMake(160, cellHeight);
    
    return attributes;
}


#pragma mark - Properties

- (void)setPinchPoint:(CGPoint)pinchPoint {
    _pinchPoint = pinchPoint;
}
-(void)setPointScale:(CGFloat)pointScale{
    _pointScale = pointScale;
}
-(void)setScalePoint:(BOOL)scalePoint{
    _scalePoint = scalePoint;
}
-(void)setChangeScale:(CGFloat)changeScale{
    _changeScale = changeScale;
}
-(void)setInitScale1:(CGFloat)initScale1{
    _initScale1 = initScale1;
}

- (void)setScale:(CGFloat)scale {
    
//    if(scale >= self.minScale) {
        _scale = scale;
         CGPoint point = [self.collectionView contentOffset];
//        if (_scale < _pointScale) {
//            point.y = _pointY/_pointScale*_scale +_pointChangeY*(_scale - _pointScale)/_pointScale;
//            point.x = _pointX/_pointScale*_scale +_pointChangeX*(_scale - _pointScale)/_pointScale;
//        }else{
            point.y = _pointY/_pointScale*_scale +_pointChangeY*(_scale - _pointScale)/_pointScale;
            point.x = _pointX/_pointScale*_scale +_pointChangeX*(_scale - _pointScale)/_pointScale;
//        }
//     NSLog(@"===%f",point.x);
    
        [self.collectionView setContentOffset:point];
    [self invalidateLayout];
//    }
}
-(void)setPointX:(CGFloat)pointX{
    _pointX = pointX;
}
-(void)setPointY:(CGFloat)pointY{
    _pointY = pointY;
}
-(void)setPointChangeX:(CGFloat)pointChangeX{
    _pointChangeX = pointChangeX;
}
-(void)setPointChangeY:(CGFloat)pointChangeY{
    _pointChangeY = pointChangeY;
}


- (CGSize)scaledItemSize:(NSInteger)column andCartoonDetail:(cartoonDetailModel *)cartoonModel{
    CGSize curItemSize = CGSizeMake(KScreenWidth, cartoonModel.propotion * KScreenWidth);
    if(_isHengPin){
        curItemSize.height = cartoonModel.propotion * KScreenheight * self.scale;
        curItemSize.width = KScreenheight * self.scale;
    }else{
        curItemSize.height = cartoonModel.propotion * KScreenWidth * self.scale;
        curItemSize.width = KScreenWidth * self.scale;
    }
    return curItemSize;
}


#pragma mark - Private

- (void)prepareItemsLayout
{
    self.itemFrames = [NSMutableArray array];
    
    if(_isHengPin){
        CGFloat allHeight = 0.0;
        for (int item = 0; item < _cellHeightArr.count; item++)
        {
            CGFloat leftHeight = 0.0;
            for (int i = 0; i<item; i++) {
                CGFloat h = [_cellHeightArr[i] propotion] * KScreenheight;
                leftHeight += h;
            }
            
            CGFloat left;
            if (self.scale>=1) {
                left = 0;
            }else{
                left = (KScreenheight - KScreenheight * self.scale)/2;
            }
            CGFloat top = leftHeight * self.scale;
            
            CGRect itemFrame = (CGRect){{left, top},
                [self scaledItemSize:item andCartoonDetail:_cellHeightArr[item]]} ;
            [self.itemFrames addObject:[NSValue valueWithCGRect:itemFrame]];
            
            if (item == _cellHeightArr.count - 1) {
                allHeight = leftHeight;
            }
            //            NSLog(@"itemFrame%@",NSStringFromCGRect(itemFrame));
        }
        
        
        self.contentSize = CGSizeMake(KScreenheight * self.scale,(allHeight+_cellHeightArr.lastObject.propotion * KScreenheight) * self.scale);
        return;
        
    }
    
    
    if (_isHeng) {
        for (int item = 0; item < _cellHeightArr.count; item++){
            CGFloat leftHeight = item * KScreenWidth;
            
            //            CGFloat left = 0;
            //            CGFloat top = leftHeight * self.scale;
            CGFloat singleHeight = [_cellHeightArr[item] propotion] * KScreenWidth;
            if (singleHeight > KScreenheight) {
                singleHeight = KScreenheight;
            }
            
            CGRect itemFrame = (CGRect){{leftHeight, 0},
                {KScreenWidth,KScreenheight}} ;
            [self.itemFrames addObject:[NSValue valueWithCGRect:itemFrame]];
            
        }
        
        
        self.contentSize = CGSizeMake(KScreenWidth * _cellHeightArr.count, 0);
    }else {
        CGFloat allHeight = 0.0;
        for (int item = 0; item < _cellHeightArr.count; item++)
        {
            CGFloat leftHeight = 0.0;
            for (int i = 0; i<item; i++) {
                CGFloat h = [_cellHeightArr[i] propotion] * KScreenWidth;
                leftHeight += h;
            }
            
            CGFloat left;
            if (self.scale>=1) {
                left = 0;
            }else{
                left = (KScreenWidth - KScreenWidth * self.scale)/2;
            }
            CGFloat top = leftHeight * self.scale;
            
            CGRect itemFrame = (CGRect){{left, top},
                [self scaledItemSize:item andCartoonDetail:_cellHeightArr[item]]} ;
            [self.itemFrames addObject:[NSValue valueWithCGRect:itemFrame]];
            
            if (item == _cellHeightArr.count - 1) {
                allHeight = leftHeight;
            }
        }
        if (self.scale>=1) {
            self.contentSize = CGSizeMake(KScreenWidth * self.scale,(allHeight+_cellHeightArr.lastObject.propotion * KScreenWidth) * self.scale);
        }else{
            self.contentSize = CGSizeMake(KScreenWidth * self.scale,(allHeight+_cellHeightArr.lastObject.propotion * KScreenWidth) * self.scale);
        }
    }
}

/*!
 @param newScale
 @param oldScale
 */
- (CGFloat) newScrollOffsetFor:(CGFloat)offset
                      newScale:(CGFloat)newScale
                      oldScale:(CGFloat)oldScale {
    
    return offset * (newScale / oldScale);
}

@end
