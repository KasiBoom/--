//
//  feedbackModel.h
//  GameSM
//
//  Created by 顾鹏 on 15/12/1.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface feedbackModel : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *modifyTime;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *deviceToken;
@property (nonatomic, copy) NSString *deviceID;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *contact;
@property (nonatomic, copy) NSString *replyUserID;
@property (nonatomic, copy) NSString *feedbackID;
@property (nonatomic, copy) NSString *type;
@property (nonatomic,assign) CGSize cellSize;


+ (NSMutableArray *)paraseFeedBackArr:(NSArray *)feedBackArr;
@end
