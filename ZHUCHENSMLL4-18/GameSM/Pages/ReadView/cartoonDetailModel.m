//
//  cartoonDetailModel.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/28.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "cartoonDetailModel.h"

@implementation cartoonDetailModel
+ (NSMutableArray *)parasArr:(NSDictionary *)CartoonDict{
    
    
    NSMutableArray *carMusArr = [NSMutableArray array];
    NSArray *CartoonArr = CartoonDict[@"results"];
    NSString *countTotal = CartoonDict[@"extraInfo"][@"countTotal"];
    NSString *lastChapterId = CartoonDict[@"extraInfo"][@"lastChapterId"];
    NSString *nextChapterId = CartoonDict[@"extraInfo"][@"nextChapterId"];
    NSString *currentAlbumId = CartoonDict[@"extraInfo"][@"currentAlbumId"];
NSString *shareUrlStr = CartoonDict[@"extraInfo"][@"shareUrl"];
    for (NSDictionary *cartoonDict in CartoonArr) {
        cartoonDetailModel *carModel = [[cartoonDetailModel alloc] init];
        carModel.id = cartoonDict[@"id"];
        carModel.cartoonId = cartoonDict[@"cartoonId"];
        carModel.chapterId = cartoonDict[@"chapterId"];
        carModel.images = cartoonDict[@"images"];
        carModel.imgWidth = [cartoonDict[@"imgWidth"] floatValue];
        carModel.imgHeight = [cartoonDict[@"imgHeight"] floatValue];
        carModel.mineType = cartoonDict[@"mineType"];
        carModel.priority = cartoonDict[@"priority"];
        carModel.status = cartoonDict[@"status"];
        carModel.createTime = cartoonDict[@"createTime"];
        carModel.modifyTime = cartoonDict[@"modifyTime"];
        carModel.cartoonName = cartoonDict[@"cartoonName"];
        carModel.chapterName = cartoonDict[@"chapterName"];
        carModel.chapterIndex = cartoonDict[@"chapterIndex"];
        carModel.countTotal = countTotal;
        carModel.lastChapterId = lastChapterId;
        carModel.nextChapterId = nextChapterId;
        carModel.currentAlbumId = currentAlbumId;
        carModel.shareUrl = shareUrlStr;
        [carMusArr addObject:carModel];
    }
    
    dispatch_apply(carMusArr.count, dispatch_get_global_queue(0, 0), ^(size_t n) {
        cartoonDetailModel *model = carMusArr[n];
        if (model.imgHeight&&model.imgWidth) {
            model.propotion = (float)model.imgHeight/model.imgWidth;

        }
        // NSLog(@"%f",model.cellHeight);
    });

    return carMusArr;
}
@end
