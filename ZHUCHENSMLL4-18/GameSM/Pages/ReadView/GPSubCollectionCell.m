//
//  GPSubCollectionCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/1/28.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "GPSubCollectionCell.h"
#import "UIImageView+AFNetworking.h"

@implementation GPSubCollectionCell 

- (void)setImageUrlll:(NSString *)imageUrlll andImage:(UIImage *)image andDelegate:(id)target{
    
    //    _contentLabel = [[UIImageView alloc] init];
    //        _contentLabel.image = [UIImage imageNamed:@"guanbi"];
    _target = target;
    [self.contentView addSubview:self.contentScrollView];
    [self.contentScrollView addSubview:self.contentLabel];
    //    [self.conte addSubview:self.contentLabel];
    
    if (imageUrlll) {
        [self.contentLabel setImageWithURL:[NSURL URLWithString:imageUrlll] placeholderImage:[UIImage imageNamed:@"guanbi"]];
    }else{
        self.contentLabel.image = image;
    }
    
    
}

//- (UIScrollView *)contentScrollView {
//    if (_contentScrollView == nil) {
//        CGRect frame = self.frame;
//        frame.origin.x = 0;
//        frame.origin.y = 0;
//        if (frame.size.height > [UIScreen mainScreen].bounds.size.height) {
//            frame.size.height = [UIScreen mainScreen].bounds.size.height;
//        }
//        _contentScrollView = [[UIScrollView alloc] initWithFrame:frame];
//        _contentScrollView.userInteractionEnabled = YES;
//        _contentScrollView.maximumZoomScale = 2.5;
//        _contentScrollView.minimumZoomScale = 1.0;
//        _contentScrollView.delegate = _target;
//    }
//    return _contentScrollView;
//}


- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.contentLabel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
