//
//  GPCollectionCell.h
//  collectionZoom
//
//  Created by 顾鹏 on 15/12/22.
//  Copyright © 2015年 顾鹏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "secondScrollView.h"
@interface GPCollectionCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *contentLabel;
@property (nonatomic, strong) secondScrollView *contentScrollView;
@property (nonatomic, strong) NSString *imageUrlll;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) BOOL isTran;
- (void)setImageUrlll:(NSString *)imageUrlll andImage:(UIImage *)image andHeight:(CGFloat) height andIsTran:(BOOL)isTran;
@end
