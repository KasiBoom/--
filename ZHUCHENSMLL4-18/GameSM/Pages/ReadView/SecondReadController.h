//
//  SecondReadController.h
//  GameSM
//
//  Created by 顾鹏 on 15/10/28.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SecondReadController : BaseViewController
@property(nonatomic,copy)NSArray *charpList;
@property(nonatomic,copy)NSArray *charpList1;
@property(nonatomic,copy)NSString *cartoonId;
@property(nonatomic,copy)NSArray *downLoadCharpList;
@property(nonatomic,copy)NSArray *allInfo;
@property(nonatomic,copy)void (^changeMark)(NSInteger tag);
@property (nonatomic,strong) UICollectionView *scrollView;
@end
