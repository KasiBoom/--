//
//  GPCollectionCell.m
//  collectionZoom
//
//  Created by 顾鹏 on 15/12/22.
//  Copyright © 2015年 顾鹏. All rights reserved.
//

#import "GPCollectionCell.h"
//#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "cartoonDetailModel.h"
@implementation GPCollectionCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.selectedBackgroundColor = [UIColor grayColor];
//        self.activedBackgroundColor = [UIColor whiteColor];
//        [self.contentView addSubview:self.contentLabel];
//        self.autoresizesSubviews = YES;
//        self.layer.borderColor = [UIColor grayColor].CGColor;
//        self.layer.borderWidth = 0.5f;
    }
    return self;
}

- (void)setImageUrlll:(NSString *)imageUrlll andImage:(UIImage *)image andHeight:(CGFloat) height andIsTran:(BOOL)isTran{
    
//    _contentLabel = [[UIImageView alloc] init];
    //        _contentLabel.image = [UIImage imageNamed:@"guanbi"];
    
    _height = height;
    _isTran = isTran;
//    _contentLabel.frame = CGRectMake(0, 0, self.frame.size.width, height);
    [self.contentView addSubview:self.contentScrollView];
    [self.contentScrollView addSubview:self.contentLabel];
    
//    self.contentLabel.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
//    self.contentLabel.bounds = CGRectMake(self.frame.size.width, self.frame.size.width * imageUrlll.propotion);
    if (imageUrlll) {
//        [_contentLabel setImageWithURL:[NSURL URLWithString:imageUrlll]];
        [_contentLabel sd_setImageWithURL:[NSURL URLWithString:imageUrlll] placeholderImage:nil options:1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [SDImageCache sharedImageCache].clearMemory;
        }];
    }else{
        _contentLabel.image = image;
    }
    
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //  NSLog(@"GridViewCell layoutSubviews");
    CGRect frame = self.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
//    frame.origin.x = 0;
//    frame.origin.y = 0;
//    if (frame.size.height > [UIScreen mainScreen].bounds.size.height) {
//        frame.size.height = [UIScreen mainScreen].bounds.size.height;
//    }
    if (_isTran) {
        
        frame.size.height = _height;
        _contentLabel.frame = frame;
        if (frame.size.height < [UIScreen mainScreen].bounds.size.height) {
            _contentLabel.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
            
        }

//            if (frame.size.height > [UIScreen mainScreen].bounds.size.height) {
            frame.size.height = [UIScreen mainScreen].bounds.size.height;
//            }
        _contentScrollView.frame = frame;
        
    }else {
        _contentLabel.frame = frame;
        
        _contentScrollView.frame = frame;
    }
    
}

- (UIImageView *) contentLabel {
    if(_contentLabel == nil) {
        CGRect frame = self.frame;
        
        frame.origin.x = 0;
        frame.origin.y = 0;
        _contentLabel = [[UIImageView alloc] init];
        _contentLabel.userInteractionEnabled = YES;
        
        
    }
    return _contentLabel;
}


- (secondScrollView *)contentScrollView {
    if (_contentScrollView == nil) {
        CGRect frame = self.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        if (frame.size.height > [UIScreen mainScreen].bounds.size.height) {
            frame.size.height = [UIScreen mainScreen].bounds.size.height;
        }
        _contentScrollView = [[secondScrollView alloc] initWithFrame:frame];
//        _contentScrollView.bounces = NO;
//        _contentScrollView.scrollEnabled = NO;
        _contentScrollView.showsHorizontalScrollIndicator = NO;
        _contentScrollView.showsVerticalScrollIndicator = NO;
//        _contentScrollView.backgroundColor = [UIColor redColor];
        
//        _contentScrollView.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    }
    return _contentScrollView;
}



@end
