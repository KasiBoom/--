//
//  secondScrollView.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/2/1.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "secondScrollView.h"

@implementation secondScrollView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [[self nextResponder] touchesBegan:touches withEvent:event];
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [[self nextResponder] touchesMoved:touches withEvent:event];
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{

    [[self nextResponder] touchesEnded:touches withEvent:event];

}

@end
