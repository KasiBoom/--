//
//  cartoonDetailModel.h
//  GameSM
//
//  Created by 顾鹏 on 15/10/28.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface cartoonDetailModel : NSObject
@property(nonatomic,copy)NSString *id;
@property(nonatomic,copy)NSString *cartoonId;
@property(nonatomic,copy)NSString *chapterId;

@property(nonatomic,copy)NSString *images;
@property(nonatomic,assign)NSInteger imgWidth;
@property(nonatomic,assign)NSInteger imgHeight;
@property(nonatomic,copy)NSString *mineType;
@property(nonatomic,copy)NSString *priority;
@property(nonatomic,copy)NSString *status;
@property(nonatomic,copy)NSString *createTime;
@property(nonatomic,copy)NSString *modifyTime;
@property(nonatomic,copy)NSString *cartoonName;
@property(nonatomic,copy)NSString *chapterName;
@property(nonatomic,copy)NSString *chapterIndex;
@property(nonatomic,assign)CGFloat propotion;
@property(nonatomic,copy)NSString *countTotal;
@property(nonatomic,copy)NSString *lastChapterId;
@property(nonatomic,copy)NSString *nextChapterId;
@property(nonatomic,copy)NSString *currentAlbumId;
@property(nonatomic,assign)CGFloat cellHeight;
@property (nonatomic, copy)NSString *shareUrl;
+ (NSMutableArray *)parasArr:(NSDictionary *)CartoonDict;
@end
