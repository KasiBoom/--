//
//  EditViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"
#import "CustomTextView.h"

@interface EditViewController : BaseViewController<UITextViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentBg;
@property (weak, nonatomic) IBOutlet CustomTextField *inputTextField;
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;

@property (nonatomic, strong) NSString *titleName;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *sign;
@property (nonatomic, assign) int type;     //0 单行  1 多行

- (IBAction)touchInView:(id)sender;
@end
