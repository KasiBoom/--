//
//  PointsDetailsViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/19.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface PointsDetailsViewController : BaseViewController

@property (nonatomic, strong) NSString *urlStr;

@end
