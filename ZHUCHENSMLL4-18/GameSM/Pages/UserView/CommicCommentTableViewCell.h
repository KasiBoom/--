//
//  CommicCommentTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/22.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommicCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *commicDesTextView;

@property (weak, nonatomic) IBOutlet UILabel *commicTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commicDesImage;

@end
