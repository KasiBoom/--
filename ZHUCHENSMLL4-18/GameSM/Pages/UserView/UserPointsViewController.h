//
//  UserPointsViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface UserPointsViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *myPointsTop;

@property (weak, nonatomic) IBOutlet UIImageView *myPointsBottom;

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@property (weak, nonatomic) IBOutlet UIButton *helpBtn;

- (IBAction)helpBtnHit:(id)sender;


@end
