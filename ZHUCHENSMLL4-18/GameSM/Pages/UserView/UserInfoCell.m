//
//  UserInfoCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserInfoCell.h"

@implementation UserInfoCell

+ (id)userInfoCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"UserInfoCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[UserInfoCell class]]) {
            return (UserInfoCell *)cellObject;
        }
    }
    return nil;
}

@end
