//
//  UserInfoViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"

@interface UserInfoViewController : BaseTableViewController<UITableViewDataSource, UITableViewDelegate, PullingRefreshTableViewDelegate>

@property (nonatomic, assign) int type;

@end
