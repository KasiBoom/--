//
//  UserViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "LYTabBarController.h"
#import "CustomMenuView.h"
//typedef void (^ablock)(int i);

@interface UserViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate,CustomMenuViewDelegate>

//@property (nonatomic, copy) ablock block;

- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize;

@property (strong, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIImageView *sexImageView;

@property (strong,nonatomic)CustomMenuView *titleView;
@property (strong,nonatomic)UIScrollView *scrollView;
@property (strong,nonatomic)UIImageView *dianImage;

@property (weak, nonatomic) IBOutlet UIButton *noticeBtn;
@property (weak, nonatomic) IBOutlet UIButton *userPointsBtn;

@property (weak, nonatomic) IBOutlet UIView *headBgView;
@property (weak, nonatomic) IBOutlet UILabel *bewrite;
@property (weak, nonatomic) IBOutlet UIButton *markBtn;

- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)editBtnClike:(id)sender;
- (IBAction)noticeBtnClike:(id)sender;
- (IBAction)userPointsBtnClike:(id)sender;

- (IBAction)mark:(id)sender;

-(NSString *)getCurrentTime;

@end
