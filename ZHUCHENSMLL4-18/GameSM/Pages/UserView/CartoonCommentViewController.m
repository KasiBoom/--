//
//  CartoonCommentViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/21.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "CartoonCommentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CommicCommentTableViewCell.h"
#import "testViewController.h"
@interface CartoonCommentViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView ;
    NSMutableArray  *_faceNameArr;
    UIImageView *_netImageView;
    UIImageView     *_tipImageView;
    
}
@property (nonatomic, strong) UITableView *noticeTableView;
@property (nonatomic, copy)NSMutableArray *dataArray;


@end

@implementation CartoonCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    LYTabBarController *tabBar = (LYTabBarController *)self.tabBarController;
    [tabBar hiddenBar:YES animated:YES];
    [self initTitleName:@"评论过的漫画"];
    
    
    [self.view addSubview:self.noticeTableView];
    [self addBackBtn];
    [self noticeList];

}

- (void)noticeList {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        (NSString *)API_URL_CARTOONCOMMENT,@"r",
                                        [NSString stringWithFormat:@"%d",999],@"size",
                                        [NSString stringWithFormat:@"%d",1],@"page",nil];
        
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeListFinish:) method:GETDATA];
    
}

- (void)noticeListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    ////    self.isLoading = NO;
//    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
//        
//        _netImageView.alpha = 0;
//        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
//            [self.dataArray addObject:item];
//        }
//    }
    
    self.dataArray = dic[@"results"];
    //
    [self.noticeTableView reloadData];
}


- (UITableView*)noticeTableView {
    if (!_noticeTableView) {
        _noticeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64) ];
        _noticeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //        _noticeTableView.separatorColor = [UIColor lightGrayColor];
        _noticeTableView.backgroundColor = [UIColor whiteColor];
        _noticeTableView.backgroundView = nil;
        _noticeTableView.dataSource = self;
        _noticeTableView.delegate = self;
    }
    return _noticeTableView;
}


- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommicCommentTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"CommicCommentTableViewCell" owner:self options:nil] lastObject];
//    NSDictionary *dict = self.dataArray[indexPath.row];
    NSLog(@"%@",self.dataArray[indexPath.row][@"contentValue"]);
    cell.commicDesTextView.text = self.dataArray[indexPath.row][@"contentValue"];
    [cell.commicDesImage setImageWithURL:[NSURL URLWithString:self.dataArray[indexPath.row][@"cartoonInfo"][@"images"]]];
    cell.commicTimeLabel.text = self.dataArray[indexPath.row][@"createTime"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[LogHelper shared] writeToApafrom_page:@"pcc" from_section:@"profile" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"detail" type:@"" id:@"0"];
    
    testViewController *testVC = [[testViewController alloc]init];
    testVC.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    [testVC setChangeCollection:^{
        
    }];
    [self presentViewController:testVC animated:YES completion:^{
        testVC.valueId = [self.dataArray objectAtIndex:indexPath.row][@"valueID"];
        testVC.customType = @"5";
    }];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
