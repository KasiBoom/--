//
//  UserInfoViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserInfoViewController.h"
#import "UserInfoCell.h"
#import "InfoDetailViewController.h"
#import "CustomTool.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface UserInfoViewController ()
{
    UIImageView *_netImageView;
}
@property (nonatomic, assign) BOOL isMore;

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArray = [NSMutableArray array];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor clearColor];
    
//--
    _netImageView = [CustomTool createImageView];
    _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 64);
    [self.view addSubview:_netImageView];
    [self loadData:NO];
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    if (self.type == 0) {
        [self initTitleName:@"我评论过的文章"];
    } else if (self.type == 1) {
        [self initTitleName:@"点赞狂魔"];
    }else if (self.type == 10){
        [self initTitleName:@"我评论过的漫画"];
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.3;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"userInfoCell";
    UserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [UserInfoCell userInfoCellOwner:self];
        
        cell.userInfoImageView.layer.borderWidth = 0.5;
        cell.userInfoImageView.layer.borderColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1].CGColor;
        cell.contentBg.layer.cornerRadius = 2;
        cell.contentBg.layer.masksToBounds = YES;
    }
    [cell.userInfoImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
    cell.userInfoTitleLabel.text = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"title"];
    cell.uesrInfoAuthLabel.text = [NSString stringWithFormat:@"作者：%@",[[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"author"]];
    cell.userInfoTimeLabel.text = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"createTimeValue"];
    cell.userInfoAppraiseLabel.text = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"praiseCount"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_type == 0) {
        [[LogHelper shared] writeToFilefrom_page:@"pc" from_section:@"profile" from_step:@"list" to_page:@"md" to_section:@"message" to_step:@"detail" type:@"" id:[[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"id"]];
    }else{
        [[LogHelper shared] writeToFilefrom_page:@"pp" from_section:@"profile" from_step:@"list" to_page:@"md" to_section:@"message" to_step:@"detail" type:@"" id:[[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"id"]];

    }
    
    InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
    detailView.infoID = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"id"];
    detailView.titleName = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"messageInfo"] objectForKey:@"title"];
    [self.navigationController pushViewController:detailView animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

-(void)loadData:(BOOL)isOrmore{
    if (isOrmore == YES) {
        page++;
        self.isMore = YES;
    } else {
        page = 1;
        self.isMore = NO;
        [self.dataArray removeAllObjects];
    }
    if (self.type == 0) {
        [self messageList];
    } else if(self.type == 1){
        [self appraiseList];
    }else if (self.type == 10){
        [self carToonList];
    }
}
//-(void)loadData:(BOOL)isOrmore{
//    if (self.isLoading) {
//        return;
//    }
//    if (isOrmore == YES) {
//        page++;
//        self.isMore = YES;
//    } else {
//        page = 1;
//        self.isMore = NO;
//        if (self.menuView.currentIndex == 0) {
//            [self.dataArray removeAllObjects];
//        } else {
//            [self.picArray removeAllObjects];
//        }
//    }

//}

#pragma mark - 加载数据
- (void)messageList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_WITH_USER_COMMENT,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    [NSString stringWithFormat:@"%d",page],@"page",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(messageListFinish:) method:GETDATA];
}

- (void)messageListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        
        if ([[dic objectForKey:@"results"] count] == 0) {
            [self bottomShow];
        }
        [self.tableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else
            self.contentStatusLabel.hidden = YES;
    }else{
        _netImageView.alpha = 1;
        UIButton *refreshBtn = [CustomTool createBtn];
        [_netImageView addSubview:refreshBtn];
        [refreshBtn addTarget:self action:@selector(messageList) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.tableView tableViewDidFinishedLoading];
}

- (void)appraiseList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_ACTIONTYPE_WITH_USER_PRAISE,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    [NSString stringWithFormat:@"%d",page],@"page",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(appraiseListFinish:) method:GETDATA];
}

- (void)appraiseListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        if ([[dic objectForKey:@"results"] count] == 0) {
            [self bottomShow];
        }
        [self.tableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else
            self.contentStatusLabel.hidden = YES;
    }else{
        _netImageView.alpha = 1;
        UIButton *refreshBtn = [CustomTool createBtn];
        [_netImageView addSubview:refreshBtn];
        [refreshBtn addTarget:self action:@selector(appraiseList) forControlEvents:UIControlEventTouchUpInside];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    [self.tableView tableViewDidFinishedLoading];
}

-(void)carToonList{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_WITHUSERCARTOONCOMMENT,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    [NSString stringWithFormat:@"%d",page],@"page",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(carToonListFinish:) method:GETDATA];
}

- (void)carToonListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        
        if ([[dic objectForKey:@"results"] count] == 0) {
            [self bottomShow];
        }
        [self.tableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else
            self.contentStatusLabel.hidden = YES;
    }else{
        _netImageView.alpha = 1;
        UIButton *refreshBtn = [CustomTool createBtn];
        [_netImageView addSubview:refreshBtn];
        [refreshBtn addTarget:self action:@selector(carToonList) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.tableView tableViewDidFinishedLoading];
}
@end
