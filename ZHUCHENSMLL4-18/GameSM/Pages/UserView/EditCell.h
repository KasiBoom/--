//
//  EditCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface EditCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *editTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *editContentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *editSexImageView;

+ (id)editCellOwner:(id)owner;

- (void)resetSize;

@end
