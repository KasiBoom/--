//
//  UserInfoCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "CustomView.h"

@interface UserInfoCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet CustomView *contentBg;
@property (weak, nonatomic) IBOutlet UILabel *userInfoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *uesrInfoAuthLabel;
@property (weak, nonatomic) IBOutlet UILabel *userInfoTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userInfoImageView;
@property (weak, nonatomic) IBOutlet UILabel *userInfoAppraiseLabel;

+ (id)userInfoCellOwner:(id)owner;

@end
