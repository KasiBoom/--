//
//  NoticeCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface NoticeCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *noticeImageView;
@property (weak, nonatomic) IBOutlet UILabel *noticeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *noticeTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *noticeContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *noticeMineLabel;
@property (weak, nonatomic) IBOutlet UIImageView *noticeImage;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *bgIV;


@property (weak, nonatomic) IBOutlet UIButton *replaybtn;
+ (id)noticeCellOwner:(id)owner;
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated;
@end
