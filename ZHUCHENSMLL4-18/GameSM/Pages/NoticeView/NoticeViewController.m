//
//  NoticeViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "NoticeViewController.h"
#import "NoticeCell.h"
#import "UIImageView+AFNetworking.h"
#import "SystemNoticeCell.h"
#import "InfoDetailViewController.h"
#import "MBProgressHUD+Add.h"
#import "InfoDetailViewController.h"
#import "GiftDetailController.h"
#import "ConstantViewController.h"
#import "testViewController.h"
#import "CustomTool.h"

#import "FaceBagModel.h"
#import "FaceBagCell.h"
#import "UserPictureWhoPraiseViewController.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5

@interface NoticeViewController ()
{
    UIImageView *_netImageView;
    UIButton    *_refreshBtn1;
    UIButton    *_refreshBtn2;
    int         _oldCurrentIndex;
    
    NSMutableArray  *_faceDataArr;
    NSMutableArray  *_faceNameArr;
    BOOL            _isFace;
    BOOL            _witchOne;
    UICollectionView *_collectionView;
    UIPageControl   *_pageC;
    UIView          *_FaceBagView;
    UITextView      *_labelFu;
    UIView          *_shadowView;
    UIImageView     *_tipImageView;
//    UIView          *_cellShadowView;
}
@property (nonatomic, assign) int chooseIndex;
@property (nonatomic, assign) int chooseRow;
@property (nonatomic, assign) BOOL isLoading;

@end

@implementation NoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //    [self initTitleName:@"通知"];
    [self addBackBtn];
    [self createTipImageView];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    _menuView.frame = CGRectMake(0, 0, 160, 44);
    _menuView.delegate = self;
    _menuView.titleArray = @[@"评论"];
    
    [self initView];
    
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10,5, 35, 35)];
    [leftBtn addTarget:self action:@selector(faceBag:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"biaoqing.png"] forState:UIControlStateNormal];
    [self.bottomView addSubview:leftBtn];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    _isFace = YES;
    _witchOne = YES;
    [self initFaceData];
    [self createFaceBag];
    [self createPageControl];
    _shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenheight, KScreenWidth, 0)];
    _shadowView.backgroundColor = [UIColor whiteColor];
    _shadowView.alpha = 0;
    [self.view addSubview:_shadowView];
    
#warning =
    UINavigationItem *item = self.navigationItem;
    item.titleView = _menuView;
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.sheetView];
    
    self.inputBgView.layer.cornerRadius = 2;
    self.inputBgView.layer.masksToBounds = YES;
    self.tucaoBg.layer.masksToBounds = YES;
    self.tucaoBg.layer.cornerRadius = 15;
    
    
    _netImageView = [CustomTool createImageView];
    [_scrollView addSubview:_netImageView];
    _scrollView.bounces = NO;
}

-(void)createTipImageView{
    _tipImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    _tipImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_tipImageView];
    _tipImageView.hidden = YES;
}

#pragma mark - 键盘
-(void)initFaceData{
    _faceDataArr = [NSMutableArray array];
    _faceNameArr = [NSMutableArray array];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        FaceBagModel *model = [[FaceBagModel alloc]init];
        NSString *imageName = [NSString stringWithFormat:@"%d#.png",i];
        model.imageName = imageName;
        [_faceDataArr addObject:model];
    }
}
//_inputView
-(void)createFaceBag{
    _FaceBagView = [[UIView alloc]init];
    _FaceBagView.frame = CGRectMake(0,0, KScreenWidth, 216);
    _FaceBagView.backgroundColor = [UIColor colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0];
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-60, 188, 60, 25)];
    [deleteBtn setImage:[UIImage imageNamed:@"shanchu_2.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deletaClike) forControlEvents:UIControlEventTouchUpInside];
    [_FaceBagView addSubview:deleteBtn];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 186) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];//colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0
    _collectionView.pagingEnabled = YES;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_FaceBagView addSubview:_collectionView];
    NSString *identifier = @"faceCell";
    [_collectionView registerClass:[FaceBagCell class] forCellWithReuseIdentifier:identifier];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView"];
    [_inputView resignFirstResponder];
}
-(void)createPageControl{
    //分页控件：宽高是系统有默认值的
    _pageC = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 195, 40, 10)];
    _pageC.center = CGPointMake(KScreenWidth/2.0,_FaceBagView.bounds.size.height -15);
    UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth/2-20, 197.5, 40, 7)];
    iv.backgroundColor = [UIColor clearColor];
    [_FaceBagView addSubview:iv];
    UIView *iv1 = [[UIView alloc]initWithFrame:CGRectMake(0.5, 0, 7, 7)];
    iv1.backgroundColor = [UIColor lightGrayColor];
    iv1.layer.masksToBounds = YES;
    iv1.layer.cornerRadius = 3.5;
    [iv addSubview:iv1];
    UIView *iv2 = [[UIView alloc]initWithFrame:CGRectMake(16.5, 0, 7, 7)];
    iv2.backgroundColor = [UIColor lightGrayColor];
    iv2.layer.masksToBounds = YES;
    iv2.layer.cornerRadius = 3.5;
    [iv addSubview:iv2];
    UIView *iv3 = [[UIView alloc]initWithFrame:CGRectMake(33, 0, 7, 7)];
    iv3.backgroundColor = [UIColor lightGrayColor];
    iv3.layer.masksToBounds = YES;
    iv3.layer.cornerRadius = 3.5;
    [iv addSubview:iv3];
    
    
    _pageC.numberOfPages =3;
    _pageC.backgroundColor = [UIColor clearColor];
    _pageC.layer.masksToBounds = YES;
    _pageC.layer.cornerRadius = 5;
    _pageC.enabled =NO;
    //设置当前选中点得颜色
    _pageC.currentPageIndicatorTintColor = [UIColor redColor];
    _pageC.currentPage =0;
    [_FaceBagView addSubview:_pageC];
}
-(void)deletaClike{
    BOOL isDeleteAll;
    isDeleteAll = NO;
    if (_inputView.text.length) {
        NSString *str = [_inputView.text substringFromIndex:_inputView.text.length-1];
        if ([str isEqualToString:@"]"]) {
            for (int i = _inputView.text.length-1; i>0; i --) {
                NSString *str1 = [_inputView.text substringWithRange:NSMakeRange(i-1, 1)];
                if ([str1 isEqualToString:@"["]) {
                    _inputView.text = [_inputView.text substringToIndex:i-1];
                    isDeleteAll = NO;
                    break;
                }
                isDeleteAll = YES;
            }
            if (isDeleteAll) {
                _inputView.text = [_inputView.text substringToIndex:_inputView.text.length - 1];
            }
            
        }else{
            _inputView.text = [_inputView.text substringToIndex:_inputView.text.length - 1];
        }
    }
}
#pragma mark - UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _faceDataArr.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"faceCell";
    FaceBagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.model = _faceDataArr[indexPath.row];
    return cell;
}
#pragma mark - UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KScreenWidth-75)/7.0, 180/5.0);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 5, 5);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
#warning - 表情点击
    NSInteger row = [indexPath row];
    _inputView.text = [NSString stringWithFormat:@"%@[%@]",_inputView.text,_faceNameArr[row]];
}
-(void)faceBag:(UIButton *)btn{
    if (_isFace) {
        [btn setImage:[UIImage imageNamed:@"jianpan.png"] forState:UIControlStateNormal];
        _inputView.inputView = _FaceBagView;
        [_inputView becomeFirstResponder];
        [_inputView reloadInputViews];
        _isFace = NO;
    }else{
        [btn setImage:[UIImage imageNamed:@"biaoqing.png"] forState:UIControlStateNormal];
        [_inputView becomeFirstResponder];
        _inputView.inputView = nil;
        [_inputView reloadInputViews];
        _isFace = YES;
    }
}




- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (!g_App.userID) {
        _menuView.currentIndex = 1;
        self.noticeTableView.scrollsToTop = YES;
        self.tableView.scrollsToTop = NO;
        [self customMenuViewSelectedWithIndex:1];
    } else {
        if (_witchOne) {
            _menuView.currentIndex = 0;
        }else{
            _menuView.currentIndex = 1;
        }
        
        self.noticeTableView.scrollsToTop = NO;
        self.tableView.scrollsToTop = YES;
        if (self.noticeUnreadNum || self.hasNew) {
            [_menuView resetBudgeWithIndex:1 show:YES];
        } else
            [_menuView resetBudgeWithIndex:1 show:NO];
            [self loadData:NO];
    }
    
    self.scrollView.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64 );
    self.tableView.frame = CGRectMake(0, 0, ScreenSizeWidth, self.scrollView.bounds.size.height);
    
    self.noticeTableView.frame = CGRectMake(self.scrollView.bounds.size.width, 0, self.noticeTableView.bounds.size.width, self.scrollView.bounds.size.height);
    
    self.bottomView.frame = CGRectMake(0, ScreenSizeHeight+300, self.view.bounds.size.width, self.bottomView.bounds.size.height);//ScreenSizeWidth
    self.sheetView.frame = CGRectMake(0, ScreenSizeHeight, ScreenSizeWidth, self.sheetView.bounds.size.height);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.isLoading = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, keyboardBounds.size.height, 0);
        [[self tableView] setScrollIndicatorInsets:e];
        [[self tableView] setContentInset:e];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.bottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.bottomView.frame.size.height - keyboardBounds.size.height, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        }];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification *)notification
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, 0, 0);
        [[self tableView] setScrollIndicatorInsets:e];
        [[self tableView] setContentInset:e];
        [UIView animateWithDuration:0.3 animations:^{
            self.bottomView.frame = CGRectMake(0, self.view.bounds.size.height, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        }];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)initView {
    
    [self.view addSubview:self.scrollView];
    [self.tableView removeFromSuperview];
    self.tableView.frame = self.scrollView.bounds;
    [self.scrollView addSubview:self.tableView];
    self.noticeTableView.frame = CGRectMake(self.scrollView.bounds.size.width, 0, self.noticeTableView.bounds.size.width, self.noticeTableView.bounds.size.height);
    [self.scrollView addSubview:self.noticeTableView];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
//    self.tableView.separatorColor = [UIColor clearColor];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width * 2, 0)];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.delegate = self;
    
    if (IsIOS7) {
        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
        if ([self.noticeTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.noticeTableView setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([self.noticeTableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.noticeTableView setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    NSLog(@"%f",self.scrollView.bounds.size.width);
}


#pragma mark - 加载数据
- (void)replayBtnPressed:(UIButton*)btn {
    [[LogHelper shared] writeToApafrom_page:@"pncc" from_section:@"p" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"r" type:@"reply_message" id:@"0"];
    self.chooseIndex = (int)btn.tag - 100;
    [self.inputView becomeFirstResponder];
}


- (IBAction)sheecBtnPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    switch (btn.tag - 100) {
        case 0: {
            _witchOne = YES;
            
            NSDictionary *dict = [self.dataArray objectAtIndex:self.chooseRow];
            if ([[dict objectForKey:@"type"] integerValue] == 1) {
                [[LogHelper shared] writeToApafrom_page:@"pncc" from_section:@"p" from_step:@"l" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:[dict objectForKey:@"valueID"]];
                InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
                detailView.infoID = [dict objectForKey:@"valueID"] ;
                [self.navigationController pushViewController:detailView animated:YES];
            } else if ([[dict objectForKey:@"type"] integerValue] == 3) {
                [[LogHelper shared] writeToApafrom_page:@"pncc" from_section:@"p" from_step:@"l" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:[dict objectForKey:@"valueID"]];

                UserPictureWhoPraiseViewController   *userPicVC = [[UserPictureWhoPraiseViewController alloc]init ];
//                NSDictionary *lastDict = self.dataArray[indexPath.row];
                userPicVC.pictureId = dict[@"valueID"];
                
                userPicVC.type = 100;
                userPicVC.allDataArr = @[];
                
                [self presentViewController:userPicVC animated:YES completion:nil];
            }else if ([[dict  objectForKey:@"type"] integerValue] == 4){
                [[LogHelper shared] writeToApafrom_page:@"pncc" from_section:@"p" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:[dict objectForKey:@"valueID"]];

                testViewController *testVC = [[testViewController alloc]init];
                testVC.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
                [testVC setChangeCollection:^{
                    
                }];
                [self presentViewController:testVC animated:YES completion:^{
                    testVC.valueId = dict[@"valueID"];
                    testVC.customType = @"5";
                }];
                //            }
            }
        }
            break;
        case 1: {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.tag = 100 + self.chooseRow;
            [self replayBtnPressed:btn];
        }
            break;
        case 2: {
            
        }
            break;
        default:
            break;
    }
    [self hideSheetView];
}

- (IBAction)sendBtnPressed:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"pmc" from_section:@"profile" from_step:@"list" to_page:@"pmcr" to_section:@"profile" to_step:@"list" type:@"rely" id:@"0"];
    if (!self.inputView.text.length) {
        [self.inputView resignFirstResponder];
        return;
    }
    
    [self replyMessage:self.inputView.text];
    
    [self.inputView resignFirstResponder];
    self.inputView.text = @"";
}

#pragma mark - getter && setter
- (UIScrollView*)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _menuView.bounds.size.height, ScreenSizeWidth, ScreenSizeHeight - 64 - _menuView.bounds.size.height)];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (PullingRefreshTableView*)noticeTableView {
    if (!_noticeTableView) {
        _noticeTableView = [[PullingRefreshTableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64) pullingDelegate:self];
        _noticeTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _noticeTableView.separatorColor = [UIColor lightGrayColor];
        _noticeTableView.backgroundColor = [UIColor clearColor];
        _noticeTableView.backgroundView = nil;
        _noticeTableView.dataSource = self;
        _noticeTableView.delegate = self;
    }
    return _noticeTableView;
}


#pragma mark - CustomMenuViewDelegate
- (void)customMenuViewSelectedWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            
            self.noticeTableView.scrollsToTop = NO;
            self.tableView.scrollsToTop = YES;
            [_menuView resetBudgeWithIndex:0 show:NO];
        }
            break;
        case 1: {
            [self hideSheetView];
            self.noticeTableView.scrollsToTop = YES;
            self.tableView.scrollsToTop = NO;
            [_menuView resetBudgeWithIndex:1 show:NO];
        }
            break;
        default:
            break;
    }
    [self loadingData];
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.bounds.size.width * index, 0) animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (!textField.text.length) {
        [textField resignFirstResponder];
        return YES;
    }
    
    [self replyMessage:textField.text];
    textField.text = @"";
    [textField resignFirstResponder];
    return YES;
}

- (void)showSheetView {
    [UIView animateWithDuration:0.3 animations:^{
        self.sheetView.frame = CGRectMake(self.sheetView.frame.origin.x, ScreenSizeHeight - 64 - self.sheetView.frame.size.height, ScreenSizeWidth, self.sheetView.bounds.size.height);
    }];
}

- (void)hideSheetView {
    [UIView animateWithDuration:0.3 animations:^{
        self.sheetView.frame = CGRectMake(self.sheetView.frame.origin.x, ScreenSizeHeight, ScreenSizeWidth, self.sheetView.bounds.size.height);
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return self.dataArray.count;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 1;
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.bounds.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        static NSString *cellIdentifier = @"noticeCell";
        NoticeCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"NoticeCell" owner:self options:nil] lastObject];
//        if (!cell) {
        cell = [NoticeCell noticeCellOwner:self];
        cell.noticeImageView.layer.cornerRadius = 18;
        cell.noticeImageView.layer.masksToBounds = YES;
        cell.replaybtn.layer.cornerRadius = 5;
        cell.replaybtn.layer.masksToBounds = YES;
        
//        UIButton * accessoryDetailDisclosureButton = [[UIButton alloc] initWithFrame:CGRectMake(-30, 0, 30, 30)];
//        [accessoryDetailDisclosureButton setImage:[UIImage imageNamed:@"1#"] forState:UIControlStateNormal];
//        [accessoryDetailDisclosureButton setImage:[UIImage imageNamed:@"2#"] forState:UIControlStateHighlighted];
//        cell.accessoryView = accessoryDetailDisclosureButton;

        cell.selectedBackgroundView = [[UIView alloc]initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (![self.dataArray count]) {
            return cell;
        }
        cell.bgView.backgroundColor = [UIColor colorWithRed:249/255.0 green:244/255.0 blue:244/255.0 alpha:1];
//        if ([[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"]) {
            [cell.noticeImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
//
//        }
        [cell.replaybtn addTarget:self action:@selector(replayBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.replaybtn.tag = indexPath.row + 100;
        
        cell.noticeNameLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"name"];
        cell.noticeNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];
        cell.noticeTimeLabel.text = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"];
//        cell.noticeContentLabel.text = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"content"];
        NSString *noticeStr;
        if ([self.dataArray[indexPath.row][@"type"] isEqualToString:@"1"]) {
            if([[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"commentInfo"] objectForKey:@"content"] length] != 0){
                noticeStr = [NSString stringWithFormat:@"回复我的评论:%@",[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"commentInfo"] objectForKey:@"content"]];
            }else{
                noticeStr = @"回复我的评论:该评论已删除";

            }
        }else if ([self.dataArray[indexPath.row][@"type"] isEqualToString:@"3"]){
            noticeStr = @"评论我的美图";

        }else if ([self.dataArray[indexPath.row][@"type"] isEqualToString:@"4"]){
            noticeStr = @"评论我的漫画";
        }
//        if (noticeStr.length>7) {
            cell.noticeMineLabel.text = noticeStr;
//        }
#warning -
        // UITextView-富文本
        NSString *contentStr = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"content"];
        NSString *cententString =contentStr;
        //去掉换行符
//        cententString = [cententString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
//        cententString = [cententString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        _labelFu = [[UITextView alloc]initWithFrame:CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-50, 20)];
        _labelFu.font = [UIFont boldSystemFontOfSize:16];
        _labelFu.userInteractionEnabled = NO;
        _labelFu.bounces = NO;
        _labelFu.editable = NO;
        [cell.contentView addSubview:_labelFu];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;// 字体的行间距
        NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:16],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        //表情
        _faceNameArr = [NSMutableArray array];
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
        NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
        for (int i =1; i < 72; i ++) {
            [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        }
#warning 正则表达式 "\\[[^\\]]+\\]"--"\\#[^\\#]+\\#"
        NSString *regexString = @"\\[[^\\]]+\\]";
        NSMutableAttributedString *attri = [_labelFu.attributedText mutableCopy];//_model.content
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
        NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
        if (arrr.count>0) {
            for (int i =0 ; i < arrr.count; i ++) {
                NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
                NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
                NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
                [attri appendAttributedString:attrStr1];
                NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
                for (int j = 0; j < _faceNameArr.count; j ++) {
                    if ([faceStr isEqualToString:_faceNameArr[j]]) {
                        NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                        attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                        attch.bounds = CGRectMake(0, -2, 30, 30);
                        NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                        [attri appendAttributedString:string1];
                        break;
                    }
                }
                cententString = [cententString substringFromIndex:result.range.location+result.range.length];
            }
            NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
            [attri appendAttributedString:attrStr2];
            [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
            _labelFu.attributedText = [attri copy];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attri boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50, MAXFLOAT) options:options context:nil];
            _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - TMARGIN-50, rect.size.height+10);
        }else{
            NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
            NSRange allRange = [cententString rangeOfString:cententString];
            [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
            [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(KScreenWidth - TMARGIN - 50, MAXFLOAT) options:options context:nil];
            _labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
            int height = (int)rect.size.height/19;

            _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-50, height*21+10);
        }
        cell.height = 90 - 10 + _labelFu.bounds.size.height;
        return cell;

    } else {
        static NSString *cellIdentifier = @"systemNoticeCell";
        SystemNoticeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [SystemNoticeCell systemNoticeCellOwner:self];
            cell.systemImageView.layer.cornerRadius = 18;
            cell.systemImageView.layer.masksToBounds = YES;
        }
        cell.systemNoticeImage.hidden = YES;
        cell.systemContentBg.hidden = YES;
        cell.systemContentInfoLabel.hidden = YES;
        if (![self.dataArray count]) {
            return cell;
        }
        cell.systemImageView.image = [UIImage imageNamed:@"27#.png"];
//        [cell.systemImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"adminIDInfo"]objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazai_bg.png"]];
        cell.systemName.text = @"麦萌君";
//        cell.systemName.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"adminIDInfo"]objectForKey:@"name"];
        cell.systemTimeLabel.text = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"];
        if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"customType"] integerValue] == 4) {
            cell.systemNoticeImage.hidden = NO;
            cell.systemContentBg.hidden = NO;
            cell.systemContentInfoLabel.hidden = NO;
            cell.systemContentInfoLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValueTitle"];
            
            cell.systemContentLabel.text = [NSString stringWithFormat:@"   %@",[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"messageTitle"]];
            [cell resetType:1];
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            [cell resetType:0];
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jinru"]];
            cell.systemContentLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"messageTitle"];
            cell.systemInfoLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValueTitle"];
        }
//        cell.systemContentInfoLabel.text = [NSString stringWithFormat:@"我的评论:%@",[[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"commentInfo"] objectForKey:@"content"]];
        
//        _cellShadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
//        _cellShadowView.backgroundColor = [UIColor blackColor];
//        _cellShadowView.alpha = 0;
//        [self.view bringSubviewToFront:_cellShadowView];
//        [cell addSubview:_cellShadowView];
        
        return cell;
    }
}

#warning 键盘
- (CGSize)sizeOfStr:(NSString *)str andFont:(UIFont *)font andMaxSize:(CGSize)size andLineBreakMode:(NSLineBreakMode)mode
{
    NSLog(@"版本号:%f",[[[UIDevice currentDevice]systemVersion]doubleValue]);
    CGSize s;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>=7.0) {
        NSLog(@"ios7以后版本");
        // NSDictionary *dic=@{NSFontAttributeName:font};
        NSMutableDictionary  *mdic=[NSMutableDictionary dictionary];
        [mdic setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
        [mdic setObject:font forKey:NSFontAttributeName];
        s = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine
                           attributes:mdic context:nil].size;//| NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
    }
    else
    {
        NSLog(@"ios7之前版本");
        s=[str sizeWithFont:font constrainedToSize:size lineBreakMode:mode];
    }
    return s;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (tableView == self.tableView) {
        _witchOne = YES;
        self.chooseRow = indexPath.row;
//        int type = (int)[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"valueType"] integerValue];
//        if (type == 2) {
//            
            [self showSheetView];
//            //        InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
//            //        detailView.infoID = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"valueID"];
//            //        [self.navigationController pushViewController:detailView animated:YES];
//        }
//    } else if (tableView == self.noticeTableView) {
        _witchOne = NO;
//        if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] != 4) {
            /*if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] == 1) {
                InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
                detailView.infoID = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"];
                [self.navigationController pushViewController:detailView animated:YES];
            } else if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] == 3) {
              UserPictureWhoPraiseViewController   *userPicVC = [[UserPictureWhoPraiseViewController alloc]init ];
                NSDictionary *lastDict = self.dataArray[indexPath.row];
                userPicVC.pictureId = lastDict[@"valueID"];
                
                userPicVC.type = 100;
                userPicVC.allDataArr = @[];
                
                [self presentViewController:userPicVC animated:YES completion:nil];
                }else if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] == 4){
                testViewController *testVC = [[testViewController alloc]init];
                testVC.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
                [testVC setChangeCollection:^{
                    
                }];
                [self presentViewController:testVC animated:YES completion:^{
                    testVC.valueId = [self.dataArray objectAtIndex:indexPath.row][@"id"];
                    testVC.customType = @"5";
                }];
//            }
        }*/
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}




#pragma mark - scrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self hideSheetView];
    self.inputView.text = nil;
    [self.inputView resignFirstResponder];
    _oldCurrentIndex = self.menuView.currentIndex;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint contentOffsetPoint = self.tableView.contentOffset;
//    NSLog(@"------%f....",contentOffsetPoint.y);
    if ([scrollView isKindOfClass:[UITableView class]]) {
        if (self.tableView == scrollView) {
            [self.tableView tableViewDidScroll:scrollView];
        } else if (self.noticeTableView == scrollView) {
            [self.noticeTableView tableViewDidScroll:scrollView];
        }
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{

    if ([scrollView isKindOfClass:[UITableView class]]) {
        if (self.tableView == scrollView) {
            [self.tableView tableViewDidEndDragging:scrollView];
        } else if (self.noticeTableView == scrollView) {
            [self.noticeTableView tableViewDidEndDragging:scrollView];
        }
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
        
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        self.menuView.currentIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
        
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        self.menuView.currentIndex = (scrollView.contentOffset.x+1) / scrollView.bounds.size.width;
        if (_oldCurrentIndex != self.menuView.currentIndex) {
            [self loadingData];
        }
    }
}
#pragma loadmore

- (void)loadingData {
    self.isLoading = NO;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isOrmore{
    if (isOrmore == YES) {
        page++;
    } else {
        page = 1;
        [self.dataArray removeAllObjects];
    }
    if (self.isLoading) {
        return;
    }
    self.isLoading = YES;
    if (self.menuView.currentIndex == 0) {
        [self noticeList];
    } else {
        [self noticeContentList];
    }
}
#pragma mark - 加载数据
- (void)noticeList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_WITHUSERREPLY,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    [NSString stringWithFormat:@"%d",page],@"page",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeListFinish:) method:GETDATA];
}

- (void)noticeListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.isLoading = NO;
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }

        [self.tableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
            _tipImageView.hidden = NO;
            _tipImageView.image = [UIImage imageNamed:@"pinglunTip.png"];
        } else{
            _tipImageView.hidden = YES;
            self.contentStatusLabel.hidden = YES;
        }
        
        
        if ([[dic objectForKey:@"results"] count] == 0 && page != 1) {
            [self bottomShow];
        }
        
        
        
    }else{
        [_refreshBtn2 removeFromSuperview];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 64 - 40);
        _netImageView.alpha = 1;
        
        _refreshBtn1 = [CustomTool createBtn];
        [_netImageView addSubview:_refreshBtn1];
        [_refreshBtn1 addTarget:self action:@selector(noticeList) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.tableView tableViewDidFinishedLoading];
}

- (void)noticeContentList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_INFORMMESSAGE_WITHINFORM,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    [NSString stringWithFormat:@"%d",page],@"page",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeContentListFinish:) method:GETDATA];
}

- (void)noticeContentListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.isLoading = NO;
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        if ([[dic objectForKey:@"results"] count] == 0 && page != 1) {
            [self bottomShow];
        }
        
        [self.noticeTableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
            _tipImageView.hidden = NO;
            _tipImageView.image = [UIImage imageNamed:@"tongzhiTip.png"];
        } else{
            _tipImageView.hidden = YES;
            self.contentStatusLabel.hidden = YES;
        }
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *noticeID = [userDefaults objectForKey:@"noticeID"];
        if (!noticeID) {
            [userDefaults setObject:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"] forKey:@"noticeID"];
            [userDefaults synchronize];
        } else if ([noticeID isEqualToString:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"]]) {

        } else if (![noticeID isEqualToString:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"]]) {
            [userDefaults setObject:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"] forKey:@"noticeID"];
            [userDefaults synchronize];
        }
    }else{
        [_refreshBtn1 removeFromSuperview];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        _netImageView.frame = CGRectMake(KScreenWidth, 0, KScreenWidth, KScreenheight - 64 - 40);
        _netImageView.alpha = 1;
        
        _refreshBtn2 = [CustomTool createBtn];
        [_netImageView addSubview:_refreshBtn2];
        [_refreshBtn2 addTarget:self action:@selector(noticeContentList) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.noticeTableView tableViewDidFinishedLoading];
}


- (void)replyMessage:(NSString*)msg {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_REPLY_MESSAGE,@"r",
                                    [[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"id"],@"contentID",
                                    msg,@"content",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(replyMessageFinish:) method:POSTDATA];
}

- (void)replyMessageFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _netImageView.alpha = 0;
        [[LogHelper shared] writeToApafrom_page:@"pncc" from_section:@"p" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"a" type:@"reply_message" id:@"0"];
        [MBProgressHUD showSuccess:@"回复成功" toView:self.view];
        [self loadData:NO];
    }else{
        _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 64 - 40);
        _netImageView.alpha = 1;
        
        UIButton *refreshBtn = [CustomTool createBtn];
        [_netImageView addSubview:refreshBtn];
        [refreshBtn addTarget:self action:@selector(replyMessage:) forControlEvents:UIControlEventTouchUpInside];
    }
}
@end
