//
//  ReceiveZanTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/14.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiveZanTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *usertime;
@property (weak, nonatomic) IBOutlet UIImageView *imageZan;

@property (strong,nonatomic)NSString *valueType;

@end
