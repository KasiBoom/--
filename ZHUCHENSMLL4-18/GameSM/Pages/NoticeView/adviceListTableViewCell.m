//
//  adviceListTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/11.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "adviceListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "adviceModel.h"
@implementation adviceListTableViewCell

//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
//    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] ) {
//        
//    }
//    return self;
//}


- (void)setAdvicemodel:(adviceModel *)advicemodel {
    _receiveNum.layer.cornerRadius = _receiveNum.frame.size.width/2;
    
    [_receiveCommentImage setImage:[UIImage imageNamed:advicemodel.imageTitle] forState:UIControlStateNormal];
    _receiveLabel.text = advicemodel.desName;
    if ([[NSString stringWithFormat:@"%@",advicemodel.numberCount] isEqualToString:@"0"]) {
        _receiveNum.hidden = YES;
    }else{
        [_receiveNum setTitle:advicemodel.numberCount forState:UIControlStateNormal];
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
