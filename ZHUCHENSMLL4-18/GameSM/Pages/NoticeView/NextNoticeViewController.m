//
//  NextNoticeViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/14.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "NextNoticeViewController.h"
#import "SystemNoticeTableViewCell.h"
#import "ReceiveZanTableViewCell.h"
#import "NoticeCell.h"
#import "PullingRefreshTableView.h"
#import "InfoDetailViewController.h"
#import "GiftDetailController.h"
#import "ConstantViewController.h"
#import "ReceiveZanTableViewCell.h"
#import "UserPictureWhoPraiseViewController.h"
#import "testViewController.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5

@interface NextNoticeViewController ()<UITableViewDataSource,UITableViewDelegate,PullingRefreshTableViewDelegate>
{
    UITableView *_tableView ;
    NSMutableArray  *_faceNameArr;
    UIImageView *_netImageView;
    UIImageView     *_tipImageView;



}
@property (nonatomic, strong) PullingRefreshTableView *noticeTableView;

@end

@implementation NextNoticeViewController

- (void)viewWillAppear:(BOOL)animated {
    LYTabBarController *tabBar = (LYTabBarController *)self.tabBarController;
    [tabBar hiddenBar:YES animated:YES];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight) style:UITableViewStylePlain];
//    _tableView.dataSource = self;
//    _tableView.delegate = self;
//    [self.view addSubview:_tableView];
    if(_type == 1){
        [self initTitleName:@"收到的赞"];
    }else{
        [self initTitleName:@"系统通知"];
    }
    
    [self.view addSubview:self.noticeTableView];
    [self addBackBtn];
    [self noticeList];
    [self createTipImageView];
    [self.view addSubview:self.sheetView];
    
    
    
}

-(void)createTipImageView{
    _tipImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    _tipImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_tipImageView];
    _tipImageView.hidden = YES;
}




- (void)noticeList {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_type == 3) {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_INFORMMESSAGE_WITHINFORM,@"r",
                                    [NSString stringWithFormat:@"%d",999],@"size",
                                    [NSString stringWithFormat:@"%d",1],@"page",nil];
    
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeListFinish:) method:GETDATA];
    }else{
        NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        (NSString *)API_URL_PICTUREZAN,@"r",
                                        [NSString stringWithFormat:@"%d",999],@"size",
                                        [NSString stringWithFormat:@"%d",1],@"page",nil];
        
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeListFinish:) method:GETDATA];

    }
}

- (void)noticeListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
////    self.isLoading = NO;
//    self.dataArrayType = [NSMutableArray array];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
//            [self.dataArrayType addObject:item[@"valueType"]];
        }
    }
//
        [self.noticeTableView reloadData];
//
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
            _tipImageView.hidden = NO;
            _tipImageView.image = [UIImage imageNamed:@"pinglunTip.png"];
        } else{
            _tipImageView.hidden = YES;
            self.contentStatusLabel.hidden = YES;
        }
//
//        
////        if ([[dic objectForKey:@"results"] count] == 0 && page != 1) {
////            [self bottomShow];
////        }
//        
//        
//        
//    }else{
////        [_refreshBtn2 removeFromSuperview];
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 64 - 40);
//        _netImageView.alpha = 1;
//        
////        _refreshBtn1 = [CustomTool createBtn];
////        [_netImageView addSubview:_refreshBtn1];
////        [_refreshBtn1 addTarget:self action:@selector(noticeList) forControlEvents:UIControlEventTouchUpInside];
//    }
    
    
//    [self.noticeTableView tableViewDidFinishedLoading];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_type == 3) {
        SystemNoticeTableViewCell *systemNotice = [[[NSBundle mainBundle] loadNibNamed:@"SystemNoticeTableViewCell" owner:self options:nil] lastObject];
        systemNotice.titleLabel.text = self.dataArray[indexPath.row][@"messageValue"][@"messageTitle"];
        systemNotice.desLabel.text = self.dataArray[indexPath.row][@"messageValue"][@"customValueTitle"];
        systemNotice.timeLabel.text = self.dataArray[indexPath.row][@"sendTime"];
        NSLog(@"sendTime%@",self.dataArray[indexPath.row][@"sendTime"]);
        systemNotice.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return systemNotice;
    }else{
        ReceiveZanTableViewCell *receiveZan = [[[NSBundle mainBundle] loadNibNamed:@"ReceiveZanTableViewCell" owner:self options:nil] lastObject];
        [receiveZan.userIcon setImageWithURL:[NSURL URLWithString:self.dataArray[indexPath.row][@"userIDInfo"][@"images"]]];
        receiveZan.username.text = self.dataArray[indexPath.row][@"userIDInfo"][@"name"];
        receiveZan.usertime.text = self.dataArray[indexPath.row][@"createTimeValue"];
        if ( [self.dataArray[indexPath.row][@"content"] rangeOfString:@"http"].location !=NSNotFound ) {
            [receiveZan.imageZan setImageWithURL:[NSURL URLWithString:self.dataArray[indexPath.row][@"content"]]];
        }
        receiveZan.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return receiveZan;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 84;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        if (_type == 3) {
            
            if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"customType"] integerValue] != 4) {
                if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"customType"] integerValue] == 1) {
                    [[LogHelper shared] writeToApafrom_page:@"pncs" from_section:@"p" from_step:@"l" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"]];
                    InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
                    detailView.infoID = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"];
                    [self.navigationController pushViewController:detailView animated:YES];
                } else if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"customType"] integerValue] == 3) {
                    [[LogHelper shared] writeToApafrom_page:@"pncs" from_section:@"p" from_step:@"l" to_page:@"gd" to_section:@"g" to_step:@"d" type:@"" id:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"]];

                    
                    GiftDetailController *detailView = [[GiftDetailController alloc] initWithNibName:@"GiftDetailController" bundle:nil];
                    detailView.giftID = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"];
                    [self.navigationController pushViewController:detailView animated:YES];
                }else if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"customType"] integerValue] == 6){
                    [[LogHelper shared] writeToApafrom_page:@"pncs" from_section:@"p" from_step:@"l" to_page:@"sf" to_section:@"p" to_step:@"l" type:@"" id:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"]];

                    ConstantViewController *constantVC = [[ConstantViewController alloc] init];
                    //                [self presentViewController:constantVC animated:YES completion:nil];
                    //                [self.navigationController pushViewController:constantVC animated:YES];
                    constantVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:constantVC];
                    [self presentViewController:nav animated:YES completion:nil];
                }else if ([[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"customType"] integerValue] == 5){
                    [[LogHelper shared] writeToApafrom_page:@"pncs" from_section:@"p" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"detail" type:@"" id:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"] objectForKey:@"customValue"]];

                    
                    testViewController *testVC = [[testViewController alloc]init];
                    testVC.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
                    [testVC setChangeCollection:^{
                        
                    }];
                    [self presentViewController:testVC animated:YES completion:^{
                        testVC.valueId = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"messageValue"]objectForKey:@"customValue"];
                        testVC.customType = @"5";
                    }];
                }
            }
        }else{
            [[LogHelper shared] writeToFilefrom_page:@"pncp" from_section:@"p" from_step:@"l" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:@"0"];
            if ([_dataArrayType[indexPath.row] isEqualToString:@"2"]) {
                UserPictureWhoPraiseViewController *userPic = [[UserPictureWhoPraiseViewController alloc] init];
                userPic.pictureId = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"valueID"] ;
                userPic.type = 100;
                userPic.allDataArr = @[];
                [self presentViewController:userPic animated:YES completion:nil];
            }else if ([_dataArrayType[indexPath.row] isEqualToString:@"1"]){
                InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
                detailView.infoID = [[_dataArray objectAtIndex:indexPath.row] objectForKey:@"valueID"];
//                detailView.titleName = [[_dataArray objectAtIndex:indexPath.row] objectForKey:@"title"];
                detailView.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:detailView animated:YES];
            }
        }
}

- (PullingRefreshTableView*)noticeTableView {
    if (!_noticeTableView) {
        _noticeTableView = [[PullingRefreshTableView alloc] initWithFrame:CGRectMake(0, -40, ScreenSizeWidth, ScreenSizeHeight) pullingDelegate:self];
        _noticeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _noticeTableView.separatorColor = [UIColor lightGrayColor];
        _noticeTableView.backgroundColor = [UIColor clearColor];
        _noticeTableView.backgroundView = nil;
        _noticeTableView.dataSource = self;
        _noticeTableView.delegate = self;
    }
    return _noticeTableView;
}


- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (NSMutableArray *)dataArrayType{
    if (!_dataArrayType) {
        _dataArrayType = [NSMutableArray array];
    }
    return _dataArrayType;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
