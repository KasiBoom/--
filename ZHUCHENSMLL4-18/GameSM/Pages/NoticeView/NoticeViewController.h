//
//  NoticeViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"
#import "CustomMenuView.h"
#import "CustomView.h"

@interface NoticeViewController : BaseTableViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, CustomMenuViewDelegate, UIScrollViewDelegate, PullingRefreshTableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UITextField *inputView;
@property (strong, nonatomic) IBOutlet UIView *sheetView;
@property (weak, nonatomic) IBOutlet CustomMenuView *menuView;
@property (weak, nonatomic) IBOutlet CustomView *inputBgView;
@property (weak, nonatomic) IBOutlet UIImageView *tucaoBg;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) PullingRefreshTableView *noticeTableView;

@property (nonatomic, assign) int noticeUnreadNum;
@property (nonatomic, assign) BOOL hasNew;

- (IBAction)sheecBtnPressed:(id)sender;
- (IBAction)sendBtnPressed:(id)sender;
@end
