//
//  ReceiveModel.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "ReceiveModel.h"
#import "userIDInfo.h"
@implementation ReceiveModel
+ (NSMutableArray *)paraFromDict:(NSDictionary *)dict {
    NSArray *results = dict[@"results"];
    NSMutableArray *returnArr = [NSMutableArray array];
    for (int i = 0; i < results.count; i ++) {
        ReceiveModel *receiveModel = [[ReceiveModel alloc] init];
        receiveModel.id = results[i][@"id"];
        receiveModel.valueType = results[i][@"valueType"];
        receiveModel.valueID = results[i][@"valueID"];
        receiveModel.content = results[i][@"content"];
        receiveModel.userId = [userIDInfo paraDict:results[i][@"userIDInfo"]];
        [returnArr addObject:receiveModel];
    }
    return returnArr;
    
}

@end
