//
//  ReceiveModel.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
@class userIDInfo;
@interface ReceiveModel : NSObject
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSString *valueType;
@property (nonatomic, copy)NSString *valueID;
@property (nonatomic, copy)NSString *createTimeValue;
@property (nonatomic, copy)NSString *content;
@property (nonatomic, copy)userIDInfo *userId;
+ (NSMutableArray *)paraFromDict:(NSDictionary *)dict;
@end
