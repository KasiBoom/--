//
//  userIDInfo.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "userIDInfo.h"

@implementation userIDInfo
+(userIDInfo *)paraDict:(NSDictionary *)dict {
    userIDInfo *userId = [[userIDInfo alloc] init];
    userId.id = dict[@"id"];
    userId.name = dict[@"name"];
    userId.images = dict[@"images"];
    return userId;
}
@end
