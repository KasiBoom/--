//
//  SystemNoticeCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/17.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface SystemNoticeCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *systemImageView;
@property (weak, nonatomic) IBOutlet UILabel *systemName;
@property (weak, nonatomic) IBOutlet UILabel *systemTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *systemContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *systemInfoLabel;

@property (weak, nonatomic) IBOutlet UIView *systemContentBg;
@property (weak, nonatomic) IBOutlet UILabel *systemContentInfoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *systemNoticeImage;
+ (id)systemNoticeCellOwner:(id)owner;

/**
 *  type | 0 推送 1 删除通知s
 *
 *  @param type
 */
- (void)resetType:(int)type;

@end
