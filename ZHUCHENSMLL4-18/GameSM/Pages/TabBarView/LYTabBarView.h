//
//  LYTabBarView.h
//  Linyi360
//
//  Created by wt on 14-9-19.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomView.h"


@protocol LYTabBarViewDelegate <NSObject>

- (void)tabbarWasSelectedWithIndex:(int)index;

@end

@interface LYTabBarView : UIView

@property (nonatomic, assign) id<LYTabBarViewDelegate> delegate;

@property (nonatomic, assign) int statusBar;

- (id)initWithFrame:(CGRect)frame withImages:(NSArray*)imgs highlightImages:(NSArray*)highlightImages withName:(NSArray *)name withLightName:(NSArray *)highlightName;

- (void)tabWasSelected:(UIButton*)btn;

@end
