//
//  LYTabBarView.m
//  Linyi360
//
//  Created by wt on 14-9-19.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import "LYTabBarView.h"
#import "AFNetworking.h"
#import "Y_X_DataInterface.h"
#import "SSTabBarItem.h"

@interface LYTabBarView ()
{
    UIImageView *_imageView;
}
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray *highlightImages;
@property (nonatomic, strong) NSArray *name;
@property (nonatomic, strong) NSArray *highlightName;

@end
@implementation LYTabBarView

- (void)dealloc {
    _delegate = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame withImages:nil highlightImages:nil withName:nil withLightName:nil];
}

- (id)initWithFrame:(CGRect)frame withImages:(NSArray*)imgs highlightImages:(NSArray*)highlightImages withName:(NSArray *)name withLightName:(NSArray *)highlightName{
    self = [super initWithFrame:frame];
    if (self) {
        self.images = imgs;
        self.highlightImages = highlightImages;
        self.name = name;
        self.highlightName = highlightName;
        
        //        self.backgroundColor = [UIColor orangeColor];
        //        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mian_tiao"]];
        UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, .5)];
//        lineView.backgroundColor = [UIColor colorWithRed:221/255.0 green:150/255.0 blue:165/255.0 alpha:1];
        lineView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1];
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 49)];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        _imageView.backgroundColor = [UIColor whiteColor];
        //        _imageView.image = [UIImage imageNamed:@"tabbar"];
        //        _imageView = imageView;
        [_imageView addSubview:lineView];
        [self addSubview:_imageView];
        [self setTabBarView];
    }
    return self;
}


- (void)setTabBarView {
    for (int i= 0 ; i < self.images.count ; i++) {
        SSTabBarItem *button = [[SSTabBarItem alloc] initWithFrame:CGRectMake(0 + self.bounds.size.width/self.images.count * i, 1, self.bounds.size.width/self.images.count, self.frame.size.height-1)];
        [self addSubview:button];
        button.tag = 100+i;

//        button.imageEdgeInsets = UIEdgeInsetsMake(0, button.bounds.size.width/2-20, 20, 0);
//        button.titleEdgeInsets = UIEdgeInsetsMake(30, 0, 0, button.bounds.size.width/2+10);
        
        [button addTarget:self action:@selector(tabWasSelected:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)resetBtnImg {
    int i = 0;
    for (UIButton *btn in self.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            btn.selected = NO;
            [btn setImage:[UIImage imageNamed:[self.images objectAtIndex:i]] forState:UIControlStateNormal];
            [btn setTitle:[NSString stringWithFormat:@"%@",[self.name  objectAtIndex:i]] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor colorWithRed:148/255.0 green:148/255.0 blue:148/255.0 alpha:1] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10];//-Bold
            [btn setImage:[UIImage imageNamed:[self.highlightImages objectAtIndex:i]] forState:UIControlStateHighlighted];

//            btn.imageView.center = CGPointMake(btn.bounds.size.width/2, 15);
//            btn.titleLabel.center = CGPointMake(btn.bounds.size.width/2, 39);
//            btn.contentHorizontalAlignment = 0;
//            btn.imageEdgeInsets = UIEdgeInsetsMake(2, btn.bounds.size.width/2-18, 20, 0);
//            btn.titleEdgeInsets = UIEdgeInsetsMake(30, 0, 0, 18);
//            btn.titleLabel.textAlignment = 0;
//            [btn setBackgroundImage:nil forState:UIControlStateNormal];
            i++;
        }
    }
}

- (void)tabWasSelected:(UIButton*)btn {
    [self resetBtnImg];
    for (UIButton *button in self.subviews) {
        
        if ([button isKindOfClass:[UIButton class]] && button.tag == btn.tag) {
//            button.imageView.center = CGPointMake(button.bounds.size.width/2, 15);
//            button.titleLabel.center = CGPointMake(button.bounds.size.width/2, 39);
            
            int index = (int)button.tag - 100;
            [btn setImage:[UIImage imageNamed:[self.highlightImages objectAtIndex:index]] forState:UIControlStateNormal];
            btn.selected = YES;
            btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
            [btn setTitleColor:[UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0] forState:UIControlStateSelected];
        }
    }
    if (_delegate && [_delegate respondsToSelector:@selector(tabbarWasSelectedWithIndex:)]) {
        [_delegate tabbarWasSelectedWithIndex:(int)btn.tag - 100];
    }
}

@end
