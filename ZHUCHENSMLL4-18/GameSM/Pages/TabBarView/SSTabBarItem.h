//
//  SSTabBarItem.h
//  SevenSnack
//
//  Created by 顾鹏 on 15/12/7.
//  Copyright © 2015年 顾鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSTabBarItem : UIButton

@property (nonatomic,strong) NSString * badgeValue;

// Tab background image
@property (nonatomic, strong) UIImage *backgroundImage;

// Tab selected background image
@property (nonatomic, strong) UIImage *selectedBackgroundImage;

@end
