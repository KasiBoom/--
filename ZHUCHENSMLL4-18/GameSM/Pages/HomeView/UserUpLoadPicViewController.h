//
//  UserUpLoadPicViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/20.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

#import "PullingRefreshTableView.h"
#import "PullPsCollectionView.h"

@interface UserUpLoadPicViewController : BaseViewController
{
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
}
@property (nonatomic, strong) PullPsCollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *picArray;
@property (nonatomic, strong) NSMutableArray *currentPicArray;
@property (nonatomic, strong) NSString *infoCount;
@property (nonatomic, strong) NSString *picCount;


@end
