//
//  CategeryCell.h
//  bang
//
//  Created by 王涛 on 15/6/6.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionViewCell.h"
#import "CustomView.h"

@interface CategeryCell : PSCollectionViewCell
//@property (weak, nonatomic) IBOutlet CustomView *contentbg;
//
//@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;


@property (strong, nonatomic) CustomView *contentbg;
@property (strong, nonatomic) CustomView *bgView;

@property (strong, nonatomic) UIImageView *categoryImage;
@property (strong, nonatomic) UIImageView *praiseNumImage;
@property (strong, nonatomic) UIImageView *praiseHandleImage;
@property (strong, nonatomic) UILabel *praiseNum;

@property (strong, nonatomic) UIImageView *userImage;
@property (strong, nonatomic) UILabel *userName;
@property (strong, nonatomic) UIImageView *praCountImage;
@property (strong, nonatomic) UILabel *praCount;
@property (strong, nonatomic) UIImageView *commentImage;
@property (strong, nonatomic) UILabel *commentCount;

@end
