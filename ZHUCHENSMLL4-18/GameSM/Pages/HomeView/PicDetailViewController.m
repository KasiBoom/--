//
//  PicDetailViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "PicDetailViewController.h"
#import "LYTabBarController.h"
#import "MBProgressHUD+Add.h"
#import "MobClick.h"

@interface PicDetailViewController ()

@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) NSArray *imgArray;
@end

@implementation PicDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    if (self.imageID) {
        [self imageDetail];
    } else {
        [self creatImgShow:[NSMutableArray arrayWithArray:self.imageData] index:self.index];
        [self refreshView];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)setImageData:(NSArray *)imageData {
    _imageData = imageData;
}

- (void)creatImgShow:(NSMutableArray*)array index:(int)index{
    self.imgArray = array;
    self.imgShowView = [[SingleImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)
                                               withSourceData:array
                                                    withIndex:index];
    self.imgShowView.sDelegate = self;
    self.imgShowView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.imgShowView];
    self.imgShowView.center = self.view.center;
}

- (IBAction)saveBtnPressed:(id)sender {
    [self savePic:nil];
}

- (IBAction)shareBtnPressed:(id)sender {
    [self sharePic:nil];
}

- (void)refreshView {
    [self.view bringSubviewToFront:self.shareBtn];
    [self.view bringSubviewToFront:self.saveBtn];
}

- (void)savePic:(UIButton*)btn {
    UIImageWriteToSavedPhotosAlbum(self.imgShowView.showImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error != NULL){
        //失败
        [MBProgressHUD showSuccess:@"保存失败" toView:self.view];
    }
    else{
        //成功
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
    }
}

- (void)sharePic:(UIButton*)btn {
    if (self.dataDic == nil) {
        return;
    }
    
    NSString *title = @"我在麦萌看到一张超赞的图片，安利给小伙伴";
    
    [UMSocialData defaultData].extConfig.qzoneData.title = title;;
    [UMSocialData defaultData].extConfig.qzoneData.url = [self.dataDic objectForKey:@"images"];
    
    [UMSocialData defaultData].extConfig.qqData.title = title;;
    [UMSocialData defaultData].extConfig.qqData.url = [self.dataDic objectForKey:@"images"];
    
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = [self.dataDic objectForKey:@"images"];
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = title;;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [self.dataDic objectForKey:@"images"];
    
    
    [UMSocialData defaultData].extConfig.sinaData.shareText = [NSString stringWithFormat:@"%@%@分享自@麦萌",title,[self.dataDic objectForKey:@"images"]];
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:[self.dataDic objectForKey:@"images"]];
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENGKEY
                                      shareText:[self.dataDic objectForKey:@"images"]
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:@[UMShareToSina, UMShareToQQ, UMShareToQzone, UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:self];
    NSDictionary *dict = @{@"shareId" : [self.dataDic objectForKey:@"id"]};
    [MobClick event:@"shareEvent" attributes:dict];
}

#pragma mark - UMSocialUIDelegate
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}


#pragma mark - SingleImageViewDelegate
- (void)singleImageViewSelectWithSingleTap {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CustomAlertViewDelegate
- (void)customAlertViewBtnWasSeletecd:(CustomAlertView *)alertView withIndex:(NSInteger)index {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 加载数据
- (void)imageDetail {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_PRETTYIMAGESDETAIL,@"r",
                                    self.imageID,@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(imageDetailFinish:) method:GETDATA];
}


- (void)imageDetailFinish:(NSDictionary*)dic {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        self.dataDic = [dic objectForKey:@"results"];
        [self creatImgShow:[NSMutableArray arrayWithObject:[[dic objectForKey:@"results"] objectForKey:@"images"]] index:0];
        [self refreshView];
    } else {
        if ([[dic objectForKey:@"code"] integerValue] == 51) {
            CustomAlertView * alertView=[[CustomAlertView alloc]initWithTitle:@"脾气差的提示君" message:[dic objectForKey:@"error"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }
    }
}

-(BOOL)shouldAutorotate{
    return YES;
}



@end
