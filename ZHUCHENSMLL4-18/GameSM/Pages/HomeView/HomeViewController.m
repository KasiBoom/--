
//
//  HomeViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

//#import "InfoDetailViewControllerMax.h"

#import "HomeViewController.h"
#import "InfoCell.h"
#import "InfoDetailViewController.h"
#import "CategeryCell.h"
#import "PicDetailViewController.h"
#import "NoticeViewController.h"
//#import "GuideViewController.h"
#import "WebViewController.h"
#import "GiftDetailController.h"
#import "GiftViewController.h"
#import "MobClick.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
#import "HZPhotoBrowser.h"
#import "HZPhotoItemModel.h"
#import "HZImagesGroupView.h"
#import "LYTabBarController.h"
#import "testViewController.h"
#import "DataBaseHelper.h"
#import "CustomTool.h"
/*缓存*/
//2.7
#import "NSString+GPAditions.h"
#import "DataBaseHelper.h"
#import "ASIDownloadCache.h"
#import "ASIHTTPRequest.h"
#import "NSDate+OTS.h"
#import "CustomIOSAlertView.h"
#import "ConstantViewController.h"
#import "Y_X_DataInterface.h"
#import <StoreKit/StoreKit.h>

#import "HomeSearchCell.h"
#import "UserUpLoadPicViewController.h"

#import "LGPhotoPickerViewController.h"
#import  "UMCheckUpdate.h"
#import "AdviceListViewController.h"

#import <sqlite3.h>
#import "FMDatabase.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface HomeViewController ()<HZPhotoBrowserDelegate,CustomAlertViewDelegate,LYTabBarViewDelegate,CustomIOSAlertViewDelegate,SKStoreProductViewControllerDelegate,UISearchBarDelegate,LGPhotoPickerViewControllerDelegate>
{
    NSInteger currentStar;
    NSMutableArray *_articleIsReadArr;
    NSMutableArray *_searchResultArr;
    NSDictionary *_dataDict;
    //2.7
    FMDatabase *_db;
    NSArray *_cacheMessageData;
    NSArray *_cacheAdData;
    BOOL    _boolCache;
}
@property (nonatomic, strong) NSMutableArray *starArray;
@property (nonatomic, strong) NSMutableArray *picArray;
@property (nonatomic, strong) NSMutableArray *currentPicArray;
@property (nonatomic, strong) NSString *infoCount;
@property (nonatomic, strong) NSString *picCount;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isMore;

@property (nonatomic, strong) CustomAdView *adView;
@property (nonatomic, strong) NSMutableArray *adInfoArray;
@property (nonatomic, strong) UITableView *findTableView;//102
@property (nonatomic, strong) UIScrollView *pictureTagView;
@property (nonatomic, strong) UIView    *pictureTopView;

@end

@implementation HomeViewController
{
    UIView     *_yaoyiyaohelpIV;
    CGFloat         _lastContentOffset;
    CGFloat         _beginScrollViewX;
    LYTabBarController *_lyTabbar;
    HZPhotoBrowser  *_browserVc;
    int             _prePage;
    int             _indexI;
    UIImageView     *_netImageView;
    UIButton        *_refreshBtn1;
    UIButton        *_refreshBtn2;
    UILabel         *_label;
    UIView          *_picTitleView;
    UIView          *_redView;
    BOOL            _isDeleteCache;
    BOOL            _isMoreRecord;
    BOOL            _isSearchArticle;
    BOOL            _isShowPicTag;
    BOOL            _isResultGoDetail;
    BOOL            _isToTopShow;
    BOOL            _isTap;
    BOOL            _showTip;
    BOOL            _isCopyData;
    //    UIButton *_refreshBtn;
    CustomButton    *_leftBtn;
    CustomButton    *_rightBtn;
    NSString        *_plistPath;
    NSString        *_thirdBtnName;
    //----
    NSMutableArray  *_ArticleSearchRecordArr;
    NSMutableArray  *_picHeadViewTagArr;
    NSMutableArray  *_picTopTagArr;
    CGFloat         _searchCurrentOffset;
    CGFloat         _searchMaximumOffset;
    int             _searchPageNum;
    int             _searchCountTotal;
    int             _selectBtnTag;
    int             _homeOrPicIndex;
    NSString        *_picBtnTagName;
    UIButton        *_goTopBtn;
    NSString        *_lastID;
//    UIView *_twoBtnView;
    UITapGestureRecognizer *_tapGesture;
    UITapGestureRecognizer *_tap1Gesture;
    UIAlertView *alert;
    NSMutableArray  *_copyData;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HomeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TOROOT" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"haveNewNotice" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changePicArr" object:nil];
}


- (NSMutableArray *)starArray {
    if (_starArray == nil) {
        _starArray = [[NSMutableArray alloc] init];
    }
    return _starArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];

/*数据缓存*/
    [self initialize];
//    if (![_db tableExists:@"MessageList"]) {
//        [self createTableS];
//    }
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"ISCACHEHOMEDATA"] isEqualToString:@"1"]){
        [self createTableS];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ISCACHEHOMEDATA"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    _cacheMessageData = [NSArray arrayWithArray:[self statusesWithParams:nil andTableName:@"MessageList" andTableKeyName:@"messageKey"]];
    _cacheAdData = [NSArray arrayWithArray:[self statusesWithParams:nil andTableName:@"AdList" andTableKeyName:@"adKey"]];
    
    
    
    _ArticleSearchRecordArr = [NSMutableArray array];
    _picHeadViewTagArr = [NSMutableArray array];
    _currentPicArray = [NSMutableArray array];
    _picTopTagArr = [NSMutableArray array];
    _isDeleteCache = YES;
    _searchResultArr = [NSMutableArray array];
    _copyData = [NSMutableArray array];
    _isMoreRecord = NO;
    _isSearchArticle = NO;
    _isShowPicTag = NO;
    _isToTopShow = NO;
    _isResultGoDetail = NO;
    _isTap = NO;
    _showTip = NO;
    _isCopyData = YES;
    // Do any additional setup after loading the view from its nib.
    //    [self initTitleName:@"麦萌"];
    _prePage = 1;
    _lyTabbar = (LYTabBarController *)self.tabBarController;
    
    if (IsIOS7) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
//    [self initTitleName:@"美图"];
    _menuView.frame = CGRectMake(0, 0, 160, 44);
    _menuView.delegate = self;
    _menuView.titleArray = @[@"资讯"];
    _menuView.currentIndex = 0;
    [self.view addSubview:_menuView];
    [self.view bringSubviewToFront:_menuView];
#warning =
//    [self createPictureTagView];
    UINavigationItem *item = self.navigationItem;
    item.titleView = _menuView;
    
    
    self.picArray = [[NSMutableArray alloc] init];
    
    [self initView];
//--------文章搜索初始化数据
    [self initArticleSearchRecordData];
    [self createSearchUI];
    
    _redView = [[UIView alloc]initWithFrame:CGRectMake(45, 0, 20, 20)];
    UILabel *num1Label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    num1Label.text = @"1";
    num1Label.textAlignment = 1;
    num1Label.font = [UIFont systemFontOfSize:14];
    num1Label.textColor = [UIColor whiteColor];
    [_redView addSubview:num1Label];
    _redView.backgroundColor = RGBACOLOR(226, 64, 84, 1.0);
    _redView.layer.masksToBounds = YES;
    _redView.layer.cornerRadius = 10;
    
    //    self.tableView.frame = self.scrollView.bounds;
    self.tableView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 40);//-64
//-----
    _rightBtn = [CustomButton buttonWithType:UIButtonTypeCustom];
    [_rightBtn setImage:[UIImage imageNamed:@"tishi_"] forState:UIControlStateNormal];
    _rightBtn.bounds = CGRectMake(0, 0, _rightBtn.imageView.image.size.width, _rightBtn.imageView.image.size.height);
    _rightBtn.showBudge = YES;
    _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self addRightItemWithButton:_rightBtn itemTarget:self action:@selector(rightBtnPressed:)];
    
    _leftBtn = [CustomButton buttonWithType:UIButtonTypeCustom];
    [_leftBtn setImage:[UIImage imageNamed:@"sousuo_3"] forState:UIControlStateNormal];
    _leftBtn.bounds = CGRectMake(0, 0, 40, 40);
    _leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    [self addLeftItemWithButton:_leftBtn itemTarget:self action:@selector(leftBtnPressed:)];
    _redView.hidden = YES;
    [_rightBtn addSubview:_redView];
//    [self readUserInfo];
    if (_cacheMessageData.count == 0) {
        _boolCache = YES;
        [self loadData:NO];
    }else{
        _boolCache = NO;
        for (NSDictionary *item in _cacheMessageData ) {
            [self.dataArray addObject:item];
        }
        self.adView.infoData = [NSMutableArray arrayWithArray:_cacheAdData];
        self.adInfoArray = [NSMutableArray arrayWithArray:_cacheAdData];
        [self addTopicList];
        page++;
    }
    
//    [self adImageList];
//    [self addTopicList];
//    [self refreshContinueData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveHomeNotification:) name:HomeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveRefreshNotification:) name:RefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadSuccess) name:@"TOROOT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRedImage:) name:@"haveNewNotice" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePicArr:) name:@"changePicArr" object:nil];
    
    [self.view bringSubviewToFront:self.hud];
    //    [self versionUpdate];
    
    [self requireAdviseMent];
//    [[DataBaseHelper shared] fetchIdMore];
    
    
    if ([[NSDate date] distanceNowDays]%2 == 0) {

        [self synInfo];
    }
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UPLOGTIME"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970])forKey:@"UPLOGTIME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    if (([[NSDate date] timeIntervalSince1970] - [[[NSUserDefaults standardUserDefaults] objectForKey:@"UPLOGTIME"] integerValue])/3600 > 0.1) {
        [self upLog];
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970])forKey:@"UPLOGTIME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
//--
    _netImageView = [CustomTool createImageView];
    [_scrollView addSubview:_netImageView];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(showBar)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [_netImageView addGestureRecognizer:swipe];
    
    
    UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineImageView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:0.5];
    [self.view addSubview:lineImageView];

    NSInteger countDays = 0;
    NSInteger todatDays = [[NSDate new] distanceNowDays];
    for (NSNumber *savedDate in [[DataBaseHelper shared] fetchDateTime]) {
        if ( [savedDate integerValue] > todatDays - 7 && [savedDate integerValue] < todatDays ) {
            countDays ++;
        }
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"APPEARED"] isEqualToString:@"0"] && countDays >= 5) {
    CustomIOSAlertView *cusIOSAlert = [[CustomIOSAlertView alloc] init];
    cusIOSAlert.tag = 0;
    [cusIOSAlert setContainerView:[self createDemoView]];
    
    [cusIOSAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"日后再说", @"提交", nil]];
    [cusIOSAlert setDelegate:self];
    
    [cusIOSAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
//        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [cusIOSAlert setUseMotionEffects:true];
    
    // And launch the dialog
    [cusIOSAlert show];
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"APPEARED"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        }
    
////操作提示
//    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"ISSHOWTHEHOMEHELP"] isEqualToString:@"1"]){
//        NSArray *windows = [UIApplication sharedApplication].windows;
//        UIWindow *win = [windows objectAtIndex:0];
//        
//        _yaoyiyaohelpIV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
//        _yaoyiyaohelpIV.userInteractionEnabled = YES;
//        _yaoyiyaohelpIV.backgroundColor = RGBACOLOR(0, 0, 0, 0.4);
//        UIImageView *imageVView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2.0-75, ScreenSizeHeight/2.0-75, 150, 150)];
//        imageVView.image = [UIImage imageNamed:@"tips_yao"];
//        imageVView.contentMode = UIViewContentModeScaleToFill;
//        UILabel *ttiLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, imageVView.bottom, ScreenSizeWidth, 60)];
//        ttiLabel.text = @"摇一摇发送反馈";
//        ttiLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
//        ttiLabel.textColor = [UIColor whiteColor];
//        ttiLabel.textAlignment = 1; 
//        [_yaoyiyaohelpIV addSubview:ttiLabel];
//        [_yaoyiyaohelpIV addSubview:imageVView];
//        [win addSubview:_yaoyiyaohelpIV];
//        UIControl *helpC1 = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
//        [helpC1 addTarget:self action:@selector(changeImage:) forControlEvents:UIControlEventTouchUpInside];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ISSHOWTHEHOMEHELP"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//        [win addSubview:helpC1];
//    }
//友盟更新提示
    [self updateYouMeng];
//请求数据判断是否有通知
    if (g_App.userID) {
        [self userDetail];
    }
}

- (void)updateYouMeng{
    [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:@{@"r":API_URL_UPDATE} object:self action:@selector(UMUpdate:)];
}

- (void)UMUpdate:(NSDictionary *)dict {
    if ([dict[@"needUpdate"] integerValue] == 1) {
        [UMCheckUpdate checkUpdateWithAppkey:UMENGKEY channel:@"App Store"];
        
    }
}

-(void)changeImage:(UIControl *)sender{
    [_yaoyiyaohelpIV removeFromSuperview];
    [sender removeFromSuperview];
}

//-(void)changePicArr:(NSNotification *)noti{
//    self.picArray = noti.object;
//    [self.collectionView reloadData];
//}
-(void)refreshRedImage:(NSNotification *)noti{
    _redView.hidden = NO;
}

-(void)createFindView{
    
}

-(void)leftBtnPressed:(UIButton *)btn{
    [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"msh" to_section:@"m" to_step:@"h" type:@"" id:@"0"];
    if (_menuView.currentIndex == 0) {
        [self.navigationController setNavigationBarHidden:YES];
        self.searchBgView.alpha = 0.5;
        [UIView animateWithDuration:0.3 animations:^{
            self.searchBar.frame = CGRectMake(5, 25, KScreenWidth-60-5, 30);
            self.backBtn.frame = CGRectMake(KScreenWidth-60, 25, 60, 30);
            self.searchView.alpha = 1;
            if (_ArticleSearchRecordArr.count == 0) {
                self.searchTableView.alpha = 0;
            }else{
                self.searchTableView.alpha = 1;
            }
            
        }];
        _isSearchArticle = YES;
        [_lyTabbar hiddenBar:YES animated:YES];
        NSLog(@"----%@",self.view.superview);
        
        
        [self.view bringSubviewToFront:self.searchBgView];
        [self.view bringSubviewToFront:self.searchView];
        [self.view bringSubviewToFront:self.searchTableView];
        if (_ArticleSearchRecordArr.count>2) {
            self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+30*2+50);
            self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*2+50);
        }else{
            if (_ArticleSearchRecordArr.count == 0) {
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 30*_ArticleSearchRecordArr.count+50);
            }else if(_ArticleSearchRecordArr.count<=2){
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 30+30*_ArticleSearchRecordArr.count+50);
            }else{
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+30*_ArticleSearchRecordArr.count+50);
            }
            
            self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+50);
        }
        [self.searchBar becomeFirstResponder];
        [self.searchTableView reloadData];
    }else if (_menuView.currentIndex == 1){
//        if (!g_App.userID) {
//            [self loginView];
//            return;
//        }
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"iuc" to_section:@"i" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        LGPhotoPickerViewController *pickerVc = [[LGPhotoPickerViewController alloc] initWithShowType:LGShowImageTypeImageBroswer];
        pickerVc.status = PickerViewShowStatusCameraRoll;
        pickerVc.maxCount = 1;   // 最多能选9张图片
        pickerVc.delegate = self;
        //    self.showType = style;
        [pickerVc showPickerVc:self];
    }
    
}

-(void)initArticleSearchRecordData{
    [_ArticleSearchRecordArr removeAllObjects];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDiretory = [paths objectAtIndex:0];
    _plistPath = [documentDiretory stringByAppendingPathComponent:@"ArticleSearchRecord.plist"];
    NSMutableArray *arr = [NSMutableArray arrayWithContentsOfFile:_plistPath];
    for (int i = arr.count-1; i >=0; i --) {
        [_ArticleSearchRecordArr addObject:arr[i]];
    }
}

-(void)createSearchUI{
    self.searchBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    self.searchBgView.backgroundColor = [UIColor blackColor];
    self.searchBgView.alpha = 0;
    self.searchBgView.userInteractionEnabled = YES;
    _tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(misSearchView)];
    _tapGesture.numberOfTapsRequired = 1;
    
    
    
    _tap1Gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(misSearchTableView)];
    _tap1Gesture.numberOfTapsRequired = 1;
    [self.view addSubview:self.searchBgView];
    
    self.searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 200)];
    self.searchView.backgroundColor = [UIColor whiteColor];
    self.searchView.alpha = 0;
    self.searchView.userInteractionEnabled = YES;
    [self.view addSubview:self.searchView];
    
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(5, -40, KScreenWidth-60-5,30)];
//    self.searchBar.showsCancelButton = YES;
//    self.searchBar.barStyle = UIBarStyleBlack;
    self.searchBar.layer.masksToBounds = YES;
    self.searchBar.layer.cornerRadius = 7;
    self.searchBar.layer.borderWidth = 0.7;
    self.searchBar.layer.borderColor = RGBACOLOR(228, 169, 180, 1.0).CGColor;
    self.searchBar.tintColor = RGBACOLOR(170, 170, 170, 1.0);
    if (IsIOS7) {
        self.searchBar.barTintColor = RGBACOLOR(170, 170, 170, 1.0);
    }
    
    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    self.searchBar.placeholder = @"搜索内容";
    self.searchBar.delegate = self;
    self.searchBar.backgroundImage = [self imageWithColor:[UIColor clearColor] size:self.searchBar.bounds.size];
    [self.searchView addSubview:self.searchBar];
    
    self.backBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-60, -40, 60, 30)];
    [self.backBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.backBtn setTitleColor:RGBACOLOR(100, 100, 100, 1.0) forState:UIControlStateNormal];
    self.backBtn.titleLabel.textAlignment = 1;
    [self.backBtn addTarget:self action:@selector(misSearchView) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView addSubview:self.backBtn];
    
    self.searchTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 2*40+50)];
    self.searchTableView.backgroundColor = [UIColor whiteColor];
    self.searchTableView.tag = 100;
    self.searchTableView.alpha = 0;
    self.searchTableView.bounces = NO;
    self.searchTableView.dataSource = self;
    self.searchTableView.delegate = self;
    self.searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.searchView addSubview:self.searchTableView];
    
    self.searchResultTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.searchBar.bottom+70, KScreenWidth, KScreenheight-60)];
    self.searchResultTableView.tag = 101;
    self.searchResultTableView.delegate = self;
    self.searchResultTableView.dataSource = self;
    self.searchResultTableView.alpha = 0;
    [self.searchView addSubview:self.searchResultTableView];
    
    self.searchResultBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.searchBar.bottom+70, KScreenWidth, KScreenheight-60)];
    self.searchResultBgView.backgroundColor = [UIColor blackColor];
    self.searchResultBgView.alpha = 0;
    [self.searchView addSubview:self.searchResultBgView];
    
    CGPoint offset = self.searchResultTableView.contentOffset;
    CGRect bounds = self.searchResultTableView.bounds;
    CGSize size = self.searchResultTableView.contentSize;
    UIEdgeInsets inset = self.searchResultTableView.contentInset;
    _searchCurrentOffset = offset.y + bounds.size.height - inset.bottom +KScreenheight-60;
    _searchMaximumOffset = size.height;
    
    
    //    [self.view bringSubviewToFront:self.searchView];
}
#pragma mark - searchBar Delegate
//开始
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
//    self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+60+30*_ArticleSearchRecordArr.count);
    if (_isMoreRecord) {
        if (_ArticleSearchRecordArr.count>2) {
            self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+30*_ArticleSearchRecordArr.count+50);
            self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+50);
        }else{
            if (_ArticleSearchRecordArr.count == 0) {
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60);
                self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 0);
            }else{
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 30+30*_ArticleSearchRecordArr.count+50);
                self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+20);
            }
//            self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+30*_ArticleSearchRecordArr.count+50);
//            self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+50);
        }
        
    }else{
        if (_ArticleSearchRecordArr.count>2) {
            self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+30*2+50);
            self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*2+50);
        }else{
            if (_ArticleSearchRecordArr.count == 0) {
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60);
                self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 0);
            }else{
                self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 30+30*_ArticleSearchRecordArr.count+50);
                self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+20);
            }
//            self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+50);
        }
    }
    if (_ArticleSearchRecordArr.count == 0) {
        self.searchTableView.alpha = 0;
    }else{
        self.searchTableView.alpha = 1;
    }
//    self.searchTableView.alpha = 1.0;
//    self.searchResultTableView.alpha = 0;
    [self.searchView bringSubviewToFront:self.searchResultTableView];
    [self.searchView bringSubviewToFront:self.searchResultBgView];
    [self.searchView bringSubviewToFront:self.searchTableView];
    if (_isTap) {
        [self.searchBgView removeGestureRecognizer:_tapGesture];
        [self.searchBgView addGestureRecognizer:_tap1Gesture];
        self.searchResultBgView.alpha = 0.5;
    }else{
        [self.searchBgView removeGestureRecognizer:_tap1Gesture];
        [self.searchBgView addGestureRecognizer:_tapGesture];
        self.searchResultBgView.alpha = 0;
    }
    
    
}
//结束
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}
-(void)misSearchTableView{
    [self.searchBar resignFirstResponder];
    self.searchTableView.alpha = 0;
    self.searchResultBgView.alpha = 0;
    _searchView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight);
}
//取消按钮点击
-(void)misSearchView{
    [self.searchBar resignFirstResponder];
    [self.navigationController setNavigationBarHidden:NO];
    [_lyTabbar hiddenBar:NO animated:YES];
    _searchBar.text = @"";
    self.searchView.alpha = 0;
    self.searchBgView.alpha = 0;
    self.searchResultTableView.alpha = 0;
    self.searchResultBgView.alpha = 0;
    self.searchTableView.alpha = 0;
    self.searchBar.frame = CGRectMake(0, -40, KScreenWidth, 40);
    _isTap = NO;
    _isMoreRecord = NO;
    _isSearchArticle = NO;
    _isResultGoDetail = NO;
    [self initArticleSearchRecordData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
}
//搜索
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (searchBar.text) {
    [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"" id:@"0" detail:@{@"searchName":_searchBar.text}];
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:_plistPath]) {
            [fileManager createFileAtPath:_plistPath contents:nil attributes:nil];
            NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
            [arr writeToFile:_plistPath atomically:YES];
        }
        
        NSMutableArray *rootArray = [NSMutableArray arrayWithContentsOfFile:_plistPath];
        NSString *userData = [[NSString alloc]init];
        userData = searchBar.text;
        BOOL isHave = YES;
        for (int i = 0; i < rootArray.count; i ++) {
            if ([searchBar.text isEqualToString:rootArray[i]]) {
                isHave = NO;
            }
        }
        if (isHave) {
            if (rootArray.count <10) {
                [rootArray addObject:userData];
                [rootArray writeToFile:_plistPath atomically:YES];
                CGRect frame = self.searchTableView.frame;
                frame.size.height += 30;
                self.searchTableView.frame = frame;
            }else{
                [rootArray removeObjectAtIndex:0];
                [rootArray addObject:userData];
                [rootArray writeToFile:_plistPath atomically:YES];
            }
        }
        [self initArticleSearchRecordData];
        [self.searchTableView reloadData];
        
        [self.searchBar resignFirstResponder];
//        self.searchView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight);
//        self.searchResultTableView.alpha = 1;
        
        self.searchTableView.alpha = 0;
        NSMutableDictionary *searchKeyword = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                              (NSString *)API_URL_MESSAGE_LIST,@"r",
                                              @"1",@"page",
                                              @"10",@"size",
                                              @"id",@"order",
                                              [NSString stringWithFormat:@"%@",searchBar.text],@"search_title",nil];
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:searchKeyword object:self action:@selector(searchKeyClike:) method:GETDATA];
        
    }else{
        
    }
}
-(void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    
}
//取消searchbar背景色
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)synInfo{
    
    NSDictionary *saveDict = @{@"r":@"cartoonCollection/synUserCollectionInfo",@"postData":[[DataBaseHelper shared] fetchCartoonPartCollectionStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:saveDict object:self action:@selector(upCollectionData:) method:POSTDATA];
    
    
    NSDictionary *readDict = @{@"r":@"cartoonReadHistory/synUserReadInfo",@"postData":    [[DataBaseHelper shared] fetchCartoonPartReadStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(upReadData:) method:POSTDATA];
    
}

- (void)upReadData:(NSDictionary *)readDict {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] forKey:@"SYNTIME"];
    
}

- (void)upCollectionData:(NSDictionary *)collectionDict {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] forKey:@"SYNTIME"];
    
}

- (void)upLog{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [[Y_X_DataInterface commonParams] enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    
    
    [manager POST:INTERFACE_PREFIXD parameters:@{@"r":@"system/uploadLog"}
constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         NSLog(@"path%@",[[LogHelper shared] getPath]);
//        [[NSBundle mainBundle] ur]
         
       BOOL isS = [formData appendPartWithFileURL:[NSURL URLWithString:[NSString stringWithFormat:@"file://%@",[[LogHelper shared] getPath]]] name:@"logFile" fileName:@"log" mimeType:@"application/rtf" error:nil];
           
     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSString *log = @" ";
         [log writeToFile:[[LogHelper shared] getPath] atomically:YES encoding:NSUTF8StringEncoding error:nil];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"--error:%@",error);
     }];
}

- (void)requireAdviseMent{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"adImage/getBootAds",@"r",
                                    @"1",@"page",
                                    @"999",@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(advertiseMent:) method:GETDATA];
    
//    }

    
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
    
    if (alertView.tag == 0) {
        
        if (buttonIndex == 1) {
            
            [YK_API_request startLoad:@"http://api.playsm.com/index.php" extraParams:@{@"r":@"feedback/score",@"score":@(currentStar)} object:self action:@selector(starRequire:) method:POSTDATA];
            
            CustomIOSAlertView *cusIOSAlert = [[CustomIOSAlertView alloc] init];
            
            cusIOSAlert.tag = 1;
            if (currentStar <3) {
                [cusIOSAlert setContainerView:[self secondCreateView:@"您愿意向我们反馈您在使用中遇到的问题吗"]];
            }else{
                [cusIOSAlert setContainerView:[self secondCreateView:@"你愿意嫁给我吗?不是不是~您愿意给我们5星好评咩"]];
            }
            
            [cusIOSAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"日后再说", @"I do", nil]];
            [cusIOSAlert setDelegate:self];
            
            [cusIOSAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];
            
            [cusIOSAlert setUseMotionEffects:true];
            
            // And launch the dialog
            [cusIOSAlert show];
        }}else if (alertView.tag == 1){
            if (buttonIndex == 1) {
                if (currentStar < 3) {
                    ConstantViewController *feedbackView = [[ConstantViewController alloc] initWithNibName:@"ConstantViewController" bundle:nil];
                    feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    UINavigationController *bac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
                    [self presentViewController:bac animated:YES completion:nil];
                    
                }else {
                    
//                    https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=999759889&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8
                    
                    NSString *str = @"";
                    
                    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0))
                    {
                        str = @"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1023347547&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
                        
                    }else {
                        
                        str = @"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1023347547&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
                        
                    }
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                }
            }
        }
}

- (void)starRequire:(NSDictionary *)requireStar {
    
}

- (UIView *)secondCreateView:(NSString *)titleDes {
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 120)];
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 250, 80)];
    textLabel.textColor = [UIColor blackColor];
    textLabel.text = titleDes;
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentCenter;
    [demoView addSubview:textLabel];
    return demoView;
}

- (UIView *)createDemoView {
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 120)];
    //
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 250, 80)];
    textLabel.textColor = [UIColor blackColor];
    textLabel.text = @"  通过这几天的使用,你对我们的服务满意吗";
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentCenter;
    [demoView addSubview:textLabel];
    
    
    for(int i = 0; i < 5; i ++){
        UIButton *cusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cusButton.frame = CGRectMake(40 + 50 * i,  80, 20, 20);
        NSLog(@"cgrect %@",NSStringFromCGRect(cusButton.frame));
        [cusButton setImage:[UIImage imageNamed:@"star_1.png"] forState:UIControlStateNormal];
        [cusButton setImage:[UIImage imageNamed:@"star_2.png"] forState:UIControlStateSelected];
        [cusButton addTarget:self action:@selector(starButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cusButton.tag = 1000 + i;
        [demoView addSubview:cusButton];
        [self.starArray addObject:cusButton];
    }
    
    return demoView;
}

- (void)starButtonClick:(UIButton *)sender {
    //    sender.selected = !sender.selected;
    for (UIButton *curBtn in self.starArray) {
        curBtn.selected = NO;
    }
    for (int i = 0; i <= sender.tag - 1000; i ++) {
        UIButton *btn = self.starArray[i];
        btn.selected = YES;
        currentStar = sender.tag - 1000;
    }
}



- (void)advertiseMent:(NSDictionary *)advDict
{
    if([advDict[@"results"] count] > 0){
        
        if (![advDict[@"results"][0][@"images"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"ADVTISE"]]) {
            [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"ADVISECOUNT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
//        [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"ADVISECOUNT"];
        
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"images"]forKey:@"ADVTISE"];
        //        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"id"] forKey:@"HOMEADID"];
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"customType"] forKey:@"HOMEADCUSTOMTYPE"];
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"customValue"] forKey:@"OUTVALUE"];
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"duration"] forKey:@"DURATION"];

        [[NSUserDefaults standardUserDefaults] synchronize];
    }else {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ADVTISE"];
        //        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"HOMEADID"];
        [[NSUserDefaults standardUserDefaults] setObject:@""forKey:@"HOMEADCUSTOMTYPE"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"OUTVALUE"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DURATION"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

-(void)showBar{
    [_lyTabbar hiddenBar:NO animated:YES];
}
- (void)refreshContinueData{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCharpteId"]&&[[NSUserDefaults standardUserDefaults] objectForKey:@"currentAlbumId"]) {
        NSDictionary * readDict = @{@"chapterId":[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCharpteId"],@"r":@"cartoonReadHistory/add",@"albumId":[[NSUserDefaults standardUserDefaults] objectForKey:@"currentAlbumId"]};
        
        NSDictionary *headers;
        if (g_App.userInfo.userID != nil) {
            headers = @{@"userID":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
        }else {
            headers = @{@"userID":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
        }
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(saveReadData:) method:@"POST" andHeaders:headers];
    }
}

- (void)saveReadData:(NSDictionary *)saveDict{
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    _isMoreRecord = NO;
//    self.searchBgView.alpha = 0;
//    self.searchView.alpha = 0;
//    self.searchResultTableView.alpha = 0;
}

-(void)viewDidAppear:(BOOL)animated{
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    if (_isResultGoDetail) {
        [self.navigationController setNavigationBarHidden:YES];
    }
    NSLog(@"--%@",(LYTabBarController*)self.tabBarController);

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toNewsTop) name:@"toNewsTop" object:nil];
    [(LYTabBarController*)self.tabBarController hiddenBar:NO animated:YES];
    
    if (g_App.userID) {
        
        [self.view bringSubviewToFront:self.bottomStatusLabel];
    }
    
    //获取本地沙盒路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"articleIsRead.plist"];
    _articleIsReadArr = [NSMutableArray arrayWithContentsOfFile:plistPath];
    [self.tableView reloadData];
    [self.searchResultTableView reloadData];
}



-(void)toNewsTop{
    
    [[LogHelper shared] writeToFilefrom_page:@"ML" from_section:@"MESSAGE" from_step:@"LIST" to_page:@"ML" to_section:@"MESSAGE" to_step:@"LIST" type:@"TOTOP" id:@"0"];
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];//即使没有显示在window上，也不会自动的将self.view释放。
    
    [self dismissViewControllerAnimated:YES completion:^{
        //        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"提示" message:@"加载失败" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        //        [view show];
    }];
    
}


//- (void)readUserInfo {
//    NSUserDefaults *userDefauls = [NSUserDefaults standardUserDefaults];
//    NSLog(@"%@", [userDefauls objectForKey:@"userID"]);
//    if ([userDefauls objectForKey:@"userID"] && [[userDefauls objectForKey:@"userID"] length]) {
//        g_App.userID = [userDefauls objectForKey:@"userID"];
//        g_App.userName = [userDefauls objectForKey:@"name"];
//        g_App.telephone = [userDefauls objectForKey:@"telephone"];
//        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
//        [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[userDefauls objectForKey:@"loginTime"]];
//        g_App.userInfo = [[UserInfo alloc] init];
//        g_App.userInfo.userID = [userDefauls objectForKey:@"userID"];
//        g_App.userInfo.sex = (int)[[userDefauls objectForKey:@"sex"] integerValue];
//        g_App.userInfo.name = [userDefauls objectForKey:@"name"];
//        g_App.userInfo.telephone = [userDefauls objectForKey:@"telephone"];
//        g_App.userInfo.signature = [userDefauls objectForKey:@"signature"];
//        g_App.userInfo.images = [userDefauls objectForKey:@"images"];
//        g_App.userInfo.loginType = [userDefauls objectForKey:@"loginType"];
//    }
//}

#pragma mark - 界面初始化
//- (PullPsCollectionView*)collectionView {
//    if (!_collectionView) {
//        _collectionView = [[PullPsCollectionView alloc] initWithFrame:CGRectMake(ScreenSizeWidth, 0, ScreenSizeWidth, _scrollView.frame.size.height-60)];
//        _collectionView.collectionViewDelegate = self;
//        _collectionView.collectionViewDataSource = self;
//        //        _collectionView.pullDelegate=self;
//        _collectionView.delegate = self;
//        _collectionView.backgroundColor = [UIColor clearColor];
//        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        _collectionView.showsVerticalScrollIndicator = YES;
//        
//        _collectionView.numColsPortrait = 2;
//        _collectionView.numColsLandscape = 2;
//        _collectionView.pullArrowImage = [UIImage imageNamed:@"blueArrow.png"];
//        _collectionView.pullBackgroundColor = [UIColor clearColor];
//        _collectionView.pullTextColor = [UIColor blackColor];
//    }
//    return _collectionView;
//}

- (void)initView {
    
    [self.view addSubview:self.scrollView];
    [self.tableView removeFromSuperview];
    [self.scrollView addSubview:self.tableView];
    
//    self.pictureTopView.frame = CGRectMake(self.scrollView.bounds.size.width, 0, self.pictureTopView.bounds.size.width, self.pictureTopView.bounds.size.height);
//    [self.scrollView addSubview:self.pictureTopView];
//    self.pictureTagView.frame = CGRectMake(self.scrollView.bounds.size.width, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
//    [self.scrollView addSubview:self.pictureTagView];
//    self.collectionView.frame = CGRectMake(self.scrollView.bounds.size.width, 50, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height-6);
//    [self.scrollView addSubview:self.collectionView];
    
    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - KScreenheight, KScreenWidth, KScreenheight)];
        view.delegate = self;
        [_collectionView addSubview:view];
        _refreshHeaderView = view;
    }
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    //    self.tableView.bounces = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.adView = [[CustomAdView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 180)];
    self.tableView.tableHeaderView = self.adView;
    self.adView.delegate = self;
#warning 第一天修改的
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width, 0)];
    //    self.scrollView.backgroundColor = [UIColor grayColor];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.bounces = NO;
    self.scrollView.scrollsToTop = NO;
    self.collectionView.scrollsToTop = NO;
    self.tableView.scrollsToTop = YES;
    
    if (IsIOS7) {
        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableView setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    
    self.topShowView = [[TopShowView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 0)];
//    [self.scrollView bringSubviewToFront:_twoBtnView];
    [self.view addSubview:_topShowView];
    
//    [self.scrollView bringSubviewToFront:_collectionView];
//    [self.scrollView bringSubviewToFront:self.pictureTagView];
//    [self.scrollView bringSubviewToFront:self.pictureTopView];
    
}

- (void)receiveHomeNotification:(NSNotification*)notification {
    if ([[notification.object objectForKey:@"type"] integerValue] == 11) {
        self.menuView.currentIndex = 0;
    } else if ([[notification.object objectForKey:@"type"] integerValue] == 12) {
        self.menuView.currentIndex = 1;
    }
    [self customMenuViewSelectedWithIndex:self.menuView.currentIndex];
}

- (void)receiveRefreshNotification:(NSNotification*)notification {
    if (g_App.userID) {
    }
}

#pragma mark - 按钮事件
- (void)rightBtnPressed:(UIButton*)btn {
    if (_menuView.currentIndex == 0) {
        _redView.hidden = YES;
        
        AdviceListViewController *noticeView = [[AdviceListViewController alloc] init];
        noticeView.adviceDict = _dataDict;
        noticeView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:noticeView animated:YES];
    }else if (_menuView.currentIndex == 1){
        if (!g_App.userID) {
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"nl" to_section:@"i" to_step:@"l" type:@"history" id:[NSString stringWithFormat:@"%d",0]];
            [self loginView];
            return;
        }
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"ipl" to_section:@"i" to_step:@"l" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
        [self.navigationController pushViewController:useruploadPic animated:YES];
    }
    
}

- (void)loginView {
    LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [loginView setSkipNext:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
        [self.navigationController pushViewController:useruploadPic animated:YES];
        
    }];
    
    CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
}

#pragma mark - getter && setter
- (UIScrollView*)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64  - 49  + 80)];//修改的，有个切换的滚动视图//_menuView.bounds.size.height
        //        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64 - 49)];
        _scrollView.scrollsToTop = NO;
        
    }
    return _scrollView;
}


#pragma mark - CustomMenuViewDelegate
//-----------导航栏元素项点击事件
- (void)customMenuViewSelectedWithIndex:(NSInteger)index {
//    [self.scrollView setContentOffset:CGPointMake(self.scrollView.bounds.size.width * index, 0) animated:YES];
//    [self loadingData];
//    
//    if (index == 0) {
//        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"move" id:@""];
//        self.tableView.scrollsToTop = YES;
////        self.collectionView.scrollsToTop = NO;
//        _isToTopShow = NO;
//         [_rightBtn setImage:[UIImage imageNamed:@"tishi_"] forState:UIControlStateNormal];
//        _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
//        [_leftBtn setImage:[UIImage imageNamed:@"sousuo_3"] forState:UIControlStateNormal];
//        
//    } else if (index == 1) {
//         [_rightBtn setImage:[UIImage imageNamed:@"shagnchuanjilv"] forState:UIControlStateNormal];
//        [_leftBtn setImage:[UIImage imageNamed:@"tianjai"] forState:UIControlStateNormal];
//        _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, -25);
//        [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"move" id:@""];
//        self.tableView.scrollsToTop = NO;
//        self.collectionView.scrollsToTop = YES;
//    }
}

#pragma mark - CustomAdViewDelegate
- (void)customAdViewSelectWithIndex:(int)index {
    [_lyTabbar hiddenBar:NO animated:YES];
    [self adImageLogAdd:[[self.adInfoArray objectAtIndex:index] objectForKey:@"id"]];
    NSInteger type = [[[self.adInfoArray objectAtIndex:index] objectForKey:@"customType"] integerValue];
    NSString *value = NSValueToString([[self.adInfoArray objectAtIndex:index] objectForKey:@"customValue"]);
    
    switch (type) {
        case 1: {
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"manhuago"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"md" to_section:@"m" to_step:@"d" type:@"banner" id:value];
            InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
            detailView.infoID = value;
            //            detailView.pushType = 2;
            [self.navigationController pushViewController:detailView animated:YES];
        }
            break;
        case 2: {
            PicDetailViewController *picDetailView = [[PicDetailViewController alloc] initWithNibName:@"PicDetailViewController" bundle:nil];
            picDetailView.imageID = value;
            [self.navigationController pushViewController:picDetailView animated:YES];
        }
            break;
        case 3: {
            GiftDetailController *detailView = [[GiftDetailController alloc] initWithNibName:@"GiftDetailController" bundle:nil];
            detailView.giftID = value;
            [self.navigationController pushViewController:detailView animated:YES];
        }
            break;
            
        case 13: {
            GiftViewController *giftView = [[GiftViewController alloc] initWithNibName:@"GiftViewController" bundle:nil];
            [self.navigationController pushViewController:giftView animated:YES];
        }
            break;
            
            
        case 20: {
            BOOL flag = NO;
            if ([value containsString:@"applibao/package/index.do"]) {
                if (!g_App.userID) {
                    LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以查看礼包哦~"];
                    [alertView show];
                    return;
                }
                flag = YES;
                
                NSString *secret = [Tool md5:[NSString stringWithFormat:@"ios%@%@",g_App.userID,@"MaiMeng2015"]];
                value = [NSString stringWithFormat:@"%@?userId=%@&type=ios&sign=%@", value, g_App.userID, secret];
            }
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.needSecret = flag;
            webView.url = value;
            
            [self.navigationController pushViewController:webView animated:YES];
        }
            break;
            
        case 21: {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:value]];
            // 友盟统计Ad
            NSDictionary *dic = @{@"aDid" : [[self.adInfoArray objectAtIndex:index] objectForKey:@"id"], @"title" : [[self.adInfoArray objectAtIndex:index] objectForKey:@"title"], @"idfa": [NSString stringWithFormat:@"%@-%@",[[self.adInfoArray objectAtIndex:index] objectForKey:@"id"],g_App.adId]};
            [MobClick event:@"adClick" attributes:dic];
        }
            break;
        case 22: {
            [[NSNotificationCenter defaultCenter] postNotificationName:RefreshNotification object:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark - PSCollectionViewDelegate
- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView viewAtIndex:(NSInteger)index
{
    CategeryCell *cell = (CategeryCell*)[collectionView dequeueReusableView];
    
    if (!cell) {
        cell = [[CategeryCell alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth/2 - 5, 30)];
    }
    [cell.categoryImage setImageWithURL:[NSURL URLWithString:[[self.picArray objectAtIndex:index] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2.png"]];
    cell.praiseNum.text = [NSString stringWithFormat:@"%@",[[self.picArray objectAtIndex:index] objectForKey:@"praiseCount"]];
    
    return cell;
}

- (CGFloat)heightForViewAtIndex:(NSInteger)index
{
    if (self.picArray.count) {
        if (![[[self.picArray objectAtIndex:index] objectForKey:@"width"] integerValue]) {
            return ScreenSizeWidth / 2 - 5;
        } else {
            return [[[self.picArray objectAtIndex:index] objectForKey:@"height"] integerValue] * (1.0*(ScreenSizeWidth / 2 - 5) / [[[self.picArray objectAtIndex:index] objectForKey:@"width"] integerValue]);
        }
    } else {
        return ScreenSizeWidth / 2 - 5;
    }
}
- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index
{
    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"pl" to_page:@"" to_section:@"p" to_step:@"c" type:@"c" channel:@"" id:[NSString stringWithFormat:@"%d",index]];

    [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:[NSString stringWithFormat:@"%d",index]];
        NSMutableArray *data = [NSMutableArray array];
        for (NSDictionary *dic in self.picArray) {
            [data addObject:[dic objectForKey:@"sourceImages"]];
        }
        self.currentPicArray = data;
    
    //启动图片浏览器
    HZPhotoBrowser *browserVc = [[HZPhotoBrowser alloc] init];
    //    browserVc.sourceImagesContainerView = self.collectionView; // 原图的父控件
    browserVc.imageCount = self.currentPicArray.count; // 图片总数
    browserVc.currentImageIndex = (int)index;
    browserVc.type = 0;
    browserVc.delegate = self;
    browserVc.dataArr = self.picArray;
    browserVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browserVc animated:YES completion:nil];
}
//- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index
//{
//    PicDetailViewController *detailView = [[PicDetailViewController alloc] initWithNibName:@"PicDetailViewController" bundle:nil];
//    detailView.imageID = [[self.picArray objectAtIndex:index] objectForKey:@"id"];
//    detailView.imageData = self.picArray;
//    detailView.imageCount = self.picArray.count;
//    detailView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:detailView animated:YES completion:nil];
//}

- (NSInteger)numberOfViewsInCollectionView:(PSCollectionView *)collectionView
{
    return [self.picArray count];
}

//- (void)pullPsCollectionViewDidTriggerRefresh:(PullPsCollectionView *)pullTableView
//{
//    [self performSelector:@selector(refreshData) withObject:nil afterDelay:0.0f];
//}
//
//- (void)pullPsCollectionViewDidTriggerLoadMore:(PullPsCollectionView *)pullTableView
//{
//    [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.0f];
//}

#pragma mark - photorowser
////临时占位图（thumbnail图
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}
////高清原图 （bmiddle图）
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[self.currentPicArray objectAtIndex:index]];
}
//
- (NSString*)photoDrowserWithIndex:(NSInteger)index {
    return [[self.picArray objectAtIndex:index] objectForKey:@"id"];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView.tag == 100) {
        return 1;
    }else if (tableView.tag == 101){
        return 1;
    }else if (tableView.tag == 102){
        return 3;
    }else{
        return self.dataArray.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 100) {
        if (_ArticleSearchRecordArr.count>2) {
            if (_isMoreRecord) {
                return _ArticleSearchRecordArr.count;
            }else{
                return 2;
            }
            
        }else{
            return _ArticleSearchRecordArr.count;
        }
    }else if(tableView.tag == 101){
        if (_searchResultArr.count>0) {
            _searchResultTableView.userInteractionEnabled = YES;
            return _searchResultArr.count;
        }else{
            _searchResultTableView.userInteractionEnabled = NO;
            return 1;
        }
        
    }else if(tableView.tag == 102){
        return 3;
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView.tag == 100) {
        if (_ArticleSearchRecordArr.count==0){
            return 0.01;
        }else{
            return 20;
        }
    }else if (tableView.tag == 101 || tableView.tag == 102){
        return 0.01;
    }else{
        return 0.5;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (tableView.tag == 100) {
        if (_ArticleSearchRecordArr.count>2) {
            if (_isMoreRecord) {
                UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 30)];
                footView.userInteractionEnabled = YES;
                
                UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 30)];
                [btn setTitle:@"清空搜索记录" forState:UIControlStateNormal];
                [btn setTitleColor:RGBACOLOR(170, 170, 170, 1.0) forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:14];
                [btn addTarget:self action:@selector(deleteAllRecord) forControlEvents:UIControlEventTouchUpInside];
                
                [footView addSubview:btn];
                return footView;
            }else{
                UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 30)];
                footView.userInteractionEnabled = YES;
                
                UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 30)];
                [btn setTitle:@"展开搜索记录" forState:UIControlStateNormal];
                [btn setTitleColor:RGBACOLOR(170, 170, 170, 1.0) forState:UIControlStateNormal];
                btn.titleLabel.font = [UIFont systemFontOfSize:14];
                [btn addTarget:self action:@selector(showAllRecord) forControlEvents:UIControlEventTouchUpInside];
                
                [footView addSubview:btn];
                return footView;
            }
        }else{
            return nil;
        }
        
    }else if(tableView.tag == 101){
        return nil;
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 50)];
//        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 0, self.tableView.bounds.size.width-40, 1)];
//        line.backgroundColor = RGBACOLOR(230, 230, 230, 1.0);
//        //    [view addSubview:line];
//        UIButton *moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 0, self.tableView.bounds.size.width-10, 45)];
//        moreBtn.layer.masksToBounds = YES;
//        moreBtn.layer.cornerRadius = 10;
//        moreBtn.layer.borderWidth = 1;
//        moreBtn.layer.borderColor = RGBACOLOR(230, 230, 230, 1.0).CGColor;    moreBtn.backgroundColor = [UIColor whiteColor];//RGBACOLOR(242,242, 242, 1.0);
//        [moreBtn setTitle:@"更多文章" forState:UIControlStateNormal];
//        [moreBtn setTitleColor:RGBACOLOR(200, 200, 200, 1.0) forState:UIControlStateNormal];
//        //    [moreBtn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
//        moreBtn.titleLabel.textAlignment = 1;
//        [moreBtn addTarget:self action:@selector(loadMoreSearchData) forControlEvents:UIControlEventTouchUpInside];
//        [view addSubview:moreBtn];
//        if (_searchPageNum*10 >= _searchCountTotal) {
//            moreBtn.hidden = YES;
//        }else{
//            moreBtn.hidden = NO;
//        }
//        return view;
    }else{
        return nil;
    }
}



-(void)showAllRecord{
    _isMoreRecord = YES;
    [self.searchBar resignFirstResponder];
    self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 60+30*_ArticleSearchRecordArr.count+50);
    self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom+5, KScreenWidth, 30*_ArticleSearchRecordArr.count+50);
    [self.searchTableView reloadData];
}
-(void)deleteAllRecord{
    
//    NSFileManager *fileManager = [[NSFileManager alloc] init];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDiretory = [paths objectAtIndex:0];
//    NSString *plistPath = [documentDiretory stringByAppendingPathComponent:@"ArticleSearchRecord.plist"];
    
    NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
    [arr writeToFile:_plistPath atomically:NO];

    [self initArticleSearchRecordData];
    
    self.searchView.frame = CGRectMake(0, 0, KScreenWidth, 30*_ArticleSearchRecordArr.count+60);
    self.searchTableView.frame = CGRectMake(0, self.searchBar.bottom, KScreenWidth, 30*_ArticleSearchRecordArr.count);
    
    [self.searchTableView reloadData];
    NSLog(@"haha  meile");
}
//
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag == 100) {
        if (_ArticleSearchRecordArr.count==0){
            return nil;
        }else{
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 20)];
            label.backgroundColor = RGBACOLOR(246, 247, 249, 1.0);
            label.text = @"  最近搜过";
            label.textColor = RGBACOLOR(170, 170, 170, 1.0);
            label.font = [UIFont systemFontOfSize:12];
            return label;
        }
    }else{
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (tableView.tag == 100) {
        if (_ArticleSearchRecordArr.count>2) {
            return 30;
        }else{
            return 0.01;
        }
        
    }else if (tableView.tag == 101){
        return 0.01;
    }else{
        return 0.5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 100) {
        return 30;
    }else if (tableView.tag == 101){
        if (_searchResultArr.count>0) {
            return 110;
        }else{
            return ScreenSizeHeight-60;
        }
        
    }else{
        return 110;
    }
}

-(void)deleteSelf:(UIButton *)btn{
    [_ArticleSearchRecordArr removeObjectAtIndex:btn.tag];
    [_ArticleSearchRecordArr writeToFile:_plistPath atomically:YES];
    
    CGRect searchViewFrame = self.searchView.frame;
    CGRect searchTableViewFrame = self.searchTableView.frame;
    if (_isMoreRecord) {
        if (_ArticleSearchRecordArr.count == 2){
            searchViewFrame.size.height -= 60;
            searchTableViewFrame.size.height -= 60;
        }else if(_ArticleSearchRecordArr.count == 0){
            searchViewFrame.size.height -= 50;
            searchTableViewFrame.size.height -= 50;
        }else{
            searchViewFrame.size.height -= 30;
            searchTableViewFrame.size.height -= 30;
        }
        self.searchView.frame = searchViewFrame;
        self.searchTableView.frame = searchTableViewFrame;
    }else{

        if(_ArticleSearchRecordArr.count == 0){
            searchViewFrame.size.height -= 50;
            searchTableViewFrame.size.height -= 50;
        }else{
            searchViewFrame.size.height -= 30;
            searchTableViewFrame.size.height -= 30;
        }
        if (_ArticleSearchRecordArr.count<3) {
            self.searchView.frame = searchViewFrame;
            self.searchTableView.frame = searchTableViewFrame;
        }
    }
    
    [self.searchTableView reloadData];
    NSLog(@"%ld",btn.tag);
}



- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 100) {//搜索
        HomeSearchCell *cell = [[HomeSearchCell alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
        cell.dataStr = _ArticleSearchRecordArr[indexPath.row];
        cell.deleteBtn.tag = [indexPath row];
        NSLog(@"----%@",_ArticleSearchRecordArr);
        [cell.deleteBtn addTarget:self action:@selector(deleteSelf:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else if (tableView.tag == 101){
        if (_searchResultArr.count>0) {
            static NSString *cellIdentifier = @"infoCell";

            InfoCell *cell = [[InfoCell alloc]init];
            cell = [[[NSBundle mainBundle] loadNibNamed:@"InfoCell" owner:self options:nil] lastObject];
            if (![self.dataArray count]) {
                return cell;
            }
            cell.contentBg.layer.cornerRadius = 2;
            
            [cell.infoImage setImageWithURL:[NSURL URLWithString:[[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
            cell.infoImage.layer.borderWidth = 0.5;
            cell.infoImage.layer.borderColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1].CGColor;
//            cell.infoContentLabel.text = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"title"];
            NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc]initWithString:[[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"title"]];
            NSRange range = [[titleStr string] rangeOfString:[NSString stringWithFormat:@"%@",_searchBar.text]];
            [titleStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
            cell.infoContentLabel.attributedText = titleStr;
            
            
            
            cell.authLabel.text = [NSString stringWithFormat:@"作者：%@",[[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"author"]];
            cell.authLabel.textColor = [UIColor colorWithRed:216/255.0 green:189/255.0 blue:189/255.0 alpha:1];
            cell.timeLabel.text = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"];
            cell.timeLabel.textColor = [UIColor colorWithRed:216/255.0 green:189/255.0 blue:189/255.0 alpha:1];
            cell.appraiseCountLabel.text = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"praiseCount"];
            [cell resetSize];
            
            //遍历   判断颜色
            for (int i = 0; i < _articleIsReadArr.count; i ++) {
                if ([cell.infoContentLabel.text isEqualToString:_articleIsReadArr[i]]) {
                    cell.infoContentLabel.textColor= [UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1];
                    return cell;
                }
            }
            return cell;
        }else{
            UITableViewCell *cell = [[UITableViewCell alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight-60)];
            UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth/2.0-50, (KScreenheight-60)/2.0-100, 100, 100)];
            iv.image = [UIImage imageNamed:@"searchnoresult.jpg"];
            [cell.contentView addSubview:iv];
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, (KScreenheight-60)/2.0+10, KScreenWidth, 40)];
            label.text = @"!找遍了都没有哦(￣3￣)。。。";
            label.textAlignment = 1;
            [cell.contentView addSubview:label];
            return cell;
        }
        
    }else{
        static NSString *cellIdentifier = @"infoCell";
        //    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        //    if (!cell) {
        //        cell = [InfoCell infoCellOwner:self];
        //    }
        InfoCell *cell = [[InfoCell alloc]init];
        cell = [[[NSBundle mainBundle] loadNibNamed:@"InfoCell" owner:self options:nil] lastObject];
        if (![self.dataArray count]) {
            return cell;
        }
        cell.contentBg.layer.cornerRadius = 2;
        
        [cell.infoImage setImageWithURL:[NSURL URLWithString:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
        cell.infoImage.layer.borderWidth = 0.5;
        cell.infoImage.layer.borderColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1].CGColor;
        cell.infoContentLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"title"];
        cell.authLabel.text = [NSString stringWithFormat:@"作者：%@",[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"author"]];
        cell.authLabel.textColor = [UIColor colorWithRed:216/255.0 green:189/255.0 blue:189/255.0 alpha:1];
        cell.timeLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"createTimeValue"];
        cell.timeLabel.textColor = [UIColor colorWithRed:216/255.0 green:189/255.0 blue:189/255.0 alpha:1];
        cell.appraiseCountLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"praiseCount"];
        [cell resetSize];
        
        //遍历   判断颜色
        for (int i = 0; i < _articleIsReadArr.count; i ++) {
            if ([cell.infoContentLabel.text isEqualToString:_articleIsReadArr[i]]) {
                cell.infoContentLabel.textColor= [UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1];
                return cell;
            }
        }
        return cell;
    }
    
}

-(void)searchKeyClike:(NSDictionary *)dict{
    _searchPageNum = 2;
    self.searchResultTableView.alpha = 1.0;
    self.searchResultBgView.alpha = 0;
    _isTap = YES;
    [self.view bringSubviewToFront:self.searchResultTableView];
    _searchView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight);
    _searchResultTableView.contentOffset = CGPointMake(0, 0);
    [_searchResultArr removeAllObjects];
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        _searchCountTotal = [dict[@"extraInfo"][@"countTotal"] intValue];
        for (NSDictionary *item in [dict objectForKey:@"results"] ) {
            [_searchResultArr addObject:item];
        }
        [_searchResultTableView reloadData];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 100) {//搜索关键词
        [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"click" id:@"0" detail:@{@"searchName":_ArticleSearchRecordArr[indexPath.row]}];
        self.searchTableView.alpha = 0;
        self.searchBar.text = [NSString stringWithFormat:@"%@",_ArticleSearchRecordArr[indexPath.row]];
        NSMutableDictionary *searchKeyword = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 (NSString *)API_URL_MESSAGE_LIST,@"r",
                                                 @"1",@"page",
                                                 @"10",@"size",
                                              @"id",@"order",
                                              [NSString stringWithFormat:@"%@",_ArticleSearchRecordArr[indexPath.row]],@"search_title",nil];
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:searchKeyword object:self action:@selector(searchKeyClike:) method:GETDATA];
        
        
        [self.searchBar resignFirstResponder];
    }else {
//        if (tableView.tag == 101){//搜索结果显示
//            InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
//            [self.navigationController setNavigationBarHidden:NO];
//            [self.navigationController pushViewController:detailView animated:YES];
//            
//            detailView.infoID = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"id"];
//            detailView.titleName = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"title"];
//            detailView.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:detailView animated:YES];
//            NSLog(@"----");
//        }else
        
        if (tableView.tag == 101) {
            _isResultGoDetail = YES;
        }else{
            _isResultGoDetail = NO;
        }
        
        [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"md" to_section:@"m" to_step:@"d" type:@"normal" id:[[self.dataArray objectAtIndex:indexPath.section]objectForKey:@"id"]];
            
//        [[LogHelper shared] writeToFilefrom_page:@"ml" to_page:@"md" from_section:<#(NSString *)#> to_section:<#(NSString *)#> to_step:<#(NSString *)#> type:<#(NSString *)#> id:<#(NSString *)#> to_section:@"m" to_step:@"detail" type:@"click"id:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"]];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"manhuago"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
//        InfoDetailViewControllerMax *detailView = [[InfoDetailViewControllerMax alloc]initWithNibName:@"InfoDetailViewControllerMax" bundle:nil];
        if (tableView.tag == 101) {
            detailView.infoID = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"id"];
            detailView.titleName = [[_searchResultArr objectAtIndex:indexPath.row] objectForKey:@"title"];
        }else{
            detailView.infoID = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"];
            detailView.titleName = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"title"];
        }
        
        //写入文章名到articleIsRead.plist
        //获取本地沙盒路径
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //获取完整路径
        NSString *documentsPath = [path objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"articleIsRead.plist"];
        NSMutableArray *rootArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
        NSString *userData = [[NSString alloc]init];
        userData = detailView.titleName;
        ///Users/zhujiaman/Library/Developer/CoreSimulator/Devices/B16008C9-C24B-4C5E-BC88-96E5AE0246C1/data/Containers/Data/Application/35F644FB-0F5D-4E99-8DB1-30BE47F6D57F/Documents/articleIsRead.plist
        BOOL isHave = YES;
        for (int i = 0; i < rootArray.count; i ++) {
            if ([detailView.titleName isEqualToString:rootArray[i]]) {
                isHave = NO;
            }
        }
        if (isHave) {
            if (rootArray.count <=500) {
                [rootArray addObject:userData];
                [rootArray writeToFile:plistPath atomically:YES];
            }else{
                [rootArray removeObjectAtIndex:0];
                [rootArray addObject:userData];
                [rootArray writeToFile:plistPath atomically:YES];
            }
        }
        detailView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailView animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)doneLoadingTableViewData{
    [self loadData:NO];
}
//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:1.0];
}
#pragma mark - scrollView
//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    [self.tableView tableViewDidEndDragging:scrollView];
//}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _lastContentOffset = scrollView.contentOffset.y;
    _beginScrollViewX = scrollView.contentOffset.x;
    
}
//always
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y< _lastContentOffset )
    {//向下拉
        if (!_isSearchArticle) {
            [_lyTabbar hiddenBar:NO animated:YES];
        }
    }
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        CGPoint contentOffsetPoint = self.tableView.contentOffset;
        CGRect frame = self.tableView.frame;
        if (contentOffsetPoint.y >= self.tableView.contentSize.height - frame.size.height - 20|| self.tableView.contentSize.height < frame.size.height)
        {
            [self.tableView tableViewDidEndDragging:scrollView];
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.0f];
        }
        [self.tableView tableViewDidScroll:scrollView];
        
    } else if([scrollView isKindOfClass:[PullPsCollectionView class]]) {
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
    }
}

//停止拖动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
//    CGSize size1 = _collectionView.contentSize;
//    CGFloat maximumOffset = size1.height;
//    if (scrollView.contentOffset.y< _lastContentOffset )
//    {//向下拉
//        [_lyTabbar hiddenBar:NO animated:YES];
//    } else
    if (scrollView. contentOffset.y >_lastContentOffset ){//向上拉
        [_lyTabbar hiddenBar:YES animated:YES];
    }
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        CGPoint contentPo = self.searchResultTableView.contentOffset;
        NSLog(@"==%f",contentPo.y);
        if (contentPo.y>self.searchResultTableView.contentSize.height-KScreenheight+64) {
            [self loadMoreSearchData];
        }
        CGPoint contentOffsetPoint = self.tableView.contentOffset;
        if (contentOffsetPoint.y < -50)
        {
            [self.tableView tableViewDidEndDragging:scrollView];
        }
        
    } else if([scrollView isKindOfClass:[PullPsCollectionView class]]) {
//        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
//        if (_collectionView.contentOffset.y>=maximumOffset - KScreenheight ) {
////            if (_isShowPicTag) {
////                
////            }else{
//                [self loadData:YES];
////            }
//            
//        }
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {//用来控制红色滚动条
        //        if (scrollView.contentOffset.x != _beginScrollViewX) {
        //            self.menuView.currentIndex = (scrollView.contentOffset.x + 1) / scrollView.bounds.size.width;
        //            NSLog(@"-----%f--%f",scrollView.contentOffset.x,scrollView.bounds.size.width);
        //            [self loadingData];
        //        }
    }
}
//减速停止
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _isToTopShow = NO;
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
    } else if([scrollView isKindOfClass:[UICollectionView class]]) {
        
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        if (scrollView.contentOffset.x != _beginScrollViewX) {
            self.menuView.currentIndex = (scrollView.contentOffset.x + 1) / scrollView.bounds.size.width;
            NSLog(@"-----%f--%f",scrollView.contentOffset.x,scrollView.bounds.size.width);
            if (self.menuView.currentIndex  == 0) {
                [_rightBtn setImage:[UIImage imageNamed:@"tishi_"] forState:UIControlStateNormal];
                _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
                [_leftBtn setImage:[UIImage imageNamed:@"sousuo_3"] forState:UIControlStateNormal];
            } else if (self.menuView.currentIndex  == 1) {
                _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, -25);
                [_rightBtn setImage:[UIImage imageNamed:@"shagnchuanjilv"] forState:UIControlStateNormal];
                [_leftBtn setImage:[UIImage imageNamed:@"tianjai"] forState:UIControlStateNormal];
            }
            [self loadingData];
        }
    }
}



- (void)loadingData {
    
    if ((_scrollView.contentOffset.x + 1) / KScreenWidth == 1) {
        
    }
    
    self.picCount = nil;
    self.infoCount = nil;
    self.isLoading = NO;
    
    switch (self.menuView.currentIndex) {
        case 0: {
            if (!self.dataArray.count) {
                [self loadData:NO];
            }
        }
            break;
        case 1: {
            if (!self.picArray.count) {
                [self loadData:NO];
            }
        }
            break;
        default:
            break;
    }
}

//上拉加载更多---资讯
-(void)loadData:(BOOL)isOrmore{
    
    if (self.isLoading) {
        return;
    }
//    if (!_isShowPicTag) {
        if (isOrmore == YES) {
            _boolCache = NO;
            page++;
            self.isMore = YES;
            _isCopyData = YES;
        } else {
            _boolCache = YES;
            page = 1;
            self.isMore = NO;
            static int one = 0;
            if (one == 0) {
                _isCopyData = YES;
                one ++;
            }else{
                _isCopyData = NO;
            }
            
            if (self.menuView.currentIndex == 0) {
                [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"refresh" id:@"0"];
                
                [self.dataArray removeAllObjects];
            } else {
                [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"refresh" id:@"0"];
                
                [self.picArray removeAllObjects];
            }
        }
//    }
    
    self.isLoading = YES;
    if (self.menuView.currentIndex == 0) {
        
//        [[LogHelper shared] writeToFilefrom_page:@"" from_section:@"m" from_step:@"l" to_page:@"crh" to_section:@"m" to_step:@"l" type:@"more" id:@"0"];
        
        
        
        [self messageList];
        [self adImageList];
        [self addTopicList];
    } else {
        
        if (_isShowPicTag) {
//            self.isLoading = NO;
            [self loadTagPicData];
        }else{
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"more" id:@"0"];
            [self prettyImageList];
        }
    }
}


#pragma mark - PullingRefreshCollectionViewDelegate
-(void)refreshData
{
    _isDeleteCache = NO;
    [self deleteTableData];
    [self loadData:NO];
}

-(void)loadMoreData
{
    [self loadData:YES];
}

- (void)pullingCollectionViewDidStartRefreshing:(PullingRefreshTableView *)tableView
{
    [self performSelector:@selector(refreshData) withObject:nil afterDelay:0.2f];
}

- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView
{
    //    [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.2f];
}

- (NSDate *)pullingTableViewRefreshingFinishedDate
{
    return [NSDate date];
}

#pragma mark - 加载数据
- (void)messageList {
    [self.hud show:YES];
    NSLog(@"----%@",self.dataArray);
    
    //    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ml" to_page:@"" to_section:@"m" to_step:@"list" type:@"more" channel:@"h" id:@""];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_MESSAGE_LIST,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    @"1",@"page",nil];
    if (self.isMore) {
        if (self.dataArray.count) {
            [exprame setObject:[[self.dataArray objectAtIndex:self.dataArray.count -1] objectForKey:@"id"] forKey:@"lastID"];
        }
    }
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(messageListFinish:) method:GETDATA];
    
    
}

-(void)misMis{
    [self.topShowView hide];
}

- (void)messageListFinish:(NSDictionary*)dic {
     _isDeleteCache = YES;
    _netImageView.alpha = 0;
    [self.hud hide:YES];
    self.isLoading = NO;
    _showTip = NO;
    int itemCount = 0;
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        if (_boolCache) {
            [self saveMessStatuses:[dic objectForKey:@"results"]];
        }
        
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        if (_isCopyData) {
            _copyData = self.dataArray;
        }
        itemCount = (int)[[dic objectForKey:@"results"] count];
        _lastID = [NSString stringWithFormat:@"%@",self.dataArray[0][@"id"]];
        [self.tableView reloadData];
        
        if (![self.dataArray count]) {
            //            self.rightBtn.hidden = YES;
            self.contentStatusLabel.hidden = NO;
        } else {
            //            self.rightBtn.hidden = NO;
            self.contentStatusLabel.hidden = YES;
        }
        if (!self.isMore) {
            if (self.infoCount) {
                if ([[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue] <= [self.infoCount intValue]){
                    BOOL right = YES;;
                    for (int i = 0; i<self.dataArray.count; i++) {
                        if ([self.dataArray[i][@"id"] isEqualToString:_copyData[0][@"id"]]) {
                            right = NO;
                            if (i!=0) {
                                self.topShowView.showInfo = [NSString stringWithFormat:@"资讯%d条参上~",i];
                                [self.topShowView show];
                                [self performSelector:@selector(misMis) withObject:nil afterDelay:2.5];
                            }
                            break;
                        }
                    }
                    if (right) {
                        self.topShowView.showInfo = [NSString stringWithFormat:@"资讯%d条参上~",10];
                        [self.topShowView show];
                        [self performSelector:@selector(misMis) withObject:nil afterDelay:2.5];
                    }
                    
//                    self.topShowView.showInfo = @" 没有更新的内容了啦~";
//                    [self.topShowView show];
                }else{
                    int i = [[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue]-[self.infoCount intValue];
                    self.topShowView.showInfo = [NSString stringWithFormat:@"资讯%d条参上~",i];
                    [self.topShowView show];
                    [self performSelector:@selector(misMis) withObject:nil afterDelay:2.5];
                }
            }
//            if (self.topShowView.showInfo) {
//                [self.topShowView show];
//            }
            self.infoCount = [[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"];
        } else {
            if (itemCount == 0) {
                [self bottomShow];
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ISCACHEMESSAGELISTDATA"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        if (self.dataArray.count == 0) {
//            [_refreshBtn2 removeFromSuperview];
//            _netImageView.alpha = 1;
//            _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 64);
//            _refreshBtn1 = [CustomTool createBtn];
//            [_netImageView addSubview:_refreshBtn1];
//            [_refreshBtn1 addTarget:self action:@selector(loadingData) forControlEvents:UIControlEventTouchUpInside];
        }else{
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2-45, KScreenheight-80-64, 90, 40)];
            label.backgroundColor = [UIColor grayColor];
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = 6;
            label.textColor = [UIColor blackColor];
            label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            label.alpha = 1;
            label.text = @"木有网啊";
            label.textAlignment = 1;
            [self.view addSubview:label];
            [UIView animateWithDuration:1 animations:^{
                label.alpha = 0;
            }];
            label = nil;
        }
    }
    
    [self.tableView tableViewDidFinishedLoading];
    
#warning 推送
//    [self preView];
}

- (void)prettyImageList {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ml" to_page:@"" to_section:@"p" to_step:@"list" type:@"" channel:@"h" id:@""];

    [self.hud show:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_PRETTYIMAGESLIST,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    @"1",@"page",nil];
    if (self.isMore) {
        if (self.picArray.count) {
            [exprame setObject:[[self.picArray objectAtIndex:self.picArray.count -1] objectForKey:@"id"] forKey:@"lastID"];
        }
    }
    
//    if (self.picCount) {
//        [exprame setObject:self.picCount forKey:@"lastCount"];
//    }
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(prettyImageListFinish:) method:GETDATA];
}

- (void)prettyImageListFinish:(NSDictionary*)dic {
    self.isLoading = NO;
    [self.hud hide:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.picArray addObject:item];
        }
        [self.collectionView reloadData];
        if (![self.picArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else{
            self.contentStatusLabel.hidden = NO;
        }
        if (!self.isMore) {//图片总数
            if (self.picCount) {
                if ([[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue] <= [self.picCount intValue]){
//                    self.topShowView.showInfo = @" 没有更新的内容了啦~";
//                    [self.topShowView show];
                }else{
                    int i = [[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue]-[self.picCount intValue];
                    self.topShowView.showInfo = [NSString stringWithFormat:@"美图%d张参上~",i];
                    [self.topShowView show];
                }
            }
//            if (self.topShowView.showInfo) {
//                [self.topShowView show];
//            }
            
            self.picCount = [[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"];
        } else {
            if ([[dic objectForKey:@"results"] count] == 0 ) {
                self.bottomStatusLabel.text = @"图都被你啃光啦~";
                [self bottomShow];
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ISCACHEPRETTYIMAGELISTDATA"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        if (self.picArray.count == 0) {
            [_refreshBtn1 removeFromSuperview];
            _netImageView.frame = CGRectMake(KScreenWidth, 0, KScreenWidth, KScreenheight - 64 - 40);
            _netImageView.alpha = 0;
            
            _refreshBtn2 = [CustomTool createBtn];
            [_netImageView addSubview:_refreshBtn2];
            [_refreshBtn2 addTarget:self action:@selector(prettyImageList) forControlEvents:UIControlEventTouchUpInside];
        }else{
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2-45, KScreenheight-80-64, 90, 40)];
            label.backgroundColor = [UIColor grayColor];
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = 6;
            label.textColor = [UIColor blackColor];
            label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            label.alpha = 1;
            label.text = @"木有网啊";
            label.textAlignment = 1;
            [self.view addSubview:label];
            [UIView animateWithDuration:1 animations:^{
                label.alpha = 0;
            }];
            label = nil;
        }
    }
    
    _collectionView.pullLastRefreshDate = [NSDate date];
    _collectionView.pullTableIsRefreshing = NO;
    _collectionView.pullTableIsLoadingMore = NO;
}

- (void)userDetail {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"user/informCount",@"r",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(userDetailFinish:) method:GETDATA];
}

- (void)userDetailFinish:(NSDictionary*)dic {
    if (![[dic objectForKey:@"code"] integerValue]) {
        NSDictionary *dict = dic[@"results"];
        
        int num1 = [dict[@"informCount"] intValue];
        if (num1 == 0) {
            _redView.hidden = YES;
        }else{
            _redView.hidden = NO;
        }
    }
}

//http://api.playsm.com/index.php?adImage/list
- (void)adImageList {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ml" to_page:@"" to_section:@"p" to_step:@"list" type:@"" channel:@"b" id:@""];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_ADIMAGE_LIST,@"r",
                                    @"1",@"page",
                                    @"999",@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(adImageListFinish:) method:GETDATA];
    
}


- (void)adImageListFinish:(NSDictionary*)dic {
    if (![[dic objectForKey:@"code"] integerValue]) {
        if (_boolCache) {
            [self saveADStatuses:[dic objectForKey:@"results"]];
        }
        self.adView.infoData = [dic objectForKey:@"results"];
        self.adInfoArray = [dic objectForKey:@"results"];
    }
}

-(void)addTopicList{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_SPECIAL_LIST,@"r",
                                    @"1",@"page",
                                    @"2",@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(addTopicListFinish:) method:GETDATA];
}

-(void)addTopicListFinish:(NSDictionary *)dic{
    if (![[dic objectForKey:@"code"] integerValue]) {
        self.adView.specialListData = [dic objectForKey:@"results"];
        NSArray *numArr = [dic objectForKey:@"results"];
        if (numArr.count > 0) {
            self.adView.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, 180+80*_Scale6 + 18+ 10);
            self.tableView.tableHeaderView = self.adView;
        }else{
            self.adView.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, 180);
            self.tableView.tableHeaderView = self.adView;
        }
    }
}

- (void)adImageLogAdd:(NSString*)adId {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ml" to_page:@"" to_section:@"p" to_step:@"list" type:@"m" channel:@"h" id:@""];

    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_ADIMAGE_LOG_ADD,@"r",
                                    adId,@"id",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(adImageLogAddFinish:) method:POSTDATA];
}

//-(void)preView{
//    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
//    NSString *infoStr = [userDefaultes stringForKey:@"myValue"];
//    NSString *testStr = [userDefaultes stringForKey:@"testViewControllerValue"];
//    
//    if (infoStr) {
//        [[LogHelper shared] writeToFilefrom_page:@"null" from_section:@"null" from_step:@"null" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:infoStr];
//        InfoDetailViewController *info = [[InfoDetailViewController alloc]init];
//        info.infoID = infoStr;
//        [self.navigationController pushViewController:info animated:YES];
//        [userDefaultes setObject:nil forKey:@"myValue"];
//        [userDefaultes synchronize];
//    }
//    if (testStr) {
//        [[LogHelper shared] writeToFilefrom_page:@"null" from_section:@"null" from_step:@"null" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:testStr];
//        testViewController *testVC = [[testViewController alloc]init];
//        [testVC setChangeCollection:^{
//            
//        }];
//        
//        testVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//        [self presentViewController:testVC animated:YES completion:^{
//            testVC.valueId = testStr;
//            testVC.customType = @"5";
//            [userDefaultes setObject:nil forKey:@"testViewControllerValue"];
//            [userDefaultes synchronize];
//        }];
//    }
//}

- (void)adImageLogAddFinish:(NSDictionary*)dic {
    if (![[dic objectForKey:@"code"] integerValue]) {
        
    }
}

#pragma mark - 美图标签
//-(void)jumpToUpLoadView:(UIButton *)btn{
//    
//}
//-(void)jumpToUserSelfView:(UIButton *)btn{
//    UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
//    [self.navigationController pushViewController:useruploadPic animated:YES];
//}
-(void)createPictureTagView{
    //----top
    self.pictureTopView = [[UIView alloc]initWithFrame:CGRectMake(ScreenSizeWidth, 0, ScreenSizeWidth, 50)];
    self.pictureTopView.backgroundColor = [UIColor whiteColor];
    UIButton *allButton = [[UIButton alloc]initWithFrame:CGRectMake(5, 10, 60, 35)];
    allButton.backgroundColor = [UIColor whiteColor];
    allButton.layer.cornerRadius = 4.0;
    allButton.layer.borderWidth = 1.0;
    allButton.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
    allButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [allButton setTitle:@"全部" forState:UIControlStateNormal];
    [allButton addTarget:self action:@selector(showAllPicture) forControlEvents:UIControlEventTouchUpInside];
    [allButton setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
    [self.pictureTopView addSubview:allButton];
    
    _picTitleView = [[UIView alloc]initWithFrame:CGRectMake(70, 0, KScreenWidth-130, 50)];
    _picTitleView.backgroundColor = [UIColor whiteColor];
    _picTitleView.alpha = 1;
    [self.pictureTopView addSubview:_picTitleView];
    
    _goTopBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth*2-55, KScreenheight-44 - 52 - 64-6, 50, 50)];
    _goTopBtn.alpha = 0;
    [_goTopBtn setImage:[UIImage imageNamed:@"top"] forState:UIControlStateNormal];
    [_goTopBtn addTarget:self action:@selector(goToTop:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:_goTopBtn];
    
    UIButton *moreTagButton = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth - 60, 10, 60, 35)];
    moreTagButton.backgroundColor = [UIColor whiteColor];
    moreTagButton.layer.cornerRadius = 4.0;
    moreTagButton.tag = 10001;
    [moreTagButton setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];//shouqiDetail
    [moreTagButton addTarget:self action:@selector(showAllPicTag:) forControlEvents:UIControlEventTouchUpInside];
    [self.pictureTopView addSubview:moreTagButton];
    
    //------tag
    self.pictureTagView = [[UIScrollView alloc]initWithFrame:CGRectMake(ScreenSizeWidth, -(ScreenSizeHeight -60), ScreenSizeWidth,ScreenSizeHeight -60)];
    self.pictureTagView.backgroundColor = [UIColor whiteColor];
    self.pictureTagView.contentSize = CGSizeMake(0, ScreenSizeHeight+100);
    self.pictureTagView.delegate = self;
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/getLabelList",@"r",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(createButtonForPicture:) method:GETDATA];
    
}

-(void)goToTop:(UIButton *)btn{
    [self.pictureTagView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)showAllPicture{
    _isShowPicTag = NO;
    UIButton *btn = (UIButton *)[self.pictureTopView viewWithTag:10001];
    //    [self.scrollView bringSubviewToFront:_collectionView];
    //    [self.scrollView bringSubviewToFront:_twoBtnView];
    _goTopBtn.alpha = 0;
    //    _twoBtnView.alpha = 1;
    btn.selected = !btn.selected;
    btn.transform = CGAffineTransformMakeRotation(0);
    [UIView animateWithDuration:0.1 animations:^{
        _picTitleView.alpha = 1;
        self.pictureTagView.frame = CGRectMake(self.scrollView.bounds.size.width, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
    }];
    
    
    
    if (_selectBtnTag) {
        UIButton *button = (UIButton *)[self.pictureTagView viewWithTag:_selectBtnTag];
        button.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        [button setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        
        UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:_selectBtnTag];
        button1.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        [button1 setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        
    }    //    [_collectionView setContentOffset:CGPointMake(0, 41) animated:YES];
    _selectBtnTag = nil;
    [self refreshData];
}

-(void)showAllPicTag:(UIButton *)btn{
    
    
    if(btn.selected){
        //        [self.scrollView bringSubviewToFront:_collectionView];
        //        [self.scrollView bringSubviewToFront:_twoBtnView];
        [[LogHelper shared] writeToFilefrom_page:@"itl" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        _goTopBtn.alpha = 0;
        [UIView animateWithDuration:0.3 animations:^{
            _picTitleView.alpha = 1;
            btn.transform = CGAffineTransformMakeRotation(0);
            self.pictureTagView.frame = CGRectMake(self.scrollView.bounds.size.width, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
        }];
        [_lyTabbar hiddenBar:NO animated:YES];
        //        _twoBtnView.alpha = 1;
        _isToTopShow = NO;
        
        //        [btn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
    }else{
        //        [self.scrollView bringSubviewToFront:self.pictureTagView];
        
        //        _goTopBtn.alpha = 1;
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"itl" to_section:@"i" to_step:@"l" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        _isToTopShow = YES;
        self.pictureTagView.alpha = 1;
        [UIView animateWithDuration:0.3 animations:^{
            _picTitleView.alpha = 0;
            btn.transform = CGAffineTransformMakeRotation(-(M_PI-0.000001));
            self.pictureTagView.frame = CGRectMake(ScreenSizeWidth, 50, ScreenSizeWidth,ScreenSizeHeight -60);
        }];
        //        _twoBtnView.alpha = 0;
        [_lyTabbar hiddenBar:YES animated:YES];
        //        [btn setImage:[UIImage imageNamed:@"shouqiDetail"] forState:UIControlStateNormal];
        [self.scrollView bringSubviewToFront:_goTopBtn];
    }
    btn.selected = !btn.selected;
}

-(void)createButtonForPicture:(NSDictionary *)dict{
    
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        NSArray *picTagArr = dict[@"results"];
        self.pictureTagView.contentSize = CGSizeMake(0, (picTagArr.count/3+1)*45+100);
        CGFloat joinComicWidth = KScreenWidth/3;
        
        for (int i = 0; i < 3; i ++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            joinComic.frame = CGRectMake(5 + (KScreenWidth-130)/3 * (i%4), 40 * (i/3)+10, (KScreenWidth-130)/3 - 7, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",picTagArr[i]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            //            joinComic.tag = [picTagArr[i][@"id"] integerValue];
            joinComic.tag = 100+i;
            [_picHeadViewTagArr addObject:[NSString stringWithFormat:@"%li",(long)joinComic.tag]];
            [_picTopTagArr addObject:[NSString stringWithFormat:@"%li",(long)joinComic.tag]];
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [_picTitleView addSubview:joinComic];
            if (i == 2) {
                _thirdBtnName = [NSString stringWithFormat:@"%@",picTagArr[i]];
            }
        }
        
        for(int i = 0; i < picTagArr.count; i++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            joinComic.frame = CGRectMake(6 + joinComicWidth * (i%3), 45 * (i/3)+7, joinComicWidth - 10, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",picTagArr[i]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            //            joinComic.tag = [picTagArr[i][@"id"] integerValue];
            joinComic.tag = 100+i;
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [self.pictureTagView addSubview:joinComic];
        }
    }
}


-(void)picWitchYouWant:(UIButton *)btn{
    if (_selectBtnTag!=0) {
        if (btn.tag != _selectBtnTag) {
            [self.picArray removeAllObjects];
            page = 1;
            UIButton *button = (UIButton *)[self.pictureTagView viewWithTag:_selectBtnTag];
            UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:_selectBtnTag];
            button.layer.borderWidth = 1.0;
            [button setTitleColor:RGBACOLOR(155, 155, 155, 1.0) forState:UIControlStateNormal];
            button.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
            button1.layer.borderWidth = 1.0;
            [button1 setTitleColor:RGBACOLOR(155, 155, 155, 1.0) forState:UIControlStateNormal];
            button1.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
        }
    }else{
        [self.picArray removeAllObjects];
    }
    if (!_selectBtnTag) {
        [self.picArray removeAllObjects];
    }
    BOOL isInTopArr = false;
    for (int i = 0; i < 3; i ++) {
        if (btn.tag == [_picHeadViewTagArr[i] intValue]) {
            UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:btn.tag];
            [button1 setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
            button1.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
            isInTopArr = YES;
        }
    }
    if (isInTopArr) {
        UIButton *button1 = (UIButton *)[self.pictureTagView viewWithTag:btn.tag];
        [button1 setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
        button1.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
    }else {
        UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:[_picHeadViewTagArr[2] intValue]];
        [_picHeadViewTagArr removeLastObject];
        [_picHeadViewTagArr addObject:[NSString stringWithFormat:@"%ld",btn.tag]];
        [button1 setTitle:btn.currentTitle forState:UIControlStateNormal];
        [button1 setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
        button1.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
        button1.tag = btn.tag;
    }
    _picBtnTagName = [NSString stringWithFormat:@"%@",btn.currentTitle];
    btn.layer.borderWidth = 1.0;
    [btn setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
    btn.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
    _selectBtnTag = (int)btn.tag;
    [self loadTagPicData];
}

-(void)loadTagPicData{
    [self.hud show:YES];
    [[LogHelper shared] writeToFilefrom_page:@"itl" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"tag" id:[NSString stringWithFormat:@"%d",0] detail:[NSString stringWithFormat:@"{'tag':'%@'}",_picBtnTagName]];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/list",@"r",[NSString stringWithFormat:@"%d",page],@"page",[NSString stringWithFormat:@"%d",size],@"size",_picBtnTagName,@"searchLabel",nil];
    //    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                                    (NSString *)API_URL_PRETTYIMAGESLIST,@"r",
    //                                    [NSString stringWithFormat:@"%d",size],@"size",
    //                                    @"1",@"page",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(showTagPic:) method:GETDATA];
}
-(void)showTagPic:(NSDictionary *)dict{
    [self.hud hide:YES];

//    [self.scrollView bringSubviewToFront:_collectionView];
//    [self.scrollView bringSubviewToFront:_twoBtnView];;
//    _twoBtnView.alpha = 1;
    
    _goTopBtn.alpha = 0;
    [UIView animateWithDuration:0.1 animations:^{
        _picTitleView.alpha = 1;
        self.pictureTagView.frame = CGRectMake(self.scrollView.bounds.size.width, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
    }];
    self.isLoading = NO;
    UIButton *btn = (UIButton *)[self.pictureTopView viewWithTag:10001];
    
//    [btn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
    btn.selected = !btn.selected;
    btn.transform = CGAffineTransformMakeRotation(0);
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        _isShowPicTag = YES;
        NSArray *picarr = dict[@"results"];
        for (NSDictionary *item in picarr ) {
            [self.picArray addObject:item];
        }
        [self.collectionView reloadData];
    }
}

-(void)loadMoreSearchData{
    NSMutableDictionary *searchKeyword = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          (NSString *)API_URL_MESSAGE_LIST,@"r",
                                          [NSString stringWithFormat:@"%d",_searchPageNum],@"page",
                                          @"10",@"size",
                                          @"id",@"order",
                                          _searchBar.text,@"search_title",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:searchKeyword object:self action:@selector(loadMoreSearchData:) method:GETDATA];
}

-(void)loadMoreSearchData:(NSDictionary *)dict{
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        for (NSDictionary *item in [dict objectForKey:@"results"] ) {
            [_searchResultArr addObject:item];
        }
        [_searchResultTableView reloadData];
        _searchPageNum ++;
    }
}
#pragma mark -
- (void)uploadSuccess{
    
    
    UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-100, ScreenSizeHeight/2 - 45, 200, 45)];
    tipView.text = @"发布成功";
    tipView.layer.masksToBounds = YES;
    tipView.textAlignment = 1;
    tipView.textColor = [UIColor whiteColor];
    tipView.layer.cornerRadius = 22.5;
    tipView.backgroundColor = [UIColor blackColor];
    tipView.alpha = 0.4;
    [self.view addSubview:tipView];
    [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:2.0];
//    alert = [[UIAlertView alloc] initWithTitle:nil message:@"发布成功" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
//    
//    [alert show];
    
    
    
//    NSTimer *timer;
//    
//    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(doTime) userInfo:nil repeats:NO];
    
}
//
//- (void)doTime{
//    [alert dismissWithClickedButtonIndex:0 animated:NO];
//    
//}
-(void)missTipView:(UILabel *)label{
    //    _showNoMore = YES;
    [label removeFromSuperview];
}
#pragma mark - 数据缓存
 /*-数据缓存-*/
- (void)initialize//打开数据库
{
    // 1.打开数据库
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"maimengCache.db"];
        _db = [FMDatabase databaseWithPath:path];
    [_db open];
}
- (void)createTableS{// 2.创表
    // 表中有三列，一个是主键，另一个是数据对象的id，一个是数据对象
    [_db executeUpdate:@"CREATE TABLE IF NOT EXISTS MessageList (id varchar(64) PRIMARY KEY, messageKey varchar(64));"];
    [_db executeUpdate:@"CREATE TABLE IF NOT EXISTS AdList (id varchar(64) PRIMARY KEY, adKey varchar(64));"];
}
//- (void)saveStatuses:(NSArray *)statuses andTableName:(NSString *)tableName andTableKeyName:(NSString *)keyName
//{
//    // statuses是字典数组
//    // 要将一个对象存进数据库的blob字段,最好先转为NSData
//    // 一个对象要遵守NSCoding协议,实现协议中相应的方法,才能转成NSData
//    for (NSDictionary *status in statuses) {
//        // NSDictionary --> NSData
//        NSData *statusData = [NSKeyedArchiver archivedDataWithRootObject:status];
////        NSString *insertStr = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@);",tableName,keyName,statusData];
////        [_db executeUpdateWithFormat:@"INSERT INTO %@(%@) VALUES (%@);",tableName,keyName,statusData];
//        [_db executeUpdateWithFormat:@"INSERT INTO MessageList(messageKey) VALUES (%@);",statusData];
//    }
//}
- (void)saveMessStatuses:(NSArray *)statuses
{
    // statuses是字典数组
    // 要将一个对象存进数据库的blob字段,最好先转为NSData
    // 一个对象要遵守NSCoding协议,实现协议中相应的方法,才能转成NSData
    for (NSDictionary *status in statuses) {
        // NSDictionary --> NSData
        NSData *statusData = [NSKeyedArchiver archivedDataWithRootObject:status];
        [_db executeUpdateWithFormat:@"INSERT INTO MessageList(messageKey) VALUES (%@);",statusData];
    }
}
- (void)saveADStatuses:(NSArray *)statuses
{
    // statuses是字典数组
    // 要将一个对象存进数据库的blob字段,最好先转为NSData
    // 一个对象要遵守NSCoding协议,实现协议中相应的方法,才能转成NSData
    for (NSDictionary *status in statuses) {
        // NSDictionary --> NSData
        NSData *statusData = [NSKeyedArchiver archivedDataWithRootObject:status];
        [_db executeUpdateWithFormat:@"INSERT INTO AdList(adKey) VALUES (%@);",statusData];
    }
}

- (NSArray *)statusesWithParams:(NSDictionary *)params andTableName:(NSString *)tableName andTableKeyName:(NSString *)keyName
{
    // 根据请求参数生成对应的查询SQL语句
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@;",tableName];
    // 执行SQL
    FMResultSet *set = [_db executeQuery:sql];
    NSMutableArray *statuses = [NSMutableArray array];
    while (set.next) {
        NSData *statusData = [set objectForColumnName:[NSString stringWithFormat:@"%@",keyName]];
        NSDictionary *status = [NSKeyedUnarchiver unarchiveObjectWithData:statusData];
        [statuses addObject:status];
    }
    return statuses;
}
-(void)deleteTableData{
    [_db executeUpdateWithFormat:@"DELETE FROM MessageList"];
    [_db executeUpdateWithFormat:@"DELETE FROM ADList"];
}

@end
