//
//  GuideViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/30.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "GuideViewController.h"
#import "Config.h"
#import "LYTabBarController.h"

@interface GuideViewController ()

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.view.backgroundColor = [UIColor blackColor];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self addImageView];
}

- (void)addImageView {
    for (int i = 0 ; i < 2 ; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.view.bounds.size.width, 0, ScreenSizeWidth, ScreenSizeHeight)];
        if (([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO) || ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(960, 640), [[UIScreen mainScreen] currentMode].size) : NO)) {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide-%d-5.jpg",i+1]];
        }
        
        if (([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO) || ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1136, 640), [[UIScreen mainScreen] currentMode].size) : NO)) {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide-%d-5.jpg",i+1]];
        }
        
        if (([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO) || ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1334, 750), [[UIScreen mainScreen] currentMode].size) : NO)) {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide-%d-6p.jpg",i+1]];
        }
        
        if ([[UIScreen mainScreen] currentMode].size.width >= 1080) {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide-%d-6p.jpg",i+1]];
        }
        
        [self.scrollView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        if (i == 1) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchInView)];
            [imageView addGestureRecognizer:recognizer];
        }
    }
    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width * 2, 0)];
}

- (void)touchInView {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    LYTabBarController *tabBar = [[LYTabBarController alloc] init];
    //    tabBar.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    [self presentViewController:tabBar animated:YES completion:nil];
    //    _rootTab = [[LYTabBarController alloc] init];
    self.view.window.rootViewController = tabBar;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
