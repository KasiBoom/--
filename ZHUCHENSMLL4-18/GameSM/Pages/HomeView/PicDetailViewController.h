//
//  PicDetailViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "SingleImageView.h"
#import "CustomAlertView.h"
#import "UMSocial.h"
#import "HZPhotoBrowser.h"
#import "UIButton+WebCache.h"
#import "HZPhotoItemModel.h"

@interface PicDetailViewController : BaseViewController<SingleImageViewDelegate, CustomAlertViewDelegate, UMSocialUIDelegate>

@property (nonatomic, strong) SingleImageView *imgShowView;

@property (nonatomic, strong) NSString *imageID;

@property (nonatomic, strong) NSArray *imageData;
@property (nonatomic, assign) int index;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;

- (IBAction)saveBtnPressed:(id)sender;
- (IBAction)shareBtnPressed:(id)sender;
@end
