//
//  InfoCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "CustomView.h"

@interface InfoCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *infoContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *authLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *infoImage;
@property (weak, nonatomic) IBOutlet CustomView *contentBg;
@property (weak, nonatomic) IBOutlet UILabel *appraiseCountLabel;

+ (id)infoCellOwner:(id)owner;

- (void)resetSize;

@end
