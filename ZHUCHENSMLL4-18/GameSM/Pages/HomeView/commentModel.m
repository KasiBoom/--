//
//  commentModel.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/1.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "commentModel.h"
#import "userOrreplyIDInfo.h"
#define TMARGIN 5
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@implementation commentModel
//{
//    NSMutableArray *_faceNameArr;
//}
//- (void)setFaceNameArr:(NSMutableArray *)faceNameArr{
//    if(_faceNameArr == nil){
//    }
//}
//
//- (NSMutableArray *)faceNameArr{
//    return _faceNameArr;
//}

+(NSMutableArray *)paraDict:(NSDictionary *)dict{
    
   NSMutableArray * _faceNameArr = [NSMutableArray array];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
    }

    
    NSArray *CommmentArr = dict[@"results"];
    NSMutableArray *commentAllInfo = [NSMutableArray array];
    for (NSDictionary *commentDict in CommmentArr) {
        commentModel *commentMo = [[commentModel alloc] init];
        commentMo.id = commentDict[@"id"];
        commentMo.type = commentDict[@"type"];
        commentMo.userIDInfo = [userOrreplyIDInfo paraseUserOrReplyIDInfo: commentDict[@"userIDInfo"]];
        commentMo.userID = commentDict[@"userID"];
        commentMo.replyUserID = commentDict[@"replyUserID"];
        commentMo.contentID = commentDict[@"contentID"];
        commentMo.valueID = commentDict[@"valueType"];
        commentMo.valueType = commentDict[@"valueID"];
        commentMo.status = commentDict[@"status"];
        commentMo.createTime = commentDict[@"createTime"];
        commentMo.modifyTime = commentDict[@"modifyTime"];
        commentMo.content = commentDict[@"content"];
        commentMo.ip = commentDict[@"ip"];
        commentMo.viewType = commentDict[@"viewType"];
        commentMo.deviceID = commentDict[@"deviceID"];
        commentMo.createTimeValue = commentDict[@"createTimeValue"];
        commentMo.contentValue = commentDict[@"contentValue"];
        if (dict[@"replyUserIDInfo"]) {
            commentMo.replyUserIDInfo = [userOrreplyIDInfo paraseUserOrReplyIDInfo:dict[@"replyUserIDInfo"]];
        }
        
        [commentAllInfo addObject:commentMo];
        
    }
    
    dispatch_apply(commentAllInfo.count, dispatch_get_global_queue(0, 0), ^(size_t n) {
        commentModel *commentMo = commentAllInfo[n];
        NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] init];
        NSString *cententString = commentMo.content;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 8;// 字体的行间距
        NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:16],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        //#warning 正则表达式 "\\[[^\\]]+\\]"--"\\#[^\\#]+\\#"
        NSString *regexString = @"\\[[^\\]]+\\]";
        //        NSMutableAttributedString *attri = [labelFu.attributedText mutableCopy];//_model.content
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
        NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
        if (arrr.count>0) {
            for (int i =0 ; i < arrr.count; i ++) {
                NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
                NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
                NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
                [attri appendAttributedString:attrStr1];
                NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
                for (int j = 0; j < _faceNameArr.count; j ++) {
                    if ([faceStr isEqualToString:_faceNameArr[j]]) {
                        NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                        attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                        attch.bounds = CGRectMake(0, -2, 30, 30);
                        NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                        [attri appendAttributedString:string1];
                        break;
                    }
                }
                cententString = [cententString substringFromIndex:result.range.location+result.range.length];
            }
            NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
            [attri appendAttributedString:attrStr2];
            [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
//            labelFu.attributedText = [attri copy];
            commentMo.attrStr = [attri copy];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect middle = [attri boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50-100, MAXFLOAT) options:options context:nil];
            commentMo.rect = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, middle.size.width, middle.size.height + 20);
            
        }else{
            NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
            NSRange allRange = [cententString rangeOfString:cententString];
            [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
            [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
            
            commentMo.attrStr = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
//            labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
            int height = (int)rect.size.height/19;
            commentMo.rect = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-40-30, height*27+7);
        }

        
    });

    

    
    return commentAllInfo;
}

//- (NSMutableArray *)returnFaceArr{
//}

@end
