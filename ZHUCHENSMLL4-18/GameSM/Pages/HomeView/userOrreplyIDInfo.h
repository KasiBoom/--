//
//  userOrreplyIDInfo.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/1.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userOrreplyIDInfo : NSObject
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *images;
+ (userOrreplyIDInfo *)paraseUserOrReplyIDInfo:(NSDictionary *)userDict;
@end
