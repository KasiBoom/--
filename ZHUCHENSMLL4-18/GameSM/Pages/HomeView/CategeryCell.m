//
//  CategeryCell.m
//  bang
//
//  Created by 王涛 on 15/6/6.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "CategeryCell.h"
#import "Config.h"

@implementation CategeryCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        self.contentbg = [[CustomView alloc] initWithFrame:self.bounds];
        [self addSubview:self.contentbg];
        self.contentbg.userInteractionEnabled = YES;
        
        self.categoryImage = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, self.bounds.size.width - 6, self.bounds.size.height - 6-40)];
        [self addSubview:self.categoryImage];
        self.categoryImage.userInteractionEnabled = YES;
        self.categoryImage.contentMode = UIViewContentModeScaleAspectFill;
        self.categoryImage.clipsToBounds = YES;
        
        self.userImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, self.bounds.size.height - 6-30, 20, 20)];
        self.userImage.layer.masksToBounds = YES;
        self.userImage.layer.cornerRadius = 10;
        [self addSubview:self.userImage];
        
        self.userName = [[UILabel alloc]initWithFrame:CGRectMake(30, self.bounds.size.height - 6-30, 100, 20)];
        self.userName.font = [UIFont systemFontOfSize:10];
        self.userName.textColor = RGBACOLOR(148, 148, 148, 1.0);
        [self addSubview:self.userName];
        
        self.praCountImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width-100, self.bounds.size.height - 6-30, 13, 13)];
        self.praCountImage.image = [UIImage imageNamed:@"meitu_dianzan_5"];
        [self addSubview:self.praCountImage];
        
        self.praCount = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.size.width-85, self.bounds.size.height - 6-30, 30, 13)];
        self.praCount.font = [UIFont systemFontOfSize:10];
        self.praCount.textColor = RGBACOLOR(148, 148, 148, 1.0);
        [self addSubview:self.praCount];
        
        self.commentImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width-50, self.bounds.size.height - 6-30, 13, 13)];
        self.commentImage.image = [UIImage imageNamed:@"meitu_pinglun_2"];
        [self addSubview:self.commentImage];
        
        self.commentCount = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.size.width-30, self.bounds.size.height - 6-30, 30, 13)];
        self.commentCount.textColor = RGBACOLOR(148, 148, 148, 1.0);
        self.commentCount.font = [UIFont systemFontOfSize:10];
        [self addSubview:self.commentCount];
        
        
//        self.praiseNumImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width - 50, self.bounds.size.height - 16, 50, 16)];
//        self.praiseNumImage.image = [UIImage imageNamed:@"dianzanyinying"];
//        [self addSubview:self.praiseNumImage];
//        
//        self.praiseHandleImage = [[UIImageView alloc]initWithFrame:CGRectMake(7, 2, 13, 13)];
//        self.praiseHandleImage.image = [UIImage imageNamed:@"dianzan_"];
//        [self.praiseNumImage addSubview:self.praiseHandleImage];
//        
//        self.praiseNum = [[UILabel alloc]initWithFrame:CGRectMake(20, 1, 36, 15)];
//        self.praiseNum.textAlignment = 1;
//        self.praiseNum.font = [UIFont systemFontOfSize:11];
//        self.praiseNum.textColor = [UIColor whiteColor];
//        [self.praiseNumImage addSubview:self.praiseNum];
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.contentbg.frame = self.bounds;
    self.bgView.frame = self.bounds;
//    self.bgView.
//    self.contentbg.layer.borderWidth = 1;
//    self.contentbg.layer.borderColor = [UIColor colorWithRed:191/255.0 green:191/255.0 blue:191/255.0 alpha:1].CGColor;
    
    self.categoryImage.frame = CGRectMake(2.5, 2.5, self.bounds.size.width - 5, self.bounds.size.height - 5-30);
    
    self.userImage.frame = CGRectMake(5, self.bounds.size.height - 6-22, 20, 20);
    if (ScreenSizeWidth<=321) {
        self.userName.frame = CGRectMake(27, self.bounds.size.height - 6-19, 60, 14);
    }else{
        self.userName.frame = CGRectMake(27, self.bounds.size.height - 6-19, 100, 14);
    }
    self.praCountImage.frame = CGRectMake(self.bounds.size.width-68, self.bounds.size.height - 6-18, 14, 14);
    self.praCount.frame = CGRectMake(self.bounds.size.width-52, self.bounds.size.height - 6-18, 18, 14);
    self.commentImage.frame = CGRectMake(self.bounds.size.width-34, self.bounds.size.height - 6-17, 14, 14);
    self.commentCount.frame = CGRectMake(self.bounds.size.width-18, self.bounds.size.height - 6-18, 18, 14);
    
//    self.praiseNumImage.frame = CGRectMake(self.bounds.size.width - 56-3, self.bounds.size.height - 17-3, 56, 17);
//    self.praiseHandleImage.frame = CGRectMake(0, 0, 20, 20);
//    self.praiseNum = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 40, 20)];
    
}

@end
