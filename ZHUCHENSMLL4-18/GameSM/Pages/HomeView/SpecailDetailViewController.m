//
//  SpecailDetailViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/16.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "SpecailDetailViewController.h"
#import "Config.h"
#import "UIImageView+WebCache.h"
#import "InfoDetailViewController.h"

@interface SpecailDetailViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate>
{
    NSString *_cellIdentifier;
    LYTabBarController *_lyTabbar;
    NSMutableArray *_dataArr;
    NSDictionary *_dictData;
}
@end

@implementation SpecailDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    _lyTabbar = (LYTabBarController *)self.tabBarController;
    [_lyTabbar hiddenBar:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackBtn];
    
    _dataArr = [NSMutableArray array];
    [self createUI];
    // Do any additional setup after loading the view.
    
    [self addSpecialList];
//    [self createShakeView];
    
}

-(void)createUI{
    
    
//    [self.view addSubview:self.titleImageView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = 10;
    flowLayout.minimumLineSpacing = 10;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight-64) collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.tag = 1000;
    _cellIdentifier = @"cellIdentifier";
    self.collectionView.backgroundColor = RGBACOLOR(240, 240, 240, 1.0);
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:_cellIdentifier];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView"];
//    self.tableView.tableFooterView = self.collectionView;
    [self.view addSubview:self.collectionView];
}

#pragma mark - UICollectionDelegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataArr.count;
}
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView" forIndexPath:indexPath];
//    UICollectionReusableView *view = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReusableView" forIndexPath:indexPath];
    self.titleImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 180)];
    self.titleImageView.backgroundColor = [UIColor whiteColor];
    [self.titleImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_dictData[@"extraInfo"][@"cover"]]]];
    [view addSubview:self.titleImageView];
    return view;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(ScreenSizeWidth, 180);
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_cellIdentifier forIndexPath:indexPath];
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (!cell) {
        cell = [[UICollectionViewCell alloc]init];
    }
    
    UIView *aaView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.bounds.size.width, 100*_Scale6+44*_Scale6)];
    aaView.layer.masksToBounds = YES;
    aaView.backgroundColor = [UIColor whiteColor];
    aaView.clipsToBounds = YES;
    aaView.layer.cornerRadius = 6;
    [cell.contentView addSubview:aaView];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.bounds.size.width, 100*_Scale6)];
    image.contentMode = UIViewContentModeScaleAspectFill;
    image.layer.masksToBounds = YES;
    image.layer.cornerRadius = 6;
    image.clipsToBounds = YES;
    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_dataArr[indexPath.row][@"images"]]]];
    [aaView addSubview:image];
    
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 100*_Scale6, cell.bounds.size.width-10, 44*_Scale6)];
    contentLabel.backgroundColor = [UIColor whiteColor];
    contentLabel.numberOfLines = 0;
    contentLabel.layer.masksToBounds = YES;
    contentLabel.layer.cornerRadius = 6;
//    contentLabel.textAlignment = 1;
    contentLabel.font = [UIFont systemFontOfSize:14];
    contentLabel.textColor = RGBACOLOR(156, 156, 156, 1.0);
    contentLabel.text = _dataArr[indexPath.row][@"title"];
    [cell.contentView addSubview:contentLabel];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [[LogHelper shared] writeToFilefrom_page:@"mtd" from_section:@"m" from_step:@"l" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:[[_dataArr objectAtIndex:indexPath.row] objectForKey:@"id"]];
    
    InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
        detailView.infoID = [[_dataArr objectAtIndex:indexPath.row] objectForKey:@"id"];
        detailView.titleName = [[_dataArr objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    
    detailView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailView animated:YES];
}


#pragma mark - UICollectionViewFlowLayoutDelegate

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(170*_Scale6, 100*_Scale6+44*_Scale6);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10,(ScreenSizeWidth-170*_Scale6*2)/3*_Scale6, 10, (ScreenSizeWidth-170*_Scale6*2)/3*_Scale6);
}
#pragma mark - 
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}
#pragma mark - 加载数据
-(void)addSpecialList{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_SPECIAL_DETAIL,@"r",
                                    _specialId,@"id",
                                    @"1",@"page",
                                    @"999",@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(addSpecialListFinish:) method:GETDATA];
}
-(void)addSpecialListFinish:(NSDictionary *)dic{
    if (![[dic objectForKey:@"code"] integerValue]) {
        _dictData = dic;
        [self initTitleName:[NSString stringWithFormat:@"%@",dic[@"extraInfo"][@"title"]]];
        _dataArr = [dic objectForKey:@"results"];
        [self.collectionView reloadData];
    }
}

- (void)initTitleName:(NSString *)title {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0 , ScreenSizeWidth- 160, 44)];
        titleLabel.backgroundColor = [UIColor clearColor];
        // titleLabel.font = [UIFont boldSystemFontOfSize:20];
        titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
        self.baseTitleLabel = titleLabel;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = title;
    
    self.navigationItem.titleView = self.baseTitleLabel;
}












- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
