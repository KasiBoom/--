//
//  SpecailDetailViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/16.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface SpecailDetailViewController : BaseViewController

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIImageView *titleImageView;
@property (strong, nonatomic) NSString *specialId;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) UITableView *tableView;

@end
