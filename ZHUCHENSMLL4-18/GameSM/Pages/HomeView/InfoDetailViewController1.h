//
//  InfoDetailViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomView.h"
#import "LoginAlertView.h"
#import "CustomAlertView.h"
#import "UMSocial.h"

@interface InfoDetailViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UITextFieldDelegate, LoginAlertViewDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, CustomAlertViewDelegate, UMSocialUIDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UIView *authView;
@property (weak, nonatomic) IBOutlet UIView *authTopView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *lowTableView;

@property (strong, nonatomic)UITableView *lowTableView1;

@property (weak, nonatomic) IBOutlet UIView *authorView;
@property (weak, nonatomic) IBOutlet UIImageView *jiantouImage;
@property (weak, nonatomic) IBOutlet UIImageView *whoImage;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorIntroduce;
@property (weak, nonatomic) IBOutlet UIView *authorBgView;

@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) NSString *infoID;
@property (nonatomic, strong) NSString *titleName;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UIButton *appraiseBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIImageView *lineImageView;

@property (weak, nonatomic) IBOutlet UIButton *FaceBtn;

@property (weak, nonatomic) IBOutlet CustomView *appraiseView;
@property (weak, nonatomic) IBOutlet UILabel *appraiseLabel;

@property (strong, nonatomic) IBOutlet UIView *gameView;
@property (weak, nonatomic) IBOutlet UIView *gameContentView;
@property (weak, nonatomic) IBOutlet UILabel *gameTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameDevLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *gameImageView;

@property (nonatomic, assign) int pushType ; // 2 轮播图 1 推送 0列表


- (IBAction)appraiseBtnPressed:(id)sender;
- (IBAction)gameDownLoadBtnPressed:(id)sender;
- (IBAction)FaceBtnClike:(UIButton *)sender;
- (IBAction)sendBtnPressed:(id)sender;
//- (NSArray *)componentsSeparatedFromString:(NSString *)fromString toString:(NSString *)toString; 
@end
