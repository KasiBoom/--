//
//  ForgetPasswdViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/14.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"
#import "CustomButton.h"

@interface ForgetPasswdViewController : BaseViewController
@property (weak, nonatomic) IBOutlet CustomTextField *telephoneText;
@property (weak, nonatomic) IBOutlet CustomTextField *codeText;
@property (weak, nonatomic) IBOutlet CustomButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)codeBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;
@end
