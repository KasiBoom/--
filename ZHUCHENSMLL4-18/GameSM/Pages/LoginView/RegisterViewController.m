//
//  RegisterViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "RegisterViewController.h"
#import "MBProgressHUD+Add.h"

@interface RegisterViewController () {
    NSTimer *timer;
    int count;
}

@property (nonatomic, assign) BOOL show;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"注册"];
    [self addBackBtn];
    
    self.view.backgroundColor = [UIColor whiteColor];//colorWithRed:242/255.0 green:244/255.0 blue:245/255.0 alpha:1
    self.show = NO;
    count = 60;
    

}

#warning 重写父视图方法
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    [buttonBack setImage:[UIImage imageNamed:@"fanhui.png"] forState:UIControlStateNormal];
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)initTitleName:(NSString *)title {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 140, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    // titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    self.navigationItem.titleView = titleLabel;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];//
    if (IsIOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];//colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopTimming];
}

- (IBAction)codeBtnPressed:(id)sender {
    if (!self.userNameText.text.length) {
        [MBProgressHUD showError:@"手机号不能为空" toView:self.view];
        return;
    }
    [self sendNote];
}

- (IBAction)registerBtnPressed:(id)sender {
    if (self.userNameText.text.length) {
        if (self.passwordText.text.length < 6) {
            [MBProgressHUD showError:@"你连个至少6位数的密码都没有吗？" toView:self.view];
            return;
        }
    }
    [self userRegister];
}

- (IBAction)changePasswdTextPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (self.show) {
        self.show = NO;
    } else
        self.show = YES;
    [self.passwordText resignFirstResponder];
    self.passwordText.secureTextEntry = !self.show;
}

- (void)goBackToHome {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 计时器相关
- (void)startTimming {
    [self stopTimming];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshTime) userInfo:nil repeats:YES];
}

- (void)stopTimming {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}

- (void)refreshTime {
    [self.codeBtn setTitle:[NSString stringWithFormat:@"还剩%d秒", count] forState:UIControlStateNormal];
    count--;
    if (count <= 0) {
        [self stopTimming];
        self.codeBtn.enabled = YES;
        [self.codeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        count = 60;
    }
}

- (void)saveUserInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:g_App.userInfo.userID forKey:@"userID"];
    [userDefaults setObject:[NSString stringWithFormat:@"%d", g_App.userInfo.sex] forKey:@"sex"];
    [userDefaults setObject:g_App.userInfo.name forKey:@"name"];
    [userDefaults setObject:g_App.userInfo.telephone forKey:@"telephone"];
    [userDefaults setObject:g_App.userInfo.signature forKey:@"signature"];
    [userDefaults setObject:g_App.userInfo.images forKey:@"images"];
    [userDefaults setObject:g_App.userInfo.loginTime forKey:@"loginTime"];
    
    [userDefaults synchronize];
}

#pragma mark - 加载数据
- (void)userRegister {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_ADD,@"r",
                                    self.userNameText.text,@"username",
                                    self.codeNameText.text,@"verifycode",
                                    [Tool md5:self.passwordText.text],@"password",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(userRegisterFinish:) method:POSTDATA];
}

- (void)userRegisterFinish:(NSDictionary*)dic {
    [[LogHelper shared] writeToFilefrom_page:@"plr" from_section:@"profile" from_step:@"r" to_page:@"ph" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [MBProgressHUD showSuccess:@"注册成功" toView:self.view];
        g_App.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userName = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"]];
        g_App.userInfo = [[UserInfo alloc] init];
        g_App.userInfo.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userInfo.sex = (int)[[[dic objectForKey:@"results"] objectForKey:@"sex"] integerValue];
        g_App.userInfo.name = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.userInfo.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        g_App.userInfo.signature = [[dic objectForKey:@"results"] objectForKey:@"signature"];
        g_App.userInfo.images = [[dic objectForKey:@"results"] objectForKey:@"images"];
        g_App.userInfo.loginTime = [[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"];
        
        [self saveUserInfo];

        [self performSelector:@selector(goBackToHome) withObject:nil afterDelay:1];
    }
}


- (void)sendNote {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_SYSTEM_SENDNOTE,@"r",
                                    self.userNameText.text,@"telephone",
                                    @"1",@"sendType",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(sendNoteFinish:) method:POSTDATA];
    
    [self startTimming];
    self.codeBtn.enabled = NO;
}

- (void)sendNoteFinish:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
    } else {
        [self stopTimming];
        
        self.codeBtn.enabled = YES;
    }
    
}
@end
