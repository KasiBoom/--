//
//  LoginViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//


#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "LYTabBarController.h"
#import "ForgetPasswdViewController.h"
#import "UMSocial.h"
#import "MBProgressHUD+Add.h"
#import "DataBaseHelper.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface LoginViewController ()
{
    UILabel *_baseTitleLabel;
    UIImageView *_navBarHairlineImageView;
    MBProgressHUD *hud;
}
@property (nonatomic, assign) BOOL show;
@property (nonatomic, assign) BOOL isAnimation;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"登录"];
    
    _navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    
    
    [self addBackBtn];
    
    self.loginContentView.layer.masksToBounds = YES;
    self.loginContentView.layer.cornerRadius = 6;
    self.userNameText.borderStyle = UITextBorderStyleNone;
    self.userNameText.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    self.userNameText.delegate = self;
    self.passWordTextField.borderStyle = UITextBorderStyleNone;
    self.passWordTextField.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    self.passWordTextField.returnKeyType = UIReturnKeyGo;
    self.passWordTextField.delegate = self;
    self.passWordTextField.clearsOnBeginEditing = NO;
//    [self addTopBtn];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"degnlu_bg@3x.jpg"]];
//    self.registerBtn.layer.masksToBounds = YES;
//    [self.registerBtn.layer setBorderWidth:2.0];
    [self.registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    
//    [self.registerBtn.layer setBorderColor:(__bridge CGColorRef _Nullable)([UIColor whiteColor])];
    
    [self createUI];
    
    
    
    self.loginBtn.layer.cornerRadius = self.loginBtn.bounds.size.height / 4;
//    self.registerBtn.layer.cornerRadius = self.registerBtn.bounds.size.height / 4;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
}
//- (void)addTopBtn {
//    UIToolbar * top=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
//    UIBarButtonItem *speace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    UIBarButtonItem *dimissKey = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleBordered target:self action:@selector(loginBtnPressed:)];
//    
//    top.items=[NSArray arrayWithObjects:speace,dimissKey, nil];
//    self.passWordTextField.inputAccessoryView = top;
//}
- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    [buttonBack setImage:[UIImage imageNamed:@"dengluguanbi_"] forState:UIControlStateNormal];
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.rightBarButtonItem = backItem;
}

- (void)initTitleName:(NSString *)title {
    if (!_baseTitleLabel) {
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 140, 44)];
        titleLabel.backgroundColor = [UIColor clearColor];
        // titleLabel.font = [UIFont boldSystemFontOfSize:20];
        titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        titleLabel.textColor = [UIColor whiteColor];
        self.baseTitleLabel = titleLabel;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = title;
    } else {
        self.baseTitleLabel.text = title;
    }
    
    self.navigationItem.titleView = self.baseTitleLabel;
}

-(void)createUI{
    
    UIImageView *Zhuangshi = [[UIImageView alloc]init];
    Zhuangshi.image = [UIImage imageNamed:@"denglu_zhuangshi.png"];
    UIButton    *loginBtn = [[UIButton alloc]init];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"denglu_5"] forState:UIControlStateNormal];
    loginBtn.layer.cornerRadius = loginBtn.bounds.size.height / 4;
    [loginBtn setTitle:@"开始卖萌" forState:UIControlStateNormal];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [loginBtn setTitleColor:[UIColor colorWithRed:234/255.0 green:54/255.0 blue:77/255.0 alpha:1.0] forState:UIControlStateNormal];
    loginBtn.titleLabel.textAlignment = 1;
    [loginBtn addTarget:self action:@selector(loginBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [loginBtn addTarget:self action:@selector(buttonBackGroundHighlighted:) forControlEvents:UIControlEventTouchDown];
    UIButton    *registerBtn = [[UIButton alloc]init];
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"denglu_6"] forState:UIControlStateNormal];
    [registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [registerBtn.backgroundColor:[UIColor colorWithRed:255/255.0 green:201/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    registerBtn.titleLabel.textAlignment = 1;
    [registerBtn addTarget:self action:@selector(registerBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [registerBtn addTarget:self action:@selector(buttonBackGroundHighlighted:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:Zhuangshi];
    [self.view addSubview:loginBtn];
    [self.view addSubview:registerBtn];
    
    if (KScreenheight <=481) {
        Zhuangshi.frame = CGRectMake(65, KScreenheight - 40 -80 - 120+20, KScreenWidth - 130, 120);
        loginBtn.frame = CGRectMake(40, KScreenheight - 94-120+20, KScreenWidth-80, 36);
        registerBtn.frame = CGRectMake(40, KScreenheight - 50 - 120+20, KScreenWidth-80, 36);
    }else if (KScreenheight >=500 && KScreenheight <= 580){
        Zhuangshi.frame = CGRectMake(65, KScreenheight - 40 - 80 - 100 -120+55, KScreenWidth - 130, 120);
        loginBtn.frame = CGRectMake(40, KScreenheight - 94 - 100-120+55, KScreenWidth-80, 36);
        registerBtn.frame = CGRectMake(40, KScreenheight - 50 - 100-120+55, KScreenWidth-80, 36);
    }else{
        Zhuangshi.frame = CGRectMake(65, KScreenheight - 40 - 80 - 100 -120-60+55, KScreenWidth - 130, 120);
        loginBtn.frame = CGRectMake(40, KScreenheight - 94 - 100-120-60+55, KScreenWidth-80, 36);
        registerBtn.frame = CGRectMake(40, KScreenheight - 50 - 100-120-60+55, KScreenWidth-80, 36);
    }
}

//-(void)buttonBackGroundHighlighted:(UIButton *)sender{
//    sender.backgroundColor = [UIColor colorWithRed:255/255.0 green:201/255.0 blue:207/255.0 alpha:1.0];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBackToPreview:(id)btn {
//    _skipNext();
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"忘记密码"];
    [string addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, string.length)];
    [self.forgetLabel setAttributedText:string];
    
    
//    self.navigationController.navigationBar.translucent = NO;
    //    self.navigationController.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];//
    if (IsIOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];//
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc]init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc]init]];
    
    _navBarHairlineImageView.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _navBarHairlineImageView.hidden = NO;
}
- (IBAction)loginBtnPressed:(CustomButton *)sender {
    if (!self.userNameText.text.length) {
        [MBProgressHUD showError:@"填手机啊混蛋" toView:self.view];
    } else if (!self.passWordTextField.text.length) {
        [MBProgressHUD showError:@"密码不能为空" toView:self.view];
    } else{
        [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"profile" from_step:@"r" to_page:@"ph" to_section:@"profile" to_step:@"a" type:@"normal" id:@"0"];
        [self login];
    }
}

- (IBAction)registerBtnPressed:(CustomButton *)sender {
    [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"profile" from_step:@"h" to_page:@"plr" to_section:@"profile" to_step:@"r" type:@"" id:@"0"];
    RegisterViewController *registerView = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerView animated:YES];
}

- (IBAction)forgetBtnPressed:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"profile" from_step:@"h" to_page:@"plfp" to_section:@"profile" to_step:@"r" type:@"" id:@"0"];
    ForgetPasswdViewController *forgetView = [[ForgetPasswdViewController alloc] initWithNibName:@"ForgetPasswdViewController" bundle:nil];
    [self.navigationController pushViewController:forgetView animated:YES];
}

- (IBAction)unionBtnPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    switch (btn.tag - 100) {
            //微信
        case 0: {
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
                if (response.responseCode == UMSResponseCodeSuccess) {
                    UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
    
                    NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                    
                   [[UMSocialDataService defaultDataService] requestSnsInformation:UMShareToWechatSession completion:^(UMSocialResponseEntity *response) {
                       NSString *gender = [NSString stringWithFormat:@"%@",response.data[@"gender"]];
                       gender = ([gender isEqualToString:@"1"] || [gender isEqualToString:@"男"])?@"1":@"2";
                       [self unionLogin:snsAccount.openId userName:snsAccount.userName gender:gender headImage:snsAccount.iconURL sourceType:11];
                       [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"profile" from_step:@"r" to_page:@"ph" to_section:@"profile" to_step:@"a" type:@"weixin" id:@"0"];
                   }];
                }
            });
        }
            break;
            //qq
        case 1: {
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
                
                //          获取微博用户名、uid、token等
                
                if (response.responseCode == UMSResponseCodeSuccess) {
                    
                    UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQQ];
                    NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                    
                    [[UMSocialDataService defaultDataService] requestSnsInformation:UMShareToQQ completion:^(UMSocialResponseEntity *response) {
                        NSString *gender = [NSString stringWithFormat:@"%@",response.data[@"gender"]];
                        gender = ([gender isEqualToString:@"1"] || [gender isEqualToString:@"男"])?@"1":@"2";
                        [self unionLogin:snsAccount.openId userName:snsAccount.userName gender:gender headImage:snsAccount.iconURL sourceType:12];
                        [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"profile" from_step:@"r" to_page:@"ph" to_section:@"profile" to_step:@"a" type:@"qq" id:@"0"];

                    }];
                }});
        }
            break;
            //微博
        case 2: {
            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
                //          获取微博用户名、uid、token等
                
                if (response.responseCode == UMSResponseCodeSuccess) {
                    UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
                    
                    NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
                    
                    [[UMSocialDataService defaultDataService] requestSnsInformation:UMShareToSina completion:^(UMSocialResponseEntity *response) {
                        NSString *gender = [NSString stringWithFormat:@"%@",response.data[@"gender"]];
                        gender = ([gender isEqualToString:@"1"] || [gender isEqualToString:@"男"])?@"1":@"2";
                        [self unionLogin:snsAccount.accessToken userName:snsAccount.userName gender:gender headImage:snsAccount.iconURL sourceType:13];
                        [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"profile" from_step:@"r" to_page:@"ph" to_section:@"profile" to_step:@"a" type:@"weibo" id:@"0"];

                    }];
                }});
            
        }
            break;
        default:
            break;
    }
}


- (IBAction)changePasswdTextPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (self.show) {
        self.show = NO;
    } else
        self.show = YES;
    [self.passWordTextField resignFirstResponder];
    self.passWordTextField.secureTextEntry = !self.show;
}


- (void)saveUserInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:g_App.userInfo.userID forKey:@"userID"];
    [userDefaults setObject:[NSString stringWithFormat:@"%d", g_App.userInfo.sex] forKey:@"sex"];
    [userDefaults setObject:g_App.userInfo.name forKey:@"name"];
    [userDefaults setObject:g_App.userInfo.telephone forKey:@"telephone"];
    [userDefaults setObject:g_App.userInfo.signature forKey:@"signature"];
    [userDefaults setObject:g_App.userInfo.images forKey:@"images"];
    [userDefaults setObject:g_App.userInfo.loginTime forKey:@"loginTime"];
//    [userDefaults setObject:g_App.userInfo.signStatus forKey:@"signStatus"];
    [userDefaults setObject:g_App.loginType forKey:@"loginType"];
    
    [userDefaults synchronize];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self startAnimation];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self stopAnimation];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (!self.userNameText.text.length) {
        [MBProgressHUD showError:@"填手机啊混蛋" toView:self.view];
    } else if (!self.passWordTextField.text.length) {
        [MBProgressHUD showError:@"密码不能为空" toView:self.view];
    } else{
        [self login];
    }
    
    return nil;
}

- (void)startAnimation {
    if (self.isAnimation) {
        return;
    }
    self.isAnimation = YES;
    
    if (KScreenheight<=481) {
        CGRect frame = self.superView.frame;
        frame.origin.y = -64;
        [UIView animateWithDuration:0.3 animations:^{
            self.superView.frame = frame;
        }];
    }
//    else{
//        [UIView animateWithDuration:0.3 animations:^{
//            self.topImageView.transform = CGAffineTransformMakeScale(1.3, 1.3);
//            self.topHeadImageView.transform = CGAffineTransformMakeScale(0.7, 0.7);
//            self.topHeadImageView.center = CGPointMake(self.topHeadImageView.center.x, 40);
//            self.loginContentView.center = CGPointMake(self.loginContentView.center.x, 225);
//        }];
//    }
}

- (void)stopAnimation {
    if (!self.isAnimation) {
        return;
    }
    self.isAnimation = NO;
    
    if (KScreenheight <=481) {
        CGRect frame = self.superView.frame;
        frame.origin.y = 64;
        [UIView animateWithDuration:0.3 animations:^{
            self.superView.frame = frame;
        }];
    }
//    else{
//        [UIView animateWithDuration:0.3 animations:^{
//            self.topImageView.transform = CGAffineTransformMakeScale(1, 1);
//            self.topHeadImageView.transform = CGAffineTransformMakeScale(1, 1);
//            self.topHeadImageView.center = CGPointMake(self.topHeadImageView.center.x, 75);
//            self.loginContentView.center = CGPointMake(self.loginContentView.center.x, self.loginContentView.bounds.size.height/2 + 145);
//        }];
//    }
    
}

#pragma mark - 加载数据
- (void)login {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_LOGIN,@"r",
                                    self.userNameText.text,@"username",
                                    [Tool md5:self.passWordTextField.text],@"password",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loginFinish:) method:POSTDATA];
}

- (void)loginFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        g_App.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userName = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"]];
        g_App.userInfo = [[UserInfo alloc] init];
        g_App.userInfo.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userInfo.sex = (int)[[[dic objectForKey:@"results"] objectForKey:@"sex"] integerValue];
        g_App.userInfo.name = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.userInfo.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        g_App.userInfo.signature = [[dic objectForKey:@"results"] objectForKey:@"signature"];
        g_App.userInfo.images = [[dic objectForKey:@"results"] objectForKey:@"images"];
        g_App.userInfo.loginTime = [[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"];
//        g_App.userInfo.signStatus = [[dic objectForKey:@"results"] objectForKey:@"signStatus"];
        g_App.loginType = @"0";
        
        [self saveUserInfo];
        [self synInfo];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"HAVELOGINED"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (_skipNext) {
            _skipNext();
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"judgeSignStatus" object:nil];
        }];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reuseData:) name:@"changeSave" object:nil];
        
        
        
    }else{
        CustomAlertView * alertView=[[CustomAlertView alloc]initWithTitle:@"脾气差的提示君" message:@"用户名密码不匹配" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
    }
}

- (void)synInfo{
    
    NSDictionary *saveDict = @{@"r":@"cartoonCollection/synUserCollectionInfo",@"postData":[[DataBaseHelper shared] fetchCartoonAllCollectionStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:saveDict object:self action:@selector(upCollectionData:) method:POSTDATA];
    
    
    NSDictionary *readDict = @{@"r":@"cartoonReadHistory/synUserReadInfo",@"postData":    [[DataBaseHelper shared] fetchCartoonAllReadStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(upReadData:) method:POSTDATA];
    
}

- (void)upReadData:(NSDictionary *)readDict {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];

}

- (void)upCollectionData:(NSDictionary *)collectionDict {
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];

}



- (void)unionLogin:(NSString*)token userName:(NSString*)name gender:(NSString*)gender headImage:(NSString*)images sourceType:(int)sourceType{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_UNION_LOGIN,@"r",
                                    token,@"sourceKey",
                                    [NSString stringWithFormat:@"%d", sourceType],@"sourceType",
                                    nil];
    if (name) {
        [exprame setObject:name forKey:@"name"];
    }
    if (gender) {
        [exprame setObject:gender forKey:@"sex"];
    }
    if (images) {
        [exprame setObject:images forKey:@"images"];
    }
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(unionLoginFinish:) method:POSTDATA];
}

- (void)unionLoginFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (![[dic objectForKey:@"errorCode"] integerValue]) { g_App.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userName = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"]];
        g_App.userInfo = [[UserInfo alloc] init];
        g_App.userInfo.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userInfo.sex = (int)[[[dic objectForKey:@"results"] objectForKey:@"sex"] integerValue];
        g_App.userInfo.name = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.userInfo.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        g_App.userInfo.signature = [[dic objectForKey:@"results"] objectForKey:@"signature"];
        g_App.userInfo.images = [[dic objectForKey:@"results"] objectForKey:@"images"];
        g_App.userInfo.loginTime = [[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"];
        g_App.loginType = @"1";
        [self saveUserInfo];
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"judgeSignStatus" object:nil];
    }
}
@end

