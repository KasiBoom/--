//
//  RegisterViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"
#import "CustomButton.h"

@interface RegisterViewController : BaseViewController
@property (weak, nonatomic) IBOutlet CustomTextField *userNameText;
@property (weak, nonatomic) IBOutlet CustomTextField *codeNameText;
@property (weak, nonatomic) IBOutlet CustomTextField *passwordText;
@property (weak, nonatomic) IBOutlet CustomButton *codeBtn;
@property (weak, nonatomic) IBOutlet CustomButton *registerBtn;

@property (weak, nonatomic) IBOutlet UILabel *warnLabel;


- (IBAction)codeBtnPressed:(id)sender;
- (IBAction)registerBtnPressed:(id)sender;
- (IBAction)changePasswdTextPressed:(id)sender;
@end
