//
//  ForgetPasswdViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/14.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "ForgetPasswdViewController.h"
#import "ResetPasswordViewController.h"

#import "MBProgressHUD+Add.h"

@interface ForgetPasswdViewController (){
    NSTimer *timer;
    int count;
}

@end

@implementation ForgetPasswdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"找回密码"];
    [self addBackBtn];
    
    self.nextBtn.layer.cornerRadius = 3;
    self.nextBtn.layer.masksToBounds = YES;
    self.nextBtn.backgroundColor = [UIColor colorWithRed:211/255.0 green:206/255.0 blue:206/255.0 alpha:1];
    
    count = 60;
}
#warning 重写父视图方法
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    
    [buttonBack setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
    
    
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)initTitleName:(NSString *)title {
//    if (!_baseTitleLabel) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 140, 44)];
        titleLabel.backgroundColor = [UIColor clearColor];
        // titleLabel.font = [UIFont boldSystemFontOfSize:20];
        titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
        self.baseTitleLabel = titleLabel;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = title;
//    } else {
//        self.baseTitleLabel.text = title;
//    }
    
    self.navigationItem.titleView = self.baseTitleLabel;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    //    self.navigationController.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];//colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1
    //    self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    if (IsIOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];//colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        NSArray *list=self.navigationController.navigationBar.subviews;
        for (id obj in list) {
            if ([obj isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView=(UIImageView *)obj;
                NSArray *list2=imageView.subviews;
                for (id obj2 in list2) {
                    if ([obj2 isKindOfClass:[UIImageView class]]) {
                        UIImageView *imageView2=(UIImageView *)obj2;
                        //                        imageView2.backgroundColor = [UIColor colorWithRed:239/255.0 green:105/255.0 blue:125/255.0 alpha:1];
                        imageView2.hidden = YES;
                    }
                }
            }
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)codeBtnPressed:(id)sender {
    if (!self.telephoneText.text.length) {
        [MBProgressHUD showError:@"你的电话号码呢？" toView:self.view];
        return;
    }
    [self sendNote];
}

- (IBAction)nextBtnPressed:(id)sender {
    if (!self.telephoneText.text.length) {
        [MBProgressHUD showError:@"你的电话号码呢？" toView:self.view];
        return;
    }
    
    if (self.codeText.text.length) {
        [self checkCode];
    } else {
        [MBProgressHUD showError:@"先填写验证码啊" toView:self.view];
        return;
    }
}

#pragma mark - 计时器相关
- (void)startTimming {
    [self stopTimming];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshTime) userInfo:nil repeats:YES];
}

- (void)stopTimming {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}

- (void)refreshTime {
    [self.codeBtn setTitle:[NSString stringWithFormat:@"还剩%d秒", count] forState:UIControlStateNormal];
    count--;
    if (count <= 0) {
        [self stopTimming];
        self.codeBtn.enabled = YES;
        
        [self.codeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        count = 60;
    }
}

#pragma mark - 加载数据
- (void)sendNote {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_SYSTEM_SENDNOTE,@"r",
                                    self.telephoneText.text,@"telephone",
                                    @"2",@"sendType",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(sendNoteFinish:) method:POSTDATA];
    [self startTimming];
    self.codeBtn.enabled = NO;
}

- (void)sendNoteFinish:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
    } else {
        [self stopTimming];
        self.codeBtn.enabled = YES;
    }
}

- (void)checkCode {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_CHECK_VERIFY_CODE,@"r",
                                    self.telephoneText.text,@"username",
                                    self.codeText.text,@"verifycode",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(checkCodeFinish:) method:POSTDATA];
}

- (void)checkCodeFinish:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        ResetPasswordViewController *resetView = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:nil];
        resetView.telephone = self.telephoneText.text;
        resetView.code = self.codeText.text;
        [self.navigationController pushViewController:resetView animated:YES];
    }
}
@end
