//
//  LoginViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"
#import "CustomButton.h"
#import "CustomView.h"

@interface LoginViewController : BaseViewController<UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userNameText;
@property (weak, nonatomic) IBOutlet UITextField *passWordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UILabel *forgetLabel;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet CustomView *loginContentView;
@property (weak, nonatomic) IBOutlet UIImageView *topHeadImageView;
@property (nonatomic,copy)  void(^skipNext)(void);
@property (weak, nonatomic) IBOutlet UIImageView *Zhuangshi;

@property (strong, nonatomic) IBOutlet UIView *superView;

- (IBAction)loginBtnPressed:(CustomButton *)sender;
- (IBAction)registerBtnPressed:(CustomButton *)sender;
- (IBAction)forgetBtnPressed:(id)sender;

- (IBAction)unionBtnPressed:(id)sender;

- (IBAction)changePasswdTextPressed:(id)sender;
@end