//
//  ResetPasswordViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/14.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()

@property (nonatomic, assign) BOOL show;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"设置新密码"];
    [self addBackBtn];
}

#warning 重写父视图方法
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    [buttonBack setImage:[UIImage imageNamed:@"fanhui.png"] forState:UIControlStateNormal];
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)initTitleName:(NSString *)title {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 140, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    // titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    self.navigationItem.titleView = titleLabel;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];//
    if (IsIOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];//colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePasswdTextPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (self.show) {
        self.show = NO;
    } else
        self.show = YES;
    
    self.passWordText.secureTextEntry = !self.show;
    [self.passWordText resignFirstResponder];
}

- (IBAction)submitBtnPressed:(id)sender {
    if (self.passWordText.text.length < 6) {
        [MBProgressHUD showError:@"你连个至少6位数的密码都没有吗？" toView:self.view];
        return;
    }
    [self resetPassword];
}

#pragma mark - 加载数据
- (void)resetPassword {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_UPDATE_PASSWORD,@"r",
                                    [Tool md5:self.passWordText.text],@"password",
                                    self.code,@"verifycode",
                                    self.telephone,@"username",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(resetPasswordFinish:) method:POSTDATA];
}

- (void)resetPasswordFinish:(NSDictionary*)dic {
    
    [[LogHelper shared] writeToFilefrom_page:@"plfp" from_section:@"profile" from_step:@"r" to_page:@"ph" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];

    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        if (g_App.userID) {
            g_App.userID = nil;
            g_App.userName = nil;
            g_App.telephone = nil;
            [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
            g_App.userInfo = nil;
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:@"" forKey:@"userID"];
            [userDefaults setObject:@"" forKey:@"sex"];
            [userDefaults setObject:@"" forKey:@"name"];
            [userDefaults setObject:@"" forKey:@"userName"];
            [userDefaults setObject:@"" forKey:@"signature"];
            [userDefaults setObject:@"" forKey:@"images"];
            [userDefaults setObject:@"" forKey:@"loginTime"];
            [userDefaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LoginNotification object:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            g_App.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
            g_App.userName = [[dic objectForKey:@"results"] objectForKey:@"name"];
            g_App.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
            [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
            [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"]];
            g_App.userInfo = [[UserInfo alloc] init];
            g_App.userInfo.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
            g_App.userInfo.sex = (int)[[[dic objectForKey:@"results"] objectForKey:@"sex"] integerValue];
            g_App.userInfo.name = [[dic objectForKey:@"results"] objectForKey:@"name"];
            g_App.userInfo.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
            g_App.userInfo.signature = [[dic objectForKey:@"results"] objectForKey:@"signature"];
            g_App.userInfo.images = [[dic objectForKey:@"results"] objectForKey:@"images"];
            g_App.userInfo.loginTime = [[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"];
            
            [self saveUserInfo];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)saveUserInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:g_App.userInfo.userID forKey:@"userID"];
    [userDefaults setObject:[NSString stringWithFormat:@"%d", g_App.userInfo.sex] forKey:@"sex"];
    [userDefaults setObject:g_App.userInfo.name forKey:@"name"];
    [userDefaults setObject:g_App.userInfo.telephone forKey:@"telephone"];
    [userDefaults setObject:g_App.userInfo.signature forKey:@"signature"];
    [userDefaults setObject:g_App.userInfo.images forKey:@"images"];
    [userDefaults setObject:g_App.userInfo.loginTime forKey:@"loginTime"];
    
    [userDefaults synchronize];
}

@end
