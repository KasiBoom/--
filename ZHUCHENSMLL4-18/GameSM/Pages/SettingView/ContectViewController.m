//
//  ContectViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "ContectViewController.h"
#import "Y_X_DataInterface.h"
#import "logTextViewController.h"
@interface ContectViewController ()<UIAlertViewDelegate>

@end

@implementation ContectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"联系我们"];
    [self addBackBtn];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0 , ScreenSizeWidth- 160, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.userInteractionEnabled = YES;
    // titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    [self.navigationController.navigationBar addSubview:titleLabel];
    
    UITapGestureRecognizer *fouthTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(fouthTap:)];
    [fouthTapGestureRecognizer setNumberOfTapsRequired:4];
    [titleLabel addGestureRecognizer:fouthTapGestureRecognizer];

    
//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://maimeng.api.appzd.net/app/about.php"]]];
}

- (void)fouthTap:(UITapGestureRecognizer *)tap {
    UIAlertView *logALert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"log"delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"我跳了", nil];
    [logALert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1){
        logTextViewController *logTextVC = [[logTextViewController alloc] initWithNibName:@"logTextViewController" bundle:nil];
        [self.navigationController pushViewController:logTextVC animated:YES];

    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.scrollView setContentSize:CGSizeMake(0, ScreenSizeHeight)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
