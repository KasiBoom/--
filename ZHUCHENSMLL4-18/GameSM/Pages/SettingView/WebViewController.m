//
//  WebViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/20.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self addBackBtn];
    
    if (self.needSecret) {
        NSString *secretAgent = [_webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        NSString *newUagent = [NSString stringWithFormat:@"%@ %@/%@",secretAgent, @"maimeng", Version];
        NSDictionary *dictionary = [[NSDictionary alloc]
                                    initWithObjectsAndKeys:newUagent, @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    } else {
        
    }
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
