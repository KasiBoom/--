//
//  MsgCell.m
//  iChat
//
//  Created by huangdl on 14-10-31.
//  Copyright (c) 2014年 1000phone. All rights reserved.
//

#import "MsgCell.h"
#import "feedbackModel.h"
#import "Tool.h"
#import "Config.h"
#import "UIImageView+AFNetworking.h"
#import "UserInfo.h"
#import "AppDelegate.h"
@implementation MsgCell
{
    UIImageView *_leftTextBox;
    UIImageView *_rightTextBox;
    UILabel *_leftLabel;
    UILabel *_rightLabel;
    UIImageView *leftIconImage;
    UIImageView *iconImage;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self uiConfig];
    }
    return self;
}

-(void)uiConfig
{
    _leftTextBox = [[UIImageView alloc]init];
    UIImage *leftImg = [UIImage imageNamed:@"liaotiankuang_2"];
    leftImg = [leftImg stretchableImageWithLeftCapWidth:20 topCapHeight:35];
    _leftTextBox.image = leftImg;
    
    _rightTextBox = [[UIImageView alloc]init];
    UIImage *rightImg = [UIImage imageNamed:@"liaotiankuang_1@3x"];
    rightImg = [rightImg stretchableImageWithLeftCapWidth:20 topCapHeight:35];
    _rightTextBox.image = rightImg;
    
    [self.contentView addSubview:_leftTextBox];
    [self.contentView addSubview:_rightTextBox];
    
    _leftLabel = [[UILabel alloc]init];
    [_leftTextBox addSubview:_leftLabel];
    
    _rightLabel = [[UILabel alloc]init];
    [_rightTextBox addSubview:_rightLabel];
    
    _leftLabel.font = [UIFont systemFontOfSize:12];
    _rightLabel.font = [UIFont systemFontOfSize:12];
    
    _leftLabel.numberOfLines = 0;
    _rightLabel.numberOfLines = 0;
    _leftLabel.backgroundColor = [UIColor clearColor];
    _rightLabel.backgroundColor = [UIColor clearColor];
    
    leftIconImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 35, 35)];
    leftIconImage.layer.cornerRadius = 17.5;
    leftIconImage.layer.masksToBounds = YES;
    [leftIconImage setImage:[UIImage imageNamed:@"backIcon"]];
    [self.contentView addSubview:leftIconImage];
    
    iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 50, 5, 35, 35)];
    iconImage.layer.cornerRadius = 17.5;
    iconImage.layer.masksToBounds = YES;
    
    if (g_App.userInfo.images) {
        [iconImage setImageWithURL:[NSURL URLWithString:g_App.userInfo.images] placeholderImage:nil];
    }else{
        [iconImage setImage:[UIImage imageNamed:@"backIcon"]];
    }
    [self.contentView addSubview:iconImage];
    self.backgroundColor = [UIColor whiteColor];
    
}



-(void)fillData
{
    _leftTextBox.hidden = YES;
    _rightTextBox.hidden = YES;
    leftIconImage.hidden = YES;
    iconImage.hidden = YES;
    if ([_model.type isEqualToString:@"0"]) {
        _rightTextBox.hidden = NO;
        iconImage.hidden = NO;
        _rightTextBox.frame = CGRectMake(ScreenSizeWidth - _model.cellSize.width - 90, 5, _model.cellSize.width + 30, _model.cellSize.height + 20);
        _rightLabel.text = _model.content;
        _rightLabel.frame = CGRectMake(10, 0, _model.cellSize.width,_model.cellSize.height+20);
        iconImage.frame = CGRectMake(ScreenSizeWidth - 50, 5, 35, 35);
    }else{
        _leftTextBox.hidden = NO;
        leftIconImage.hidden = NO;
        _leftTextBox.frame = CGRectMake(50, 5, _model.cellSize.width + 30, _model.cellSize.height + 20);
        _leftLabel.text = _model.content;
        _leftLabel.frame = CGRectMake(15, 5, _model.cellSize.width ,_model.cellSize.height);
        leftIconImage.frame = CGRectMake(10, 5, 35, 35);
    }
    
}



//+ (void)calcHeight:(feedbackModel *)model
//{
//    CGSize size = [Tool sizeOfStr:model.content andFont:[UIFont systemFontOfSize:12.0] andMaxSize:CGSizeMake(200, 9999999) andLineBreakMode:NSLineBreakByCharWrapping];
//    model.cellSize = size;
////    CGSize size = [self sizeOfStr:model.content andFont:[UIFont systemFontOfSize:12] andMaxSize:CGSizeMake(200, 9999999) andLineBreakMode:NSLineBreakByWordWrapping];
////    model.cellSize = size;
//}






- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
