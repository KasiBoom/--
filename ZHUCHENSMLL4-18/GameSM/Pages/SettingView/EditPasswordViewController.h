//
//  EditPasswordViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"

@interface EditPasswordViewController : BaseViewController

@property (weak, nonatomic) IBOutlet CustomTextField *oldPasswordText;
@property (weak, nonatomic) IBOutlet CustomTextField *currentPasswordText;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *passwdInfo;

- (IBAction)editBtnPressed:(id)sender;

- (IBAction)changePasswdTextPressed:(id)sender;
- (IBAction)changeCurentPasswdtTextPressed:(id)sender;

- (IBAction)forgetBtnPressed:(id)sender;
@end
