//
//  MsgCell.h
//  iChat
//
//  Created by huangdl on 14-10-31.
//  Copyright (c) 2014年 1000phone. All rights reserved.
//

#import <UIKit/UIKit.h>
@class feedbackModel;
@interface MsgCell : UITableViewCell

@property (nonatomic,strong) feedbackModel *model;
-(void)fillData;

+(void)calcHeight:(feedbackModel *)model;

@end










