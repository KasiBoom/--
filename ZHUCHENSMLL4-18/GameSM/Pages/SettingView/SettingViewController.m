//
//  SettingViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SettingViewController.h"
#import "ContectViewController.h"
#import "FeedbackViewController.h"
#import "EditPasswordViewController.h"
#import "ConstantViewController.h"
#import "FreeResViewController.h"
#import "DataBaseHelper.h"
#import "ZXScanCodeViewController.h"
#define Version             [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey]

@interface SettingViewController ()

@property (nonatomic, strong) NSMutableArray *picArray;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initTitleName:@"设置"];
    [self addBackBtn];
    
//    self.dataArray = [NSMutableArray arrayWithObjects:@"清除缓存",@"接受消息",@"联系我们",@"免责声明",@"意见反馈",@"手机流量看漫画",@"手机流量下漫画",@"漫画更新提示",@"修改密码", nil];
//    self.picArray = [NSMutableArray arrayWithObjects:@"qinli",@"jieshouxiaoxi",@"lianxiwomen",@"mianze",@"fankui_",@"wifi",@"xiazai1",@"genxintishi",@"xiugai", nil];
    self.dataArray = [NSMutableArray arrayWithObjects:@"清除缓存",@"接受消息",@"联系我们",@"免责声明",@"意见反馈",@"扫一扫",@"手机流量看漫画",@"手机流量下漫画",@"修改密码", nil];
    self.picArray = [NSMutableArray arrayWithObjects:@"qinli",@"jieshouxiaoxi",@"lianxiwomen",@"mianze",@"fankui_",@"saoyisao",@"wifi",@"xiazai1",@"xiugai", nil];
    NSLog(@"dddd%f",ScreenSizeWidth);
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight) style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
//    self.tableView.backgroundColor = [UIColor redColor];
//    self.tableView.backgroundView = nil;
    
    self.tableView.tableFooterView = self.bottomView;
    
//    if (IsIOS7) {
//        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//            [self.tableView setSeparatorInset:UIEdgeInsetsZero];
//        }
//        if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
//            [self.tableView setLayoutMargins:UIEdgeInsetsZero];
//        }
//    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.logoutBtn.layer.cornerRadius = self.logoutBtn.bounds.size.height / 2;
    self.logoutBtn.layer.masksToBounds = YES;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 50)];
    view.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, view.bounds.size.width - 60, 30)];
    label.font = [UIFont systemFontOfSize:18];
    label.textColor = [UIColor darkTextColor];
    label.text = [NSString stringWithFormat:@"麦萌 v%@", Version];
    label.numberOfLines = 1;
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    self.versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, view.bounds.size.width - 60, 30)];
    self.versionLabel.textColor = [UIColor lightGrayColor];
    self.versionLabel.font = [UIFont systemFontOfSize:14];
    self.versionLabel.textAlignment = NSTextAlignmentCenter;
    self.versionLabel.text = [NSString stringWithFormat:@"当前版本号：%@", ShortVersion];
    [UIColor whiteColor];
//    self.tableView.tableHeaderView = view;
}
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);

    [buttonBack setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
    
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    if (!g_App.userID) {
        
        self.dataArray = [NSMutableArray arrayWithObjects:@"清除缓存",@"接受消息",@"联系我们",@"免责声明",@"意见反馈",@"扫一扫",@"手机流量看漫画",@"手机流量下漫画", nil];
        self.picArray = [NSMutableArray arrayWithObjects:@"qinli",@"jieshouxiaoxi",@"lianxiwomen",@"mianze",@"fankui_",@"saoyisao",@"wifi",@"xiazai1", nil];
        
        self.logoutBtn.hidden = YES;
        [self.tableView reloadData];
    } else {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"] isEqualToString:@"1"]) {
            
            self.dataArray = [NSMutableArray arrayWithObjects:@"清除缓存",@"接受消息",@"联系我们",@"免责声明",@"意见反馈",@"扫一扫",@"手机流量看漫画",@"手机流量下漫画", nil];
            self.picArray = [NSMutableArray arrayWithObjects:@"qinli",@"jieshouxiaoxi",@"lianxiwomen",@"mianze",@"fankui_",@"saoyisao",@"wifi",@"xiazai1", nil];
        } else {
            self.dataArray = [NSMutableArray arrayWithObjects:@"清除缓存",@"接受消息",@"联系我们",@"免责声明",@"意见反馈",@"扫一扫",@"手机流量看漫画",@"手机流量下漫画",@"修改密码", nil];
            self.picArray = [NSMutableArray arrayWithObjects:@"qinli",@"jieshouxiaoxi",@"lianxiwomen",@"mianze",@"fankui_",@"saoyisao",@"wifi",@"xiazai1",@"xiugai", nil];
        }
    }
//    self.tableView.frame = self.view.bounds;
}

#pragma mark - 按钮事件
- (void)switchBtnPressed:(UISwitch*)switchBtn {
    
}

- (IBAction)logoutBtnPressed:(id)sender {
    [self logout];
}

//清除缓存
-(void)clearCash{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       NSString *cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                       
                       NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];

                       for (NSString *p in files) {
                           NSError *error;
                           NSString *path = [cachPath stringByAppendingPathComponent:p];
                           if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                               [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                           }
                       }
                       [self performSelectorOnMainThread:@selector(clearCacheSuccess) withObject:nil waitUntilDone:YES];}
                   );
}

-(void)clearCacheSuccess{
    
    [self.tableView reloadData];
}



#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return 70;
    }  else if (section == self.dataArray.count - 1) {
        return 30;
    } else
        return 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 70)];
        view.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 7, view.bounds.size.width - 60, 56)];
        label.font = [UIFont systemFontOfSize:13];
        label.textColor = [UIColor lightGrayColor];
        
        label.text = @"如果你要关闭或开启麦萌的新消息通知，请在iPhone的“设置” —— “通知” 功能中，找到应用程序 “麦萌” 更改";
        label.numberOfLines = 3;
        [view addSubview:label];
        return view;
    }else if (section == self.dataArray.count - 1) {
        return self.versionLabel;
    }else
        return nil;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 100, 13, 85, 18)];
        label.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:label];
        label.tag = 100;
        label.textColor = [UIColor darkTextColor];
        label.font = [UIFont systemFontOfSize:14];
        label.hidden = YES;
    }
//    if (indexPath.section != self.dataArray.count - 1) {
      cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jinru"]];
//}
    
    UILabel *switchBtn = (UILabel*)[cell.contentView viewWithTag:100];
    switchBtn.hidden = YES;
    cell.textLabel.text = [self.dataArray objectAtIndex:indexPath.section];
    cell.imageView.image = [UIImage imageNamed:[self.picArray objectAtIndex:indexPath.section]];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
    cell.detailTextLabel.text = nil;
    if (indexPath.section != 1) {
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    } else {
        switchBtn.hidden = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = nil;
        
        if ([self pushNotificationOpen]) {
            switchBtn.text = @"已开启";
        } else
            switchBtn.text = @"未开启";
    }
    if (indexPath.section == 0) {
        cell.detailTextLabel.text = [Tool displayCache];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"] isEqualToString:@"0"]) {
        
        if (indexPath.section == self.dataArray.count - 3) {
            cell.accessoryView = nil;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 60, 5, 20, 10)];
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] isEqualToString:@"0"]) {
                [switchButton setOn:NO];
            }else{
                [switchButton setOn:YES];
            }
            switchButton.tag = 50000;
            [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:switchButton];
        }
        
        if (indexPath.section == self.dataArray.count - 2) {
            cell.accessoryView = nil;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 60, 5, 20, 10)];
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DOWNLOADBUTTON"] isEqualToString:@"0"]) {
                [switchButton setOn:NO];
            }else{
                [switchButton setOn:YES];
            }
            switchButton.tag = 60000;
            [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:switchButton];
        }
        
//        if (indexPath.section == self.dataArray.count - 2) {
//            cell.accessoryView = nil;
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//            UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 60, 5, 20, 10)];
//            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CARTOONUPDATEHINT"] isEqualToString:@"0"]) {
//                [switchButton setOn:NO];
//            }else{
//                [switchButton setOn:YES];
//            }
//            switchButton.tag = 70000;
//            [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
//            [cell addSubview:switchButton];
//        }
    }else{
        if (indexPath.section == self.dataArray.count - 2) {
            cell.accessoryView = nil;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 60, 5, 20, 10)];
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] isEqualToString:@"0"]) {
                [switchButton setOn:NO];
            }else{
                [switchButton setOn:YES];
            }
            switchButton.tag = 50000;
            [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:switchButton];
        }
        
        if (indexPath.section == self.dataArray.count - 1) {
            cell.accessoryView = nil;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenSizeWidth - 60, 5, 20, 10)];
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DOWNLOADBUTTON"] isEqualToString:@"0"]) {
                [switchButton setOn:NO];
            }else{
                [switchButton setOn:YES];
            }
            switchButton.tag = 60000;
            [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:switchButton];
        }
    }
    
    return cell;
}

- (void)switchAction:(UISwitch *)switchButton{
    if (switchButton.tag == 50000) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] isEqualToString:@"0"]) {
            [switchButton setOn:YES];
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"OPENTURN"];
            
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psh" to_section:@"profile" to_step:@"h" type:@"flow_read_comic" id:@"" detail:@{@"isOn":@"true"}];

        }else{
            [switchButton setOn:NO];
            [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"OPENTURN"];
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psh" to_section:@"profile" to_step:@"h" type:@"flow_read_comic" id:@"" detail:@{@"isOn":@"false"}];

        }
        
    }else  if (switchButton.tag == 60000){
        
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DOWNLOADBUTTON"] isEqualToString:@"0"]) {
            [switchButton setOn:YES];
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"DOWNLOADBUTTON"];
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psh" to_section:@"profile" to_step:@"h" type:@"flow_download_commic" id:@"" detail:@{@"isOn":@"true"}];
        }else{
            [switchButton setOn:NO];
            [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"DOWNLOADBUTTON"];
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psh" to_section:@"profile" to_step:@"h" type:@"flow_download_commic" id:@"" detail:@{@"isOn":@"false"}];
            
        }
    }else if (switchButton.tag == 70000){
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CARTOONUPDATEHINT"] isEqualToString:@"0"]) {
            [switchButton setOn:YES];
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"CARTOONUPDATEHINT"];
        }else{
            [switchButton setOn:NO];
            [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"CARTOONUPDATEHINT"];
        }
    }
}

- (BOOL)pushNotificationOpen {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType types = [[UIApplication sharedApplication] currentUserNotificationSettings].types;
        return (types & UIRemoteNotificationTypeAlert);
    } else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        return (types & UIRemoteNotificationTypeAlert);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0: {
            
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psh" to_section:@"profile" to_step:@"h" type:@"clear_cache" id:@"0"];

            CustomAlertView *alertView = [[CustomAlertView alloc] initWithTitle:@"脾气差的提示君" message:@"确定清除缓存吗？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消"];
            [alertView show];
        }
            break;
        case 1: {
        }
            break;
        case 2: {
            ContectViewController *contectView = [[ContectViewController alloc] initWithNibName:@"ContectViewController" bundle:nil];
            [self.navigationController pushViewController:contectView animated:YES];
        }
            break;
        case 3: {
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psd" to_section:@"profile" to_step:@"d" type:@"" id:@"0"];
            FreeResViewController *freeVC = [[FreeResViewController alloc] init];
            [self.navigationController pushViewController:freeVC animated:YES];
//            ConstantViewController *feedbackView = [[ConstantViewController alloc] initWithNibName:@"ConstantViewController" bundle:nil];
//            feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            UINavigationController *bac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
//            [self presentViewController:bac animated:YES completion:nil];
////            [self.navigationController pushViewController:feedbackView animated:YES];
            
        }
            break;
            
        case 4: {
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"pscu" to_section:@"profile" to_step:@"d" type:@"" id:@"0"];
            ConstantViewController *feedbackView = [[ConstantViewController alloc] initWithNibName:@"ConstantViewController" bundle:nil];
            feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            UINavigationController *bac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
            [self presentViewController:feedbackView animated:YES completion:nil];
            
        }
            break;
        case 5:
        {
            ZXScanCodeViewController *zxScan = [[ZXScanCodeViewController alloc] init];
            [self.navigationController pushViewController:zxScan animated:YES];
        }
            break;
            
        case 8: { if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"] isEqualToString:@"0"]) {
            [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"psmp" to_section:@"profile" to_step:@"r" type:@"" id:@"0"];
            EditPasswordViewController *editView = [[EditPasswordViewController alloc] initWithNibName:@"EditPasswordViewController" bundle:nil];
            [self.navigationController pushViewController:editView animated:YES];
        }
        }
            break;
            
        default:
            break;
    }
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

#pragma mark - CustomAlertViewDelegate
- (void)customAlertViewBtnWasSeletecd:(CustomAlertView *)alertView withIndex:(NSInteger)index {
    if (!index) {
        [self clearCash];
    }
}

#pragma mark - 加载数据
- (void)logout {
    
    [[LogHelper shared] writeToFilefrom_page:@"psh" from_section:@"profile" from_step:@"h" to_page:@"pse" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self synInfo];

}

- (void)logoutFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [[DataBaseHelper shared] dropTableView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
        
        g_App.userID = nil;
        g_App.userName = nil;
        g_App.telephone = nil;
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        g_App.userInfo = nil;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"userID"];
        [userDefaults setObject:@"" forKey:@"sex"];
        [userDefaults setObject:@"" forKey:@"name"];
        [userDefaults setObject:@"" forKey:@"userName"];
        [userDefaults setObject:@"" forKey:@"signature"];
        [userDefaults setObject:@"" forKey:@"images"];
        [userDefaults setObject:@"" forKey:@"loginTime"];
        [userDefaults setObject:@"" forKey:@"loginType"];
        [userDefaults setObject:@"0" forKey:@"HAVENREQUEST"];
        
        
        [userDefaults synchronize];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (void)synInfo{
    
    NSDictionary *saveDict = @{@"r":@"cartoonCollection/synUserCollectionInfo",@"postData":[[DataBaseHelper shared] fetchCartoonAllCollectionStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:saveDict object:self action:@selector(upCollectionData:) method:POSTDATA];
    
    
    NSDictionary *readDict = @{@"r":@"cartoonReadHistory/synUserReadInfo",@"postData":    [[DataBaseHelper shared] fetchCartoonAllReadStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(upReadData:) method:POSTDATA];
    
}

//NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                (NSString *)API_URL_USER_LOGOUT,@"r",nil];
//[YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(logoutFinish:) method:POSTDATA];


- (void)upReadData:(NSDictionary *)readDict {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_LOGOUT,@"r",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(logoutFinish:) method:POSTDATA];
    
}

- (void)upCollectionData:(NSDictionary *)collectionDict {
    
}

@end
