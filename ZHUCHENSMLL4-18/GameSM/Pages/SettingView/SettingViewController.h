//
//  SettingViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomAlertView.h"

@interface SettingViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, CustomAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;
@property (strong, nonatomic) IBOutlet UIView *bottomView;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (strong, nonatomic) UILabel *versionLabel;

- (IBAction)logoutBtnPressed:(id)sender;

@end
