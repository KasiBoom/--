//
//  FreResViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/12/8.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "FreeResViewController.h"
#import "CustomAlertView.h"


@interface FreeResViewController ()<UIAlertViewDelegate>

@end

@implementation FreeResViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addBackBtn];
    [self initTitleName:@"免责声明"];
    // Do any additional setup after loading the view from its nib.
    UIWebView *_webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    //    _webView.scrollView.contentSize = CGSizeMake(0, ScreenSizeHeight + 100);
    NSString *strUrl = @"http://api.playsm.com/disclaimer.html";
    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    [_webView setScalesPageToFit:YES];
    
    [self.view addSubview:_webView];
    
}

- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    
    [buttonBack setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
    
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}
- (void)initTitleName:(NSString *)title {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0 , ScreenSizeWidth- 160, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.userInteractionEnabled = YES;
    // titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    self.baseTitleLabel = titleLabel;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    
    UITapGestureRecognizer *fouthTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(fouthTap:)];
    [fouthTapGestureRecognizer setNumberOfTapsRequired:4];
    [titleLabel addGestureRecognizer:fouthTapGestureRecognizer];
    
    self.navigationItem.titleView = self.baseTitleLabel;
}

-(void)fouthTap:(UITapGestureRecognizer *)recognizer{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"选择环境" message:nil delegate:self cancelButtonTitle:@"正式环境" otherButtonTitles:@"测试环境", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        INTERFACE_PREFIX = @"http://apitest.playsm.com/index.php?";
        INTERFACE_PREFIXD = @"http://apitest.playsm.com/index.php?";
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ISDEBUGEN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else if (buttonIndex == 0){
        INTERFACE_PREFIX = @"http://api.playsm.com/index.php?";
        INTERFACE_PREFIXD = @"http://api.playsm.com/index.php?";
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ISDEBUGEN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
