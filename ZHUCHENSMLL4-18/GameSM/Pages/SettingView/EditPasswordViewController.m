//
//  EditPasswordViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "EditPasswordViewController.h"
#import "MBProgressHUD+Add.h"
#import "ForgetPasswdViewController.h"

@interface EditPasswordViewController ()

@property (nonatomic, assign) BOOL show;
@property (nonatomic, assign) BOOL currentShow;

@end

@implementation EditPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"修改密码"];
    [self addBackBtn];
    
    self.submitBtn.layer.cornerRadius = 3;
    self.submitBtn.layer.masksToBounds = YES;
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"忘记密码"];
    [string addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, string.length)];
    [self.passwdInfo setAttributedText:string];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePasswdTextPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (self.show) {
        self.show = NO;
    } else
        self.show = YES;
    [self.oldPasswordText resignFirstResponder];
    self.oldPasswordText.secureTextEntry = !self.show;
}

- (IBAction)changeCurentPasswdtTextPressed:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    
    if (self.currentShow) {
        self.currentShow = NO;
    } else
        self.currentShow = YES;
    [self.currentPasswordText resignFirstResponder];
    self.currentPasswordText.secureTextEntry = !self.currentShow;
}

- (IBAction)forgetBtnPressed:(id)sender {
    ForgetPasswdViewController *forgetView = [[ForgetPasswdViewController alloc] initWithNibName:@"ForgetPasswdViewController" bundle:nil];
    [self.navigationController pushViewController:forgetView animated:YES];
}

- (IBAction)editBtnPressed:(id)sender {
    if (!self.oldPasswordText.text.length) {
        [MBProgressHUD showError:@"请输入你的原始密码~" toView:self.view];
        return;
    }
    if (self.oldPasswordText.text.length < 6) {
        [MBProgressHUD showError:@"你的原始密码格式不对哦" toView:self.view];
        return;
    }
    if (self.currentPasswordText.text.length < 6) {
        [MBProgressHUD showError:@"你连个至少6位数的密码都没有吗？" toView:self.view];
        return;
    }
    if ([self.oldPasswordText.text isEqualToString:self.currentPasswordText.text]) {
        [MBProgressHUD showError:@"新旧密码不能一样啊~" toView:self.view];
        return;
    }
    [self resetPassword];
}

#pragma mark - 加载数据
- (void)resetPassword {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_RESET_PASSWORD,@"r",
                                    [Tool md5:self.oldPasswordText.text],@"sourcePassword",
                                    [Tool md5:self.currentPasswordText.text],@"newPassword",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(resetPasswordFinish:) method:POSTDATA];
}

- (void)resetPasswordFinish:(NSDictionary*)dic {
    
    [[LogHelper shared] writeToFilefrom_page:@"psmp" from_section:@"profile" from_step:@"r" to_page:@"psh" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];

    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
        g_App.userID = nil;
        g_App.userName = nil;
        g_App.telephone = nil;
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        g_App.userInfo = nil;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"userID"];
        [userDefaults setObject:@"" forKey:@"sex"];
        [userDefaults setObject:@"" forKey:@"name"];
        [userDefaults setObject:@"" forKey:@"userName"];
        [userDefaults setObject:@"" forKey:@"signature"];
        [userDefaults setObject:@"" forKey:@"images"];
        [userDefaults setObject:@"" forKey:@"loginTime"];
        [userDefaults synchronize];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:LoginNotification object:nil];
    }
}
@end
