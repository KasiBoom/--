//
//  ScanViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/30.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "ScanViewController.h"

@interface ScanViewController ()
{
    UIWebView *_webView;
}
@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
    [_webView loadHTMLString:_html baseURL:nil];
    [self.view addSubview:_webView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
