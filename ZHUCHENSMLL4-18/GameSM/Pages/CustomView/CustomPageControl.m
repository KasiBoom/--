//
//  CustomPageControl.m
//  Haoye_lvpai
//
//  Created by wt on 15/4/9.
//  Copyright (c) 2015年 lianghuigui. All rights reserved.
//

#import "CustomPageControl.h"

@interface CustomPageControl ()

@property (nonatomic, retain) UIImage *normalImage;
@property (nonatomic, retain) UIImage *currentImage;

@end

@implementation CustomPageControl

- (void)dealloc
{
    
}

- (id)initWithFrame:(CGRect)frame withNormalImage:(UIImage *)normalImage currentImage:(UIImage *)currentImage pageCount:(int)count
{
    self = [super initWithFrame:frame];
    if (self) {
        self.normalImage = normalImage;
        self.currentImage = currentImage;
        self.pageCount = count;
        [self resetView];
        [self resetCurrentPageIndex:0];
    }
    return self;
}

- (void)resetView {
    self.backgroundColor = [UIColor clearColor];
    for (int i = 0; i < self.pageCount; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - 6 * (2 * self.pageCount - 1))/2 + 5 * 2 * i, (self.frame.size.height - 5.5) /2, 6, 5.5)];
        imageView.tag = 100 + i;
        imageView.image = self.normalImage;
        [self addSubview:imageView];
    }
}

//滑动完之后调用的协议方法里调用的方法。
- (void)resetCurrentPageIndex:(int)currentIndex {
    self.currentIndex = currentIndex;
    for (UIImageView *imageView in self.subviews) {     //假设有五个白点，
        
        if ([imageView isKindOfClass:[UIImageView class]]) {    //不为空，是当前页，给当前红色点，不是白色点。
            
            if ((imageView.tag - 100) == currentIndex) {
                imageView.image = self.currentImage;
            } else {
                imageView.image = self.normalImage;
            }
        }
    }
}
@end
