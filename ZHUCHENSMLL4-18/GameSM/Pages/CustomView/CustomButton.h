//
//  CustomButton.h
//  bang
//
//  Created by wt on 15/5/13.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

@property (nonatomic, assign) BOOL showBudge;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UIColor *budegColor;

- (void)resetBudgeWithNum:(NSString*)budgeNum;

@end
