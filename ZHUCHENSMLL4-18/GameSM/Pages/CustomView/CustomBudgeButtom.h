//
//  CustomBudgeButtom.h
//  GameSM
//
//  Created by 王涛 on 15/8/2.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBudgeButtom : UIButton

@property (nonatomic, assign) BOOL showBudge;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UIColor *budegColor;

- (void)resetBudgeWithNum:(NSString*)budgeNum;

@end
