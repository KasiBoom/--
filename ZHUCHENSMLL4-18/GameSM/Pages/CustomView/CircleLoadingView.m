//
//  CircleLoadingView.m
//  GameSM
//
//  Created by 王涛 on 15/7/17.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "CircleLoadingView.h"

#define kWidth      4

@interface CircleLoadingView ()

@property (nonatomic, assign) float angle;
@property (nonatomic, retain) UIColor *coler;
@property (nonatomic, retain) UIColor *fillColer;
@property (nonatomic, strong) UIImageView *circleImageView;
@property (nonatomic, strong) UIImageView *circleLeftEyeImageView;
@property (nonatomic, strong) UIImageView *circleRightEyeImageView;
@property (nonatomic, strong) UIImageView *circleMouthImageView;

@end

@implementation CircleLoadingView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.fillColer = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1];
        self.coler = [UIColor colorWithRed:225/255.0 green:66/255.0 blue:88/255.0 alpha:1];
        
        self.backgroundColor = [UIColor clearColor];
//---脸
        self.circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, 2.5, self.bounds.size.width - 5, self.bounds.size.height - 5)];
        self.circleImageView.image = [UIImage imageNamed:@"lian"];
        [self addSubview:self.circleImageView];
//        self.circleImageView.hidden = YES;
//---左眼
        self.circleLeftEyeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 13, 13)];
        self.circleLeftEyeImageView.image = [UIImage imageNamed:@"eye"];
        [self.circleImageView addSubview:self.circleLeftEyeImageView];
//---右眼
        self.circleRightEyeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(23, 9, 15, 15)];
        self.circleRightEyeImageView.image = [UIImage imageNamed:@"eye"];
        [self.circleImageView addSubview:self.circleRightEyeImageView];
//---嘴巴
        self.circleMouthImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 25, 38, 17)];
        self.circleMouthImageView.image = [UIImage imageNamed:@"zui"];
        [self.circleImageView addSubview:self.circleMouthImageView];
    }
    return self;
}

- (void)updateAngle:(float)angle {
    self.angle = angle;
    [self setNeedsDisplay];
}

//- (void)drawRect:(CGRect)rect {
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGRect viewRect = rect;
//    
//    CGContextSetLineWidth(context, kWidth);
//    CGContextSetLineCap(context, kCGLineCapButt);
//    CGContextSetStrokeColorWithColor(context, self.fillColer.CGColor);
//    CGContextAddArc(context, viewRect.size.width/2, viewRect.size.height/2, viewRect.size.width/2 - 5, 0, 2 * M_PI , 0);
//    CGContextDrawPath(context, kCGPathStroke);
//    
//    CGContextSetLineWidth(context, kWidth );
//    CGContextSetLineCap(context, kCGLineCapButt);
//    CGContextSetStrokeColorWithColor(context, self.coler.CGColor);
//    CGContextAddArc(context, viewRect.size.width/2, viewRect.size.height/2, viewRect.size.width/2 - 5, 1.5 * M_PI, 1.5 * M_PI + (fabsf(self.angle) * 1.0 / 360) * 2 * M_PI , 0);
//    CGContextDrawPath(context, kCGPathStroke);
//}

- (void)startCircleAnimation {
    [self updateAngle:0];
    self.circleImageView.hidden = NO;
    [self startTranslationAnimation];
}

- (void)stopCircleAnimation {
//    self.circleImageView.hidden = YES;
    [self stopRotateAnimation];
}
//----旋转动画
- (void)startRotateAnimation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI/8 ];//* 2.0
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 0.1;
    rotationAnimation.repeatCount = 1000;//你可以设置到最大的整数值
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [self.circleImageView.layer addAnimation:rotationAnimation forKey:@"Rotation"];
}

- (void)stopRotateAnimation
{
    [UIView animateWithDuration:0.3f animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self.layer removeAllAnimations];
        [self.circleLeftEyeImageView.layer removeAllAnimations];
        [self.circleRightEyeImageView.layer removeAllAnimations];
        [self.circleImageView.layer removeAllAnimations];
        self.alpha = 1;
    }];
}
//----放大再缩小
- (void)startTranslationAnimation{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    animation.duration=4.0f;
    animation.autoreverses=NO;
    animation.repeatCount=1000;
    animation.toValue=[NSNumber numberWithInt:0];
    animation.fromValue=[NSNumber numberWithInt:0];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    CABasicAnimation *animationZoomIn=[CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animationZoomIn.duration=0.3f;
    animationZoomIn.autoreverses=YES;
    animationZoomIn.repeatCount=1000;
    animationZoomIn.toValue=[NSNumber numberWithFloat:1.5];
    animationZoomIn.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    CABasicAnimation *animationZoomOut=[CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animationZoomOut.beginTime=0.3f;
    animationZoomOut.duration=0.3f;
    animationZoomOut.autoreverses=YES;
    animationZoomOut.repeatCount=1000;
    animationZoomOut.toValue=[NSNumber numberWithFloat:0.8];
    animationZoomOut.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CAAnimationGroup *group=[CAAnimationGroup animation];
    group.duration= 30.0f;
    group.animations=[NSArray arrayWithObjects:animationZoomIn, animationZoomOut,nil];//animation,
    group.removedOnCompletion=NO;
    group.fillMode=kCAFillModeForwards;
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI/9];//* 2.0
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 0.2;
    rotationAnimation.autoreverses = YES;
    rotationAnimation.repeatCount = 1000;//你可以设置到最大的整数值
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [self.circleImageView.layer addAnimation:rotationAnimation forKey:@"Rotation"];
//    [self.circleMouthImageView.layer addAnimation:rotationAnimation forKey:@"Rotation"];
    [self.circleLeftEyeImageView.layer addAnimation:group forKey:@"nil"];
    [self.circleRightEyeImageView.layer addAnimation:group forKey:@"nil"];
}










@end


