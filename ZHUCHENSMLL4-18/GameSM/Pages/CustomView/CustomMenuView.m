//
//  CustomMenuView.m
//  bang
//
//  Created by 王涛 on 15/5/24.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "CustomMenuView.h"
#import "CustomBudgeButtom.h"
#import "Config.h"

@interface CustomMenuView ()

@property (nonatomic, strong) UIView *bottomView;

@end

@implementation CustomMenuView

- (id)initWithFrame:(CGRect)frame withTitleArray:(NSArray *)array {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleArray = array;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    return self;
}

- (void)setTitleArray:(NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = titleArray;
    }
    [self resetView];
}

- (void)resetView {
    for (int i = 0 ; i < self.titleArray.count ; i++) {
        CustomBudgeButtom *btn = [[CustomBudgeButtom alloc] initWithFrame:CGRectMake(i * (self.bounds.size.width / self.titleArray.count), 0, self.bounds.size.width / self.titleArray.count - 1, self.bounds.size.height - 2)];
        [btn setTitle:[self.titleArray objectAtIndex:i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btn.tag = 100 + i;
        [btn addTarget:self action:@selector(btnWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        btn.layer.cornerRadius = 0;
        [self addSubview:btn];
        if (i == 0) {
            if (self.type == 1) {
                btn.alpha = 1.0;
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }else{
                btn.alpha = 0.7;
                [btn setTitleColor:[UIColor colorWithRed:226/255.0 green:64/255.0 blue:84/255.0 alpha:1] forState:UIControlStateNormal];
            }
            
            btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        }
    }
    for (int i = 0; i < self.titleArray.count - 1; i++) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake((i+1)*(self.bounds.size.width / self.titleArray.count), (self.bounds.size.height - 20)/2 - 1, 1, 20)];
        lineView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
//        [self addSubview:lineView];
    }
    
    if (self.type == 1) {
        self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width/ self.titleArray.count/4, self.bounds.size.height - 8, self.bounds.size.width/ self.titleArray.count/2, 8)];
        self.bottomView.backgroundColor = [UIColor clearColor];
        UIImageView *toImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width/ self.titleArray.count/2/2-12, 0, 24, 8)];
        toImage.image = [UIImage imageNamed:@"xuanzhong"];
        [self.bottomView addSubview:toImage];
    }else{
        self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width/ self.titleArray.count/4, self.bounds.size.height - 4, self.bounds.size.width/ self.titleArray.count/2, 4)];
        self.bottomView.backgroundColor = [UIColor colorWithRed:226/255.0 green:64/255.0 blue:84/255.0 alpha:1];
        self.bottomView.layer.masksToBounds = YES;
        self.bottomView.layer.cornerRadius = 2;
    }
    
   
    if (self.titleArray.count != 1) {
        [self addSubview:self.bottomView];
    }
}

- (void)setCurrentIndex:(NSInteger)currentIndex {
    _currentIndex = currentIndex;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 100 + currentIndex;
    [self bottomViewChange:btn];
}

- (void)bottomViewChange:(UIButton*)btn {
    [UIView animateWithDuration:0.25 animations:^{
        self.bottomView.frame = CGRectMake(self.currentIndex * self.bounds.size.width/self.titleArray.count+self.bounds.size.width/ self.titleArray.count/4, self.bottomView.frame.origin.y, self.bounds.size.width / self.titleArray.count/2, self.bottomView.bounds.size.height);
    }];
    for (CustomBudgeButtom *button in self.subviews) {
        if ([button isKindOfClass:[CustomBudgeButtom class]]) {
            if (button.tag == btn.tag) {
                if (self.type != 1) {
                     [button setTitleColor:[UIColor colorWithRed:226/255.0 green:64/255.0 blue:84/255.0 alpha:1] forState:UIControlStateNormal];
                }
                 button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
            } else {
                 button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
                if (self.type == 1) {
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }else{
                    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];                   
                }
                
            }
        }
    }
    
}

- (void)btnWasPressed:(UIButton*)btn {
    for (CustomBudgeButtom *button in self.subviews) {
        if ([button isKindOfClass:[CustomBudgeButtom class]]) {
            if (button.tag == btn.tag) {
                if (self.type != 1) {
                    [button setTitleColor:[UIColor colorWithRed:226/255.0 green:64/255.0 blue:84/255.0 alpha:1] forState:UIControlStateNormal];
                }
                
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
            } else {
                if (self.type != 1) {
                    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                }
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
            }
        }
    }
    self.currentIndex = btn.tag - 100;
    
    if (_delegate && [_delegate respondsToSelector:@selector(customMenuViewSelectedWithIndex:)]) {
        [_delegate customMenuViewSelectedWithIndex:btn.tag - 100];
    }
}

- (void)resetBudge:(NSString *)budgeNum withIndex:(NSInteger)index {
    int i = 0;
    for (CustomBudgeButtom *button in self.subviews) {
        if ([button isKindOfClass:[CustomBudgeButtom class]]) {
            if (i == index) {
                [button resetBudgeWithNum:budgeNum];
            }
            i++;
        }
    }
}

- (void)setShowBudge:(BOOL)showBudge {
    for (CustomBudgeButtom *btn in self.subviews) {
        if ([btn isKindOfClass:[CustomBudgeButtom class]]) {
            btn.showBudge = showBudge;
        }
    }
}

- (void)resetBudgeWithIndex:(NSInteger)index show:(BOOL)show{
    int i = 0;
    for (CustomBudgeButtom *button in self.subviews) {
        if ([button isKindOfClass:[CustomBudgeButtom class]]) {
            if (i == index) {
                button.showBudge = show;
            }
            i++;
        }
    }
}

@end
