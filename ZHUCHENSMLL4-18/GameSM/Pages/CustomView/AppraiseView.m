//
//  AppraiseView.m
//  bang
//
//  Created by 王涛 on 15/7/6.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "AppraiseView.h"

@implementation AppraiseView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self resetView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)setCanChoose:(BOOL)canChoose {
    _canChoose = canChoose;
    [self resetView];
}

- (void)resetView {
    for (UIButton *imageView in self.subviews) {
        if ([imageView isKindOfClass:[UIButton class]]) {
            [imageView removeFromSuperview];
        }
    }
    for (int i = 0 ; i < 5; i++) {
        UIButton *imageView = [[UIButton alloc] initWithFrame:CGRectMake(5 + 20 * i, (self.frame.size.height - 15)/2, 15, 15)];
        if (i < self.leavl) {
            [imageView setImage:[UIImage imageNamed:@"xingxing1.png"] forState:UIControlStateNormal];
        } else {
            [imageView setImage:[UIImage imageNamed:@"xingxing2.png"] forState:UIControlStateNormal];
        }
        if (self.canChoose) {
            [imageView addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        imageView.tag = i;
        [self addSubview:imageView];
    }
}

- (void)btnPressed:(UIButton*)btn {
    self.leavl = btn.tag + 1;
    [self resetView];
}
@end
