//
//  CalenderView.h
//  bang
//
//  Created by 王涛 on 15/6/3.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalenderViewDelegate <NSObject>

- (void)calenderChooseDays:(NSMutableArray*) array;

@end

@interface CalenderView : UIView

@property (nonatomic, strong) NSDate *calendarDate;
@property (nonatomic, assign) id<CalenderViewDelegate> delegate;

@end
