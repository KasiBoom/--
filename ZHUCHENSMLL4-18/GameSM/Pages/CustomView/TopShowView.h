//
//  TopShowView.h
//  GameSM
//
//  Created by 王涛 on 15/7/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopShowView : UIView

@property (nonatomic, strong) NSString *showInfo;

- (void)show;

- (void)hide;

@end
