//
//  CustomTool.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/12/1.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CustomTool.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)





@implementation CustomTool

//创建按钮Btn
+(UIButton *)createBtn{
    UIButton *Btn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth/2 - 50, KScreenheight - 64 - 40 - 80, 100, 30)];
    Btn.layer.masksToBounds = YES;
    Btn.layer.cornerRadius = 6;
    Btn.layer.borderWidth = 2.0;
    Btn.layer.borderColor = [UIColor grayColor].CGColor;
    [Btn setTitle:@"刷新试试?" forState:UIControlStateNormal];
    Btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [Btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    return Btn;
}

//创建加载失败视图
+(UIImageView *)createImageView{
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"wangluobuhao"];
    imageView.userInteractionEnabled = YES;
//创建提示label
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, KScreenWidth, 40)];
    contentLabel.text = @"网络好像出了点问题呢?!";
    contentLabel.textAlignment = 1;
    [imageView addSubview:contentLabel];
    imageView.alpha = 0;
    return imageView;
}

//自定义Nav
+(UIView *)createNavView{
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 64)];
    view1.backgroundColor = [UIColor whiteColor];
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 64)];
//    view.backgroundColor = [UIColor colorWithRed:228/255.0 green:62/255.0 blue:82/255.0 alpha:0.95];
//    [view1 addSubview:view];
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 63, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:229/255.0 green:123/255.0 blue:136/255.0 alpha:1];
    [view1 addSubview:lineView];
    return view1;
}




@end
