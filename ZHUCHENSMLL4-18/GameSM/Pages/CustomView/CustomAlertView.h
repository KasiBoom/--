//
//  CustomAlertView.h
//  AnXin
//
//  Created by wt on 14-3-27.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomAlertView;
@protocol CustomAlertViewDelegate <NSObject>

- (void)customAlertViewBtnWasSeletecd:(CustomAlertView*)alertView withIndex:(NSInteger)index;

@end

@interface CustomAlertView : UIView

- (void)show;
- (void)hide;
- (id)initWithTitle:(NSString*)title message:(NSString*)message delegate:(id<CustomAlertViewDelegate>)delegate cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles;

@end
