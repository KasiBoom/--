//
//  CustomMenuView.h
//  bang
//
//  Created by 王涛 on 15/5/24.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomView.h"

@protocol CustomMenuViewDelegate <NSObject>

@optional
- (void)customMenuViewSelectedWithIndex:(NSInteger)index;

@end

@interface CustomMenuView : CustomView

@property (nonatomic, assign) id<CustomMenuViewDelegate> delegate;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) NSArray *titleArray;

@property (nonatomic, assign) BOOL showBudge;

//判断不同的界面
@property (nonatomic, assign) int type;

- (id)initWithFrame:(CGRect)frame withTitleArray:(NSArray*)array;
- (void)resetBudge:(NSString*)budgeNum withIndex:(NSInteger)index;

- (void)resetBudgeWithIndex:(NSInteger)index show:(BOOL)show;

@end
