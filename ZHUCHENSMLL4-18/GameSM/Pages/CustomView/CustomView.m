//
//  CustomView.m
//  Haoye_lvpai
//
//  Created by wt on 15/4/17.
//  Copyright (c) 2015年 lianghuigui. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self resetViewBorder];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self resetViewBorder];
    }
    return self;
}

//定义边框的宽度和颜色。
- (void)resetViewBorder {
    //    self.layer.borderWidth = 0.5;
    //    self.layer.borderColor = [UIColor colorWithRed:207/255.0 green:207/255.0 blue:207/255.0 alpha:1].CGColor;
}

@end
