//
//  CustomTextView.m
//  AnXin
//
//  Created by wt on 14-4-11.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import "CustomTextView.h"

#import "Config.h"

@implementation CustomTextView

- (void)dealloc {
    _CDelegate = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self addTopBtn];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addTopBtn];
    }
    return self;
}

- (void)addTopBtn {
    UIToolbar * top=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    UIBarButtonItem * speace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem * dimissKey = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissKeyBoard)];

    top.items=[NSArray arrayWithObjects:speace,dimissKey, nil];
    
    self.inputAccessoryView = top;
}

- (void)dismissKeyBoard {
    [self resignFirstResponder];
}

- (void)clean {
    [self resignFirstResponder];
}

@end
