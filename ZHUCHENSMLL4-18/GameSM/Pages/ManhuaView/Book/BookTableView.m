//
//  BookTableView.m
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BookTableView.h"
#import "BookCell.h"
#import "BookModel.h"
#import "DownloadModel.h"
#import "CollectionModel.h"
#import "Y_X_DataInterface.h"
#import "AFNetworking.h"
//#import "testViewController.h"
#import "CategoryDetailsModel.h"
#import "BookDetailsViewController.h"
#import "UIView+Addtion.h"
#import "SaveWatchDownViewController.h"
#import "DataBaseHelper.h"
#import "cartoonIdModel.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface BookTableView ()
{
    NSMutableArray *_dataArr;
//    NSMutableArray *_downLoadArr;
    UIImageView     *_tipImageView;
}
@end

@implementation BookTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 64-44);
        self.delegate  = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        NSMutableArray *arr = [[DataBaseHelper shared] fetchSaveCartoonId];
        NSMutableArray *bookArr = [NSMutableArray array];
        for (int i = 0; i < arr.count; i ++) {
            BookModel *model = [[BookModel alloc] init];
            cartoonIdModel *cartoonId = arr[i];
            model.name = cartoonId.name;
            model.images = cartoonId.image;
            model.id = cartoonId.cartoonId;
            [bookArr addObject:model];
        }
        _downLoadArr = bookArr;
    }
    return self;
}

#pragma mark -初始化
-(void)setType:(NSString *)type{
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"ISSHOWTHEBOOKHELP"] isEqualToString:@"1"]){
        NSArray *windows = [UIApplication sharedApplication].windows;
        UIWindow *win = [windows objectAtIndex:0];
        
        _tipImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
        _tipImageView.userInteractionEnabled = YES;
        _tipImageView.image = [UIImage imageNamed:@"tips_kong"];
        _tipImageView.contentMode = UIViewContentModeScaleToFill;
        [win addSubview:_tipImageView];
        
        UIControl *helpC = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
        [helpC addTarget:self action:@selector(misSelf:) forControlEvents:UIControlEventTouchUpInside];
        [_tipImageView addSubview:helpC];
        _tipImageView.hidden = YES;
    }
    
    if (_bookDataArr.count + _dataArr.count + _collectionArr.count == 0) {
        _tipImageView.hidden = NO;
    }
}
-(void)misSelf:(UIControl *)helpC{
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"ISSHOWTHEBOOKHELP"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [helpC removeFromSuperview];
    [_tipImageView removeFromSuperview];
}
#pragma mark-UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1 && _dataArr.count > 0) {
        return 1;
    }else if(section == 1 && _dataArr.count == 0){
        return 0;
    }else if (section == 0 && _bookDataArr.count > 0){
        return 1;
    }else if (section == 0 && _bookDataArr.count == 0){
        return 0;
    }else if (section == 2 && _downLoadArr.count  > 0){
        return 1;
    }else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    static NSString *identify = @"BookCell";
    BookCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        cell = [[BookCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify andSection:section];
    
    
    [self deselectRowAtIndexPath:indexPath animated:YES];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (section == 0) {
//        cell.model = _bookDataArr[indexPath.row];
//        cell.model1 = _bookDataArr[indexPath.row+1];
//        cell.model2 = _bookDataArr[indexPath.row+2];
//        NSMutableArray *arr =[[NSMutableArray alloc] init];
//        for (int i = 0; i < _bookDataArr.count; i ++) {
//            BookModel *model = [[BookModel alloc] init];
//            [model setValuesForKeysWithDictionary:_bookDataArr[i]];
//            [arr addObject:model];
//            
//        }
        cell.cateModels = _bookDataArr;
        
    }
    if (section == 1){
//        cell.model = _dataArr[indexPath.row];
//        cell.model1 = _dataArr[indexPath.row+1];
//        cell.model2 = _dataArr[indexPath.row+2];
//        NSMutableArray *arr =[[NSMutableArray alloc] init];
//        for (int i = 0; i < _dataArr.count; i ++) {
//            BookModel *model = [[BookModel alloc] init];
//            [model setValuesForKeysWithDictionary:_dataArr[i]];
//            [arr addObject:model];
        
//        }
        cell.cateModels = _dataArr;
    }
    
    if (section == 2) {
        
        cell.cateModels = _downLoadArr;
    }

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 52;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"1";
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *iv = [[UIView alloc]init];
    iv.backgroundColor = [UIColor whiteColor];
    iv.frame = CGRectMake(0, 20, self.bounds.size.width, 30);
    UIImageView *uiv = [[UIImageView alloc]initWithFrame:CGRectMake(12, 20, 12, 12)];
    [iv addSubview:uiv];
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(-1,-1, ScreenSizeWidth + 2, 0.7)];
    bottomView.layer.borderWidth = 0.5f;
    bottomView.layer.borderColor = [[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0] CGColor];
    [iv addSubview:bottomView];
    
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(35, 16, 100, 20)];
    label.font = [UIFont systemFontOfSize:18.0];
    label.textAlignment = 0;
    label.textColor = [UIColor colorWithRed:224/255.0 green:63/255.0 blue:84/255.0 alpha:1.0];
//    label.textAlignment = NSTextAlignmentCenter;
    [iv addSubview:label];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(120, 18, self.frame.size.width - 100, 15)];
    [btn setTitleColor:[UIColor colorWithRed:224/255.0 green:63/255.0 blue:84/255.0 alpha:1.0] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:18];
    
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //    [btn addTarget:self action:@selector(gengduoView:) forControlEvents:UIControlEventTouchUpInside];
    if (_bookDataArr.count + _downLoadArr.count + _dataArr.count >0) {
        [btn setImage:[UIImage imageNamed:@"jinru"] forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, self.frame.size.width - 120 - 15, 0 , 0);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, self.frame.size.width - 120 - 50, 0, 0);
    }else{
//        [btn setImage:[UIImage imageNamed:@"jinru"] forState:UIControlStateNormal];
//        btn.imageEdgeInsets = UIEdgeInsetsMake(0, self.frame.size.width - 120 - 30, 0 , 0);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, self.frame.size.width - 120-30, 0, 0);
    }
    
    
    [iv addSubview:btn];
    
    if (section == 0) {
        label.text = @"最近阅读";
        uiv.image = [UIImage imageNamed:@"zuijinyuedu"];
        [btn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)_bookDataArr.count] forState:UIControlStateNormal];
        
        [btn addTarget:self action:@selector(moreNearReadView:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (section == 1) {
        label.text = @"已收藏";
        uiv.image = [UIImage imageNamed:@"yishoucang"];
        [btn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)_collectionArr.count] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(moreCollectionView:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    if (section == 2){
        label.text = @"已下载";
        uiv.image = [UIImage imageNamed:@"yixiazai"];
        [btn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)_downLoadArr.count] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(moreDownLoadView:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return iv;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cf" to_page:@"cfh" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:@""];
    NSString *to_page;
    switch (indexPath.section) {
        case 0:
            to_page = @"crl";
            break;
        case 1:
            to_page = @"cfl";
            break;
        case 2:
            to_page = @"cdl";
            break;
            
        default:
            break;
    }
    [[LogHelper shared] writeToFilefrom_page:@"cfh" from_section:@"c" from_step:@"h" to_page:to_page to_section:@"c" to_step:@"l" type:@"" id:@"0"];
    
    SaveWatchDownViewController *SWDVC = [[SaveWatchDownViewController alloc] init];
    SWDVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    SWDVC.moveType = indexPath.section;
    SWDVC.bookDataArr = _bookDataArr;
    SWDVC.collectionArr = _collectionArr;
    
//    UINavigationController *swdcNav = [[UINavigationController alloc] initWithRootViewController:SWDVC];
    [self.viewController presentViewController:SWDVC animated:YES completion:nil ];

    
    
}
//设置cell的高度,不能拿到cell因为出现循环引用
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return KScreenWidth/3*___Scale+32;
}


#pragma mark - 跳转
- (void)moreNearReadView:(UIButton *)moreLuBtn{
//    BookDetailsViewController *bookDetails = [[BookDetailsViewController alloc] init];
//    bookDetails.bookArr = _bookDataArr;
//    bookDetails.titleName = @"最近阅读";
//    [self.viewController.navigationController pushViewController:bookDetails animated:YES];
    SaveWatchDownViewController *SWDVC = [[SaveWatchDownViewController alloc] init];
    SWDVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    SWDVC.moveType = 0;
    SWDVC.bookDataArr = _bookDataArr;
    SWDVC.collectionArr = _collectionArr;
    
//    UINavigationController *swdcNav = [[UINavigationController alloc] initWithRootViewController:SWDVC];
    [self.viewController presentViewController:SWDVC animated:YES completion:nil ];
    
    //    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_CARTOONUSERREADHISTORY_DETAIL];
//    NSLog(@"%@",str);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"nearread" object:str];
//    NSLog(@"%@",str);
}

- (void)moreDownLoadView:(UIButton *)downBtn{
    SaveWatchDownViewController *SWDVC = [[SaveWatchDownViewController alloc] init];
    SWDVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    SWDVC.moveType = 2;
    SWDVC.bookDataArr = _bookDataArr;
    SWDVC.collectionArr = _collectionArr;
    
//    UINavigationController *swdcNav = [[UINavigationController alloc] initWithRootViewController:SWDVC];
    [self.viewController presentViewController:SWDVC animated:YES completion:nil ];
    
}


- (void)moreCollectionView:(UIButton *)moreNewBtn{
    
    SaveWatchDownViewController *SWDVC = [[SaveWatchDownViewController alloc] init];
    SWDVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    SWDVC.collectionArr = _collectionArr;
    SWDVC.bookDataArr = _bookDataArr;
    SWDVC.moveType = 1;
//    UINavigationController *swdcNav = [[UINavigationController alloc] initWithRootViewController:SWDVC];
    [self.viewController presentViewController:SWDVC animated:YES completion:nil ];
    
//    BookDetailsViewController *bookDetails = [[BookDetailsViewController alloc] init];
//    bookDetails.bookArr = _collectionArr;
//    bookDetails.titleName = @"已收藏";
//    [self.viewController.navigationController pushViewController:bookDetails animated:YES];
//    [self.viewController presentViewController:bookDetails animated:YES completion:nil];
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_BOOKCOLLECTIONLIST_LIST];
//    NSLog(@"%@",str);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"MoreC" object:str];
    
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_BOOKCOLLECTIONLIST_LIST];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"newnew" object:str];
//    NSLog(@"跳转到更多");
    
    
}

#pragma mark - 加载数据
- (void)setBookDataArr:(NSMutableArray *)bookDataArr{
    _bookDataArr = bookDataArr;
    [self reloadData];
    
}

- (void)setCollectionArr:(NSMutableArray *)collectionArr{
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    _collectionArr = collectionArr;

    _dataArr = collectionArr;
    [self reloadData];
}

- (void)setDownLoadArr:(NSMutableArray *)downLoadArr{
    NSMutableArray *arr = [[DataBaseHelper shared] fetchSaveCartoonId];
    NSMutableArray *bookArr = [NSMutableArray array];
    for (int i = 0; i < arr.count; i ++) {
        BookModel *model = [[BookModel alloc] init];
        cartoonIdModel *cartoonId = arr[i];
        model.name = cartoonId.name;
        model.images = cartoonId.image;
        model.id = cartoonId.cartoonId;
        [bookArr addObject:model];
    }
    _downLoadArr = bookArr;
    [self reloadData];

}
@end
