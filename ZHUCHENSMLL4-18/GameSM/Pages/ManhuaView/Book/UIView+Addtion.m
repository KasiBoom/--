//
//  UIView+Addtion.m
//  味库1.0
//
//  Created by qianfeng on 14-11-10.
//  Copyright (c) 2014年 alex. All rights reserved.
//

#import "UIView+Addtion.h"

@implementation UIView (Addtion)

- (UIViewController *)viewController
{
    id next = [self nextResponder];
    
    do {
        if ([next isKindOfClass:[UIViewController class]]) {
            
            UIViewController *vc = (UIViewController *)next;
            if (vc.navigationController != nil) {
                return vc;
            }
        }
        next = [next nextResponder];
        
    } while (next != nil);
    

    return nil;
}

@end
