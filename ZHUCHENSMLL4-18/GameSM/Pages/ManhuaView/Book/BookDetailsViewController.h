//
//  BookDetailsViewController.h
//  GameSM
//
//  Created by 顾鹏 on 15/10/22.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CartoonInfoAll;
@interface BookDetailsViewController : UIViewController
@property(nonatomic,retain)NSMutableArray *bookArr;
@property(nonatomic,copy)NSString *titleName;
@property(nonatomic,retain)NSMutableArray *rootArray;
@end
