//
//  CategoryDetailsCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "CategoryDetailsCell.h"
#import "CategoryDetailsModel.h"
#import "BookModel.h"
#import "Config.h"
#import "UIImageView+AFNetworking.h"
#import "CartoonInfoAll.h"
@interface CategoryDetailsCell ()
{
    UIImageView *_imageView;
    UILabel     *_titleLabel;
    
    UILabel *_continueLabel;
}
@end

@implementation CategoryDetailsCell

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width*___Scale)];
//        _imageView.backgroundColor = [UIColor redColor];
        _imageView.layer.cornerRadius = 4.0;
        _imageView.layer.masksToBounds = YES;
        [self addSubview:_imageView];
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _imageView.bottom, 100, 20)];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_titleLabel];
        
        _updataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_imageView.frame) - 20, CGRectGetWidth(_imageView.frame), 20)];
        _updataLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
        _updataLabel.textColor = [UIColor whiteColor];
        _updataLabel.font = [UIFont systemFontOfSize:12.0];
        _updataLabel.textAlignment = 1;
        
        _redImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth(_imageView.frame)-10, CGRectGetHeight(_imageView.frame) - 18, 8, 8)];
        _redImageView.layer.cornerRadius = 4;
        _redImageView.backgroundColor = [UIColor redColor];
        _redImageView.hidden = YES;
//        [_updataLabel addSubview:_redImageView];
        
        _continueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame) - 15, CGRectGetWidth(self.frame), 15)];
        _continueLabel.textColor = RGBACOLOR(157, 157, 157, 1.0);
        _continueLabel.font = [UIFont systemFontOfSize:12.0];
        _continueLabel.textAlignment = 0;
        [self.contentView addSubview:_continueLabel];
        
        _add = [UIButton buttonWithType:UIButtonTypeCustom];
        _add.userInteractionEnabled = NO;
        _add.frame = CGRectMake(self.width - 30, 0, 30, 30);
        [_add setImage:[UIImage imageNamed:@"recipe_normal"] forState:UIControlStateNormal];
        [_add setImage:[UIImage imageNamed:@"recipe_add"] forState:UIControlStateSelected];
//        [add addTarget:self action:@selector(addDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        _add.hidden = YES;
        [self addSubview:_add];
    }
    return self;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    _add.selected = isSelected;
}



-(void)setModel:(CategoryDetailsModel *)model{
    _model = model;
    _titleLabel.text = _model.name;
    if (model.updateInfo) {
        _updataLabel.text = model.updateInfo;
        [_imageView addSubview:_updataLabel];
    }
    
//    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_async(globalQueue, ^{
//        __block UIImage *image = nil;
//        dispatch_sync(globalQueue, ^{
//            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model.images]]]];
//        });
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            _imageView.image = image;
//        });
//    });
    [_imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model.images]]];
}

- (void)setBookModel:(CartoonInfoAll *)bookModel {
    _bookModel = bookModel;
//    if (_bookModel.currentReadChapterId || _bookModel.chapterIndex) {//chapterIndex
//        if (_bookModel.chapterIndex) {
//            _titleLabel.text = [NSString stringWithFormat:@"撸至%@话",_bookModel.chapterIndex];
//            _updataLabel.text = bookModel.updateInfo;
//        }else{
//            _titleLabel.text = [NSString stringWithFormat:@"撸至%@话",_bookModel.currentReadChapterId];
//            _updataLabel.text = bookModel.updateInfo;
//        }
//    }else{
    _titleLabel.text = [bookModel name];
    _updataLabel.text = [bookModel updateInfo];
    if ([self.bookModel chapterIndex]) {
        _continueLabel.text = [NSString stringWithFormat:@"撸至%@话",[self.bookModel chapterIndex]];
    }else{
        _continueLabel.text = @"未观看";
    }
    
//}
    [self.contentView addSubview:_updataLabel];
    [self.contentView addSubview:_redImageView];
    [_imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[bookModel images]]]];
    
//    _imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",bookModel[@"images"]]]]];

}

@end
