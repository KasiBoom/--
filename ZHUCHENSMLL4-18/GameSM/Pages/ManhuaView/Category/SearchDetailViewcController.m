//
//  SearchDetailViewcController.m
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchDetailViewcController.h"

@interface SearchDetailViewcController ()

@end

@implementation SearchDetailViewcController
- (UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleDefault;
}
- (IBAction)backSearchView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"返回到搜索的tableview界面");
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *statusBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
    statusBarView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:statusBarView];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    [self creatView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)creatView{
    _searchresultsCollectionView = [[SearchresultsCollectionView alloc]initWithFrame:CGRectMake(0, _resultLabel.bottom, ScreenSizeWidth, ScreenSizeHeight)];
    _searchresultsCollectionView.backgroundColor = RGBACOLOR(235, 235, 235, 1);
    [self.view addSubview:_searchresultsCollectionView ];
}





@end
