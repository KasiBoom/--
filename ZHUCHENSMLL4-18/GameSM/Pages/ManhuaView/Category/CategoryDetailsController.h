//
//  CategoryDetailsController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullingRefreshTableView.h"
//#import "PullingRefreshCollectionView.h"
#import "PullPsCollectionView.h"
#import "BaseViewController.h"
@interface CategoryDetailsController : BaseViewController
@property(nonatomic,copy)NSString *titleName;
@property(nonatomic,strong)NSString *type;
@end
