//
//  SearchresultCell.m
//  GameSM
//
//  Created by mac on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchresultCell.h"

@implementation SearchresultCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatView];
    }
    return self;
}

- (void)creatView{
    
    _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,(ScreenSizeWidth-KSearchresultsitemInset*2*3)/3,120)];
    _imageView.backgroundColor = [UIColor clearColor];
    _imageView.image = [UIImage imageNamed:@"jiazaitu_2"];
    [self addSubview:_imageView];
    
    _titleLable  = [[UILabel alloc]initWithFrame:CGRectMake(0, _imageView.bottom, _imageView.width, 26)];
    _titleLable.text = @"火影忍者";
    _titleLable.font = [UIFont systemFontOfSize:15];
    _titleLable.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_titleLable];
    
}
@end
