//
//  SearchresultsCollectionView.m
//  GameSM
//
//  Created by mac on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchresultsCollectionView.h"
#import "SearchresultCell.h"

@implementation SearchresultsCollectionView{
    NSString *_identfy;
}

- (instancetype)initWithFrame:(CGRect)frame{
        //1.创建布局对象
    UICollectionViewFlowLayout *Layout = [[UICollectionViewFlowLayout alloc]init];
    Layout.minimumInteritemSpacing = KSearchresultsitemInset;
    Layout.minimumLineSpacing = KSearchresultsitemInset;
    Layout.sectionInset = UIEdgeInsetsMake(KSearchresultsitemInset, 4, KSearchresultsitemInset, 4);
    Layout.itemSize = CGSizeMake((ScreenSizeWidth-KSearchresultsitemInset*2*3)/3.0,120+26);
    self.backgroundColor = [UIColor whiteColor];

    self = [super initWithFrame:frame collectionViewLayout:Layout];
    if (self) {
        [self creatView];
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}


- (void)creatView{
    
    //3.注册单元格类型
    _identfy = @"SearchresultCell";
    [self registerClass:[SearchresultCell class] forCellWithReuseIdentifier:_identfy];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SearchresultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identfy forIndexPath:indexPath];
    return cell;
}

@end
