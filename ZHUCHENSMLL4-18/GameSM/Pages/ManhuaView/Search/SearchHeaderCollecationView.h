//
//  SearchHeaderCollecationView.h
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseCollectionView.h"


@interface SearchHeaderCollecationView : BaseCollectionView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
@property(nonatomic,strong)NSArray *keyArrays;

@property(nonatomic,strong)NSMutableArray *searchDataArr;

@property(nonatomic,strong)NSArray *keywordArr;
@property(nonatomic,strong)NSArray *localKeywordArr;

@property(nonatomic,strong)NSMutableDictionary *searchDataCount;
@property(nonatomic,copy)void(^changeText)(NSString *text);
- (float)getcollecationViewHeight;
@end
