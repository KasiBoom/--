//
//  SearchHeaderModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/12.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchHeaderModel : NSObject

@property(nonatomic,strong)NSString *keyword;

@end
