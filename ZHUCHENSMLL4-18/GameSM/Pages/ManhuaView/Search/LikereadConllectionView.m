//
//  LikereadConllectionView.m
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "LikereadConllectionView.h"
#import "LikereadCell.h"
#import "LikereadModel.h"
#import "Y_X_DataInterface.h"
#import "Config.h"
#import "AFNetworking.h"


#define kcellHeight (([UIScreen mainScreen].bounds.size.width-50)/3.0) * ___Scale+20
#define cellWidth (([UIScreen mainScreen].bounds.size.width-50)/3.0)

@interface LikereadConllectionView ()
{
    NSMutableArray *_dataArr;
    NSString *_identfy;
    
    
    NSMutableArray *_iddArr;
}
@end

@implementation LikereadConllectionView

- (instancetype)initWithFrame:(CGRect)frame {
    
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 3;
    layout.minimumLineSpacing = 8;
//    layout.sectionInset = UIEdgeInsetsMake(4, 4, 4, 4);
//    layout.itemSize = CGSizeMake(cellWidth, kcellHeight);

    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
//        [self creatView];
        //2.设置代理
        self.delegate = self;
        self.dataSource = self;
        self.pagingEnabled = YES;
        self.showsHorizontalScrollIndicator = YES;
        self.showsVerticalScrollIndicator   = YES;
        
        //3.注册单元格类型
        _identfy = @"LikereadCell1";
        [self registerClass:[LikereadCell class] forCellWithReuseIdentifier:_identfy];
    }
    return self;
}

- (void)creatView{
}

#pragma mark UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
        return _dataArr.count;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(cellWidth, kcellHeight);
}

#pragma mark 设置每一项距离其它项的上下左右间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
        LikereadCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identfy forIndexPath:indexPath];
        cell.model = _dataArr[indexPath.row];
        return cell;

}
//单元格的点击事件,跳转到下个界面
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = [indexPath row];
    [[LogHelper shared] writeToFilefrom_page:@"csh" from_section:@"c" from_step:@"h" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:_iddArr[row]];

    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_iddArr[row]];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"everybodywatch" object:str];
}

#pragma mark-set 方法计算高度
- (float)getimageCollecationViewHeight{
    int count1 = 10/4;
    int count2 = 10%4;
    float height = ((count1+count2) *kcellHeight + (count1+1) * 4)-100;
    NSLog(@"%f",height);
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
    return height;
}
#pragma mark - 加载数据
-(void)setSearchDataListArr:(NSMutableArray *)searchDataListArr{
    _dataArr = [NSMutableArray array];
    _iddArr = [NSMutableArray array];
    for (NSDictionary *dict in searchDataListArr) {
        LikereadModel *mb = [[LikereadModel alloc]init];
        [mb setValuesForKeysWithDictionary:dict];
        [_dataArr addObject:mb];
        [_iddArr addObject:dict[@"id"]];
    }
    [self reloadData];
}

@end
