//
//  ThemeImageTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/22.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class bigRecommentModel;
@class CartoonInfoAll;
@interface ThemeImageTableViewCell : UITableViewCell
@property(nonatomic,copy)bigRecommentModel *bigRecommModel;
@property(nonatomic,copy)CartoonInfoAll *cartoonModel;

@end
