

//
//  RecommentTableView.m
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "RecommentTableView.h"
#import "bigRecommentModel.h"
#import "ManhuaViewController.h"
#import "RecommentCell.h"
#import "RecommentModel.h"
#import "BookDetailsViewController.h"
#import "UIView+Addtion.h"
#import "EGORefreshTableHeaderView.h"
#import "CommicTableViewCell.h"
#import "ImageTableViewCell.h"
#import "singleImageTableViewCell.h"
#import "CartoonInfoAll.h"
#import "InfoDetailViewController.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define BASECOLOR [UIColor colorWithRed:255/255.0 green:67/255.0 blue:89/255.0 alpha:1]
@interface RecommentTableView ()
{
    NSMutableArray *_dataArr;
    NSString       *_tag;
}
@end

@implementation RecommentTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    self  = [super initWithFrame:frame style:style];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(adddata:) name:@"dataarr" object:nil];
    if (self) {
        [self creatView];
    }
    return self;
}

- (void)creatView{
    //1.创建头部视图
    _recommentHeardView = [[RecommentHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, KrecommentCellHeight)];
    _recommentHeardView.backgroundColor = [UIColor clearColor];
    _recommentHeardView.width = ScreenSizeWidth;
    self.tableHeaderView = _recommentHeardView;
    self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight-113);
    //2.设置代理
    self.delegate = self;
    self.dataSource = self;
    
    //3.设置单元格
    self.showsVerticalScrollIndicator = NO;
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.backgroundColor = [UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0];
    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - KScreenheight, KScreenWidth, KScreenheight)];
        view.delegate = self;
        [self addSubview:view];
        _refreshHeaderView = view;
//        [view release];
        
    }
}

#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    _reloading = YES;
    
}

- (void)doneLoadingTableViewData{
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self];
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}

#pragma mark EGORefreshTableHeaderDelegate Methods
//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRecommentTableView" object:nil];
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
    
}

#pragma mark- get -set 


#pragma mark-UITableView delegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    bigRecommentModel *bigM = _dataArr[section];
    if (([bigM.type isEqualToString:@"0"]&&bigM.cartoonSetList.count ==  0)||[bigM.type isEqualToString:@"2"]||[bigM.type isEqualToString:@"1"]||[bigM.type isEqualToString:@"3"]) {
//            if([_dataArr[section] cartoonSetList].count == 0){
//                return 0;
//            }
        return 0;
        
    }
    return 30;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return @"";
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *whiteView = [[UIView alloc]initWithFrame:CGRectMake(0,0,KScreenWidth,30)];
    whiteView.backgroundColor = [UIColor whiteColor];
    return whiteView;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSLog(@"为了section%d",section);
    if (section == 0) {
        if([_dataArr[section] cartoonSetList].count == 0){
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 0.01)];
            line.backgroundColor = [UIColor whiteColor];
            return line;
        }
    }
    
    bigRecommentModel *bigModel = _dataArr[section];
    if ([bigModel.type isEqualToString:@"0"]||[bigModel.type isEqualToString:@"10"]) {
        
    
    UIView *iv = [[UIView alloc]init];
    iv.backgroundColor = [UIColor whiteColor];
    iv.frame = CGRectMake(0, 0, self.bounds.size.width, 30);
    UIImageView *uiv = [[UIImageView alloc]initWithFrame:CGRectMake(10, 12, 15, 15)];
//    [iv addSubview:uiv];
    
//    UIView*  bottomView = [[UIView alloc] initWithFrame:CGRectMake(-1,-1, ScreenSizeWidth + 2, 1)];
//    bottomView.layer.borderWidth = 1.0;
//    bottomView.layer.borderColor = [[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0] CGColor];
//    bottomView.backgroundColor = [UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0];
//    [iv addSubview:bottomView];
    
        
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 100, 20)];
    label.textColor = BASECOLOR;
    label.text = [_dataArr[section] title];
    label.font = [UIFont systemFontOfSize:16];
    
    [iv addSubview:label]; 
 
  if([bigModel.type isEqualToString:@"0"]){
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width -50, 10, 60, 20)];
    [btn setTitle:@"更多" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:155/255.0 green:155/255.0 blue:155/255.0 alpha:1.0] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
//    [btn setImage:[UIImage imageNamed:@"action_row.png"] forState:UIControlStateNormal];
//    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    btn.imageEdgeInsets = UIEdgeInsetsMake(5, 45, 5, 0);
    [btn addTarget:self action:@selector(moreNewView:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = section;
            [iv addSubview:btn];
  }
    return iv;
    }else{
        return nil;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger section = [indexPath section];
    bigRecommentModel *bigModel = _dataArr[section];
    static NSString *identify = @"RecommentCell";
    
    if ([[bigModel type] isEqualToString:@"0"]||[[bigModel type] isEqualToString:@"10"]) {
        //    }
        RecommentCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        //    if (cell ==nil) {
        cell = [[RecommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify andSection:section andContinueBool:bigModel.cartoonSetList.count>3?YES:NO andMaxSection:_dataArr.count];
        
        
        //取消单元格选中
        [self deselectRowAtIndexPath:indexPath animated:YES];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.recommentCollection.ludic = _dataArr[section];
        return cell;
        
    }else if ([bigModel.type isEqualToString:@"1"]){
//        CommicTableViewCell *ImageVC = [[[NSBundle mainBundle] loadNibNamed:@"CommicTableViewCell" owner:self options:nil] lastObject];
        CommicTableViewCell *ImageVC = [[CommicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
        ImageVC.selectionStyle = UITableViewCellSelectionStyleNone;
        
        ImageVC.bigRecommModel = _dataArr[section];
        return ImageVC;
        
    }else if ([bigModel.type isEqualToString:@"2"]){
        singleImageTableViewCell *singleTC = [[[NSBundle mainBundle] loadNibNamed:@"singleImageTableViewCell" owner:self options:nil] lastObject];
        singleTC.selectionStyle = UITableViewCellSelectionStyleNone;
        
        singleTC.bigModel = _dataArr[section];
        return singleTC;
        
    }else {
//        ImageTableViewCell *doubleTC = [[[NSBundle mainBundle] loadNibNamed:@"ImageTableViewCell" owner:self options:nil] lastObject];
        ImageTableViewCell *doubleTC = [[ImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
        doubleTC.selectionStyle = UITableViewCellSelectionStyleNone;
        doubleTC.bigRecommModel = _dataArr[section];
        return doubleTC;
        
    }
}

#pragma mark - 点击事件
- (void)moreLuView:(UIButton *)moreLuBtn{
    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"cfh" to_section:@"c" to_step:@"l" type:@"c" channel:@"" id:@""];

    //--跳转到继续撸。
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,@"cartoonReadHistory/getUserReadHistoryList"];//API_URL_RECOMMENTLU_LIST
//    NSLog(@"%@",str);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"LuLu" object:str];
//    NSLog(@"跳转到更多");
//    
//    BookDetailsViewController *bookDetails = [[BookDetailsViewController alloc] init];
//    bookDetails.bookArr = _recommentLuDataArr;
//    bookDetails.titleName = @"继续撸";
//    [self.viewController.navigationController pushViewController:bookDetails animated:YES];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(YXGetUrl:) name:@"continueLOL" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MANHUACONTINUE" object:nil];
}

- (void)moreNewView:(UIButton *)moreNewBtn{
//--跳转到最新更新。
    if([[_dataArr[moreNewBtn.tag] title] isEqualToString:@"继续撸"]){
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MANHUACONTINUE" object:nil];
    }else{
        
    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@&page=1&size=24",INTERFACE_PREFIXD,[_dataArr[moreNewBtn.tag] url],[_dataArr[moreNewBtn.tag] valueID]];
    NSArray *titleStr =@[str,[_dataArr[moreNewBtn.tag] title]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newnew" object:titleStr];
        NSLog(@"跳转到更多");}
}

- (void)moreDownloadView:(UIButton *)moreDownloadBtn{
    //--跳转到最多点击。
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_RECOMMENTTOP_LIST];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newnew" object:str];
    NSLog(@"跳转到更多");
}

- (void)moreCollectionView:(UIButton *)moreCollectionBtn{
    //--跳转到最多收藏。
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_RECOMMENTCOLLECTION_LIST];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newnew" object:str];
    NSLog(@"跳转到更多");
}

#pragma mark - tableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSUInteger section = [indexPath section];
    bigRecommentModel *bigModel = _dataArr[indexPath.section];
//    if (section == 0) {
//        
//    }else if  (section == 1){
//        
//    }
    if ([bigModel.type isEqualToString:@"1"]) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,bigModel.cartoonInfoAll.id];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recomment-1" object:str];
        [[LogHelper shared] writeToApafrom_page:@"crh" from_section:@"c" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id: bigModel.cartoonInfoAll.id];
        
        
    }else if ([bigModel.type isEqualToString:@"2"]){
        [[LogHelper shared] writeToApafrom_page:@"crh" from_section:@"c" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:bigModel.valueID];
        
        LYTabBarController *mmdrawControl = (LYTabBarController *)[self.window rootViewController];
        UINavigationController *nav = [mmdrawControl.viewControllers objectAtIndex:mmdrawControl.selectedIndex];
        UIViewController *currentViewControl = nav.topViewController;
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"manhuago"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        self.viewController.navigationController.navigationBar.hidden = NO;

        InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
        detailView.infoID = bigModel.valueID;
        detailView.titleName = bigModel.title;
        detailView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self.viewController.navigationController pushViewController:detailView animated:YES];
        
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (_recommentLuDataArr.count > 0) {
    bigRecommentModel *bigRecomm = _dataArr[indexPath.section];
    if ([bigRecomm.type isEqualToString:@"0"]) {
        if (indexPath.section == 0) {
            
            if([_dataArr[indexPath.section] cartoonSetList].count == 0){
                return 0;
            }else if( [_dataArr[indexPath.section] cartoonSetList].count <= 3&& [_dataArr[indexPath.section] cartoonSetList].count >=1){
                return (KScreenWidth-24)/3*___Scale+45;
            }else if([_dataArr[indexPath.section] cartoonSetList].count == 6){
                return  ((KScreenWidth-24)/3*___Scale+42)*2 - 20;
            }else{
                return 0;
            }
//        }else{
//            return ((KScreenWidth-24)/3*___Scale+42)*2 - 30;
//        }
        }else{
            return ((KScreenWidth-24)/3*___Scale+42)*2 - 20;

        }
    }else if([bigRecomm.type isEqualToString:@"3"]){
        return 110;
    }else if([bigRecomm.type isEqualToString:@"1"]){
        return 153*__Scale6;
    }else if ([bigRecomm.type isEqualToString:@"10"]){
        return ((KScreenWidth-24)/3*___Scale+42)*3 - 45;
    }else{
        return 110 * __Scale6;
    }
    
//    return 0;
//    }else{
//    return (KScreenWidth-24)/3*___Scale+28;
//    }
}

-(void)setRecommentDataArr:(NSMutableArray *)recommentDataArr andRecommentHotDataArr:(NSMutableArray *)recommentHotDataArr andRecommentCollectionDataArr:(NSMutableArray *)recommentCollectionDataArr andRecommentLuDataArr:(NSMutableArray *)recommentLuDataArr {
    
    _recommentDataArr = recommentDataArr;
    
    _recommentHotDataArr = recommentHotDataArr;
    _recommentCollectionDataArr = recommentCollectionDataArr;
    _recommentLuDataArr = recommentLuDataArr;

    for (NSDictionary *dic in _recommentDataArr) {
        RecommentModel *model = [[RecommentModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [_dataArr addObject:model];
    }
    
    [self reloadData];
}

- (void)setRecommentAllDict:(NSDictionary *)recommentAllDict {
    _recommentAllDict = recommentAllDict;
    _dataArr = [bigRecommentModel paraseArr:recommentAllDict];
    [self reloadData];
}

-(void)setRecommentHotDataArr:(NSMutableArray *)recommentHotDataArr{
    _recommentHotDataArr = recommentHotDataArr;

//    [self reloadData];
}

-(void)setRecommentCollectionDataArr:(NSMutableArray *)recommentCollectionDataArr{
    _recommentCollectionDataArr = recommentCollectionDataArr;
//    [self reloadData];
}
-(void)setRecommentLuDataArr:(NSMutableArray *)recommentLuDataArr{
    _recommentLuDataArr = recommentLuDataArr;
        [self reloadData];

}
#pragma mark - PullTableViewDelegate

@end
