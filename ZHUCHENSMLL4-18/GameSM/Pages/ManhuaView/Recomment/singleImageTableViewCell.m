//
//  singleImageTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "singleImageTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "bigRecommentModel.h"
@implementation singleImageTableViewCell
- (void)setBigModel:(bigRecommentModel *)bigModel {
    [_bigImage setImageWithURL:[NSURL URLWithString:bigModel.images]];
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
