//
//  RecommentCollectionViewCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "RecommentCollectionViewCell.h"
#import "RecommentCollectionModel.h"
#import "Config.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@interface RecommentCollectionViewCell ()
{
    UIImageView *_imageView;
    UILabel     *_nameLabel;
    UILabel     *_updataLabel;
    UILabel     *_luLabel;
}
@end

@implementation RecommentCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}
-(void)createView{
    
    _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(krecommentCellInset, krecommentCellInset, (ScreenSizeWidth-6*krecommentCellInset)/3, (ScreenSizeWidth-6*krecommentCellInset)/3*___Scale)];
    _imageView.layer.cornerRadius = 4;
    _imageView.clipsToBounds = YES;
//    _imageView.backgroundColor = [UIColor cyanColor];
    [self.contentView addSubview:_imageView];
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(_imageView.left, _imageView.bottom,(ScreenSizeWidth-6*krecommentCellInset)/3, 20)];
    _nameLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_nameLabel];
    
    _luLabel = [[UILabel alloc]initWithFrame:CGRectMake(_imageView.left, _nameLabel.bottom,(ScreenSizeWidth-6*krecommentCellInset)/3, 10)];

    _luLabel.font = [UIFont systemFontOfSize:10];
    _luLabel.textColor = [UIColor grayColor];
    _luLabel.hidden = YES;
    [self.contentView addSubview:_luLabel];
    
//    if (0) {
        _updataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_imageView.frame) - 20, CGRectGetWidth(_imageView.frame), 20)];
        _updataLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
        _updataLabel.textColor = [UIColor whiteColor];
        _updataLabel.font = [UIFont systemFontOfSize:12.0];
        _updataLabel.textAlignment = 1;
        [_imageView addSubview:_updataLabel];
//    }
    
}


-(void)setModel:(RecommentCollectionModel *)model{
    _model = model;
    [_imageView setImageWithURL:[NSURL URLWithString:model.images]];
    _nameLabel.text = model.name;
    _updataLabel.text = [NSString stringWithFormat:@"%@",model.updateInfo];
    if (model.chapterIndex) {
        _luLabel.hidden = NO;
        _luLabel.text = [NSString stringWithFormat:@"撸至%@话",model.chapterIndex];
    }
}

@end
