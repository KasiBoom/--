//
//  RecommentCollection.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseCollectionView.h"
@class bigRecommentModel;
@interface RecommentCollection : BaseCollectionView

@property(nonatomic, strong)NSArray *luArr;
@property(nonatomic, strong)bigRecommentModel *ludic;

@end
