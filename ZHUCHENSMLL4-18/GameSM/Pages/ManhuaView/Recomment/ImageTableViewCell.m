//
//  ImageTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "ImageTableViewCell.h"
#import "bigRecommentModel.h"
#import "UIImageView+AFNetworking.h"
#import "Y_X_DataInterface.h"
#import "ThemeViewController.h"
#import "UIView+Addtion.h"
@implementation ImageTableViewCell
{
    UIImageView *_leftView;
    UIImageView *_rightView;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = RGBACOLOR(239, 239, 239, 1.0);
        _leftView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, (ScreenSizeWidth - 15)/2,  90)];
        _leftView.userInteractionEnabled = YES;
        _rightView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + (ScreenSizeWidth - 15)/2, 10, (ScreenSizeWidth - 15)/2, 90)];
        _rightView.userInteractionEnabled = YES;
        [self.contentView addSubview:_leftView];
        [self.contentView addSubview:_rightView];
        _leftView.layer.cornerRadius = 5.0;
        _rightView.layer.cornerRadius = 5.0;
        _leftView.layer.masksToBounds = YES;
        _rightView.layer.masksToBounds = YES;
        
    }
    return self;
}

- (void)setBigRecommModel:(bigRecommentModel *)bigRecommModel{
    _bigRecommModel = bigRecommModel;
    [_leftView setImageWithURL:[NSURL URLWithString:bigRecommModel.images]];
    [_rightView setImageWithURL:[NSURL URLWithString:bigRecommModel.images2]];
    
    UITapGestureRecognizer *leftTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftTap:)];
    [_leftView addGestureRecognizer:leftTap];
    
    UITapGestureRecognizer *rightTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightTap:)];
    [_rightView addGestureRecognizer:rightTap];
    
    
}

- (void)leftTap:(UITapGestureRecognizer *)left{
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@&page=1&size=24",INTERFACE_PREFIXD,[_bigRecommModel url],[_bigRecommModel valueID]];
//    NSArray *titleStr =@[str,[_bigRecommModel title]];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"newnew" object:titleStr];
//    NSLog(@"跳转到更多");
    
    [[LogHelper shared] writeToApafrom_page:@"crh" from_section:@"c" from_step:@"l" to_page:@"ctd" to_section:@"c" to_step:@"l" type:@"topic" id:_bigRecommModel.valueID];
    
    ThemeViewController *theme = [[ThemeViewController alloc] init];
//    [self.viewController.navigationController pushViewController:theme animated:YES];
    UINavigationController *nac = [[UINavigationController alloc] initWithRootViewController:theme];
    theme.id = _bigRecommModel.valueID;
    theme.url = _bigRecommModel.url;
    theme.titleName = _bigRecommModel.title;
    theme.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.viewController presentViewController:nac animated:YES completion:nil];
    
}

- (void)rightTap:(UITapGestureRecognizer *)right{
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@&page=1&size=24",INTERFACE_PREFIXD,[_bigRecommModel url2],[_bigRecommModel valueID2]];
//    NSArray *titleStr =@[str,[_bigRecommModel title]];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"newnew" object:titleStr];
//    NSLog(@"跳转到更多");
    
    [[LogHelper shared] writeToApafrom_page:@"crh" from_section:@"c" from_step:@"l" to_page:@"ctd" to_section:@"c" to_step:@"l" type:@"topic" id:_bigRecommModel.valueID2];

    ThemeViewController *theme = [[ThemeViewController alloc] init];
    //    [self.viewController.navigationController pushViewController:theme animated:YES];
    UINavigationController *nac = [[UINavigationController alloc] initWithRootViewController:theme];
    theme.id = _bigRecommModel.valueID2;
    theme.url = _bigRecommModel.url2;
    theme.titleName = _bigRecommModel.title;

    theme.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.viewController presentViewController:nac animated:YES completion:nil];

}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
