//
//  RecommentHeadView.h
//  GameSM
//
//  Created by mac on 15/9/22.
//  Copyright (c) 2015年 王涛. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CustomView.h"
#import "CustomPageControl.h"       //封装的自定义page控件
#import "testViewController.h"


@protocol CustomAdViewDelegate <NSObject>

@optional
- (void)customAdViewSelectWithIndex:(int)index;

@end
@interface RecommentHeadView : CustomView<UIScrollViewDelegate>

//最终继承UIView
@property (nonatomic, strong) NSMutableArray *infoData;//数据
@property(nonatomic,strong)testViewController *testVC;
@property (nonatomic, assign) id<CustomAdViewDelegate> delegate;//自定义的代理

@end
