//
//  singleImageTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class bigRecommentModel;
@interface singleImageTableViewCell : UITableViewCell
@property (nonatomic, copy)bigRecommentModel *bigModel;
@property (weak, nonatomic) IBOutlet UIImageView *bigImage;

@end
