//
//  ImageTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class bigRecommentModel;
@interface ImageTableViewCell : UITableViewCell
@property (nonatomic, copy)bigRecommentModel *bigRecommModel;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;

@end
