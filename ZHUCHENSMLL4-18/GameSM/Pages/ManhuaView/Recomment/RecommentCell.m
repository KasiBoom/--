//
//  RecommentCell.m
//  GameSM
//
//  Created by mac on 15/9/22.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "RecommentCell.h"

#import "RecommentModel.h"
#import "Config.h"

#import "SearchHeaderCollecationView.h"

#import "Y_X_DataInterface.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "CartoonInfoAll.h"
#import "RecommentCollection.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface RecommentCell ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UILabel *_nameLabel1;
    UIImageView *_iv1;
    NSMutableArray *_dataArr;
    NSMutableArray *_LudataArr;
    NSMutableArray *_LuChapterId;
    NSMutableArray *_TopdataArr;
    NSMutableArray *_CollectiondataArr;
    NSInteger  _tag;
    
    NSString       *_albumIdStr;
    
    UITapGestureRecognizer *_kiTap;

    UIView *_cellView;
    UIView *_sessionView;
    UIView *_msgView;
    BOOL _continueBool;
    UIView *bottomView;
}
@property (nonatomic, strong)UICollectionView *collectionView;
@end

@implementation RecommentCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andSection:(NSInteger)section andContinueBool:(BOOL)continueBool andMaxSection:(NSInteger)maxSection{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.recommentCollection  = [[RecommentCollection alloc]init];
        if (section == 0&&!continueBool) {
            self.recommentCollection.frame = CGRectMake(0, 0, ScreenSizeWidth, ((KScreenWidth-24)/3*___Scale+42));
        }else if (section == maxSection - 1){
            self.recommentCollection.frame = CGRectMake(0, 0, ScreenSizeWidth, ((KScreenWidth-24)/3*___Scale+42)*3);
            
        }else{
            self.recommentCollection.frame = CGRectMake(0, 0, ScreenSizeWidth, ((KScreenWidth-24)/3*___Scale+42)*2);
        }
        self.recommentCollection.backgroundColor = [UIColor clearColor];
        self.recommentCollection.scrollEnabled = NO;
        [self.contentView addSubview:self.recommentCollection];
    }
    return self;
    
}

- (void)creatView1{

//-----------------------1话
    UIControl *manhuaControl1 = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth/3, (KScreenWidth-6*krecommentCellInset)/3*___Scale+42)];
    [_msgView addSubview:manhuaControl1];
    
    _iv1 = [[UIImageView alloc]initWithFrame:CGRectMake(krecommentCellInset, krecommentCellInset,(KScreenWidth-6*krecommentCellInset)/3, (KScreenWidth-6*krecommentCellInset)/3*___Scale)];
    _iv1.layer.cornerRadius = 4;
    _iv1.clipsToBounds = YES;
    [_iv1 setImageWithURL:[NSURL URLWithString:_model.images]];
    _iv1.userInteractionEnabled = YES;
    UITapGestureRecognizer *kitTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jump1:)];
    _kiTap = kitTap;
    [_iv1 addGestureRecognizer:kitTap];
    [manhuaControl1 addSubview:_iv1];
    
    
    _nameLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(_iv1.left, _iv1.bottom,(KScreenWidth-6*krecommentCellInset)/3, 20)];
    _nameLabel1.font = [UIFont systemFontOfSize:13.0];
    _nameLabel1.text = _model.name;
    [manhuaControl1 addSubview:_nameLabel1];
    
    if (_model.updateInfo) {
        
    UILabel *updataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_iv1.frame) - 20, CGRectGetWidth(_iv1.frame), 20)];
    updataLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
    updataLabel.text = _model.updateInfo;
    updataLabel.textColor = [UIColor whiteColor];
    updataLabel.font = [UIFont systemFontOfSize:12.0];
    updataLabel.textAlignment = 1;
    [_iv1 addSubview:updataLabel];}
    
    
    UILabel *gengxinLable1 = [[UILabel alloc]initWithFrame:CGRectMake(_iv1.left, _nameLabel1.bottom, (KScreenWidth-6*krecommentCellInset)/3, 10)];
    if (_model.chapterIndex) {
        gengxinLable1.text = [NSString stringWithFormat:@"撸至%@话",_model.chapterIndex];
        bottomView.frame = CGRectMake(-1, 190, ScreenSizeWidth + 2, 2);
    }
    gengxinLable1.font = [ UIFont systemFontOfSize:10];
    gengxinLable1.textColor = [UIColor grayColor];
    [manhuaControl1 addSubview:gengxinLable1];
    
    
}
- (void)createView2{
//---------------------------------------2话
    UIControl *manhuaControl2 = [[UIControl alloc]initWithFrame:CGRectMake(KScreenWidth/3, 0, (KScreenWidth-6*krecommentCellInset)/3, (KScreenWidth-6*krecommentCellInset)/3*___Scale+42)];
    [_msgView addSubview:manhuaControl2];
    
    UIImageView *manhuaimgv2 = [[UIImageView alloc]initWithFrame:CGRectMake(krecommentCellInset, krecommentCellInset, (KScreenWidth-6*krecommentCellInset)/3, (KScreenWidth-6*krecommentCellInset)/3*___Scale)];
    [manhuaimgv2 setImageWithURL:[NSURL URLWithString:_model1.images]];

//    [manhuaimgv2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model1[@"images"]]]]]];
    manhuaimgv2.userInteractionEnabled = YES;
    manhuaimgv2.layer.cornerRadius = 4;
    manhuaimgv2.clipsToBounds = YES;
    UITapGestureRecognizer *kitTap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jump2)];
    [manhuaimgv2 addGestureRecognizer:kitTap1];
    [manhuaControl2 addSubview:manhuaimgv2];
    
    UILabel *titleLable2 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv2.left, manhuaimgv2.bottom,(KScreenWidth-6*krecommentCellInset)/3, 20)];
    titleLable2.text = _model1.name;
    titleLable2.font  = [UIFont systemFontOfSize:14];
    [manhuaControl2 addSubview:titleLable2];

    UILabel *gengxinLable2 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv2.left, titleLable2.bottom, (_cellView.width-6*krecommentCellInset)/3,10)];
    if (_model1.chapterIndex) {
        gengxinLable2.text = [NSString stringWithFormat:@"撸至%@话",_model1.chapterIndex];
    }
    gengxinLable2.font = [UIFont systemFontOfSize:10];
    gengxinLable2.textColor = [UIColor grayColor];
    [manhuaControl2 addSubview:gengxinLable2];
    
    if (_model1.updateInfo) {
        
        UILabel *updataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(manhuaimgv2.frame) - 20, CGRectGetWidth(manhuaimgv2.frame), 20)];
        updataLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
        updataLabel.text = _model1.updateInfo;
        updataLabel.textColor = [UIColor whiteColor];
        updataLabel.font = [UIFont systemFontOfSize:12.0];
        updataLabel.textAlignment = 1;
        [manhuaimgv2 addSubview:updataLabel];}

}
//-----------------3.话
- (void)createView3 {
    UIControl *manhuaControl3 = [[UIControl alloc]initWithFrame:CGRectMake(KScreenWidth/3*2 , 0, (KScreenWidth-6*krecommentCellInset)/3*___Scale, (KScreenWidth-6*krecommentCellInset)/3*___Scale+42)];
    [_msgView addSubview:manhuaControl3];
    
    UIImageView *manhuaimgv3 = [[UIImageView alloc]initWithFrame:CGRectMake(krecommentCellInset, krecommentCellInset, (KScreenWidth-6*krecommentCellInset)/3, (KScreenWidth-6*krecommentCellInset)/3*___Scale)];
    [manhuaimgv3 setImageWithURL:[NSURL URLWithString:_model2.images]];
    manhuaimgv3.userInteractionEnabled = YES;
    manhuaimgv3.layer.cornerRadius = 4;
    manhuaimgv3.clipsToBounds = YES;
    UITapGestureRecognizer *kitTap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jump3)];
    [manhuaimgv3 addGestureRecognizer:kitTap2];
    [manhuaControl3 addSubview:manhuaimgv3];
    
    UILabel *titleLable3 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv3.left, manhuaimgv3.bottom,(KScreenWidth-6*krecommentCellInset)/3, 20)];
    titleLable3.text = _model2.name;
    titleLable3.font = [UIFont systemFontOfSize:14];
    [manhuaControl3 addSubview:titleLable3];
    
    UILabel *gengxinLable3 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv3.left, titleLable3.bottom,(_cellView.width-6*krecommentCellInset)/3, 10)];
    if (_model2.chapterIndex) {
        gengxinLable3.text = [NSString stringWithFormat:@"撸至%@话",_model2.chapterIndex];;
    }
    gengxinLable3.font = [UIFont systemFontOfSize:10];
    gengxinLable3.textColor = [UIColor grayColor];
    [manhuaControl3 addSubview:gengxinLable3];
    
    if (_model2.updateInfo) {
        
        UILabel *updataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(manhuaimgv3.frame) - 20, CGRectGetWidth(manhuaimgv3.frame), 20)];
        updataLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
        updataLabel.text = _model2.updateInfo;
        updataLabel.textColor = [UIColor whiteColor];
        updataLabel.font = [UIFont systemFontOfSize:12.0];
        updataLabel.textAlignment = 1;
        [manhuaimgv3 addSubview:updataLabel];
    }
}

#pragma mark - 点击事件
-(void)jump1:(id)sender{
        
    if (_model.currentReadChapterId) {
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"crd" to_section:@"c" to_step:@"l" type:@"" channel:@"" id:_model.currentReadChapterId];
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"" id:_model.currentReadChapterId];
      
        NSArray *allInfo = @[_model.id,_model.name,_model.images];
        NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_model.currentReadChapterId];
        NSArray *strAllInfo = @[allInfo,str];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu-2" object:strAllInfo];
    }else{
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:_model.id];

        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:_model.id];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_model.id];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recomment-1" object:str];
    }

}

-(void)jump2{

    if (_model1.currentReadChapterId) {
        
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"" id:_model1.currentReadChapterId];

//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"crd" to_section:@"c" to_step:@"l" type:@"" channel:@"" id:_model1.currentReadChapterId];
        
        NSArray *allInfo = @[_model1.id,_model1.name,_model1.images];

        NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_model1.currentReadChapterId];
        NSArray *strAllInfo = @[allInfo,str];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu-2" object:strAllInfo];
    }else{
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:_model1.id];
        
        
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:_model1.id];

        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_model1.id];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recomment-1" object:str];
    }

}

-(void)jump3{
    if (_model2.currentReadChapterId) {
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"crd" to_section:@"c" to_step:@"l" type:@"" channel:@"" id:_model2.currentReadChapterId];
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"" id:_model2.currentReadChapterId];

        
        NSArray *allInfo = @[_model2.id,_model2.name,_model2.images];

        NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_model2.currentReadChapterId];
        NSArray *strAllInfo = @[allInfo,str];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu-2" object:strAllInfo];
    }else{
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:_model2.id];
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:_model2.id];


        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_model2.id];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recomment-1" object:str];
    }

}


#pragma mark - 数据解析
-(void)analyzeLuData{
    _LudataArr = [NSMutableArray arrayWithCapacity:0];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_RECOMMENTLU_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            NSString *idNum = dict[@"id"];
            NSString *chapterid = dict[@"chapterId"];
            [_LudataArr addObject:idNum];
            [_LuChapterId addObject:chapterid];
        }
        NSLog(@"%@",_dataArr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
-(void)analyzeData{
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_RECENTUPDATE_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            NSString *idNum = dict[@"id"];
            [_dataArr addObject:idNum];
        }
        NSLog(@"%@",_dataArr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)analyzeTopData{
    _TopdataArr = [NSMutableArray arrayWithCapacity:0];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_RECOMMENTTOP_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",backData);
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            NSString *idNum = dict[@"id"];
            [_TopdataArr addObject:idNum];
            NSLog(@"%@",_TopdataArr);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)analyzeCollectionData{
    _CollectiondataArr = [NSMutableArray arrayWithCapacity:0];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_RECOMMENTCOLLECTIN_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            NSString *idNum = dict[@"id"];
            [_CollectiondataArr addObject:idNum];
            NSLog(@"%@",_CollectiondataArr);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
#pragma mark-跳转

//-(void)setModel:(RecommentModel *)model{
//    _model = model;
//    [self creatView1];
//}
//-(void)setModel1:(RecommentModel *)model1{
//    _model1 = model1;
//    [self createView2];
//}
//-(void)setModel2:(RecommentModel *)model2{
//    _model2 = model2;
//    [self createView3];
//}

//-(void)tagNum:(NSNotification *)noti{
//    _tag = [noti.object intValue];
//}
//

@end
