//
//  UIView+UIViewController.h
//  05 Responder
//
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewController)

- (UIViewController *)viewController;

@end
